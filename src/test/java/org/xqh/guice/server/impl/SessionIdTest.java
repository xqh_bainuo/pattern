package org.xqh.guice.server.impl;

import com.google.inject.Guice;
import org.junit.Before;
import org.junit.Test;
import org.xqh.study.google.guice.server.ServerModule;
import org.xqh.study.google.guice.server.SessionManager;

import javax.inject.Inject;

/**
 * @ClassName SessionIdTest @Description TODO @Author xuqianghui @Date 2022/5/23 16:11 @Version 1.0
 */
public class SessionIdTest {

  @Inject private SessionManager sessionManager;

  @Before
  public void setUp() {
    Guice.createInjector(new ServerModule()).injectMembers(this);
  }

  @Test
  public void sessionIdTest() throws InterruptedException {
    Long sessionId1 = sessionManager.getSessionId();
    Thread.sleep(1000);
    Long sessionId2 = sessionManager.getSessionId();
    System.out.println(sessionId1 == sessionId2);
  }
}
