package org.xqh.guice.server.impl;

import com.alibaba.fastjson.JSON;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.util.Modules;
import org.junit.Before;
import org.junit.Test;
import org.xqh.study.google.guice.server.OrderService;
import org.xqh.study.google.guice.server.PaymentService;
import org.xqh.study.google.guice.server.PriceService;
import org.xqh.study.google.guice.server.ServerModule;

import javax.inject.Inject;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName OrderServiceTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 13:49
 * @Version 1.0
 */

class PriceServiceMock implements PriceService{
    @Override
    public long getPrice(long orderId) {
        return 11225L;
    }

    @Override
    public Set<String> getSupportedCurrencies() {
        return null;
    }

    @Override
    public Map<String, Object> testMapBinder() {
        return null;
    }
}

public class OrderServiceTest {


    @Inject
    private OrderService orderService;

    @Inject
    private PriceService priceService;

    @Inject
    private PaymentService paymentService;

//    @Inject @Named("supportedCurrencies")
//    Provider<List<String>> supportedCurrenciesProvider;

    @Before public void setUp(){
        Guice.createInjector(Modules.override(new ServerModule()).with(
                new AbstractModule() {
                    @Override
                    protected void configure() {
                        bind(PriceService.class).to(PriceServiceMock.class);
                    }
                })
        ).injectMembers(this);
    }

    @Test
    public void testSendPayment(){
//        OrderService orderService = Guice.createInjector(new ServerModule()).getInstance(OrderService.class);
        orderService.sendToPayment(123L);
    }

    @Test
    public void getSupportedCurrencies(){
    //        throw new RuntimeException(supportedCurrenciesProvider.get().toString());
    //        throw new RuntimeException(priceService.getSupportedCurrencies().toString() +
    // priceService.testMapBinder().toString());
    System.out.println(JSON.toJSONString(paymentService.testList()));
    }
}
