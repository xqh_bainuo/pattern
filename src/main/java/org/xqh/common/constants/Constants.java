package org.xqh.common.constants;

/**
 * @ClassName Constants
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/6/30 18:01
 * @Version 1.0
 */
public class Constants {
    public final static String SIGN_HMAC_SHA256 = "HmacSHA256";
    public final static String CHARSET_UTF8 = "UTF-8";
}
