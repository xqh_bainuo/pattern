package org.xqh.study.pattern.partterns.abstractFactory;

public interface Castle {

    String getDescription();
}
