package org.xqh.study.pattern.partterns.adapter;

public interface RowingBoat {
   void row();
}
