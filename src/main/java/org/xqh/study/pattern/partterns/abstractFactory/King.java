package org.xqh.study.pattern.partterns.abstractFactory;

public interface King {

    String getDescription();
}
