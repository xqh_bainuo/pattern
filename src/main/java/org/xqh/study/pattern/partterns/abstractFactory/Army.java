package org.xqh.study.pattern.partterns.abstractFactory;

public interface Army {

    String getDescription();
}
