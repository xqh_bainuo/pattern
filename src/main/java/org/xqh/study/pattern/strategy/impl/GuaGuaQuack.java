package org.xqh.study.pattern.strategy.impl;

import org.xqh.study.pattern.strategy.QuackBehavior;

public class GuaGuaQuack implements QuackBehavior {

	public void quack() {
		System.out.println("呱呱叫...");
	}

}
