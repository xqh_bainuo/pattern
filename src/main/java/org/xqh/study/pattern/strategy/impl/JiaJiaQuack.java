package org.xqh.study.pattern.strategy.impl;

import org.xqh.study.pattern.strategy.QuackBehavior;

public class JiaJiaQuack implements QuackBehavior {

	public void quack() {
		System.out.println("戛戛加...");
	}

}
