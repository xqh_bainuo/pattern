package org.xqh.study.proxy;

public interface TargetInterface {
   
	void method(String param);
}
