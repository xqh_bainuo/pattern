package org.xqh.study.map;

/**
 * @ClassName BitMapTest
 * @Description 位图
 * @Author xuqianghui
 * @Date 2019/12/25 10:13
 * @Version 1.0
 */
public class BitMapTest {

    public static void main(String[] args) {
        byte[] bit = new byte[100];
        System.out.println(10 >> 2);
        System.out.println(10/8);

        System.out.println(10&7);

        System.out.println(16 >> 3);


        int i = 2;
        System.out.println(0b11111111 >>>(7 - i));
    }
}
