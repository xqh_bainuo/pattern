package org.xqh.study.leetcode.mulitthread;

import java.util.function.IntConsumer;

/**
 * @ClassName FizzBuzzSnyc
 * @Description 同步锁 方式实现 (超时)
 * @Author xuqianghui
 * @Date 2020/12/25 17:27
 * @Version 1.0
 */
public class FizzBuzzSnyc {
    /**
     * 请你实现一个有四个线程的多线程版  FizzBuzzSnyc， 同一个 FizzBuzzSnyc 实例会被如下四个线程使用：
     *
     * 线程A将调用 fizz() 来判断是否能被 3 整除，如果可以，则输出 fizz。
     * 线程B将调用 buzz() 来判断是否能被 5 整除，如果可以，则输出 buzz。
     * 线程C将调用 fizzbuzz() 来判断是否同时能被 3 和 5 整除，如果可以，则输出 fizzbuzz。
     * 线程D将调用 number() 来实现输出既不能被 3 整除也不能被 5 整除的数字。
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/fizz-buzz-multithreaded
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */

    class FizzBuzz {
        private int n;

        private int current;

        private Object lock = new Object();

        public FizzBuzz(int n) {
            this.n = n;
            current = 1;
        }

        // printFizz.run() outputs "fizz".
        public void fizz(Runnable printFizz) throws InterruptedException {
            b:while (true){
                synchronized (lock){
                    checkProcess(1);
                    if(current > n){
                        lock.notifyAll();
                        break b;
                    }
                    if(current % 3 == 0 && current % 5 != 0){
                        printFizz.run();//输出 "fizz"
                        current ++;
                    }

                    lock.notifyAll();
                }
            }
        }

        // printBuzz.run() outputs "buzz".
        public void buzz(Runnable printBuzz) throws InterruptedException {
            b:while (true){
                synchronized (lock){
                    checkProcess(2);
                    if(current > n){
                        lock.notifyAll();
                        break b;
                    }
                    if(current % 5 == 0 && current % 3 != 0){
                        printBuzz.run();
                        current ++;
                    }
                    lock.notifyAll();
                }
            }
        }

        // printFizzBuzz.run() outputs "fizzbuzz".
        public void fizzbuzz(Runnable printFizzBuzz) throws InterruptedException {
            b:while (true){
                synchronized (lock){
                    checkProcess(3);
                    if(current > n){
                        lock.notifyAll();
                        break b;
                    }
                    if(current % 3 == 0 && current % 5 == 0){
                        printFizzBuzz.run();
                        current ++;
                    }

                    lock.notifyAll();
                }
            }
        }

        // printNumber.accept(x) outputs "x", where x is an integer.
        public void number(IntConsumer printNumber) throws InterruptedException {
            b:while (true){
                synchronized (lock){
                    checkProcess(4);
                    if(current > n){
                        lock.notifyAll();
                        break b;
                    }
                    if(current % 3 != 0 && current % 5 != 0){
                        printNumber.accept(current);
                        current ++;
                    }
                    lock.notifyAll();
                }
            }
        }

        public void checkProcess(int type) throws InterruptedException {
            if(type == 1){
                if(current % 3 != 0 || (current % 3 == 0 && current % 5 == 0)){
                    lock.wait();//释放锁
                }
            }else if(type == 2){
                if(current % 5 != 0 || (current % 3 == 0 && current % 5 == 0)){
                    lock.wait();//释放锁
                }
            }else if(type == 3){
                if(current % 3 != 0 || current % 5 != 0){
                    lock.wait();//释放锁
                }
            }else if(type == 4){
                if(current % 3 == 0 || current % 5 == 0){
                    lock.wait();//释放锁
                }
            }
        }
    }


//    public static Object obj = new Object();
//
//    public static void main(String[] args) throws InterruptedException {
//        new Thread(new TheadTest1(1), "t1").start();
//        new Thread(new TheadTest1(2), "t2").start();
//        new Thread(new TheadTest1(3), "t3").start();
//        Thread.sleep(3000);
//        synchronized (obj){
//            obj.notifyAll();
//        }
//    }
//
//    public static class TheadTest1 implements Runnable{
//        private int a;
//
//        public TheadTest1(int a){
//            this.a = a;
//        }
//
//
//        @SneakyThrows
//        @Override
//        public void run() {
//            synchronized (obj){
//                System.out.println(Thread.currentThread().getName()+".......获取锁");
//                obj.wait();
//                System.out.println(Thread.currentThread().getName()+".......等待后执行");
//                Thread.sleep(2000);
//                obj.notifyAll();
//            }
//        }
//    }

}
