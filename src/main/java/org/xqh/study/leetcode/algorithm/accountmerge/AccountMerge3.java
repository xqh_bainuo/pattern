package org.xqh.study.leetcode.algorithm.accountmerge;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @ClassName AccountMerge3
 * @Description 别人提交的 答案
 * @Author xuqianghui
 * @Date 2021/1/21 11:32
 * @Version 1.0
 */
public class AccountMerge3 {

    int[] parent;

    public List<List<String>> accountsMerge(List<List<String>> accounts) {
        int n = accounts.size();
        String[] name = new String[n];
        parent = new int[n];
        HashMap<String, Integer> map = new HashMap<>();//email和 集合序号
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            List<String> list = accounts.get(i);
            name[i] = list.get(0);//账户名称
            for (int j = 1; j < list.size(); j++) {
                String a = list.get(j);
                if (map.containsKey(a)) {
                    list.remove(j);
                    union(map.get(a), i);
                    j--;
                } else map.put(a, i);
            }
        }
        for (int i = 0; i < n; i++)
            find(i);
        List<List<String>> ans = new ArrayList<List<String>>();
        for (int i = 0; i < n; i++) {
            if (i == parent[i]) {
                List<String> list = new ArrayList<>();
                list.add(name[i]);
                for (int j = 1; j < accounts.get(i).size(); j++) {
                    list.add(accounts.get(i).get(j));
                }
                for (int j = i + 1; j < n; j++) {
                    if (parent[j] == i) {
                        for (int k = 1; k < accounts.get(j).size(); k++) {
                            list.add(accounts.get(j).get(k));
                        }
                    }
                }
                ans.add(list);
            }
        }
        for (int i = 0; i < ans.size(); i++) {
            List<String> list = ans.get(i);
            String a = list.remove(0);
            Collections.sort(list);
            list.add(0, a);
        }
        return ans;
    }

    public int find(int x) {
        while (x != parent[x]) {
            int temp = parent[x];
            parent[x] = parent[temp];
            x = parent[x];
        }
        return x;
    }

    public void union(int x, int y) {
        int x1 = find(x);
        int y1 = find(y);
        if (x1 < y1)
            parent[y1] = x1;
        else parent[x1] = y1;
    }
}

