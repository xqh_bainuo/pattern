package org.xqh.study.leetcode.algorithm;

/**
 * @ClassName ListNode
 * @Description 链表
 * @Author xuqianghui
 * @Date 2021/2/15 10:47
 * @Version 1.0
 */
public class ListNode {

    public int val;
    public ListNode next;

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString() {
        return ""+val;
    }
}
