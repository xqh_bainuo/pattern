package org.xqh.study.leetcode.algorithm.twopointer;

/**
 * @ClassName MinSubArrayLen
 * @Description
 * @Author xuqianghui
 * @Date 2021/2/12 21:46
 * @Version 1.0
 */
public class MinSubArrayLen {

    public static void main(String[] args) {
        int[] nums = {1,1,1,1,1,1,1,1};
        System.out.println(minSubArrayLen(11, nums));
    }

    /**
     * 给定一个含有 n 个正整数的数组和一个正整数 target 。
     * <p>
     * 找出该数组中满足其和 ≥ target 的长度最小的 连续子数组 
     * [numsl, numsl+1, ..., numsr-1, numsr] ，并返回其长度。如果不存在符合条件的子数组，返回 0 。
     *
     * https://leetcode-cn.com/problems/minimum-size-subarray-sum/
     * @param target
     * @param nums
     * @return
     */
    public static int minSubArrayLen(int target, int[] nums) {

        int minLen = nums.length + 1;
        int len = nums.length;
        int left = 0;
        int right = 0;
        int sum = 0;
        while (right < len){
            sum += nums[right];
            right ++;
            while (sum >= target){
                sum -= nums[left];
                left ++;
                if(sum < target){
                    minLen = Math.min(minLen, right - left + 1);
                }
            }
        }
        if(minLen == nums.length + 1){
            minLen = 0;
        }

        return minLen;
    }

}
