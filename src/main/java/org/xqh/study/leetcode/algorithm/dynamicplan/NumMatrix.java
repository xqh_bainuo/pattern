package org.xqh.study.leetcode.algorithm.dynamicplan;

/**
 * @ClassName NumMatrix
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/3/2 9:39
 * @Version 1.0
 */
public class NumMatrix {

    int[][] matrix;
    int[][] sum;

    public  NumMatrix(int[][] matrix) {
        this.matrix = matrix;
        if(matrix.length == 0){
            return ;
        }
        this.sum = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            int tmp = 0;
            for (int j = 0; j < matrix[i].length; j++) {
                tmp += matrix[i][j];
                sum[i][j] = tmp;
            }
        }
    }

//    public NumMatrix(int[][] matrix){
//        this.matrix = matrix;
//    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        int res = 0;
        for(int i = row1; i <= row2; i ++){
            res += sum[i][col2];
            if(col1 > 0){
                res -= sum[i][col1 - 1];
            }
        }
        return res;
    }

    public int sumRegion2(int row1, int col1, int row2, int col2) {
        if(this.matrix.length == 0 || this.matrix[0].length == 0){
            return -1;
        }
        int res = 0;
        for(int i = row1; i <= row2; i ++){
        }
        return res;
    }

    public static void main(String[] args) {
        int[][] matrix = {{3, 0, 1, 4, 2},
                {5, 6, 3, 2, 1},
                {1, 2, 0, 1, 5},
                {4, 1, 0, 1, 7},
                {1, 0, 3, 0, 5}
        };

        int[][] matrix2 = {{}};

        NumMatrix m = new NumMatrix(matrix);
        System.out.println(m.sumRegion(2, 1, 4, 3));
//        System.out.println(m.sumRegion(1, 1, 2, 2));
//        System.out.println(m.sumRegion(1, 2, 2, 4));
    }
}
