package org.xqh.study.leetcode.algorithm.studyplan;

/**
 * @ClassName MinStack
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/3/24 17:17
 * @Version 1.0
 */
public class MinStack {

    StackNode head;


        /** initialize your data structure here. */
        public MinStack() {

        }

        public void push(int val) {
            if(null != head){
                int min = Math.min(val, head.min);
                StackNode nn = new StackNode(val, min);
                head.next = nn;
                nn.prev = head;
                head = nn;
            }else {
                //初始化 第一个节点
                StackNode nn = new StackNode(val, val);
                head = nn;
            }
        }

        public void pop() {
            if(head != null){
                StackNode prev = head.prev;
                head = null;
                head = prev;
            }
        }

        public int top() {
            if(null != head){
                return head.val;
            }
            return -1;
        }

        public int getMin() {
            if(null != head){
                return head.min;
            }
            return -1;
        }

        public static class StackNode{
            int val;
            StackNode next;//后节点
            int min;//当前节点 最小值
            StackNode prev;//前节点
            public StackNode(int val){
                this.val = val;
            }

            public StackNode(int val, int min){
                this.val = val;
                this.min = min;
            }
        }

    public static void main(String[] args) {
        MinStack min = new MinStack();
        min.push(2147483646);
        min.push(2147483646);
        min.push(2147483647);
        System.out.println(min.top());
        min.pop();
        System.out.println(min.getMin());
        min.pop();
        min.push(2147483647);
        System.out.println(min.top());;
        System.out.println(min.getMin());
        min.push(-2147483648);
        System.out.println(min.top());;
        System.out.println(min.getMin());;
        min.pop();
        System.out.println(min.getMin());;


    }
}
