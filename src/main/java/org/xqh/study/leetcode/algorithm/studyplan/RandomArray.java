package org.xqh.study.leetcode.algorithm.studyplan;

import java.util.Random;

/**
 * @ClassName RandomArray
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/3/10 11:35
 * @Version 1.0
 */
public class RandomArray {

    class Solution {

        int[] origin;
        int[] ranArr;

        private Random random = new Random();

        public Solution(int[] nums) {
            this.origin = nums;
        }

        /** Resets the array to its original configuration and return it. */
        public int[] reset() {
            ranArr = origin.clone();
            return ranArr;
        }

        /** Returns a random shuffling of the array. */
        public int[] shuffle() {
            ranArr = origin.clone();
            for(int i = 0; i < ranArr.length; i ++){
                swapArray(ranArr, i, getRangeIdx(i, ranArr.length));
            }
            return ranArr;
        }

        public int getRangeIdx(int i, int j){
            return random.nextInt(j - i) + i;
        }

        public void swapArray(int[] nums, int i, int j){
            int tmp = nums[i];
            nums[i] = nums[j];
            nums[j] = tmp;
        }
    }
}
