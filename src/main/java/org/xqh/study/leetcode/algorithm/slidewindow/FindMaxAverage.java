package org.xqh.study.leetcode.algorithm.slidewindow;

/**
 * @ClassName FindMaxAverage
 * @Description 子数组最大平均数 I
 * https://leetcode-cn.com/problems/maximum-average-subarray-i/
 * @Author xuqianghui
 * @Date 2021/2/4 20:23
 * @Version 1.0
 */
public class FindMaxAverage {

    public static void main(String[] args) {
        int[] nums = {1,12,-5,-6,50,3};
        System.out.println(findMaxAverage(nums, 4));
    }

    public static double findMaxAverage(int[] nums, int k) {

        int total = 0;
        for (int i = 0; i < k; i++){
            total += nums[i];
        }
        int max = total;
        for(int i = 1; i <= nums.length - k; i++){
            total = total + nums[i+k-1] - nums[i-1];
            if(total > max){
                max  = total;
            }
        }
        return ((double)max)/k;
    }
}
