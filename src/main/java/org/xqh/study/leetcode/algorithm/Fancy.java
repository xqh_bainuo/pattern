package org.xqh.study.leetcode.algorithm;

/**
 * @ClassName Fancy
 * @Description https://leetcode-cn.com/problems/fancy-sequence/
 * 请实现 Fancy 类 ：
 *
 * Fancy() 初始化一个空序列对象。
 * void append(val) 将整数 val 添加在序列末尾。
 * void addAll(inc) 将所有序列中的现有数值都增加 inc 。
 * void multAll(m) 将序列中的所有现有数值都乘以整数 m 。
 * int getIndex(idx) 得到下标为 idx 处的数值（下标从 0 开始），并将结果对 109 + 7 取余。如果下标大于等于序列的长度，请返回 -1 。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/fancy-sequence
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 * @Author xuqianghui
 * @Date 2021/1/8 15:39
 * @Version 1.0
 */
public class Fancy {

    private int[] nums;

    public Fancy() {
        this.nums = new int[0];
    }

    public void append(int val) {
        int[] tmp = new int[this.nums.length + 1];
        tmp[tmp.length - 1] = val;
        for(int i = 0; i < this.nums.length; i++){
            tmp[i] = this.nums[i];
        }
        this.nums = tmp;
    }

    public void addAll(int inc) {
        for(int i = 0; i < nums.length; i ++){
            nums[i] = nums[i] + inc;
        }
    }

    public void multAll(int m) {
        for(int i = 0; i < nums.length; i ++){
            nums[i] = nums[i] * m;
        }
    }

    public int getIndex(int idx) {
        if(idx > nums.length - 1){
            return -1;
        }
        return nums[idx];
    }
}
