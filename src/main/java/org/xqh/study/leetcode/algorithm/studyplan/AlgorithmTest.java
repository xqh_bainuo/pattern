package org.xqh.study.leetcode.algorithm.studyplan;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import org.springframework.util.NumberUtils;
import org.springframework.util.StopWatch;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName AlgorithmTest
 * @Description 算法测试题
 * @Author xuqianghui
 * @Date 2021/2/20 9:25
 * @Version 1.0
 */
public class AlgorithmTest {

    public static void main22(String[] args) {
        System.out.println(calc(32));
        System.out.println((int) 'a');
        System.out.println((int) 'z');
    }

    public static boolean isHappy(int n) {
        boolean result = true;
        int tmp = calc(n);
        Set<Integer> set = new HashSet<>();
        while (tmp != 1) {
            if (set.contains(tmp)) {
                result = false;
                break;
            }
            set.add(tmp);
            tmp = calc(tmp);
        }
        return result;
    }

    /**
     * 每一位 平方
     *
     * @param n
     * @return
     */
    public static int calc(int n) {
        int res = 0;

        while (n > 0) {
            int a = n % 10;
            res += Math.pow(a, 2);
            n = n / 10;
        }
        return res;
    }

    public static int calc11(int n) {
        int ret = 0;
        String a = String.valueOf(n);
        char[] array = a.toCharArray();
        for (char c : array) {
            ret += Math.pow((c - '0'), 2);
        }
        return ret;
    }

    public static void main333333(String[] args) {
//        System.out.println(isPalindrome("A man, a plan, a canal: Panama"));
//        partition("aabcba");
//        String str = "aabcba";
    }

    public static boolean isPalindrome(String s) {
        StringBuilder sb = new StringBuilder();
        s = s.toLowerCase();
        char[] array = s.toCharArray();
        for (char c : array) {
            if ((c >= 48 && c <= 57) || (c >= 97 && c <= 122)) {
                sb.append(c);
            }
        }
        char[] narry = sb.toString().toCharArray();
        int left = 0, right = narry.length - 1;
        for (; left <= (narry.length + 1) / 2; left++, right--) {
            if (narry[left] != narry[right]) {
                return false;
            }
        }
        return true;
    }

    public static void mainaa(String[] args) {
        String str = "ccaacabacb";
//        String str = "aab";

        System.out.println(JSON.toJSONString(partition(str)));
    }

    /**
     * 分割回文串
     * https://leetcode-cn.com/problems/palindrome-partitioning/
     *
     * @param s
     * @return
     */
    public static List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<>();
        char[] arr = s.toCharArray();
        int len = arr.length;
        boolean[][] plan = new boolean[len][len];//动态规划
        for (int k = 1; k <= len; k++) {
            //滑动窗口 长度为k
            for (int i = 0; i <= len - k; i++) {
                boolean flag = arr[i] == arr[i + k - 1];
                plan[i][i + k - 1] = (k <= 2) ? flag : (flag && plan[i + 1][i + k - 2]);
            }
        }
        //深度优先搜索
        Deque<String> deque = new ArrayDeque<>();//
        flashBack(s, result, deque, plan, 0, len);
        return result;
    }


    /**
     * DFS 回溯算法
     *
     * @param s
     * @param res
     * @param deque
     * @param plan
     * @param depth
     * @param len
     */
    public static void flashBack(String s, List<List<String>> res, Deque<String> deque, boolean[][] plan, int depth, int len) {
        if (depth == len) {
            res.add(new ArrayList<>(deque));
            return;
        }
        for (int i = depth; i < len; i++) {
            if (plan[depth][i]) {
                deque.offer(s.substring(depth, i + 1));
                flashBack(s, res, deque, plan, i + 1, len);
                deque.pollLast();
            }
        }
    }


    /**
     * 查找 一下个 回文子串集合
     *
     * @param plan
     * @param cur
     * @return
     */
    public static List<int[]> findNextItems(boolean[][] plan, Integer cur, boolean init, Map<Integer,
            List<int[]>> map, Map<Integer, boolean[]> usedMap) {
        List<int[]> result = new ArrayList<>();
        int start = init ? cur : cur + 1;
        for (int i = start; i < plan.length; i++) {
            boolean flag = false;
            for (int j = i; j < plan[0].length; j++) {
                if (plan[i][j]) {
                    flag = true;
                    result.add(new int[]{i, j});
                }
            }
            if (flag) {
                break;//如果  当前 row查到 就只查当前行记录
            }
        }
        if (init) {
            map.put(-1, result);
            usedMap.put(-1, new boolean[result.size()]);
        } else {
            map.put(cur, result);
        }
        return result;
    }

    public static class HuiWen {
        List<int[]> items;//子集
        List<int[]> link;//存储结果
        HuiWen parent;//父节点
        boolean isLast;//是否是最后的节点

        HuiWen(List<int[]> items, HuiWen parent, int[] fragment) {
            this.items = items;
            this.parent = parent;
            this.isLast = items.size() == 0;
            this.link = new ArrayList<>();
            if (null != parent && null != parent.link) {
                this.link.addAll(parent.link);
            }
            this.link.add(fragment);
        }
    }

    /**
     * https://leetcode-cn.com/problems/permutations/
     * 给定一个 没有重复 数字的序列，返回其所有可能的全排列。
     *
     * @param nums
     * @return
     */
    public static List<List<Integer>> permute(int[] nums) {
        int len = nums.length;
        List<List<Integer>> res = new ArrayList<>();
        if (len == 0) {
            return res;
        }
        boolean[] used = new boolean[len];// 标识 元素是否已使用
        Deque<Integer> stack = new ArrayDeque<>();
        recursion(nums, 0, len, used, res, stack);
        return res;
    }

    public static void recursion(int[] nums, int depth, int len, boolean[] used, List<List<Integer>> res, Deque<Integer> stack) {
        if (depth == len) {// 表示 所有数据都遍历完
            res.add(new ArrayList<>(stack));
            return;
        }

        for (int i = 0; i < len; i++) {
            if (used[i]) {
                continue;
            }
            stack.offer(nums[i]);
            used[i] = true;
            recursion(nums, depth + 1, len, used, res, stack);
            stack.pollLast();
            used[i] = false;//恢复状态  顾名思义 "回溯算法"
        }
    }

    public static void main123(String[] args) {
        int[] nums = {1, 2, 3};
        System.out.println(JSON.toJSONString(permute(nums)));
    }

    public static void main111334(String[] args) {
        List<String> wordDict = Lists.newArrayList("leet", "code");
        String s = "leetcode";
        StopWatch sw = new StopWatch();
        sw.start("单词拆分start..");
        System.out.println(wordBreak2(s, wordDict));
        ;
        sw.stop();
        System.out.println(sw.prettyPrint());
    }

    /**
     * 单词拆分
     * https://leetcode-cn.com/problems/word-break/
     *
     * @param s
     * @param wordDict
     * @return
     */
    public static boolean wordBreak(String s, List<String> wordDict) {
        int len = s.length();
        boolean[] dp = new boolean[len + 1];
        dp[0] = true;
        for (int i = 1; i <= len; i++) {
            for (int j = 0; j < i; j++) {
                if (dp[j] && wordDict.contains(s.substring(j, i))) {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[len];
    }

    /**
     * 单词拆分 二
     * https://leetcode-cn.com/problems/word-break-ii/
     *
     * @param s
     * @param wordDict
     * @return
     */
    public static List<String> wordBreak2(String s, List<String> wordDict) {
        char[] arr = s.toCharArray();
        int len = arr.length;
        boolean[][] dp = new boolean[len][len];
        for (int k = 1; k <= len; k++) {
            for (int i = 0; i <= len - k; i++) {
                if (wordDict.contains(s.substring(i, i + k))) {
                    dp[i][i + k - 1] = true;
                }
            }
        }
        List<String> res = new ArrayList<>();
        Deque<String> deque = new ArrayDeque<>();
        dfsBreak(deque, 0, res, dp, len, s);
        return res;
    }

    public static void dfsBreak(Deque<String> deque, int depth, List<String> res, boolean[][] dp, int len, String s) {
        if (depth == len) {
            List<String> list = new ArrayList<>(deque);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    sb.append(list.get(i));
                } else {
                    sb.append(list.get(i)).append(" ");
                }
            }
            res.add(sb.toString());
            return;
        }

        for (int i = depth; i < dp.length; i++) {
            if (dp[depth][i]) {
                deque.offer(s.substring(depth, i + 1));
                dfsBreak(deque, i + 1, res, dp, len, s);
                deque.pollLast();
            }
        }
    }

    /**
     * @param s
     * @param t
     * @return
     */
    public static boolean isAnagram(String s, String t) {
        int[] freq = new int[26];
        if (s.length() != t.length()) {
            return false;
        }
        char[] sa = s.toCharArray();
        for (char c : sa) {
            freq[c - 'a']++;
        }
        char[] ta = t.toCharArray();
        for (char ct : ta) {
            if (freq[ct - 'a'] == 0) {
                return false;
            }
            freq[ct - 'a']--;
        }
        return true;
    }


    public static void main12233(String[] args) {
        String s = "leetcode";
        System.out.println(firstUniqChar(s));
    }


    public static int firstUniqChar(String s) {
        int[] freq = new int[26];
        char[] arr = s.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            freq[arr[i] - 'a']++;
        }

        for (int i = 0; i < arr.length; i++) {
            if (freq[arr[i] - 'a'] == 1) {
                return i;
            }
        }

        return -1;
    }

    /**
     * 翻转字符
     *
     * @param s
     */
    public static void reverseString(char[] s) {
        int len = s.length;
        int mid = (len + 1) / 2;
        for (int i = 0; i < mid; i++) {
            char t = s[i];
            s[i] = s[len - i - 1];
            s[len - i - 1] = t;
        }
    }

    public static int maxProduct2(int[] nums){
        int len = nums.length;

        int maxF = nums[0], res = nums[0], minF = nums[0];
        for(int i = 1; i < len; i++){
            int preMax = maxF, preMin = minF;
            maxF = Math.max(nums[i], Math.max(preMax * nums[i], preMin * nums[i]));
            minF = Math.min(nums[i], Math.min(preMin * nums[i], preMax * nums[i]));
            res = Math.max(maxF, res);
        }

        return res;
    }

    /**
     * 乘积最大子数组
     *
     * @param nums
     * @return
     */
    public static int maxProduct(int[] nums) {
        int maxF = nums[0], minF = nums[0], ans = nums[0];
        int length = nums.length;
        for (int i = 1; i < length; ++i) {
            int mx = maxF, mn = minF;
            maxF = Math.max(mx * nums[i], Math.max(nums[i], mn * nums[i]));
            minF = Math.min(mn * nums[i], Math.min(nums[i], mx * nums[i]));
            ans = Math.max(maxF, ans);
        }
        return ans;
    }

    public static void mai222n(String[] args) {
//        int[] nums = {-2, 0, 3, 4, -1, 5, 6, -3};
        int[] nums = {1, 0, 2, 3, 0, 5, 0, 6};
        moveZeroes(nums);
        System.out.println(maxProduct(nums));
//        System.out.println(maxSubArray(nums));
    }

    public static int maxSubArray(int[] nums) {
        int len = nums.length;
        if(len == 1){
            return nums[0];
        }
        int res = Math.max(Integer.MIN_VALUE, nums[0]);
        int sum = nums[0];
        for(int i = 1; i < len; i ++){
            if(sum < 0){
                sum = 0;
            }
            sum += nums[i];
            res = Math.max(res, sum);
        }
        return res;
    }

    public static boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>(nums.length);
        for(int i:nums){
            if(set.contains(i)){
                return true;
            }
            set.add(i);
        }
        return false;
    }

    public static void moveZeroes(int[] nums) {

        int len = nums.length;
        int left = 0;
        int preZero = -1;//
        while (left < len){
            if(nums[left] == 0){//当前位置 为0
                preZero = preZero == -1 ? left : preZero;
            }else {
                if(preZero != -1){
                    //将当前位置 与 第一个 0 替换
                    int tmp = nums[left];
                    nums[left] = 0;
                    nums[preZero] = tmp;
                    preZero ++;
                }
            }
            left ++;
        }
        ConcurrentHashMap<String, Object> cmap = new ConcurrentHashMap();
        cmap.put("abbb", "");

    }


    /**
     * 找出出现次数 最多的元素
     * @param nums
     * @return
     */
    public static int majorityElement(int[] nums) {
        int len = nums.length;
        int count = 0;
        int res = nums[0];
        for(int i = 0; i < len; i ++){
            if(count == 0){
                res = nums[i];
                count ++;
                continue;
            }

            if(res == nums[i]){
                count ++;
            }else {
                count --;
            }
        }
        return res;
    }

    /***
     * 数组元素 向右 移动 k 位
     * @param nums
     * @param k
     */
    public static void rotate(int[] nums, int k) {
        int len = nums.length;
        k = k % len;
        if( k == 0){
            return ;
        }
        fzArray(nums, 0, len - 1);
        fzArray(nums, 0, k - 1);
        fzArray(nums, k, len - 1);
    }

    /**
     * 数组翻转
     * @param nums
     * @param i  起始位置
     * @param j  结束位置
     */
    public static void fzArray(int[] nums, int i, int j){
        int mid = (i + j)/2;
        for(int k = i; k <= mid; k++){
            int tmp = nums[k];
            nums[k] = nums[j - (k - i)];
            nums[j - (k - i)] = tmp;
        }
    }
    public static void maindfggg(String[] args) {
//        int[] nums = {-2, 2, 3, 4, 1, 5, 6, -3};
        int[] nums = {2,1,5,0,4,6};
        int[] nums2 = {5, 6, 9, 7,11, 22, 1, 3, 19};
        System.out.println(JSON.toJSONString(increasingTriplet(nums)));
//        System.out.println(maxSubArray(nums));
    }
    /**
     * 找出两个数组 交集
     * @param nums1
     * @param nums2
     * @return
     */
    public static int[] intersect(int[] nums1, int[] nums2) {
        if(nums1.length > nums2.length){
            int[] tmp = nums1;
            nums1 = nums2;
            nums2 = tmp;
        }
//        Map<Integer, Integer> map = new HashMap<>();
//        for(int n:nums2){
//            map.put(n, map.getOrDefault(n, 0) + 1);
//        }
//
//        int[] result = new int[nums1.length];
//        int count = 0;
//        for(int n:nums1){
//            int cur = map.getOrDefault(n, 0);
//            if( cur > 0){
//                result[count] = n;
//                count ++;
//                cur --;
//                map.put(n, cur);
//            }
//        }
//        return Arrays.copyOfRange(result, 0, count);
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        int[] result = new int[nums1.length];
        int idx = 0, secondIdx = 0;
        a: for(int i = 0; i < nums1.length; i ++){
            b: for(int j = secondIdx; j < nums2.length; j ++){
                if(nums2[j] == nums1[i]){
                    result[idx] = nums1[i];
                    idx ++;
                    secondIdx = j + 1;
                    break b;
                }
            }
        }
        return Arrays.copyOfRange(result, 0, idx);

//        int len1 = nums1.length;
//        int len2 = nums2.length;
//        if(len1 == 0 || len2 == 0){
//            return new int[0];
//        }
//
//        sortArray(nums1);
//        sortArray(nums2);
//        int secondIdx = 0, left = -1, right = 0;
//        int[] result = new int[len1];
//        int count = 0;
//        a: for(int i = 0; i < len1; i ++){
//            b: for(int j = secondIdx; j < len2; j ++ ){
//                if(nums2[j] == nums1[i]){
//                    secondIdx = j + 1;
//                    result[count] = nums1[i];
//                    count ++;
//                    break b;
//                }
//            }
//        }
    }

    /**
     * 插入排序
     * @param nums
     */
    public static void sortArray(int[] nums){
        for(int i = 1; i < nums.length; i ++){
            int cur = nums[i];
            for(int j = i - 1; j >= 0; j --){
                if(nums[j] > cur){
                    nums[j+1] = nums[j];
                    nums[j] = cur;
                }
            }
        }
    }

    public static boolean increasingTriplet(int[] nums) {
        if(nums.length < 3){
            return false;
        }
        int min = Integer.MAX_VALUE;
        int mid = Integer.MAX_VALUE;
        for(int i = 0; i < nums.length; i ++){
            int n = nums[i];
            if(n > mid){
                return true;
            }else if(n <= min){
                min = n;
            }else {
                mid = n;
            }
        }
        return false;
    }

    /**
     * https://leetcode-cn.com/problems/n-queens/
     * N 皇后 问题
     * @param n
     * @return
     */
    public static List<List<String>> solveNQueens2(int n) {
        List<List<String>> result = new ArrayList<>();
        boolean[][] dp = new boolean[n][n];
        Deque<String> deque = new ArrayDeque<>();
        for(int i = 0; i < 2; i ++){
            if(i == 0){
                deque.offer(".");
                dfs2(0, 0, deque, dp, result, n);
                deque.pollLast();
            }else {
                deque.offer("Q");
                dp[0][0] = true;
                dfs2(0, 0, deque, dp, result, n);
                deque.pollLast();
                dp[0][0] = false;
            }
        }
        return result;
    }

    /**
     * 深度 优先搜索
     * @param r 行序号
     * @param c 列序号
     * @param deque 栈
     * @param dp 存储已经存放 女王的位置.
     * @param result 响应结果
     * @param n 总长度
     */
    public static void dfs2(int r, int c, Deque<String> deque, boolean[][] dp, List<List<String>> result, int n){
        if(deque.size() == n * n){
            int count = 0;
            for(int i = 0; i < dp.length; i ++){
                for(int j = 0; j < dp[0].length; j ++){
                    if(dp[i][j]){
                        count ++;
                    }
                }
            }
            if(count == n){
                //所有符号都填充完毕
                List<String> items = new ArrayList<>(deque);
                List<String> retItems = new ArrayList<>();
                for(int i = 0; i < n; i ++){
                    StringBuilder sb = new StringBuilder();
                    for(int j = i * n; j < (i+1) * n; j++){
                        sb.append(items.get(j));
                    }
                    retItems.add(sb.toString());
                }
                result.add(retItems);
            }
            return ;
        }
        int nr = r, nc = c;
        if(c == n - 1){
            nc = 0;
            nr = r + 1;
        }else {
            nc = c + 1;
        }
        for(int i = 0; i < 2; i++){
            if(i == 0){
                deque.offer(".");
                dfs2(nr, nc, deque, dp, result, n);
                deque.pollLast();
            }else {
                if(judgeAppendQ(dp, nr, nc)){
                    deque.offer("Q");
                    dp[nr][nc] = true;
                    dfs2(nr, nc, deque, dp, result, n);
                    deque.pollLast();
                    dp[nr][nc] = false;
                }
            }
        }
    }

    /**
     * 判断是否 可以移动
     * @param dp
     * @param r
     * @param c
     * @return
     */
    public static boolean judgeAppendQ(boolean[][] dp, int r, int c){
        int rows = dp.length;
        int cols = dp[0].length;
        //判断行 是否存在Q
        for(int i = 0; i < cols; i ++){
            if(dp[r][i]){
                return false;
            }
        }
        //判断列 是否已存在Q
        for(int j = 0; j < rows; j ++){
            if(dp[j][c]){
                return false;
            }
        }

        //判断 左斜线
        int tr = r, tc = c;
        while (tr >= 0 && tr < rows && tc >= 0 && tc < cols){
            if(dp[tr][tc]){
                return false;
            }
            tr++;
            tc++;
        }

        int tr2 = r, tc2 = c;
        while (tr2 >= 0 && tr2 < rows && tc2 >= 0 && tc2 < cols){
            if(dp[tr2][tc2]){
                return false;
            }
            tr2--;
            tc2++;
        }

        int tr3 = r, tc3 = c;
        while (tr3 >= 0 && tr3 < rows && tc3 >= 0 && tc3 < cols){
            if(dp[tr3][tc3]){
                return false;
            }
            tr3++;
            tc3--;
        }


        int tr4 = r, tc4 = c;
        while (tr4 >= 0 && tr4 < rows && tc4 >= 0 && tc4 < cols){
            if(dp[tr4][tc4]){
                return false;
            }
            tr4--;
            tc4--;
        }

        return true;
    }

    public static void main(String[] args) {
        int[][] matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
        System.out.println(searchMatrix(matrix, 25));
//        int[][] matrix = {{-1, -1}};
//        System.out.println(searchMatrix(matrix, -2));
        System.out.println(JSON.toJSONString(solveNQueens(4)));
        int[] nums = {3, 5, 1, 6, 11, 2, 8};
        findKthLargest(nums, 2);
    }

    /**
     * N皇后 问题
     * @param n
     * @return
     */
    public static List<List<String>> solveNQueens(int n) {

        char[][] chess = new char[n][n];//棋盘
        for(int i = 0; i < n ; i++){
            Arrays.fill(chess[i], '.');//先都填充 .
        }
        List<List<String>> result = new ArrayList<>();
        backTrace(chess, 0, n, result);
        return result;
    }

    /**
     * 回溯算法
     * @param row
     * @param n
     * @param result
     */
    public static void backTrace(char[][] chess, int row, int n, List<List<String>> result){
        if(row == n){
            //记录 数据
            List<String> items = new ArrayList<>();
            for(int i = 0; i < n; i++){
                items.add(new String(chess[i]));
            }
            result.add(items);
            return ;
        }

        a: for(int i = 0; i < n; i++){
            //判断当前列 是否可以放 Q
            b: for(int j = 0; j < row; j ++){
                if(chess[j][i] == 'Q'){
                    continue a;
                }
            }

            //判断对角线  左侧对角线
            c: for(int r = row, c = i; r >= 0 && c >= 0; r --, c --){
                if(chess[r][c] == 'Q'){
                    continue a;
                }
            }

            //判断对角线  左侧对角线
            d: for(int r = row, c = i; r >= 0 && c < n; r --, c ++){
                if(chess[r][c] == 'Q'){
                    continue a;
                }
            }

            //该位置 可以放 Q
            chess[row][i] = 'Q';
            backTrace(chess, row + 1, n, result);
            chess[row][i] = '.';
        }
    }

    /**
     * 搜索 二位矩阵
     * @param matrix
     * @param target  找到目标值
     * @return
     */
    public static boolean searchMatrix(int[][] matrix, int target) {

        int rows = matrix.length;
        int cols = matrix[0].length;

        int fr = rows - 1, fc = 0;
        while (fr >= 0 && fc < cols){
            if(matrix[fr][fc] > target){
                fr --;
            }else if(matrix[fr][fc] < target){
                fc ++;
            }else {
                return true;
            }
        }
        return false;
    }


    /**
     * 回溯算法
     * @param row
     * @param n
     * @param result
     */
    public static void backTrace(int row, int n, List<List<String>> result){
        
    }

    public static void jiuyuan(int[][] ct){
        int[][] tmp = {
                {1, 1, 0, 1, 0, 1, 1},
                {1, 1, 0, 1, 1, 1, 1},
                {0, 1, 1, 0, 1, 1, 1},
                {1, 0, 1, 0, 1, 1, 1},
                {1, 1, 1, 1, 1, 0, 1},
                {1, 0, 1, 0, 1, 1, 1},
                {1, 1, 1, 1, 1, 0, 1}};
        int len = ct.length;
        boolean[][] dp = new boolean[len][len];//记录人走过的位置


    }

    public static int[] productExceptSelf(int[] nums) {
        int sum = 1;
        int zeroCount = 0;
        for(int n:nums){
            if(n != 0){
                sum = sum * n;
            }else {
                zeroCount ++;
            }
        }
        int[] res = new int[nums.length];
        for(int i = 0; i < nums.length; i ++){
            if(zeroCount == 0){
                res[i] = sum / nums[i];
            }else if(zeroCount == 1){
                //只有一个0
                if(nums[i] != 0){
                    res[i] = 0;
                }else {
                    res[i] = sum;
                }
            }else{
                //如果有多个0
                res[i] = 0;
            }
        }
        return res;
    }

    public static int findKthLargest(int[] nums, int k) {
//        Arrays.sort(nums);
//        return nums[nums.length - k];
        PriorityQueue<Integer> queue = new PriorityQueue<>(k, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });
        int count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (count < k) {
                queue.offer(nums[i]);
                count++;
            } else {
                int peek = queue.peek();
                if (nums[i] > peek) {
                    queue.poll();
                    queue.offer(nums[i]);
                }
            }
        }

        return queue.peek();
    }
}
