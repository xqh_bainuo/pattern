package org.xqh.study.leetcode.algorithm.slidewindow;

/**
 * @ClassName MaxScore
 * @Description 可获得最大点数
 *
 * https://leetcode-cn.com/problems/maximum-points-you-can-obtain-from-cards/
 * @Author xuqianghui
 * @Date 2021/2/6 9:42
 * @Version 1.0
 */
public class MaxScore {

    public static void main(String[] args) {
        int[] points = {1,2,3,4,5,6,1};
        maxScore(points, 3);
    }

    /**
     * 依次从首尾 取数据 得到最大的 可以 查 数组中最小的 连续元素
     * @param cardPoints
     * @param k
     * @return
     */
    public static int maxScore(int[] cardPoints, int k) {
        int len = cardPoints.length;
        int subLen = len - k;
        int totalSum = 0;
        int sum = 0;
        for(int i = 0; i < subLen; i++){
            sum += cardPoints[i];
        }
        totalSum += sum;
        for(int i = subLen; i < len; i ++){
            totalSum += cardPoints[i];
        }
        int min = sum;
        for(int i = 1; i < len - subLen + 1; i++){
            sum += cardPoints[i + subLen - 1];
            sum -= cardPoints[i -1];
            min = Math.min(min, sum);
        }

        return totalSum - min;

    }
}
