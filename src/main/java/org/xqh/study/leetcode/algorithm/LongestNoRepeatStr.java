package org.xqh.study.leetcode.algorithm;


import scala.Int;

import java.util.*;

/**
 * @ClassName LongestNoRepeatStr
 * @Description 取最长的不重复的 字符串长度
 * https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/
 * @Author xuqianghui
 * @Date 2021/1/22 18:28
 * @Version 1.0
 */
public class LongestNoRepeatStr {

    public static void main(String[] args) {
        System.out.println(lengthOfLongestSubstring("abcbfg"));
//        System.out.println(lengthOfLongestSubstring("aaaaaa"));
//        System.out.println(lengthOfLongestSubstring("abcdaaa"));
//        System.out.println(lengthOfLongestSubstring("abba"));
//        System.out.println(lengthOfLongestSubstring("abcbfg"));
    }

    public static int lengthOfLongestSubstring(String s) {
        if("".equals(s)){
            return 0;
        }
        int maxLen = 0;
        char[] array = s.toCharArray();
        int idx = 0;
        int len = 0;
        int start = 0;
        while (idx < array.length){
            char cur = array[idx];
            boolean flag = false;
            for(int i = start; i < idx; i ++){
                if(array[i] == cur){
                    start = i + 1;
                    flag = true;
                    break;
                }
            }
            if(flag){
                len = idx - start + 1;
            }else {
                len ++;
            }
            idx ++;
            if(len > maxLen){
                maxLen = len;
            }
        }
        return maxLen;
    }
}
