package org.xqh.study.leetcode.algorithm;

/**
 * @ClassName TestDemo
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/9/11 16:13
 * @Version 1.0
 */
public class TestDemo {

    public static void main(String[] args) {
        // 101 & 0011
        System.out.println(5 & 3);
    }

    public int findIntegers(int n) {

        int ret = 0;
        for(int i = 0; i < n; i ++){
            System.out.println(i & 3);
        }

        return ret;
    }
}
