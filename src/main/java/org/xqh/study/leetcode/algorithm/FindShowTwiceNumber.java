package org.xqh.study.leetcode.algorithm;

import java.util.Arrays;

/**
 * @ClassName FindShowTwiceNumber
 * @Description 找出出现两次的数字
 * 2.5亿个数字中, 只有一个数字 出现两次, 其他都只出现一次
 * @Author xuqianghui
 * @Date 2021/2/8 17:57
 * @Version 1.0
 */
public class FindShowTwiceNumber {

    public static void main(String[] args) {
        int[] nums = {3, 9, 11, 18, 10, 7, 8, 6, 12, 5, 18};
        System.out.println(findTwiceNum(nums));;
    }

    /**
     * 变量 sum 为 元素 异或 之和
     * 如果
     * @param nums
     * @return
     */
    public static int findTwiceNum(int[] nums){
        int len = nums.length;
        int max = 0;
        for(int i=0; i < len; i ++){
            max = Math.max(nums[i], max);
        }
        max = Math.max(len, max);
        int[] tmpArr = new int[max + 1];
        Arrays.fill(tmpArr, -1);
        int ret = 0;
        for(int i = 0; i < len; i++){
            int cur = nums[i];
            if(tmpArr[cur] == -1){
                tmpArr[cur] = cur;
            }else {
                return cur;
            }
        }
        return ret;
    }
}
