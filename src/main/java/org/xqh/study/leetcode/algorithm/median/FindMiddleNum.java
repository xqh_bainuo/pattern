package org.xqh.study.leetcode.algorithm.median;

import com.alibaba.fastjson.JSON;

/**
 * @ClassName FindMiddleNum
 * @Description 查找中位数
 * @Author xuqianghui
 * @Date 2021/1/5 18:29
 * @Version 1.0
 */
public class FindMiddleNum {

    /**
     * https://leetcode-cn.com/problems/median-of-two-sorted-arrays/
     * 查找 两个数组的中位数
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int[] nums3 = combineArray(nums1, nums2);
        System.out.println(JSON.toJSONString(nums3));
        if (nums3.length == 0) {
            return 0;
        }
        if (nums3.length == 1) {
            return Double.valueOf(nums3[0]);
        }
        int mod = nums3.length / 2;
        if (nums3.length % 2 == 0) {
            //偶数
            return Double.valueOf(nums3[mod - 1] + nums3[mod]) / 2;
        } else {
            return Double.valueOf(nums3[mod]);
        }
    }

    /**
     * 合并两个正序数组
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public static int[] combineArray(int[] nums1, int[] nums2) {
        int[] retArr = new int[nums1.length + nums2.length];
        int m = nums1.length;//数组一长度
        int n = nums2.length;//数组二长度
        int idx1 = 0;//数组1 指针
        int idx2 = 0;//数组2 指针
        //设置合并数组 第一位
        for (int i = 0; i < retArr.length; i++) {
            if(idx1 == m){//数组1已经 遍历完
                retArr[i] = nums2[idx2];
                idx2 ++;
                continue;
            }

            if(idx2 == n){//数组2 已经遍历完
                retArr[i] = nums1[idx1];
                idx1 ++;
                continue;
            }

            if(nums1[idx1] <= nums2[idx2]){
                retArr[i] = nums1[idx1];
                idx1++;
            }else {
                retArr[i] = nums2[idx2];
                idx2++;
            }
        }

        return retArr;
    }

    public static void main(String[] args) {
        int[] a1 = {1,2};
        int[] a2 = {3,4};
        System.out.println(findMedianSortedArrays(a1, a2));
    }

    @Deprecated
    public static double findMedianSortedArrays3(int[] nums1, int[] nums2) {
        //长度小的 数组放前面
        if (nums1.length > nums2.length) {
            int[] tmp = nums1;
            nums1 = nums2;
            nums2 = tmp;
        }
        int m = nums1.length;
        int n = nums2.length;
        if (m + n == 0) {
            return 0;
        }
        if (m + n == 1) {
            return nums2[0];
        }
        /**
         * m + n 是 奇数 中位数 所在位置 为
         * nums1[i-1] <= nums2[j] && nums2[j-1] <= nums1[i]
         */
        int totalLen = (m + n + 1) / 2;
        int idx1 = 0;
        int idx2 = 0;
        a:
        for (int i = 0; i <= nums1.length; i++) {
            b:
            for (int j = 0; j <= nums2.length; j++) {
                if (i + j == totalLen) {
                    if (((i - 1 >= 0 && j < n) ? nums1[i - 1] <= nums2[j] : true) && (j - 1 >= 0 && i < m) ? nums2[j - 1] <= nums1[i] : true) {
                        idx1 = i;
                        idx2 = j;
                        break a;
                    }
                }
            }
        }
        //中位数 为 中间两个数 取平均
        int n1 = idx1 == 0 ? nums2[idx2 - 1] : (idx2 == 0 ? nums1[idx1 - 1] : nums1[idx1 - 1] >= nums2[idx2 - 1] ? nums1[idx1 - 1] : nums2[idx2 - 1]);
        int n2 = idx1 == m ? nums2[idx2] : (idx2 == n ? nums1[idx1] : (nums1[idx1] <= nums2[idx2] ? nums1[idx1] : nums2[idx2]));
        if ((m + n) % 2 == 0) {
            return Double.valueOf(n1 + n2) / 2;
        } else {
            return n1;
        }
    }


    /**
     * 官方题解
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public static double findMedianSortedArrays444(int[] nums1, int[] nums2) {
        int length1 = nums1.length, length2 = nums2.length;
        int totalLength = length1 + length2;
        if (totalLength % 2 == 1) {//奇数
            int midIndex = totalLength / 2;
            double median = getKthElement(nums1, nums2, midIndex + 1);
            return median;
        } else {
            int midIndex1 = totalLength / 2 - 1, midIndex2 = totalLength / 2;
            double median = (getKthElement(nums1, nums2, midIndex1 + 1) + getKthElement(nums1, nums2, midIndex2 + 1)) / 2.0;
            return median;
        }
    }

    /**
     * 找到第k大 的元素
     *
     * @param nums1
     * @param nums2
     * @param k
     * @return
     */
    public static int getKthElement(int[] nums1, int[] nums2, int k) {
        /* 主要思路：要找到第 k (k>1) 小的元素，那么就取 pivot1 = nums1[k/2-1] 和 pivot2 = nums2[k/2-1] 进行比较
         * 这里的 "/" 表示整除
         * nums1 中小于等于 pivot1 的元素有 nums1[0 .. k/2-2] 共计 k/2-1 个
         * nums2 中小于等于 pivot2 的元素有 nums2[0 .. k/2-2] 共计 k/2-1 个
         * 取 pivot = min(pivot1, pivot2)，两个数组中小于等于 pivot 的元素共计不会超过 (k/2-1) + (k/2-1) <= k-2 个
         * 这样 pivot 本身最大也只能是第 k-1 小的元素
         * 如果 pivot = pivot1，那么 nums1[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums1 数组
         * 如果 pivot = pivot2，那么 nums2[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums2 数组
         * 由于我们 "删除" 了一些元素（这些元素都比第 k 小的元素要小），因此需要修改 k 的值，减去删除的数的个数
         */

        int length1 = nums1.length, length2 = nums2.length;
        int index1 = 0, index2 = 0;
        int kthElement = 0;

        while (true) {
            // 边界情况
            if (index1 == length1) {
                return nums2[index2 + k - 1];
            }
            if (index2 == length2) {
                return nums1[index1 + k - 1];
            }
            if (k == 1) {
                return Math.min(nums1[index1], nums2[index2]);
            }

            // 正常情况
            int half = k / 2;
            int newIndex1 = Math.min(index1 + half, length1) - 1;
            int newIndex2 = Math.min(index2 + half, length2) - 1;
            int pivot1 = nums1[newIndex1], pivot2 = nums2[newIndex2];
            if (pivot1 <= pivot2) {
                k -= (newIndex1 - index1 + 1);
                index1 = newIndex1 + 1;
            } else {
                k -= (newIndex2 - index2 + 1);
                index2 = newIndex2 + 1;
            }
        }
    }

    /**
     * 查找中位数 官方题解思路 二分法查找
     *
     * @param nums1
     * @param nums2
     * @return
     */
    public static double findMedianSortedArrays555(int[] nums1, int[] nums2) {

        //把小数组 放上面
        if (nums1.length > nums2.length) {
            int[] tmp = nums1;
            nums1 = nums2;
            nums2 = tmp;
        }

        int m = nums1.length;
        int n = nums2.length;

        int totalLeft = (m + n + 1) / 2;//中位数 位置

        //在 nums1的 区间 [0,m]里查找恰当的分割线.
        int left = 0;//
        int right = m;//
        while (left < right) {
            int i = left + (right - left + 1) / 2;//分割线在第一个数组 左边元素的个数
            int j = totalLeft - i; //分割线 在 第二个数组 左边元素的个数
            if (nums1[i - 1] > nums2[j]) {
                //下一个 搜索区间 [left, i-1]
                right = i - 1;
            } else {
                //下一个搜索区间 [i, right], 如果这个区间 只有两个元素的时候
                left = i;
            }
        }
        int i = left;//数组1 分割线位置
        int j = totalLeft - i;//数组二分割线位置
        int oneLeftVal = i == 0 ? Integer.MIN_VALUE : nums1[i-1];
        int oneRightVal = i == m ? Integer.MAX_VALUE : nums1[i];
        int twoLeftVal = j == 0 ? Integer.MIN_VALUE : nums2[j-1];
        int twoRightVal = j == n ? Integer.MAX_VALUE : nums2[j];

        int val1 = Math.max(oneLeftVal, twoLeftVal);
        int val2 = Math.min(oneRightVal, twoRightVal);
        if((m+n) % 2 == 0){
            return Double.valueOf(val1 + val2)/2;
        }else {
            return val1;
        }
    }


}
