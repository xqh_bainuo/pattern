package org.xqh.study.spi.java;

/**
 * @ClassName IAnimal
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/12/18 11:43
 * @Version 1.0
 */
public interface IAnimal {

    void shout();
}
