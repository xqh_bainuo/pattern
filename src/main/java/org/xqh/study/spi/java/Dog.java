package org.xqh.study.spi.java;

/**
 * @ClassName Dog
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/12/18 11:44
 * @Version 1.0
 */
public class Dog implements IAnimal {
    @Override
    public void shout() {
        System.out.println("wang...");
    }
}
