package org.xqh.study.spi.java;

/**
 * @ClassName Cat
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/12/18 11:43
 * @Version 1.0
 */
public class Cat implements IAnimal {
    @Override
    public void shout() {
        System.out.println("miao...");
    }
}
