package org.xqh.study.spi.java;

import java.util.ServiceLoader;

/**
 * @ClassName SpiTest
 * @Description java spi 示例
 * @Author xuqianghui
 * @Date 2020/12/18 11:44
 * @Version 1.0
 */
public class SpiTest {

    /**
     * @see resources/META-INF/services/org.xqh.study.spi.java.IAnimal
     * @param args
     */
    public static void main(String[] args) {
        ServiceLoader<IAnimal> animals = ServiceLoader.load(IAnimal.class);
        for(IAnimal an:animals){
            an.shout();
        }
    }
}
