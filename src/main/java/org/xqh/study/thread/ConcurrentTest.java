package org.xqh.study.thread;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName ConcurrentTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/12/7 14:51
 * @Version 1.0
 */
public class ConcurrentTest {

    private ConcurrentHashMap<String, Object> map = new ConcurrentHashMap<>();
    static String key = "DH00002010000838";
    public static void main(String[] args) {

        ConcurrentTest test = new ConcurrentTest();
        for(int i = 0; i < 100; i ++){
            new Thread(new TestThread(test, i)).start();
        }
    }

    public static class TestThread implements Runnable{
        private ConcurrentTest obj;
        private int val;

        public TestThread(ConcurrentTest obj, int val){
            this.obj = obj;
            this.val = val;
        }
        @Override
        public void run() {
            obj.pubVal(key+val, val);
            System.out.println(System.currentTimeMillis() +":"+ obj.map.size());
        }
    }

    public Object pubVal(String key, Object val){
        map.put(key, val);
        return val;
    }
}
