package org.xqh.study.google.guice.printdemo;

import com.google.inject.AbstractModule;
import org.xqh.study.google.guice.Applets;

/**
 * @ClassName PrintLineModule
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 19:20
 * @Version 1.0
 */
public class PrintLineModule extends AbstractModule {


    @Override
    protected void configure() {
//        bind(MyApplet.class).annotatedWith(Names.named("println")).to(PrintLineApplet.class);
        Applets.register(binder()).named("println").to(PrintLineApplet.class);
    }

}
