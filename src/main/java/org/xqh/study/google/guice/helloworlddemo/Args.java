package org.xqh.study.google.guice.helloworlddemo;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @ClassName Args
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 19:22
 * @Version 1.0
 */
@BindingAnnotation
@Retention(RetentionPolicy.RUNTIME)
public @interface Args {
}
