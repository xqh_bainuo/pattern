package org.xqh.study.google.guice.server;

/**
 * @ClassName OrderService
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 9:59
 * @Version 1.0
 */
public interface OrderService {
    void sendToPayment(long orderId);
}
