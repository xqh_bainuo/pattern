package org.xqh.study.google.guice.printdemo;

import org.xqh.study.google.guice.MyApplet;

/**
 * @ClassName PrintLineApplet @Description TODO @Author xuqianghui @Date 2022/5/23 19:58 @Version
 * 1.0
 */
public class PrintLineApplet implements MyApplet {

  @Override
  public void run() {
    System.out.println("printlnApplet");
  }
}
