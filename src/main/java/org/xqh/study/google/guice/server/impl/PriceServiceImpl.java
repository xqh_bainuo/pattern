package org.xqh.study.google.guice.server.impl;

import org.xqh.study.google.guice.server.PriceService;

import javax.inject.Inject;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName PriceServiceImpl
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 13:41
 * @Version 1.0
 */
public class PriceServiceImpl implements PriceService {

    private final Set<String> supportedCurrencies;
    private final Map<String, Object> testMapBinder;

    @Inject
    public PriceServiceImpl(Set<String> supportedCurrencies, Map<String, Object> testMapBinder) {
        this.testMapBinder = testMapBinder;
        this.supportedCurrencies = supportedCurrencies;
    }

    @Override
    public long getPrice(long orderId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<String> getSupportedCurrencies() {
        return supportedCurrencies;
    }

    @Override
    public Map<String, Object> testMapBinder() {
        return testMapBinder;
    }
}
