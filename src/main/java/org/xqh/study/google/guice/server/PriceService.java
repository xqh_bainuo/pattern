package org.xqh.study.google.guice.server;

import java.util.Map;
import java.util.Set;

/**
 * @ClassName PriceService
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 10:18
 * @Version 1.0
 */
public interface PriceService {

    long getPrice(long orderId);

    Set<String> getSupportedCurrencies();

    Map<String, Object> testMapBinder();
}
