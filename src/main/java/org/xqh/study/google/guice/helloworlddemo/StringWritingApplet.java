package org.xqh.study.google.guice.helloworlddemo;

import org.xqh.study.google.guice.MyApplet;

import javax.inject.Inject;
import javax.inject.Provider;

/** 1.0 */
public class StringWritingApplet implements MyApplet {

  private MyDestination destination;
  private Provider<String> stringProvider;

  @Inject
  public StringWritingApplet(MyDestination destination, @Output Provider<String> stringProvider) {
    super();
    this.destination = destination;
    this.stringProvider = stringProvider;
  }

  private void writeString() {
    destination.write(stringProvider.get());
  }

  public void run() {
    writeString();
  }
}
