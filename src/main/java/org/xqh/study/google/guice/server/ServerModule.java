package org.xqh.study.google.guice.server;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import org.xqh.study.google.guice.server.impl.OrderServiceImpl;
import org.xqh.study.google.guice.server.impl.PaymentServiceImpl;
import org.xqh.study.google.guice.server.impl.PriceServiceImpl;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName ServerModule
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 13:42
 * @Version 1.0
 */
public class ServerModule extends AbstractModule {

    @Override
    protected void configure() {

        install(new ChinaModule());
        install(new GlobalModule());

        bind(OrderService.class).to(OrderServiceImpl.class);
        bind(PriceService.class).to(PriceServiceImpl.class);
        bind(PaymentService.class).to(PaymentServiceImpl.class);

        //这样 绑定 sessionId是个固定值 不能调整
//        bind(Long.class).annotatedWith(SessionId.class).toInstance(System.currentTimeMillis());
//        bind(Long.class).annotatedWith(Names.named("appId")).toInstance(456L);

        bind(new TypeLiteral<List<String>>(){})
                .annotatedWith(Names.named("supportedCurrencies"))
                .toInstance(Arrays.asList("CNY", "USD", "EUR"));

        //测试代码 重新指定 PriceService实现实例.
//        bind(PriceServiceImpl.class).toInstance(new PriceServiceImpl(){
//            @Override
//            public long getPrice(long orderId) {
//                return 567L;
//            }
//        });
    }

    @Provides @SessionId Long generateSessionId(){
        return System.currentTimeMillis();
    }

//    @Provides @Named("supportedCurrencies")
//    List<String> getSupportedCurrencies(PriceService priceService) {
//        return priceService.getSupportedCurrencies();
//    }
}
