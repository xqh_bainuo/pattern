package org.xqh.study.google.guice.helloworlddemo;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.xqh.study.google.guice.Applets;

import java.io.PrintStream;
import java.util.List;

/**
 * @ClassName HelloWorldModule @Description TODO @Author xuqianghui @Date 2022/5/20 16:02 @Version
 * 1.0
 */
public class HelloWorldModule extends AbstractModule {

  @Override
  protected void configure() {
//    bind(MyApplet.class).annotatedWith(Names.named("hello")).to(StringWritingApplet.class);
    Applets.register(binder()).named("hello").to(StringWritingApplet.class);
    bind(MyDestination.class).to(PrintStreamWriter.class);
    bind(PrintStream.class).toInstance(System.out);
//    bind(String.class).annotatedWith(Output.class).toInstance("Hello World");
    //        bind(String.class).toProvider(new Provider<String>() {
    //            @Override
    //            public String get() {
    //                return "Hello World";
    //            }
    //        });
  }

  // 可用 bind(String.class).toInstance("Hello World"); 方式替代
  //    @Provides private String getString(){
  //        return "Hello World";
  //    }

  @Provides @Output String getOutputString(@Args List<String> args){
      return args.get(0);
  }
}
