package org.xqh.study.google.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.xqh.study.google.guice.helloworlddemo.Args;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName CommandLineModule
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 20:17
 * @Version 1.0
 */
public class CommandLineModule extends AbstractModule {
    private final String[] args;

    public CommandLineModule(String[] args) {
        this.args = args;
    }

    @Override
    protected void configure() {

    }


    @Provides
    @Args
    List<String> getCommandLineArgs(){
        return Arrays.asList(args).subList(1, args.length);
    }
}
