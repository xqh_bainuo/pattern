package org.xqh.study.google.guice.server;

import com.google.inject.Provider;

import javax.inject.Inject;

/**
 * @ClassName SessionManager
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 10:18
 * @Version 1.0
 */
public class SessionManager {

    private final Provider<Long> sessionProvider;

    @Inject
    public SessionManager(@SessionId Provider<Long> sessionProvider){
        this.sessionProvider = sessionProvider;
    }

    public Long getSessionId() {
        return sessionProvider.get();
    }
}
