package org.xqh.study.google.guice.server.impl;

import org.xqh.study.google.guice.server.OrderService;
import org.xqh.study.google.guice.server.SessionManager;
import org.xqh.study.google.guice.server.PaymentService;
import org.xqh.study.google.guice.server.PriceService;

import javax.inject.Inject;

/**
 * @ClassName OrderServiceImpl
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 10:17
 * @Version 1.0
 */
public class OrderServiceImpl implements OrderService {

    //Dependencies
    private final PriceService priceService;
    private final PaymentService paymentService;
    private final SessionManager sessionManager;


    //States
    private Long orderPaid = 0L;

    @Inject
    public OrderServiceImpl(PriceService priceService, PaymentService paymentService, SessionManager sessionManager) {
        this.priceService = priceService;
        this.paymentService = paymentService;
        this.sessionManager = sessionManager;
    }

    public void sendToPayment(long orderId){
        long price = priceService.getPrice(orderId);
        paymentService.pay(orderId, price, sessionManager.getSessionId());
        orderPaid = orderPaid + 1;
    }


}
