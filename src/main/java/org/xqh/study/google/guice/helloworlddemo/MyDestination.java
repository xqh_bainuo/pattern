package org.xqh.study.google.guice.helloworlddemo;

/**
 * @ClassName MyDestination
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/19 19:15
 * @Version 1.0
 */
public interface MyDestination {

    void write(String hello_world);
}
