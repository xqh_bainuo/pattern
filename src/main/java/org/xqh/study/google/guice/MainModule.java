package org.xqh.study.google.guice;

import com.google.inject.AbstractModule;
import org.xqh.study.google.guice.helloworlddemo.HelloWorldModule;
import org.xqh.study.google.guice.printdemo.PrintLineModule;

/**
 * @ClassName MainModule
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/20 15:32
 * @Version 1.0
 */
public class MainModule extends AbstractModule {

    @Override
    protected void configure() {
        install(new HelloWorldModule());
        install(new PrintLineModule());
    }

}
