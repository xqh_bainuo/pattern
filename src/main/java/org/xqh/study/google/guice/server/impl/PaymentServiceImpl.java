package org.xqh.study.google.guice.server.impl;

import lombok.extern.slf4j.Slf4j;
import org.xqh.study.google.guice.server.PaymentService;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * @ClassName PaymentServiceImpl
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 13:41
 * @Version 1.0
 */
@Slf4j
public class PaymentServiceImpl implements PaymentService {

    private final List<String> testList;

    @Inject
    public PaymentServiceImpl(@Named("supportedCurrencies") List<String> testList) {
        this.testList = testList;
    }

    @Override
    public List<String> testList() {
        return testList;
    }

    @Override
    public void pay(long orderId, long price, Long sessionId) {
      log.info("pay {} $ for order: {}, sessionId: {}", price, orderId, sessionId);
    }
}
