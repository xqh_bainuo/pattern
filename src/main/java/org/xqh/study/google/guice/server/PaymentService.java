package org.xqh.study.google.guice.server;

import java.util.List;

/**
 * @ClassName PaymentService
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 10:18
 * @Version 1.0
 */
public interface PaymentService {
    void pay(long orderId, long price, Long sessionId);

    List<String> testList();
}
