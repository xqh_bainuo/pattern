package org.xqh.study.google.guice.server;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.multibindings.Multibinder;

/**
 * @ClassName ChinaModule
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 18:23
 * @Version 1.0
 */
public class ChinaModule extends AbstractModule {

    @Override
    protected void configure() {
        //Adds CNY support
        Multibinder.newSetBinder(binder(), String.class).addBinding().toInstance("CNY");
        MapBinder.newMapBinder(binder(), String.class, Object.class).addBinding("key").toInstance("value111");
    }
}
