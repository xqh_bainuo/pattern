package org.xqh.study.google.guice.helloworlddemo;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @ClassName Output
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/20 16:07
 * @Version 1.0
 */
@Retention(RUNTIME)
@BindingAnnotation
public @interface Output {
}
