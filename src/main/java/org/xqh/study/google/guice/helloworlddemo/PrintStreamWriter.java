package org.xqh.study.google.guice.helloworlddemo;

import javax.inject.Inject;
import java.io.PrintStream;

/**
 * @ClassName PrintStreamWriter @Description TODO @Author xuqianghui @Date 2022/5/19 19:19 @Version
 * 1.0
 */
public class PrintStreamWriter implements MyDestination {

  private PrintStream destination;

  @Inject
  public PrintStreamWriter(PrintStream destination) {
    this.destination = destination;
  }

  @Override
  public void write(String hello_world) {
    destination.println(hello_world);
  }
}
