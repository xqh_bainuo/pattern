package org.xqh.study.google.guice.server;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

/**
 * @ClassName GlobalModule
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 18:23
 * @Version 1.0
 */
public class GlobalModule extends AbstractModule {
    @Override
    protected void configure() {
        //Adds USD, EUR support
        Multibinder multibinder = Multibinder.newSetBinder(binder(), String.class);

        multibinder.addBinding().toInstance("USD");
        multibinder.addBinding().toInstance("EUR1");
    }
}
