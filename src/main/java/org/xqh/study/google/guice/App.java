package org.xqh.study.google.guice;


import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * @ClassName App
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/19 17:16
 * @Version 1.0
 */
public class App {

  /**
   * bootstrap:
   * parse command line
   * set up environment
   * kick off main logic
   * @param args
   */
  public static void main(String[] args) {

    Injector injector = Guice.createInjector(new MainModule(), new CommandLineModule(args));
    Applets.get(injector, args[0]).run();
  }
}
