package org.xqh.study.google.guice.server;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @ClassName SessionId
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/23 16:42
 * @Version 1.0
 */
@BindingAnnotation
@Retention(RetentionPolicy.RUNTIME)
public @interface SessionId {
}
