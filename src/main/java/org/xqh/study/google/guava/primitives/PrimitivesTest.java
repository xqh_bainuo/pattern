package org.xqh.study.google.guava.primitives;

import com.google.common.collect.Lists;
import com.google.common.primitives.*;
import org.junit.Test;

import java.util.Arrays;

/**
 * 原生类型工具
 * 1、Booleans
 * 2、Bytes
 * 3、Chars
 * 4、Doubles
 * 5、FLoats
 * 6、Ints
 * 7、Longs
 * 8、Shorts
 * 9、ImmutableDoubleArray、ImmutableIntArray、ImmutableLongArray
 * 10、SignedBytes
 * 11、UnsignedBytes、UnsignedInteger、UnsignedInts、UnsignedLong、UnsignedLongs
 *
 * 参考博客 ： https://www.yiibai.com/guava/guava_ints.html
 */
public class PrimitivesTest {
    //Java的原生类型就是指基本类型：byte、short、int、long、float、double、char和boolean。
    //原生类型不能当作对象或泛型的类型参数使用，这意味着许多通用方法都不能应用于它们。
    // Guava提供了若干通用工具，包括原生类型数组与集合API的交互，原生类型和字节数组的相互转换，
    // 以及对某些原生类型的无符号形式的支持。

    //原生 Boolean 工具类
    @Test
    public void testBooleans() {
        //1 返回由指定数组支持的固定大小的列表，类似 Arrays.asList(Object[]).
        System.out.println(Booleans.asList(true, false, true, false));

        //2 比较两个指定的布尔值的标准方式(假的比真的少考虑以下)。
        System.out.println(Booleans.compare(true, false));

        //3 每个数组提供组合成一个单一的数组，则值返回。
        System.out.println(Arrays.toString(Booleans.concat(Booleans.toArray(Booleans.asList(true, false)), Booleans.toArray(Booleans.asList(true, true)))));

        //4 如果数组里面存在target元素，则返回true
        System.out.println(Booleans.contains(Booleans.toArray(Booleans.asList(true, false)), true));

        //5 返回为true值的数目。
        System.out.println(Booleans.countTrue(true, false, true, true, false));

        //6 返回一个包含相同的值数组的数组，但保证是一个规定的最小长度。
        System.out.println(Arrays.toString(Booleans.ensureCapacity(Booleans.toArray(Booleans.asList(true, false)), 2, 3)));

        //7 返回哈希码的值;等于调用的结果 ((Boolean) value).hashCode().
        System.out.println(Booleans.hashCode(true));

        //8 返回目标数组的首次出现的索引值。
        System.out.println(Booleans.indexOf(Booleans.toArray(Booleans.asList(true, false)), true));

        //9 返回指定目标的第一个匹配的起始位置数组内，或-1，如果不存在。
        System.out.println(Booleans.indexOf(Booleans.toArray(Booleans.asList(true, false, true)), Booleans.toArray(Booleans.asList(true, false))));

        //10 返回包含由分离器分离所提供的布尔值的字符串。
        System.out.println(Booleans.join(",", true, false, true, true));

        //11 返回target 在数组中最后一个出现的索引值。
        System.out.println(Booleans.lastIndexOf(Booleans.toArray(Booleans.asList(true, false)), true));

        //12 返回一个比较器，它比较两个布尔数组字典顺序。
        System.out.println(Booleans.lexicographicalComparator());

        //13 复制Boolean实例集合到原始的布尔值的新数组。
        System.out.println(Arrays.toString(Booleans.toArray(Lists.newArrayList(true, true, false, false))).toString());

    }

    //原生 Byte 工具类
    @Test
    public void testBytes() {
        //1 返回包含集合的每个值的数组，转换为字节值中的方式Number.byteValue()
        System.out.println(Arrays.toString(Bytes.toArray(Lists.newArrayList(1, 2, 3))));

        //2 返回由指定数组支持的固定大小的列表，类似 Arrays.asList(Object[]).
        System.out.println(Bytes.asList(Bytes.toArray(Lists.newArrayList(1, 2, 3))));

        //3 则返回来自每个阵列提供组合成一个单一的阵列值。
        System.out.println(Arrays.toString(Bytes.concat(Bytes.toArray(Lists.newArrayList(1, 2, 3)), Bytes.toArray(Lists.newArrayList(1, 2, 3)))));

        //4 返回true，如果目标是否存在在任何地方数组元素。
        System.out.println(Bytes.contains(Bytes.toArray(Lists.newArrayList(1, 2, 3)), (byte) 2));

        //5 返回一个包含相同的值数组的数组，但保证是一个规定的最小长度。
        System.out.println(Arrays.toString(Bytes.ensureCapacity(Bytes.toArray(Lists.newArrayList(1, 2, 3)), 2, 4)));

        //6 返回哈希码的值;等于调用的结果 ((Byte) value).hashCode().
        System.out.println(Bytes.hashCode((byte) 2));

        //7 返回目标数组的首次出现的索引值。
        System.out.println(Bytes.indexOf(Bytes.toArray(Lists.newArrayList(1, 2, 3)), (byte)2));

        //8 返回指定目标的第一个匹配的起始位置数组内，或-1如果不存在。
        System.out.println(Bytes.indexOf(Bytes.toArray(Lists.newArrayList(1, 2, 3)), Bytes.toArray(Lists.newArrayList(1, 2))));

        //9 返回目标在数组中最后一个出场的索引的值。
        System.out.println(Bytes.lastIndexOf(Bytes.toArray(Lists.newArrayList(1, 2, 3)), (byte)2));

    }

    //原生 Char 工具类
    @Test
    public void testChars() {
        /*
        1
        static List<Character> asList(char... backingArray)
        返回由指定数组支持的固定大小的列表，类似 Arrays.asList(Object[]).

        2
        static char checkedCast(long value)
        返回char值等于value值，如果可能的话。

        3
        static int compare(char a, char b)
        比较两个指定的char值。

        4
        static char[] concat(char[]... arrays)
        每个数组提供组合成一个单一的数组，则返回值。

        5
        static boolean contains(char[] array, char target)
        返回true，如果target是否存在在任何地方数组元素。

        6
        static char[] ensureCapacity(char[] array, int minLength, int padding)
        返回一个包含相同的值数组的数组，但保证是一个规定的最小长度。

        7
        static char fromByteArray(byte[] bytes)
        返回char值，其大端表示被存储在第一个2字节的字节;相当于 ByteBuffer.wrap(bytes).getChar().

        8
        static char fromBytes(byte b1, byte b2)
        返回char值的字节表示是给定2个字节，在big-endian的顺序;相当于 Chars.fromByteArray(new byte[] {b1, b2}).

        9
        static int hashCode(char value)
        返回哈希码的值;等于调用的结果 ((Character) value).hashCode().

        10
        static int indexOf(char[] array, char target)
        返回目标数组的首次出现的索引值。

        11
        static int indexOf(char[] array, char[] target)
        返回指定目标的第一个匹配的起始位置数组内，或-1，如果不存在。

        12
        static String join(String separator, char... array)
        返回包含由分离器分离所提供的char值字符串。

        13
        static int lastIndexOf(char[] array, char target)
        返回target 在数组中最后一个出现的索引值。

        14
        static Comparator<char[]> lexicographicalComparator()
        返回一个比较器，它比较两个字符数组字典顺序。

        15
        static char max(char... array)
        返回在数组中的最大值。

        16
        static char min(char... array)
        返回出现在数组最小值。

        17
        static char saturatedCast(long value)
        返回值char最近的值。

        18
        static char[] toArray(Collection<Character> collection)
        复制字符实例的集合到原始char值的新数组。

        19
        static byte[] toByteArray(char value)
        返回在2元素的字节数组值大端表示;相当于 ByteBuffer.allocate(2).putChar(value).array().

         */
    }

    //原生 Double 工具类
    @Test
    public void testDoubles() {
        /*
        1
        static List<Double> asList(double... backingArray)
        返回由指定数组支持的固定大小的列表，类似 Arrays.asList(Object[]).

        2
        static int compare(double a, double b)
        比较两个指定的double值。

        3
        static double[] concat(double[]... arrays)
        每个数组提供组合成一个单一的数组，则返回值。

        4
        static boolean contains(double[] array, double target)
        返回true，如果target是否存在在任何地方数组元素。

        5
        static double[] ensureCapacity(double[] array, int minLength, int padding)
        返回一个包含相同的值数组的数组，但保证是一个规定的最小长度。

        6
        static int hashCode(double value)
        返回哈希码的值;等于调用 ((Double) value).hashCode() 的结果.

        7
        static int indexOf(double[] array, double target)
        返回目标数组的首次出现的索引值。

        8
        static int indexOf(double[] array, double[] target)
        返回指定目标的第一个匹配的起始位置数组内，或-1，如果不存在。

        9
        static boolean isFinite(double value)
        返回true，如果值代表一个实数。

        10
        static String join(String separator, double... array)
        返回包含所提供的double 值的字符串，所指定的转换为字符串 Double.toString(double), 及相隔分离。

        11
        static int lastIndexOf(double[] array, double target)
        返回target 在数组中最后一个出现的索引值。

        12
        static Comparator<double[]> lexicographicalComparator()
        返回一个比较，比较两个double阵列字典顺序。

        13
        static double max(double... array)
        返回存在于数组的最大值，使用比较为相同的规则 Math.max(double, double).

        14
        static double min(double... array)
        返回存在于数组的最小值，使用比较为相同的规则 Math.min(double, double).

        15
        static Converter<String,Double> stringConverter()
        返回字符串转换之间和double采用序列化器对象 Double.valueOf(java.lang.String) and Double.toString().

        16
        static double[] toArray(Collection<? extends Number> collection)
        返回一个包含集合中的每个值的数组，转换为double值的方式Number.doubleValue().

        17
        static Double tryParse(String string)
        解析指定的字符串作为一个双精度浮点值。
         */
    }

    //原生 FLoat 工具类
    @Test
    public void testFLoats() {
        /*
        1
        static List<Float> asList(float... backingArray)
        返回由指定数组支持的固定大小的列表，类似 Arrays.asList(Object[]).

        2
        static int compare(float a, float b)
        通过比较两个指定的浮点值 Float.compare(float, float).

        3
        static float[] concat(float[]... arrays)
        每个数组提供组合成一个单一的数组，则返回值。

        4
        static boolean contains(float[] array, float target)
        返回true，如果target是否存在在任何地方数组元素。

        5
        static float[] ensureCapacity(float[] array, int minLength, int padding)
        返回一个包含相同的值数组的数组，但保证是一个规定的最小长度。

        6
        static int hashCode(float value)
        返回哈希码的值;等于调用的结果 ((Float) value).hashCode().

        7
        static int indexOf(float[] array, float target)
        返回目标数组的首次出现的索引值。

        8
        static int indexOf(float[] array, float[] target)
        返回指定目标的第一个匹配的起始位置数组内，或-1，如果不存在。

        9
        static boolean isFinite(float value)
        返回true，如果值代表一个实数。

        10
        static String join(String separator, float... array)
        返回包含所提供的浮点值，所指定的Float.toString(float)，并通过分离器分离转换为字符串的字符串。

        11
        static int lastIndexOf(float[] array, float target)
        返回target 在数组中最后一个出场的索引值。

        12
        static Comparator<float[]> lexicographicalComparator()
        返回一个比较，比较两个浮点阵列字典顺序。

        13
        static float max(float... array)
        返回存在于数组的最大值，使用比较为相同的规则 Math.min(float, float).

        14
        static float min(float... array)
        返回存在于数组的最小值，使用比较为相同的规则 Math.min(float, float).

        15
        static Converter<String,Float> stringConverter()
        返回使用字符串和浮点数之间的一个转换器序列化对象 Float.valueOf(java.lang.String) 和 Float.toString().

        16
        static float[] toArray(Collection<? extends Number> collection)
        返回一个包含集合中的每个值的数组，转换为浮点值的方式Number.floatValue().

        17
        static Float tryParse(String string)
        解析指定的字符串作为单精度浮点值。
         */
    }

    //原生 Int 工具类
    @Test
    public void testInts() {
        /*
        1
        static List<Integer> asList(int... backingArray)
        返回由指定数组支持的固定大小的列表，类似Arrays.asList(Object[]).

        2
        static int checkedCast(long value)
        返回int值等于值，如果可能的话。

        3
        static int compare(int a, int b)
        比较两个指定的int值。

        4
        static int[] concat(int[]... arrays)
        每个阵列提供组合成一个单一的阵列，则返回值。

        5
        static boolean contains(int[] array, int target)
        返回true，如果target是否存在在任何地方数组元素。

        6
        static int[] ensureCapacity(int[] array, int minLength, int padding)
        返回一个包含相同的值数组的数组，但保证是一个规定的最小长度。

        7
        static int fromByteArray(byte[] bytes)
        返回int值，其大端表示存储在第一个4字节的字节;相当于ByteBuffer.wrap(bytes).getInt().

        8
        static int fromBytes(byte b1, byte b2, byte b3, byte b4)
        返回int值的字节表示的是给定的4个字节，在big-endian的顺序;相当于 Ints.fromByteArray(new byte[] {b1, b2, b3, b4}).

        9
        static int hashCode(int value)
        返回值的哈希码; 等于调用 ((Integer) value).hashCode() 的结果

        10
        static int indexOf(int[] array, int target)
        返回值目标数组的第一次亮相的索引。

        11
        static int indexOf(int[] array, int[] target)
        返回指定目标的第一个匹配的起始位置数组内，或-1，如果不存在。

        12
        static String join(String separator, int... array)
        返回包含由分离器分离所提供的整型值的字符串。

        13
        static int lastIndexOf(int[] array, int target)
        返回target 在数组中最后一个出场的索引值。

        14
        static Comparator<int[]> lexicographicalComparator()
        返回一个比较，比较两个int数组字典顺序。

        15
        static int max(int... array)
        返回出现在数组中的最大值。

        16
        static int min(int... array)
        返回最小值出现在数组。

        17
        static int saturatedCast(long value)
        返回最接近的int值。

        18
        static Converter<String,Integer> stringConverter()
        返回使用字符串和整数之间的一个转换器序列化对象 Integer.decode(java.lang.String) 和 Integer.toString().

        19
        static int[] toArray(Collection<? extends Number> collection)
        返回包含集合的每个值的数组，转换为int值的方式Number.intValue().

        20
        static byte[] toByteArray(int value)
        返回一个4元素的字节数组值大端表示;相当于 ByteBuffer.allocate(4).putInt(value).array().

        21
        static Integer tryParse(String string)
        解析指定的字符串作为符号十进制整数。
         */
    }

    //原生 Long 工具类
    @Test
    public void testLongs() {
        /*
        1
        static List<Long> asList(long... backingArray)
        返回由指定数组支持的固定大小的列表，类似Arrays.asList(Object[]).

        2
        static int compare(long a, long b)
        比较两个指定数的long值。

        3
        static long[] concat(long[]... arrays)
        每个数组提供组合成一个单一的数组，则返回值。

        4
        static boolean contains(long[] array, long target)
        返回true，如果target是否存在在任何地方数组元素。

        5
        static long[] ensureCapacity(long[] array, int minLength, int padding)
        返回一个包含相同的值数组的数组，但保证是一个规定的最小长度。

        6
        static long fromByteArray(byte[] bytes)
        返回long值，其大端表示存储在头8个字节的字节;相当于ByteBuffer.wrap(bytes).getLong().

        7
        static long fromBytes(byte b1, byte b2, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8)
        返回long值，字节表示的是给定的8个字节，在big-endian的顺序;相当于 Longs.fromByteArray(new byte[] {b1, b2, b3, b4, b5, b6, b7, b8}).

        8
        static int hashCode(long value)
        返回哈希码的值;等于调用 ((Long) value).hashCode() 的结果

        9
        static int indexOf(long[] array, long target)
        返回目标数组的首次出现的索引值。

        10
        static int indexOf(long[] array, long[] target)
        返回指定目标的第一个匹配的起始位置数组内，或-1，如果不存在。

        11
        static String join(String separator, long... array)
        返回包含由分离器分离所提供long 的字符串值。

        12
        static int lastIndexOf(long[] array, long target)
        返回target 在数组中最后一个出场的索引值。

        13
        static Comparator<long[]> lexicographicalComparator()
        返回一个比较，比较两个long数组字典顺序。

        14
        static long max(long... array)
        返回出现在数组中的最大值。

        15
        static long min(long... array)
        返回最小值出现在数组。

        16
        static Converter<String,Long> stringConverter()
        返回使用字符串和长整型之间的转换可序列化器对象Long.decode(java.lang.String) 和 Long.toString().

        17
        static long[] toArray(Collection<? extends Number> collection)
        返回包含集合的每个值的数组，转换为一个long值的方式Number.longValue().

        18
        static byte[] toByteArray(long value)
        返回字节数组值大端在8元素的表示;相当于 ByteBuffer.allocate(8).putLong(value).array().

        19
        static Long tryParse(String string)
        Parses the specified string as a signed decimal long value.
         */
    }

    //原生 Short 工具类
    @Test
    public void testShorts() {
        /*
        1
        static List<Short> asList(short... backingArray)
        返回由指定数组支持的固定大小的列表，类似 Arrays.asList(Object[]).

        2
        static short checkedCast(long value)
        返回 short 值，该值等于value，如果可能的话。

        3
        static int compare(short a, short b)
        比较两个指定的short值。

        4
        static short[] concat(short[]... arrays)
        每个阵列提供组合成一个单一的阵列，则返回其值。

        5
        static boolean contains(short[] array, short target)
        返回true，如果目标是否存在在任何地方数组元素。

        6
        static short[] ensureCapacity(short[] array, int minLength, int padding)
        返回一个包含相同的值数组的数组，但保证是一个规定的最小长度。

        7
        static short fromByteArray(byte[] bytes)
        返回短值，其大端表示被存储在最前2字节的字节;相当于 ByteBuffer.wrap(bytes).getShort().

        8
        static short fromBytes(byte b1, byte b2)
        返回short值的字节表示的是给定2个字节，以 big-endian 顺序; 相当于 Shorts.fromByteArray(new byte[] {b1, b2}).

        9
        static int hashCode(short value)
        返回值的哈希码;等于调用的结果 ((Short) value).hashCode().

        10
        static int indexOf(short[] array, short target)
        返回值目标数组的首次出现的索引。

        11
        static int indexOf(short[] array, short[] target)
        返回指定目标的第一个匹配的起始位置数组或-1，如果不存在这样的发生。

        12
        static String join(String separator, short... array)
        返回包含由分离器分离所提供的短值的字符串。

        13
        static int lastIndexOf(short[] array, short target)
        返回目标在数组中最后一个出现的索引的值。

        14
        static Comparator<short[]> lexicographicalComparator()
        返回一个比较，比较两个 short 阵列字典顺序。

        15
        static short max(short... array)
        返回出现在数组中的最大值。

        16
        static short min(short... array)
        返回出现在数组的最小值。

        17
        static short saturatedCast(long value)
        返回short最接近int的值。

        18
        static Converter<String,Short> stringConverter()
        返回使用字符串和shorts之间的一个转换器序列化对象 Short.decode(java.lang.String) and Short.toString().

        19
        static short[] toArray(Collection<? extends Number> collection)
        返回包含集合的每个值的数组，转换为 short 值的方式Number.shortValue().

        20
        static byte[] toByteArray(short value)
        返回在2元素的字节数组值大尾数法表示;相当于 ByteBuffer.allocate(2).putShort(value).array().
         */
    }

    //不变数组工具类
    @Test
    public void testImmutableXXX() {
        //不变整形数组。里面的具体方法，点进去看就那么几个，这里不举例了
        ImmutableIntArray.copyOf(Lists.newArrayList(1, 3, 4));

        //不变长整形数组。里面的具体方法，点进去看就那么几个，这里不举例了
        ImmutableLongArray.copyOf(Lists.newArrayList(1L, 2L));

        //不变双精度形数组。里面的具体方法，点进去看就那么几个，这里不举例了
        ImmutableDoubleArray.copyOf(Lists.newArrayList(1.1, 2.2));
    }

    //有符号 Byte工 具类
    @Test
    public void testSignedBytes() {
        //判断参数值是否超过byte范围，没有则返回原值，超过则返回边界值
        SignedBytes.saturatedCast(12);

        //其他的还有几个方法，点进去就那么几个，这里不举例了
    }

    //无符号工具类
    @Test
    public void testUnsignedXXX() {
        //Bytes工具类没有定义任何区分有符号和无符号字节的方法，而是把它们都放到了SignedBytes和UnsignedBytes工具类中，因为字节类型的符号性比起其它类型要略微含糊一些。
        //int和long的无符号形式方法在UnsignedInts和UnsignedLongs类中，但由于这两个类型的大多数用法都是有符号的，Ints和Longs类按照有符号形式处理方法的输入参数。
        //此外，Guava为int和long的无符号形式提供了包装类，即UnsignedInteger和UnsignedLong，以帮助你使用类型系统，以极小的性能消耗对有符号和无符号值进行强制转换。


        //总共以下五种无符号的工具类，里面具体的方法，有兴趣的点进去看看
        UnsignedBytes.compare((byte)1, (byte)2);

        UnsignedInteger.valueOf(1);
        UnsignedLong.valueOf(1);

        UnsignedInts.compare(1, 2);
        UnsignedLongs.compare(1, 2);
    }
}
