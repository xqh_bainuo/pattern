package org.xqh.study.google.guava.reflect;

/**
 * com.guava.test.reflect.AddServiceImpl
 */
public class AddServiceImpl implements AddService {

    @Override
    public int add(int a, int b) {
        return a + b;
    }
}
