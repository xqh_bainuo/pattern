package org.xqh.study.google.guava.reflect;

/**
 * com.guava.test.reflect.AddService
 */
public interface AddService {

    int add(int a, int b);

}
