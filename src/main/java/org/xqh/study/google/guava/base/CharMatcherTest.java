package org.xqh.study.google.guava.base;

import com.google.common.base.CharMatcher;
import org.junit.Test;

/**
 * 提供了各种方法来处理各种JAVA char类型值
 */
public class CharMatcherTest {

    @Test
    public void test() {
        //入参字符串是否全部是目标字符
        System.out.println(CharMatcher.is('a').matchesAllOf("aaa"));//true
        //入参字符串是否至少存在一个目标字符
        System.out.println(CharMatcher.is('a').matchesAnyOf("aba"));//true
        //入参字符串是否全部不存在目标字符
        System.out.println(CharMatcher.is('a').matchesNoneOf("aba"));//false

        //入参字符串出现目标字符的次数
        System.out.println(CharMatcher.is('a').countIn("aaa")); // 3
        //入参字符串首次出现目标字符的索引位置
        System.out.println(CharMatcher.is('a').indexIn("java")); // 1
        //入参字符串最后出现目标字符的索引位置
        System.out.println(CharMatcher.is('a').lastIndexIn("java")); // 3

        //将入参字符串中匹配到目标字符的保留
        System.out.println(CharMatcher.is('a').retainFrom("bazaar")); // "aaa"
        //将入参字符串中匹配到目标字符的删除
        System.out.println(CharMatcher.is('a').removeFrom("bazaar")); // "bzr"
        //将入参字符串中匹配到0--9数字的保留
        System.out.println(CharMatcher.inRange('0', '9').retainFrom("abc12d34ef")); // 1234
        // 当然也可以用Digit类，不过最近的版本已经被标记为Deprecated
        // 区别在于Digit类处理了字符0到9的各种unicode码，不过大多数情况还是处理ASCII数字，所以建议使用inRange
        System.out.println(CharMatcher.digit().retainFrom("abc12d34ef")); // 1234

        //将入参字符串中匹配到0--9数字的或者‘d’字符的保留
        System.out.println(CharMatcher.inRange('0','9').or(CharMatcher.is('d')).retainFrom("abc12d34ef")); // 12d34
        //将入参字符串中匹配到0--9数字并且在1--2范围内的保留
        System.out.println(CharMatcher.inRange('0','9').and(CharMatcher.inRange('1', '2')).retainFrom("abc12d34ef")); //12

    }
}
