package org.xqh.study.google.guava.cache;

import com.google.common.base.Ticker;
import com.google.common.cache.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 缓存
 * 1、simple(); //最简单模式
 * 2、maximumSize(); //设置最大存储
 * 3、expireAfterWrite(); //设置过期时间（写入之后一定时间后删除）
 * 4、expireAfterAccess(); //设置过期时间（超过一定时间没有被访问则删除）
 * 5、weakValues(); //弱引用
 * 6、invalidate(); //显示清除
 * 7、removalListener(); //移除监听器
 * 8、autoLoad(); //自动加载
 * 9、recordStats(); //统计信息
 * 10、loadingCache(); //LoadingCache
 * 11、allParamDemo
 */
public class CacheTest {

    /**
     * 最简单模式。
     * 可以通过CacheBuilder类构建一个缓存对象，CacheBuilder类采用builder设计模式，它的每个方法都返回CacheBuilder本身，直到build方法被调用
     */
    @Test
    public void simple() {
        Cache<String, String> cache = CacheBuilder.newBuilder().build();
        cache.put("key", "value");
        System.out.println(cache.getIfPresent("key"));
    }

    /**
     * 设置最大存储。
     * Guava Cache可以在构建缓存对象时指定缓存所能够存储的最大记录数量。
     * 当Cache中的记录数量达到最大值后再调用put方法向其中添加对象，Guava会先从当前缓存的对象记录中选择一条删除掉，腾出空间后再将新的对象存储到Cache中。
     */
    @Test
    public void maximumSize() {
        Cache<String, String> cache = CacheBuilder.newBuilder()
                .maximumSize(2)
                .build();

        cache.put("key1","value1");
        cache.put("key2","value2");
        cache.put("key3","value3");

        System.out.println("第一个值：" + cache.getIfPresent("key1"));
        System.out.println("第二个值：" + cache.getIfPresent("key2"));
        System.out.println("第三个值：" + cache.getIfPresent("key3"));
    }

    /**
     * 设置过期时间（写入之后一定时间后删除）。
     * 在构建Cache对象时，可以通过CacheBuilder类的expireAfterWrite方法为缓存中的对象指定过期时间，
     * 过期的对象将会被缓存自动删除。其中，expireAfterWrite方法指定对象被写入到缓存后多久过期。
     */
    @Test
    public void expireAfterWrite() throws InterruptedException {
        Cache<String, String> cache = CacheBuilder.newBuilder()
                .maximumSize(2)
                .expireAfterWrite(3, TimeUnit.SECONDS)
                .build();

        cache.put("key1","value1");

        int time = 1;
        while(true) {
            System.out.println("第" + time++ + "次取到key1的值为：" + cache.getIfPresent("key1"));
            Thread.sleep(1000);
            if(time > 10) {
                break;
            }
        }
    }

    /**
     * 设置过期时间（超过一定时间没有被访问则删除）。
     * 在构建Cache对象时，可以通过CacheBuilder类的expireAfterAccess方法为缓存中的对象指定过期时间，
     * 过期的对象将会被缓存自动删除。expireAfterAccess指定对象多久没有被访问后过期。
     */
    @Test
    public void expireAfterAccess() throws InterruptedException {
        Cache<String,String> cache = CacheBuilder.newBuilder()
                .maximumSize(2)
                .expireAfterAccess(3,TimeUnit.SECONDS)
                .build();
        cache.put("key1","value1");
        int time = 1;
        while(true) {
            Thread.sleep(1000);
            System.out.println("睡眠" + time++ + "秒后取到key1的值为：" + cache.getIfPresent("key1"));
            if(time > 10) {
                break;
            }
        }

        Thread.sleep(3000);
        System.out.println("睡眠" + (time+2) + "秒后取到key1的值为：" + cache.getIfPresent("key1"));
    }

    /**
     * 弱引用。
     * 通过weakKeys和weakValues方法指定Cache只保存对缓存记录key和value的弱引用。
     * 这样当没有其他强引用指向key和value时，key和value对象就会被垃圾回收器回收
     */
    @Test
    public void weakValues() throws InterruptedException {
        Cache<String, Object> cache = CacheBuilder.newBuilder()
                .maximumSize(2)
                .weakValues()
                .build();
        Object value = new Object();
        cache.put("key", value);

        value = null;//原对象不再有强引用

        System.gc();
        TimeUnit.SECONDS.sleep(1L);

        System.out.println(cache.getIfPresent("key"));
        System.out.println(cache.size());

    }

    /**
     * 显示清除。
     */
    @Test
    public void invalidate() {
        Cache<String,String> cache = CacheBuilder.newBuilder().build();
        Object value = new Object();
        cache.put("key1","value1");
        cache.put("key2","value2");
        cache.put("key3","value3");

        List<String> list = new ArrayList<>();
        list.add("key1");
        list.add("key2");

        //批量清除list中全部key对应的记录
        cache.invalidateAll(list);

        System.out.println(cache.getIfPresent("key1"));
        System.out.println(cache.getIfPresent("key2"));
        System.out.println(cache.getIfPresent("key3"));
    }

    /**
     * 移除监听器。
     * removalListener方法为Cache指定了一个移除监听器，这样当有记录从Cache中被删除时，监听器listener就会感知到这个事件。
     */
    @Test
    public void removalListener() {
        RemovalListener<String, String> listener = new RemovalListener<String, String>() {
            @Override
            public void onRemoval(RemovalNotification<String, String> notification) {
                System.out.println("[" + notification.getKey() + ":" + notification.getValue() + "] is removed!");
            }
        };

        Cache<String,String> cache = CacheBuilder.newBuilder()
                .maximumSize(3)
                .removalListener(listener)
                .build();

        cache.put("key1","value1");
        cache.put("key2","value2");
        cache.put("key3","value3");
        cache.put("key4","value4");

        cache.invalidate("key2");
    }

    /**
     * 自动加载
     * Cache的get方法有两个参数，第一个参数是要从Cache中获取记录的key，第二个记录是一个Callable对象。
     * 当缓存中已经存在key对应的记录时，get方法直接返回key对应的记录。
     * 如果缓存中不包含key对应的记录，Guava会启动一个线程执行Callable对象中的call方法，call方法的返回值会作为key对应的值被存储到缓存中，并且被get方法返回。
     */
    @Test
    public void autoLoad() throws InterruptedException {
        Cache<String,String> cache = CacheBuilder.newBuilder()
                .maximumSize(3)
                .build();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("thread1");
                try {
                    String value = cache.get("key", new Callable<String>() {
                        @Override
                        public String call() throws Exception {
                            System.out.println("load1"); //加载数据线程执行标志
                            Thread.sleep(1000); //模拟加载时间
                            return "111auto load by Callable";
                        }
                    });
                    System.out.println("thread1 " + value);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("thread2");
                try {
                    String value = cache.get("key", new Callable<String>() {
                        @Override
                        public String call() throws Exception {
                            System.out.println("load2"); //加载数据线程执行标志
                            Thread.sleep(1000); //模拟加载时间
                            return "222auto load by Callable";
                        }
                    });
                    System.out.println("thread2 " + value);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        //防止主线程提前终止
        Thread.sleep(5000);
    }

    /**
     * 统计信息。
     * 可以对Cache的命中率、加载数据时间等信息进行统计。
     * 在构建Cache对象时，可以通过CacheBuilder的recordStats方法开启统计信息的开关。
     * 开关开启后Cache会自动对缓存的各种操作进行统计，调用Cache的stats方法可以查看统计后的信息。
     */
    @Test
    public void recordStats() {
        Cache<String,String> cache = CacheBuilder.newBuilder()
                .maximumSize(3)
                .recordStats() //开启统计信息开关
                .build();

        cache.put("key1","value1");
        cache.put("key2","value2");
        cache.put("key3","value3");
        cache.put("key4","value4");

        cache.getIfPresent("key1");
        cache.getIfPresent("key2");
        cache.getIfPresent("key3");
        cache.getIfPresent("key4");
        cache.getIfPresent("key5");
        cache.getIfPresent("key6");

        //获取统计信息
        System.out.println(cache.stats());

        /*
        缓存统计信息：
        requestCount：返回Cache查找方法返回缓存或未缓存值的次数。这被定义为hitCount + missCount。
        hitCount：返回Cache查找方法返回缓存值的次数。
        hitRate：返回已命中的缓存请求的比率。这被定义为hitCount / requestCount
        missCount：返回Cache查找方法返回未缓存（新加载）值的次数
        missRate：返回未命中的缓存请求的比率。这被定义为missCount / requestCount
        loadCount：返回Cache查找方法尝试加载新值的总次数。这包括成功的加载操作以及抛出异常的操作
        loadSuccessCount：返回Cache查找方法成功加载新值的次数。
        loadExceptionCount：返回Cache查找方法在加载新值时抛出异常的次数
        totalLoadTime：返回缓存加载新值所花费的纳秒总数
        averageLoadPenalty：返回加载新值所花费的平均时间
        evictionCount：返回条目被驱逐的次数。
        */
    }

    /**
     * LoadingCache。
     * LoadingCache是Cache的子接口，相比较于Cache，当从LoadingCache中读取一个指定key的记录时，
     * 如果该记录不存在，则LoadingCache可以自动执行加载数据到缓存的操作。
     *
     * 当调用LoadingCache的get方法时，如果缓存不存在对应key的记录，
     * 则CacheLoader中的load方法会被自动调用从外存加载数据，load方法的返回值会作为key对应的value存储到LoadingCache中，
     * 并从get方法返回。
     */
    @Test
    public void loadingCache() {
        CacheLoader<String, String> loader = new CacheLoader<String, String>() {
            @Override
            public String load(String key) throws Exception {
                Thread.sleep(1000); //休眠1s，模拟加载数据
                System.out.println(key + " is loaded from a cacheLoader!");
                return key + "'s value";
            }
        };

        LoadingCache<String,String> loadingCache = CacheBuilder.newBuilder()
                .maximumSize(3)
                .build(loader);//在构建时指定自动加载器

        try{
            System.out.println(loadingCache.get("key1"));
            System.out.println(loadingCache.get("key2"));
            System.out.println(loadingCache.get("key3"));

            System.out.println("======================");

            System.out.println(loadingCache.get("key1"));
            System.out.println(loadingCache.get("key2"));
            System.out.println(loadingCache.get("key3"));
        }catch (ExecutionException e) {

        }
    }

    //Cache的所有参数配置说明
    @Test
    public void testAllParam() {
        CacheBuilder.newBuilder()
                //是在指定时间内没有被创建/覆盖，则指定时间过后，再次访问时，会去刷新该缓存，在新值没有到来之前，始终返回旧值
                .refreshAfterWrite(2, TimeUnit.SECONDS)
                //当key被write(put或者reload)操作之后，空闲多久后被删除。下次取的时候从loading中取
                .expireAfterWrite(2, TimeUnit.SECONDS)
                //当key在被“read/write”操作之后，空闲多久后被删除。下次取的时候从loading中取
                .expireAfterAccess(2, TimeUnit.SECONDS)
                //指定cache中允许的key的个数。如果key的个数即将超过阀值(边际)，将会根据LRU算法，剔除那些不常用的K-V
                .maximumSize(5)
                //打开统计信息
                .recordStats()
                //使用弱引用存储键。当键没有其它（强或软）引用时，缓存项可以被垃圾回收。
                .weakKeys()
                //使用弱引用存储值。当值没有其它（强或软）引用时，缓存项可以被垃圾回收。
                .weakValues()
                //使用软引用存储值。软引用只有在响应内存需要时，才按照全局最近最少使用的顺序回收。
                .softValues()
                //允许的并发数
                .concurrencyLevel(10)
                //初始缓存大小
                .initialCapacity(1)
                //如果你的每个Key-Value有不同的“weights”--比如每个K-V占用不同的内存量，我们可以用“weight”来描述这一指标。
                //当cache中put一个key时，都会计算它的weight值，并累加，当达到maximumWeight阀值时，会触发剔除操作。
                .maximumWeight(55)
                .weigher(new Weigher<String, String>() {
                    @Override
                    public int weigh(String key, String value) {
                        return 0;
                    }
                })
                //缓存移除监听器
                .removalListener(new RemovalListener<String, String>() {
                    @Override
                    public void onRemoval(RemovalNotification<String, String> notification) {
                        System.out.println("[" + notification.getKey() + ":" + notification.getValue() + "] is removed!");
                    }
                })
                //对定时回收进行测试时,不一定非得花费两秒钟去测试两秒的过期。你可以使用 Ticker 接口和 CacheBuilder.ticker(Ticker)方法在缓存中自定义一个时间源,而不是非得用系统时钟。
                .ticker(new Ticker() {
                    @Override
                    public long read() {
                        return 0;
                    }
                })
                //build()无参时创建Cache对象，有参时创建LoadingCache对象
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        Thread.sleep(1000); //休眠1s，模拟加载数据
                        System.out.println(key + " is loaded from a cacheLoader!");
                        return key + "'s value";
                    }
                });
    }

}
