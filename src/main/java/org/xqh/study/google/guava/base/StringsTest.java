package org.xqh.study.google.guava.base;

import com.google.common.base.Strings;
import org.junit.Test;

/**
 * 字符串工具类
 */
public class StringsTest {

    @Test
    public void test() {
        String name = "test";

        //如果输入的字符串对象是null或者输入的字符串内容为空，那么就返回true。
        System.out.println("isNullOrEmpty:" + Strings.isNullOrEmpty(name)); //false

        //如果字符串对象为空，则返回空字符串""，否则就返回原字符串。
        System.out.println("nullToEmpty : " + Strings.nullToEmpty(name)); //test

        //与上面的emptyToNull()方法相反了，如果输入的是空字符串，那么就返回null，否则返回原字符串。
        System.out.println("emptyToNull : " + Strings.emptyToNull(name)); //test

        //查询两个字符串的最长公共前缀。如果没有公共前缀的话就返回空字符串。
        System.out.println("commonPrefix : " + Strings.commonPrefix("aa", "abc")); //a

        //返回两个字符串的最长公共后缀。如果没有公共后缀的话就返回空字符串。
        System.out.println("commonSuffix : " + Strings.commonSuffix("aa", "cda")); //a

        //返回给定的模板字符串，每次出现的“％s”都替换为args中对应的参数值; 或者，如果占位符和参数计数不匹配，则返回该字符串形式.
        System.out.println("lenientFormat : " + Strings.lenientFormat("aa%scc%see%s", "bb", "dd")); //aabbccddee%s

        //将输入的字符串重复拼接count次
        System.out.println("repeat : " + Strings.repeat("aa", 5)); //aaaaaaaaaa

        //结果返回一个长度至少是minLength的字符串，如果string长度不够就在它前面添加若干个padChar，以使结果字符串长度为minLength。
        System.out.println("padStart : " + Strings.padStart("aa", 5, 'b')); //bbbaa

        //与上面的padStart方法相反了。如果长度不够，在string后补padChar。
        System.out.println("padEnd : " + Strings.padEnd("aa", 5, 'b')); //aabbb
    }
}
