package org.xqh.study.google.guava.base;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import org.junit.Test;

/**
 * 字符串处理的连接器
 */
public class JoinerTest {

    @Test
    public void test() {
        //以分号分割并且跳过 null 值
        System.out.println(Joiner.on(";").skipNulls().join("a", null, "b", "c")); //a;b;c
        //以逗号分割并且跳过 null 值
        System.out.println(Joiner.on(",").skipNulls().join(Lists.newArrayList("a", null, "b", "c"))); //a,b,c

        //将 null 值替换为指定的字符串
        System.out.println(Joiner.on(";").useForNull("d").join("a", null, "b", "c")); //a;d;b;c
        //将 null 值替换为指定的字符串
        System.out.println(Joiner.on(",").useForNull("d").join(Lists.newArrayList("a", null, "b", "c"))); //a,d,b,c

        //将map拼接格式化输出
        System.out.println(Joiner.on(";").withKeyValueSeparator("=").join(ImmutableMap.of("k1", "v1", "k2", "v2"))); //k1=v1;k2=v2

    }
}
