package org.xqh.study.google.guava.eventbus;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.concurrent.Executors;

/**
 * 事件总线
 * 1、EventBus 同步模式
 * 2、AysncEventBus  异步模式
 * 3、DeadEvent 死亡事件
 */
public class EventbusTest {

    // EventBus是Guava的事件处理机制，是设计模式中的观察者模式（生产/消费者编程模型）的优雅实现，
    // 在应用中可以处理一些异步任务。
    // 对于事件监听和发布订阅模式，EventBus是一个非常优雅和简单解决方案，我们不用创建复杂的类和接口层次结构。

    class MyObserver {
        //@Subscribe的方法必须是Public的
        //@Subscribe的方法必须是只有一个参数的
        //@Subscribe的方法根据参数类型决定哪个能够接收到消息
        //需要防止并发调用时，可在@Subscribe注解下再加上@AllowConcurrentEvents

        @Subscribe
        public void onEvent(String message) {
            System.out.println(LocalDateTime.now() + " String观察者收到消息：" + message);
            try{
                Thread.sleep(3000);
            }catch (InterruptedException e) {

            }
            System.out.println(LocalDateTime.now() + " String观察者执行完毕");
        }

        @Subscribe
        public void onEvent(Integer message) {
            System.out.println(LocalDateTime.now() + " Integer观察者收到消息：" + message);
            try{
                Thread.sleep(3000);
            }catch (InterruptedException e) {

            }
            System.out.println(LocalDateTime.now() + " Integer观察者执行完毕");
        }

        @Subscribe
        public void onEvent(Object message) {
            System.out.println(LocalDateTime.now() + " Object观察者收到消息：" + message);
            try{
                Thread.sleep(3000);
            }catch (InterruptedException e) {

            }
            System.out.println(LocalDateTime.now() + " Object观察者执行完毕");
        }

        @Subscribe
        public void onEvent(DeadEvent event) {
            System.out.printf("从类=%s，收到死亡事件，事件内容=%s%n", event.getSource().getClass(), event.getEvent());
        }
    }

    //同步模式
    @Test
    public void testEventBus() {
        EventBus eventBus = new EventBus();
        eventBus.register(new MyObserver());//注册事件
        eventBus.post(3.9);//触发事件处理
        System.out.println(LocalDateTime.now() + " 发布者执行完毕");
    }

    //异步模式
    @Test
    public void testAysncEventBus() {
        AsyncEventBus eventBus = new AsyncEventBus(Executors.newFixedThreadPool(3));
        eventBus.register(new MyObserver());//注册事件
        eventBus.post("aaaa");//触发事件处理
        System.out.println(LocalDateTime.now() + " 发布者执行完毕");

        try{
            Thread.sleep(5000);
        }catch (InterruptedException e) {
            
        }
    }


}
