package org.xqh.study.google.guava.base;

import com.google.common.base.Preconditions;
import org.junit.Test;

/**
 * 优雅的参数校验
 */
public class PreconditionsTest {

    @Test
    public void test() {
        int age = 10;
        String name = null;

        //检查boolean是否为真。
        // 失败时抛出的异常类型: IllegalArgumentException
        Preconditions.checkArgument(age == 10);
        Preconditions.checkArgument(age == 10, "error");

        //检查value不为null，直接返回value。
        // 失败时抛出的异常类型：NullPointerException
        Preconditions.checkNotNull(name);
        Preconditions.checkNotNull(name, "error");

        //检查对象的一些状态。
        // 失败时抛出的异常类型：IllegalStateException
        Preconditions.checkState(age == 10);
        Preconditions.checkState(age == 10, "error");

        //检查index是否为在一个长度为size的list，string或array合法的范围，index的范围区间是[0, size)。
        // 失败时抛出的异常类型：IndexOutOfBoundsException
        Preconditions.checkElementIndex(1, 10);
        Preconditions.checkElementIndex(1, 10, "error");

        //检查位置index是否为在合法的范围。 index的范围区间是[0， size]。
        //失败时抛出的异常类型：IndexOutOfBoundsException
        Preconditions.checkPositionIndex(1, 10);
        Preconditions.checkPositionIndex(1, 10, "error");

        //检查[start, end)是一个长度为size的list， string或array合法的范围子集。0<=start<=end<=size。
        //失败时抛出的异常类型：IndexOutOfBoundsException
        Preconditions.checkPositionIndexes(1,3, 10);
    }
}
