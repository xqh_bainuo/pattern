package org.xqh.study.google.guava.io;

import org.junit.Test;

/**
 * I/O
 * 1、ByteStreams
 * 2、CharStreams
 * 3、Resources
 * 4、Closeables
 * 5、Flushables
 * 6、Files
 * 7、MoreFiles
 */
public class IOTest {


    //关于IO操作Guava给我们提供了很多工具类，大大提高了我们开发效率。
    // 涉及到的工具类主要有：
    // ByteStreams：ByteStreams里面提供用于处理字节数组和I / O流的实用程序方法。
    // CharStreams：提供用于处理字符流的实用方法。
    // Resources：提供用于处理类路径中的资源的实用程序方法。
    // Closeables：Closeables对象的实用方法。让我们调用一些close方法更加的方便一点。
    // Flushables： Flushables对象的一些使用方法，让我们调用flush方法更加的方便一点。
    // Files：Files类提供使用文件相关的一些实用程序方法。
    // MoreFiles：MoreFiles类是Files类的一个补充类,MoreFiles里面的方法也是操作文件相关的方法,不过MoreFiles针对的是Path类。
    @Test
    public void testIO() {
        //详见博客 https://www.jianshu.com/p/047e3135c398
    }
}
