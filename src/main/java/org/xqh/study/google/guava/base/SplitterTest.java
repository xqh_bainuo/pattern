package org.xqh.study.google.guava.base;

import com.google.common.base.Splitter;
import org.junit.Test;

import java.util.regex.Pattern;

/**
 * 字符串处理的分割器
 */
public class SplitterTest {

    @Test
    public void test() {
        //用指定字符切分字符串,并转换成list
        Splitter.on("|").split("aa|bb").forEach(System.out::println); //aa bb
        Splitter.on("|").splitToList("aa|bb").forEach(System.out::println); //aa bb
        Splitter.on("|").splitToStream("aa|bb").forEach(System.out::println); //aa bb

        //忽略掉空的字符串或者多余的分割符
        Splitter.on("|").omitEmptyStrings().splitToList("cc|dd|||").forEach(System.out::println); //cc dd

        //忽略掉字符串中的空格
        Splitter.on("|").omitEmptyStrings().trimResults().splitToList("ee | ff|||").forEach(System.out::println); //ee ff

        //固定长度分割
        Splitter.on("|").fixedLength(4).splitToList("aaaabbbbccccdddd").forEach(System.out::println); //aaaa bbbb cccc dddd

        //指定长度分割，以#来分割，分3部分成 a b #c#d#e 3部分
        Splitter.on("#").limit(3).splitToList("a#b#c#d#e#").forEach(System.out::println); //a b c#d#e#

        //将字符串分割后组装成 map
        System.out.println(Splitter.on("&").withKeyValueSeparator("=").split("id=123&name=zhangsan")); //{id=123, name=zhangsan}

        //传入字符的正则分割
        Splitter.onPattern("\\|").splitToList("hello|world").forEach(System.out::println); //hello world

        //传入pattern的正则分割
        Splitter.on(Pattern.compile("\\|")).omitEmptyStrings().trimResults().splitToList("a|b|c||").forEach(System.out::println); //a b c

        //传入pattern 转换成map
        System.out.println(Splitter.on(Pattern.compile("\\|")).omitEmptyStrings().trimResults().withKeyValueSeparator("=").split("a=b|c=d")); //{a=b, c=d}

    }
}
