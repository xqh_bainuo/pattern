package org.xqh.study.google.guava;

/**
 * @ClassName CountingBloomFilterDemo
 * @Description 带计数器的 布隆过滤器
 * @Author xuqianghui
 * @Date 2019/12/25 22:53
 * @Version 1.0
 */
public class CountingBloomFilterDemo {
//    public static void main(String[] args) {
//        CountingBloomFilter<String> cbf = new FilterBuilder(1000, 0.01)
//                .buildCountingBloomFilter();
//        cbf.add("http://www.unisound.com");
//        cbf.add("http://www.baidu.com");
//        cbf.add("http://www.qq.com");
//
//        cbf.remove("http://www.baidu.com");
//
//        System.out.println(cbf.contains("http://www.unisound.com"));
//        System.out.println(cbf.contains("http://www.baidu.com"));
//        System.out.println(cbf.contains("http://www.qq.com"));
//
//    }
}
