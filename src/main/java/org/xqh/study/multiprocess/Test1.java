package org.xqh.study.multiprocess;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @ClassName Test1
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/4/20 21:42
 * @Version 1.0
 */
public class Test1 {

    public static void main(String[] args) throws IOException {
        FileOutputStream fOut = new FileOutputStream("D:\\Test1.txt");

        fOut.close();

        System.out.println("被调用成功!");
    }
}
