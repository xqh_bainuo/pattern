package org.xqh.study.multiprocess;

import com.alibaba.fastjson.JSON;
import org.xqh.utils.ObjectUtils;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName MultiProcessTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/4/20 21:41
 * @Version 1.0
 */
public class MultiProcessTest {

    public static void main(String[] args) throws IOException, InterruptedException {
//        //创建一个进程 , 调用Test1.java文件
//        ProcessBuilder pb = new ProcessBuilder("java", "Test1");
//
//        Process p = pb.start();
//        Thread.sleep(1000);
//        p.destroy();
//        List<String> list = ReadTxtFileUtils.readTxt(new File("D:\\work\\secureCRT\\upload\\test_device.txt"));
//
//        Map<Integer, AtomicInteger> count = new HashMap<>();
//        int size = 8;
//        for(String sn : list){
//            int hash = ObjectUtils.getHashCode(sn);
//            int key = hash % size;
//            if(count.containsKey(key)){
//                count.get(key).incrementAndGet();
//            }else {
//                count.put(key, new AtomicInteger(1));
//            }
//        }
//
//        System.out.println(JSON.toJSONString(count)); //{0:1213,1:1229,2:1239,3:1263,4:1277,5:1283,6:1222,7:1274} 结果 比较平均
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
    }


}
