package org.xqh.study.elasticsearch;

import org.xqh.utils.encrypt.EncryptUtils;

import javax.swing.*;
import java.util.Random;

/**
 * @ClassName ESSearchDemo
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/5/9 15:16
 * @Version 1.0
 */
public class ESSearchDemo {

    public static class TestTmdbThread implements Runnable{

        @Override
        public void run() {

        }
    }



    private static String url = "https://api.themoviedb.org/3/configuration/languages?api_key=07a84f556b333b97c6aadfa827ebf1d1&language=zh-CN";
    public static void main(String[] args) {
        Random random = new Random();
        for(int i = 0; i < 100 ; i ++){
            System.out.println(random.nextInt(10));
        }
        System.out.println(EncryptUtils.getSha256("13537758232"));

        String url = "https://api.themoviedb.org/3/configuration/languages?api_key=07a84f556b333b97c6aadfa827ebf1d1&language=zh-CN";

        for(int i = 0; i < 100; i ++){

        }
        //1.创建QueryBuilder(即设置查询条件)这儿创建的是组合查询(也叫多条件查询),后面会介绍更多的查询方法
        /*组合查询BoolQueryBuilder
         * must(QueryBuilders)   :AND
         * mustNot(QueryBuilders):NOT
         * should:               :OR
         */
//        BoolQueryBuilder builder = QueryBuilders.boolQuery();
//        //builder下有must、should以及mustNot 相当于sql中的and、or以及not
//
//        //设置模糊搜索,博客的简诉中有学习两个字
//        builder.must(QueryBuilders.fuzzyQuery("sumary", "学习"));
//
//        //设置要查询博客的标题中含有关键字
//        builder.must(new QueryStringQueryBuilder("man").field("springdemo"));
//
//        //按照博客的评论数的排序是依次降低
//        FieldSortBuilder sort = SortBuilders.fieldSort("commentSize").order(SortOrder.DESC);
//
//        //设置分页(从第一页开始，一页显示10条)
//        //注意开始是从0开始，有点类似sql中的方法limit 的查询
//        PageRequest page = new PageRequest(0, 10);
//
//        //2.构建查询
//        NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
//        //将搜索条件设置到构建中
//        nativeSearchQueryBuilder.withQuery(builder);
//        //将分页设置到构建中
//        nativeSearchQueryBuilder.withPageable(page);
//        //将排序设置到构建中
//        nativeSearchQueryBuilder.withSort(sort);
//        //生产NativeSearchQuery
//        NativeSearchQuery query = nativeSearchQueryBuilder.build();
//
//        //3.执行方法1
//        Page<EsBlog> page = esBlogRepository.search(query);
//        //执行方法2：注意，这儿执行的时候还有个方法那就是使用elasticsearchTemplate
//        //执行方法2的时候需要加上注解
//        //@Autowired
//        //private ElasticsearchTemplate elasticsearchTemplate;
//        List<EsBlog> blogList = elasticsearchTemplate.queryForList(query, EsBlog.class);
//
//        //4.获取总条数(用于前端分页)
//        int total = (int) page.getTotalElements();
//
//        //5.获取查询到的数据内容（返回给前端）
//        List<EsBlog> content = page.getContent();
//
//        return content;
    }
}
