package org.xqh.study.jna.demosdk;

import com.sun.jna.Callback;
import com.sun.jna.Library;

/**
 * @ClassName GoLibrary
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/10 17:10
 * @Version 1.0
 */
public interface GoLibrary extends Library {
    // 注册回调函数
    void SetListener(Callback callback);

}
