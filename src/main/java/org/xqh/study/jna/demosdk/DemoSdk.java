package org.xqh.study.jna.demosdk;

import com.sun.jna.Callback;
import com.sun.jna.Native;

/**
 * @ClassName DemoSdk
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/10 17:10
 * @Version 1.0
 */
public class DemoSdk {

    public static void main(String[] args) {
        String workPath = "D:\\work\\so\\";
        GoLibrary INSTANCE = Native.load(workPath + "sdkdemo.dll", GoLibrary.class);
        // 实现回调函数
        DemoCallback callback = new DemoCallback() {
            @Override
            public void invoke(int eventId, String message) {
                System.out.println("Received from Go: " + eventId + ", msg: " + message);
            }
        };

        // 注册回调
        INSTANCE.SetListener(callback);
        System.out.println("Callback registered.");

    }

    // 定义回调接口
    public interface DemoCallback extends Callback {
        void invoke(int eventId, String message); // 回调方法
    }
}
