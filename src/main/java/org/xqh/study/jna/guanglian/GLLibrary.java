package org.xqh.study.jna.guanglian;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import org.xqh.study.jna.guanglian.GlModel.*;

/**
 * @ClassName GuangLianLibrary
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/3/22 19:04
 * @Version 1.0
 */
public interface GLLibrary extends Library {

//    int glp_configure(GlCfgParamV1Struct.ByReference struct);

    /**
     * 初始化配置 v2
     *
     * @param pointer
     * @return
     */
    int glp_configure_v2(CfgInitModel pointer);

    /**
     * 客户端启动sdk
     *
     * @param struct
     * @return
     */
    int glc_start(GlCliStartParamStruct.ByReference struct);

    /**
     * 客户端停止sdk
     */
    void glc_stop();

    int gld_start(GlDevStartParamStruct.ByReference struct);

    /**
     * 接受连接
     * @param params
     * @return
     */
    int gld_accept(GldAcceptParamStruct.ByReference params);

    void gld_stop();


    /**
     * 连接设备
     * @param struct
     * @return
     */
    int glc_connect(GlcConnectParamStruct.ByReference struct);


    /**
     * 销毁连接
     * @param remote_peerid
     * @return
     */
    int glp_destroy(String remote_peerid);


    /**
     * Note:
     * 12 当传入params->listen_port为0时，sdk内部将随机监听一个服务端口，
     * 13 并将params->listen_port值修改为新的端口号
     * 14
     * 15 20220512:
     * 16 新增rule字段，访字段为厂商定制字段，需要双方协商。
     * 17 非定制场景中，默认传0即可。
     * @param struct
     * @return
     */
    int glc_add_service_mapping(GlcSvmapParamStruct.ByReference struct);

    /**
     * 3 Note:
     * 4 params->remote_peerid 与 params->listen_port 为必须传入参数
     * 5 其中：listen_port 必须为glc_add_service_mapping中返回的listen_port
     * 6 SDK内部会根据remote_peerid，删除listen_port上的监听服务
     * @param struct
     * @return
     */
    int glc_del_service_mapping(GlcSvmapParamStruct.ByReference struct);

    //获取连接 信息
    int glp_peer_info(String remotePeerid, GlPeerInfoStruct.ByReference struct);
}

