package org.xqh.study.jna.guanglian;

import com.alibaba.fastjson.JSON;
import com.sun.jna.Native;
import org.xqh.study.jna.guanglian.GlModel.*;

/**
 * @ClassName GuangLianSDK
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/3/22 19:03
 * @Version 1.0
 */
public class GuangLianSDK {

//    public static String tunnel_host = "glmgr-gl.ugreen.cloud";
//    public static String stun_host = "stun-gl.ugreen.cloud";

    public static String tunnel_host = "glmgr-pro.ugreen.cloud";
    public static String stun_host = "stun-pro.ugreen.cloud";

//    public static String tunnel_host = "test-glmgr.ugreen.cloud";
//    public static String stun_host = "test-stun.ugreen.cloud";


//    public static String tunnel_host = "glmgr.ugreen.cloud";
//    public static String stun_host = "stun.ugreen.cloud";

//    public static String tunnel_host = "glmgr-aar.ugreen.cloud";
//    public static String stun_host = "stun-aar.ugreen.cloud";

    public static String log_path = "D:\\work\\so\\guanglian\\log\\gl-jna.log";

    public static void main(String[] args) throws InterruptedException {
        GLLibrary instance = Native.load("D:\\work\\so\\ugreen_winx64_glp2p.dll", GLLibrary.class);
//        String remotePeerId = "GL3959d00ac259481ebe50c89291e81851";
        String remotePeerId = "GL02ccdcdbb0ac418aa8553253791aea04";
        String localPeerId = "guanglian-test-xqh1122";
        //初始化配置
        initGLCfgV2(instance);

        //启动sdk
//        startClientSdk(instance, localPeerId);
        startDeviceSdk(instance, remotePeerId);

//        //连接设备
//        connectToDevice(instance, remotePeerId, "");
////
//        //添加映射
//        addServiceMapping(instance, remotePeerId, 9999, "127.0.0.1", 2000);
////
//        new Thread(()->{
//             while (true){
//                 glpPeerInfo(instance, remotePeerId);
//                 try {
//                     Thread.sleep(5000);
//                 } catch (InterruptedException e) {
//                     throw new RuntimeException(e);
//                 }
//             }
//        }).start();
//

        Thread.sleep(10000);
//        System.out.println("====销毁连接=====");
//        instance.glp_destroy(localPeerId);
//        delServiceMapping(instance, remotePeerId, 2000);

        Thread.sleep(1000000);
        stopClientSdk(instance);
    }


    public static void connectToDevice(GLLibrary instance, String remotePeerId, String accessToken) {
        GlcConnectParamStruct.ByReference struct = new GlcConnectParamStruct.ByReference();
        struct.remote_peerid = remotePeerId;
        struct.access_token = accessToken;
        struct.timeout_sec = 20;//超时时间
        ConnectedCallback connectCb = new ConnectedCallbackImpl();
        DisconnectedCallback disconnectCb = new DisconnectedCallbackImpl();
        ReceiveMsgCallback receiveMsgCb = new ReceiveMsgCallbackImpl();
        struct.connected_cb = connectCb;
        struct.disconnect_cb = disconnectCb;
        struct.recv_msg_cb = receiveMsgCb;
        int connRet = instance.glc_connect(struct);
        System.out.println("connect to " + remotePeerId + " result: " + connRet);
    }

    public static void startDeviceSdk(GLLibrary instance, String localPeerId) {
        GlDevStartParamStruct.ByReference struct = new GlDevStartParamStruct.ByReference();
        DeviceCallbackImpl cb = new DeviceCallbackImpl();
        cb.instance = instance;
        struct.local_peerid = localPeerId;
        struct.someone_callme_cb = cb;
        int startRet = instance.gld_start(struct);
        System.out.println("device-sdk启动结果: " + startRet);
    }

    public static void stopDeviceSdk(GLLibrary instance) {
        instance.gld_stop();
        System.out.println("device-sdk stop结束.");
    }

    public static void startClientSdk(GLLibrary instance, String localPeerId) {
        GlCliStartParamStruct.ByReference struct = new GlCliStartParamStruct.ByReference();

        struct.local_peerid = localPeerId;
        int startRet = instance.glc_start(struct);
        System.out.println("client-sdk启动结果: " + startRet);

    }

    public static void stopClientSdk(GLLibrary instance) {
        instance.glc_stop();
        System.out.println("client-sdk stop结束.");
    }


    public static void glpPeerInfo(GLLibrary instance, String peerId) {
        GlPeerInfoStruct.ByReference info = new GlPeerInfoStruct.ByReference();

        int ret = instance.glp_peer_info(peerId, info);
        System.out.println("call peer info api result: " + ret + ", peerInfo: " + JSON.toJSONString(info));
    }

    public static void addServiceMapping(GLLibrary instance, String remotePeerId, int destPort, String destHost, int listenPort) {
        GlcSvmapParamStruct.ByReference param = new GlcSvmapParamStruct.ByReference();
        param.remote_peerid = remotePeerId;
        param.destination_port = destPort;
        param.destination_host = destHost;
        param.listen_port = listenPort;
        param.rule = 0;
        int mappingRet = instance.glc_add_service_mapping(param);
        System.out.println("添加映射结果: " + mappingRet);
    }

    public static void delServiceMapping(GLLibrary instance, String remotePeerId, int listenPort) {
        GlcSvmapParamStruct.ByReference param = new GlcSvmapParamStruct.ByReference();
        param.remote_peerid = remotePeerId;
        param.listen_port = listenPort;
        int mappingRet = instance.glc_del_service_mapping(param);
        System.out.println("删除映射结果: " + mappingRet);
    }

    public static void initGLCfgV2(GLLibrary instance) {
        //1. 初始化配置
        GlpServerModel sip_server = new GlpServerModel();
        GlpServerModel stun_server = new GlpServerModel();
        //"glmgr-gl.ugreen.cloud", 443, 1, "", ""
        //"stun-gl.ugreen.cloud", 443, 1, "", ""


        CfgInitModel cfgParam = new CfgInitModel();
        //sip_server, stun_server, 128, 0, 1024, log_path
        cfgParam.aes_size = 0;
        cfgParam.log_lev = 1;
        cfgParam.log_path = log_path;
        cfgParam.log_max_size = 1024 * 1024 * 100;

        sip_server.server_host = tunnel_host;
//        sip_server.server_host = "test-glmgr.ugreen.cloud";
        sip_server.server_port = 443;
        sip_server.use_tls = 1;

        stun_server.server_host = stun_host;
//        stun_server.server_host = "test-stun.ugreen.cloud";
        stun_server.server_port = 3479;
        stun_server.use_tls = 1;

        cfgParam.sip_server = sip_server;
        cfgParam.stun_server = stun_server;

        int cfgRet = instance.glp_configure_v2(cfgParam);
        System.out.println("初始化配置结果: " + cfgRet);
    }

}

