package org.xqh.study.jna.guanglian;

import com.sun.jna.Native;

/**
 * @ClassName GuanglianCliTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/6/20 9:06
 * @Version 1.0
 */
public class GuanglianCliTest {

    public static void main(String[] args) throws InterruptedException {
        GLLibrary instance = Native.load("D:\\work\\so\\ugreen_winx64_glp2p.dll", GLLibrary.class);
        String remotePeerId = "GL02ccdcdbb0ac418aa8553253791aea04";
        String localPeerId = "guanglian-test-xqh1122222211122222222222aass";
        //初始化配置
        GuangLianSDK.initGLCfgV2(instance);

        //启动sdk
        GuangLianSDK.startClientSdk(instance, localPeerId);

                //连接设备
        GuangLianSDK.connectToDevice(instance, remotePeerId, "7682#R53IJYD9Gn48G6Ib");
//
        //添加映射
        GuangLianSDK.addServiceMapping(instance, remotePeerId, 9999, "192.168.88.122", 2000);
        Thread.sleep(10000000);
    }
}
