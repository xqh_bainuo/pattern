package org.xqh.study.jna.test2;

import com.sun.jna.*;

import java.util.Collections;

/**
 * @ClassName JNAUsage
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/4/7 16:49
 * @Version 1.0
 */
public class JNAUsage {

    public interface CLibrary extends Library {
        CLibrary INSTANCE = (CLibrary)
                Native.load((Platform.isWindows() ? "msvcrt" : "c"),
                        CLibrary.class);

        void printf(String format, Object... args);
    }

    public static void main(String[] args) {
        CLibrary.INSTANCE.printf("Hello, World\n");
        for (int i = 0; i < args.length; i++) {
            CLibrary.INSTANCE.printf("Argument %d: %s\n", i, args[i]);
        }

        TypeConverter stringConverter = new TypeConverter() {
            @Override
            public Object toNative(Object value, ToNativeContext context) {
                if (value == null)
                    return null;
                if (value instanceof String[]) {
                    return new StringArray((String[])value, true);
                }
                return new WString(value.toString());
            }
            @Override
            public Object fromNative(Object value, FromNativeContext context) {
                if (value == null)
                    return null;
                return value.toString();
            }
            @Override
            public Class<?> nativeType() {
                return WString.class;
            }
        };
//        addTypeConverter(String.class, stringConverter);
//        addToNativeConverter(String[].class, stringConverter);

//        TestLibrary lib = Native.load("testlib", TestLibrary.class, Collections.singletonMap(Library.OPTION_TYPE_MAPPER, mapper));
    }
}
