package org.xqh.study.jna.uglinkp2p;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 * @ClassName CallbackLibrary
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/10 15:14
 * @Version 1.0
 */
public interface CallbackLibrary extends Library {
    // 加载动态库
    CallbackLibrary INSTANCE = Native.load("callback", CallbackLibrary.class);

    // 注册回调函数的方法
    void register_callback(Callback callback);

    // 触发回调的方法
    void trigger_callback();
}
