package org.xqh.study.jna.uglinkp2p;

import com.sun.jna.Callback;

/**
 * @ClassName Callback
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/10 15:15
 * @Version 1.0
 */
interface TestCallback extends Callback {
    void invoke(String message); // 回调函数
}
