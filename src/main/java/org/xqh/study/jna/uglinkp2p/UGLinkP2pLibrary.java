package org.xqh.study.jna.uglinkp2p;

import com.sun.jna.Callback;
import com.sun.jna.Library;
import org.xqh.study.jna.guanglian.GlModel;

/**
 * @ClassName UGLinkP2pLibary
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/9 14:10
 * @Version 1.0
 */
public interface UGLinkP2pLibrary  extends Library {

    int InitSdkCfg(String signalAddr, String stunAddr, String clientId, String cliDesc, String vkey, String certFile, String keyFile, String caFile, String sdkPar, int udpPort);

    String QuerySDKStatus();

    void StopUGLinkSDK();
    int StartUGLinkSDK();

    void RegisterCallback(Callback cb);

    int AddConnection(int localPort, String dstAddr);

    int DelConnection(String dstAddr);

    void SetLogPath(String logPath);

    /**
     * //export StartUGLinkSDK
     * func StartUGLinkSDK(signalAddr, stunAddr, clientId, cliDesc, vkey, certFile, keyFile, caFile, sdkPar string, udpPort int) int {
     * 	return client.StartUGLinkSDK(signalAddr, stunAddr, clientId, cliDesc, vkey, certFile, keyFile, caFile, sdkPar, udpPort)
     * }
     *
     * //export StopUGLinkSDK
     * func StopUGLinkSDK() {
     * 	client.StopUGLinkSDK()
     * }
     *
     * //export RegisterCallback
     * func RegisterCallback(cb func(eventId int, msg string)) {
     * 	client.SetCallback(cb)
     * }
     *
     * //export AddConnection
     * func AddConnection(localPort int, dstAddr string) int {
     * 	return client.AddConnection(localPort, dstAddr)
     * }
     *
     * //export DelConnection
     * func DelConnection(dstAddr string) int {
     * 	return client.DeleteAppCliConnection(dstAddr)
     * }
     *
     * //export SetLogPath
     * func SetLogPath(path string) {
     * 	common.InitLogConfig(path)
     * }
     */
}
