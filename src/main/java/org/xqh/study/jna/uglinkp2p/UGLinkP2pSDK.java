package org.xqh.study.jna.uglinkp2p;

import com.sun.jna.Callback;
import com.sun.jna.Native;
import com.sun.jna.Structure;
import com.sun.jna.Structure.ByReference;
import org.xqh.study.jna.guanglian.GLLibrary;

/**
 * @ClassName UGLinkP2pSDK
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/9 14:11
 * @Version 1.0
 */
public class UGLinkP2pSDK {

    public interface UGLinkSdkCallback extends Callback {
        void OnMessageReceived(int eventId, String msg);
    }

    public static class UGLinkSdkCallbackImpl implements UGLinkSdkCallback  {
        @Override
        public void OnMessageReceived(int eventId, String msg) {
            System.out.println("触发回调函数: "+ eventId + ", msg: " + msg);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        runUglinkP2p();
//        new Thread(()-> {
//            try {
//                runUglinkP2p();
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        }).start();
//
//        new Thread(()-> {
//            try {
//                runUglinkP2p();
//            } catch (InterruptedException e) {
//                throw new RuntimeException(e);
//            }
//        }).start();

//        Thread.sleep(360000000);
    }

    public static void runUglinkP2p() throws InterruptedException {
        String workPath = "D:\\work\\so\\";
//        DemoLibrary library = Native.load(workPath + "sdkdemo.dll", DemoLibrary.class);
//        System.out.println(library.testAdd(100, 2001));
        String sdkPar = "{\"cpt\":0,\"bct\":\"tcp\",\"dt\":5,\"cel\":3,\"cesc\":\"-1,-5\",\"czel\":5,\"cem\":\"\",\"mka\":-1,\"mws\":-1,\"oft\":true,\"rit\":5,\"czrd\":true,\"pubKey\":\"\",\"hbcd\":0,\"hbt\":2,\"hbwt\":2,\"hbwc\":0,\"pcs\":1}";
        UGLinkP2pLibrary instance = Native.load(workPath + "uglinkp2p.dll", UGLinkP2pLibrary.class);
        instance.SetLogPath(workPath + "uglinkp2p.log");
        instance.InitSdkCfg("58.49.151.18:8023", "stun.miwifi.com:3478", "testappclient4", "test app client", "c2d0ddf20fd7409d8a38c950f53e5a09", "", "", "", sdkPar, 6000);
        instance.StartUGLinkSDK();
        instance.AddConnection(2000, "192.168.3.2:9999");
        registerCallback(instance);
        new Thread(()-> {
            for (int i = 0; i < 100; i ++ ){
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("当前sdk状态: " + instance.QuerySDKStatus());
            }
        }).start();

//        System.out.println("---------------------开始sleep 10 秒");
//        Thread.sleep(10000);
//        System.out.println("---------------------停止sdk");
//        instance.StopUGLinkSDK();
//        Thread.sleep(2000);
//        instance.StartUGLinkSDK();
//        instance.AddConnection(2000, "192.168.3.2:9999");
//        registerCallback(instance);
        Thread.sleep(360000000);
    }
    private static void registerCallback(UGLinkP2pLibrary instance){
        UGLinkSdkCallback cb = new UGLinkSdkCallback(){
            @Override
            public void OnMessageReceived(int eventId, String msg) {
                System.out.println(String.format("-------------------触发回调函数 eventId: %s, msg: %s", eventId, msg));
            }
        };
        instance.RegisterCallback(cb);
    }
}


