package org.xqh.study.jna.uglinkp2p;

import com.sun.jna.Callback;
import com.sun.jna.Library;

/**
 * @ClassName DemoLibrary
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/9 14:45
 * @Version 1.0
 */
public interface DemoLibrary extends Library {

    int testAdd(int a, int b);
}
