package org.xqh.study.jna.test;

import com.alibaba.fastjson.JSON;
import com.sun.jna.Library;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName JnaTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/3/27 13:04
 * @Version 1.0
 */
public class JnaTest {


    public static interface JnaTestLibrary extends Library {
        int SDF_Ext_GenEnvKeyPairBlob_ECC(Pointer var1, NativeLong var2, ECCrefPublicKey.ByReference pucPublicKey, ENVELOPEDKEYBLOB.ByReference blob);
    }

    public static class ENVELOPEDKEYBLOB extends Structure {
        public NativeLong ulAsymmAlgID;
        public NativeLong ulSymmAlgID;
        public ECCCipher ECCCipherBlob;
        public ECCrefPublicKey PubKey;
        public byte[] cbEncryptedPriKey = new byte[64];

        @Override
        protected List getFieldOrder() {
            return Arrays.asList("ulAsymmAlgID", "ulSymmAlgID", "ECCCipherBlob", "PubKey", "cbEncryptedPriKey");

        }

        public static class ByValue extends ENVELOPEDKEYBLOB implements Structure.ByValue {
            public ByValue() {
            }
        }

        public static class ByReference extends ENVELOPEDKEYBLOB implements Structure.ByReference {
            public ByReference() {
            }
        }
    }

    public static class ECCCipher extends Structure {
        public byte[] x = new byte[64];
        public byte[] y = new byte[64];
        public byte[] M = new byte[32];
        public int L;
        public byte[] C = new byte[1024];

        public static class ByValue extends ECCCipher implements Structure.ByValue {
            public ByValue() {
            }
        }

        public static class ByReference extends ECCCipher implements Structure.ByReference {
            public ByReference() {
            }
        }
    }

    public static class ECCrefPublicKey extends Structure {
        public int bits;
        public byte[] x = new byte[64];
        public byte[] y = new byte[64];


        @Override
        protected List<String> getFieldOrder() {
            return Arrays.asList(new String[] {"bits", "x","y"});
        }

        @Override
        public String toString() {
            return JSON.toJSONString(this);
        }

        public static class ByValue extends ECCrefPublicKey implements Structure.ByValue {
            public ByValue() {

            }
        }

        public static class ByReference extends ECCrefPublicKey implements Structure.ByReference {
            public ByReference() {

            }
        }
    }

    public static void main(String[] args) {
        ECCCipher ECCCipherBlob = new ECCCipher();
        ECCrefPublicKey pubKey = new ECCrefPublicKey();
        ENVELOPEDKEYBLOB.ByReference pEnvelopedKeyBolb = new ENVELOPEDKEYBLOB.ByReference();
        pEnvelopedKeyBolb.ECCCipherBlob = ECCCipherBlob;
        pEnvelopedKeyBolb.PubKey = pubKey;
        pEnvelopedKeyBolb.cbEncryptedPriKey = new byte[64];
//        rv = api.Test_SDF_Ext_GenEnvKeyPairBlob_ECC(hSessionHandle, sm4, EncPubKey, pEnvelopedKeyBolb);
    }
}
