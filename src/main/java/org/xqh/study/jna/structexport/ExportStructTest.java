package org.xqh.study.jna.structexport;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Structure;
import com.sun.jna.Structure.FieldOrder;

/**
 * @ClassName ExportStructTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/5/12 17:17
 * @Version 1.0
 */
public class ExportStructTest {

    public static interface  ExportStructLibrary extends Library {
        void TestExport(TestStruct.ByReference struct);
    }

    /**
     * 客户端sdk启动参数
     */
    @FieldOrder({"num", "msg"})
    public static class TestStruct extends Structure {

        public int num;
        public String msg;

        public static class ByReference extends TestStruct implements Structure.ByReference {
        }

        public static class ByValue extends TestStruct implements Structure.ByValue {
        }
    }

    public static void main(String[] args) {
        ExportStructLibrary instance = Native.load("D:\\work\\so\\struct-export.dll", ExportStructLibrary.class);
        TestStruct.ByReference test = new TestStruct.ByReference();
        test.num = 100;
        test.msg = "hehehe";
        instance.TestExport(test);

        System.out.println(test.num);
        System.out.println(test.msg);
    }
}
