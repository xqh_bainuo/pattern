package org.xqh.study.jna.dllexport;

import com.sun.jna.*;
import com.sun.jna.Structure.FieldOrder;

/**
 * @ClassName DllExport
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/4/10 15:16
 * @Version 1.0
 */
public interface DllExport extends Library {

    public static void main(String[] args) {
        DllExport instance = Native.load("D:\\work\\so\\dll-export.dll", DllExport.class);
//
//        System.out.println(instance.initTest(getServerInstance(9999)));

//        System.out.println(instance.initConfigV4(new TestConfigModelV4()));
//
//
//
//
//        System.out.println(instance.initTest2(getServerInstance(2)));
//
//        Memory memory = new Memory(2048);
        TestConfigModel.ByReference p = new TestConfigModel.ByReference();
        p.aes_size = 1024;
        p.log_path = "fffff";
        p.log_lev = 10;
        p.log_max_size = 1024;
        System.out.println(instance.initConfig(p));

//        TestConfigModelV3 v3 = new TestConfigModelV3();
//        v3.log_path = "fffffv333";
//        v3.aes_size = 1024;
//        v3.log_lev = 10;
//        v3.log_max_size = 1024;
//        System.out.println(instance.initConfigV3(v3));
    }

    public static TestServerModel.ByReference getServerInstance(int idx){
        TestServerModel.ByReference sm2 = new TestServerModel.ByReference();
        sm2.server_host = String.format("https://www.baidu.com-%s", idx);
        sm2.server_port = 100 + idx;
        sm2.cert_path = String.format("cert_path-%s", idx);
        sm2.use_tls = idx;
        sm2.twauth_pem_path = String.format("twauth_pem_path_%s", idx);
        return sm2;
    }

    public static TestServerModel.ByValue getServerValueInstance(int idx){
        TestServerModel.ByValue sm2 = new TestServerModel.ByValue();
        sm2.server_host = String.format("https://valuewww.baidu.com-%s", idx);
        sm2.cert_path = String.format("cert_path-%s", idx);
        sm2.server_port = 100 + idx;
        sm2.use_tls = idx;
        sm2.twauth_pem_path = String.format("twauth_pem_path_%s", idx);
        return sm2;
    }

    int add(int a, int b);

    int initConfig(TestConfigModel.ByReference model);
    int initConfigV3(TestConfigModelV3 model);
    int initConfigV4(TestConfigModelV4 model);

    int initTest(TestServerModel.ByReference sm);
    int initTest2(TestServerModel.ByReference model);






    /**
     * 配置项
     */
    @FieldOrder({"server_host",  "server_port", "use_tls", "cert_path", "twauth_pem_path"})
    public static class TestServerModel extends Structure{

        public String server_host;
        public int server_port;
        public int use_tls;/* 使用使用tls连接 */

        public String cert_path;/* 使用tls连接时，指定证书文件，可选 */

        public String twauth_pem_path;/* 双向认证公钥文件 */


        public static class ByReference extends TestServerModel implements Structure.ByReference {
        }

        public static class ByValue extends TestServerModel implements Structure.ByValue {
        }


    }


    @FieldOrder({"aes_size", "log_lev", "log_max_size", "log_path", "sip_server", "stun_server"})
    public static class TestConfigModel extends Structure {

        public int aes_size;/* 0: none, 128/192/256: aes_key_size */

        /* lib log info */
        public int log_lev;/* 0:trace,1:debug,2:info,3:warn,4:error,5:null */
        public int log_max_size;
        public String log_path;

        public TestServerModel.ByValue sip_server = getServerValueInstance(3);
        public TestServerModel.ByValue stun_server = getServerValueInstance(4);

        public static class ByReference extends TestConfigModel implements Structure.ByReference {
        }

        public static class ByValue extends TestConfigModel implements Structure.ByValue {
        }
    }

    @FieldOrder({"aes_size", "log_lev", "log_max_size", "log_path"})
    public static class TestConfigModelV3 extends Structure {

        public int aes_size;/* 0: none, 128/192/256: aes_key_size */

        /* lib log info */
        public int log_lev;/* 0:trace,1:debug,2:info,3:warn,4:error,5:null */
        public int log_max_size;
        public String log_path;


        public static class ByReference extends TestConfigModelV3 implements Structure.ByReference {
        }

        public static class ByValue extends TestConfigModelV3 implements Structure.ByValue {
        }
    }

    @FieldOrder({"sip_server", "stun_server"})
    public static class TestConfigModelV4 extends Structure {

        public TestServerModel.ByValue sip_server = getServerValueInstance(3);
        public TestServerModel.ByValue stun_server = getServerValueInstance(4);

        public static class ByReference extends TestConfigModelV3 implements Structure.ByReference {
        }

        public static class ByValue extends TestConfigModelV3 implements Structure.ByValue {
        }
    }
}
