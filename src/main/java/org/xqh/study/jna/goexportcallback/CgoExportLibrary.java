package org.xqh.study.jna.goexportcallback;

import com.sun.jna.Library;
import org.xqh.study.jna.goexportcallback.CgoExportCallback.CgoExportCB;
import org.xqh.study.jna.goexportcallback.CgoExportCallback.CgoExportCB2;

/**
 * @ClassName CgoExportLibrary
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/5/12 15:05
 * @Version 1.0
 */
public interface CgoExportLibrary  extends Library {

    void SetListener(CgoExportCB cb);

    void SetListener2(CgoExportCB2 cb);
}
