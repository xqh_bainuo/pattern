package org.xqh.study.jna.goexportcallback;

import com.sun.jna.Callback;
import com.sun.jna.Native;

/**
 * @ClassName CgoExportCallback
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/5/12 15:04
 * @Version 1.0
 */
public class CgoExportCallback {

    public static void main(String[] args) throws InterruptedException {
        CgoExportLibrary instance = Native.load("D:\\work\\so\\cgo-export-callback.dll", CgoExportLibrary.class);
        new Thread(()-> {
            CgoExportCB cb = new CgoExportCBImpl();
            instance.SetListener(cb);
        }).start();

        new Thread(()-> {
            CgoExportCB2 cb2 = new CgoExportCB2Impl();
            instance.SetListener2(cb2);
        }).start();

        Thread.sleep(100000);
    }

    public interface CgoExportCB extends Callback {
        void cb(String event);
    }

    public static class CgoExportCBImpl implements CgoExportCB {
        @Override
        public void cb(String event) {
            System.out.println("receive event msg: " + event);
        }
    }


    public interface CgoExportCB2 extends Callback {
        void cb2(String event);
    }

    public static class CgoExportCB2Impl implements CgoExportCB2 {
        @Override
        public void cb2(String event) {
            System.out.println("receive event msg22222: " + event);
        }
    }
}
