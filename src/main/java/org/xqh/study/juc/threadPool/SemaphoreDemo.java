package org.xqh.study.juc.threadPool;

import java.util.concurrent.Semaphore;

/**
 * Created by leo on 2017/9/20.
 * 信号量 (允许多个线程同时访问)
 */
public class SemaphoreDemo implements Runnable{
    final Semaphore semap=new Semaphore(5);

    static Semaphore test = new Semaphore(0);
    @Override
    public void run() {
        try {
            semap.acquire();
            Thread.sleep(2000);
            System.out.println(Thread.currentThread().getId()+" done!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }finally {
            semap.release();
        }
    }

    public static void main(String[] args) {

//        ExecutorService ex= Executors.newFixedThreadPool(20);
//        final SemaphoreDemo demo=new SemaphoreDemo();
//        for(int i=0;i<20;i++){
//            ex.execute(demo);
//        }
//        ex.shutdown();
    }

    public static class TestThread implements Runnable{

        private int idx;
        public TestThread(int idx){
            this.idx = idx;

        }

        @Override
        public void run() {

        }
    }


}
