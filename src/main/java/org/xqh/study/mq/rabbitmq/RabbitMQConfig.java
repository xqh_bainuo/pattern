package org.xqh.study.mq.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

/**
 * @ClassName RabbitMQConfig
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/10/13 14:45
 * @Version 1.0
 */
public class RabbitMQConfig {

    public static String exchange = "device-direct-exchange";
    public static String queueName = "api_analysis_queue";

    public static String consumeTagA = "consume_tag_a";

    public static String consumeTagB = "consume_tag_b";


    public static String routeKey = "api_analysis_routing_key";

    public static AMQP.BasicProperties getBasicProperties(){
        AMQP.BasicProperties properties = new AMQP.BasicProperties();

        return properties;
    }


    public static ConnectionFactory initConnectionFactory(){
        ConnectionFactory cf = new ConnectionFactory();

        //ConnectionFactory factory = new ConnectionFactory();
//        factory.setUri("amqp://guest:guest@localhost:5672");
//        Map<String, Object> clientProperties = new HashMap<>();
//        clientProperties.put("author", "xuqianghui");
//        clientProperties.put("time", new Date());
//        factory.setClientProperties(clientProperties);


        cf.setHost("120.232.63.149");
        cf.setVirtualHost("/");
        cf.setPort(5672);
        cf.setUsername("admin");
        cf.setPassword("abc.123ASD");


        //本地docker rabbitmq
//        cf.setHost("localhost");
//        cf.setVirtualHost("/");
//        cf.setPort(5672);
//        cf.setUsername("guest");
//        cf.setPassword("guest");
        return cf;
    }

    public static Channel createChannel() throws IOException, TimeoutException {
        Connection c = initConnectionFactory().newConnection();
        Channel channel = c.createChannel();
        channel.exchangeDeclare(RabbitMQConfig.exchange, BuiltinExchangeType.DIRECT, true);
        // 声明持久化、排他的、非自动删除的队列
        channel.queueDeclare(RabbitMQConfig.queueName, true, true, false, new HashMap<>());
        // 声明持久化、非自动删除、绑定类型为 direct 的交换器
        channel.queueBind(queueName, exchange, routeKey);
        return channel;
    }

    public static Channel createConsumeChannel() throws IOException, TimeoutException {
        Connection c = initConnectionFactory().newConnection();
        Channel channel = c.createChannel();
        // 声明持久化、非自动删除、绑定类型为 direct 的交换器
//        channel.queueBind(queueName, exchange, routeKey);
        return channel;
    }
}
