package org.xqh.study.mq.rocketmq.rocketmqclientjava;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.apis.*;
import org.apache.rocketmq.client.apis.message.Message;
import org.apache.rocketmq.client.apis.producer.Producer;
import org.apache.rocketmq.client.apis.producer.SendReceipt;
import org.xqh.study.mq.rocketmq.RocketMQConst;

import java.util.logging.Logger;

/**
 * @ClassName ProducerExample
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/10/26 19:38
 * @Version 1.0
 */
@Slf4j
public class ProducerExample {

    public static void main(String[] args) throws ClientException {
        // 接入点地址，需要设置成Proxy的地址和端口列表，一般是xxx:8081;xxx:8081。
        // 消息发送的目标Topic名称，需要提前创建。
        ClientServiceProvider provider = ClientServiceProvider.loadService();
        ClientConfigurationBuilder builder = ClientConfiguration.newBuilder().setEndpoints(RocketMQConst.endpoints);
        SessionCredentialsProvider sessionCredentialsProvider =
                new StaticSessionCredentialsProvider(RocketMQConst.accessKey, RocketMQConst.accessSecret);
        builder.setCredentialProvider(sessionCredentialsProvider);
        builder.enableSsl(true);
        ClientConfiguration configuration = builder.build();

        // 初始化Producer时需要设置通信配置以及预绑定的Topic。
        Producer producer = provider.newProducerBuilder()
                .setTopics(RocketMQConst.testTopic)
                .setClientConfiguration(configuration)
                .build();
        // 普通消息发送。
        Message message = provider.newMessageBuilder()
                .setTopic(RocketMQConst.testTopic)
                // 设置消息索引键，可根据关键字精确查找某条消息。
                .setKeys("messageKey")
                // 设置消息Tag，用于消费端根据指定Tag过滤消息。
                .setTag("messageTag")
                // 消息体。
                .setBody("messageBody".getBytes())
                .build();
        try {
            // 发送消息，需要关注发送结果，并捕获失败等异常。
            SendReceipt sendReceipt = producer.send(message);
            log.info("Send message successfully, messageId={}", sendReceipt.getMessageId());
        } catch (ClientException e) {
            log.error("Failed to send message", e);
        }
        // producer.close();
    }
}
