package org.xqh.study.mq.rocketmq;

/**
 * @ClassName RocketMQConst
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/7/7 11:39
 * @Version 1.0
 */
public class RocketMQConst {

//    public static final String endpoints = "mq-center.ugnas.com:9876";
//    public static final String endpoints = "mq-center.ugnas.com:9876";
    public static final String endpoints = "mq-center.ugnas.com:9876";

    public final static String accessKey = "rocketmq2";

    public final static String accessSecret = "OTBmNDg8Y2";

    public final static String produceGroup = "test_produce_group";

    public final static String consumerGroup = "test_consumer_group";

    public final static String testGroup = "groupB";

    public final static String testTag = "test_tag";

    public final static String testTopic = "testCommonTopic";
}
