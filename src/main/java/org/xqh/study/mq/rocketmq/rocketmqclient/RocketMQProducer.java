package org.xqh.study.mq.rocketmq.rocketmqclient;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.xqh.study.mq.rocketmq.RocketMQConst;

/**
 * @ClassName RocketMQProducer
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/10/26 19:26
 * @Version 1.0
 */
public class RocketMQProducer {

    public static void main(String[] args) throws Exception{
        // 构造Producer时，必须指定groupId
        DefaultMQProducer producer = new DefaultMQProducer(RocketMQConst.produceGroup);
        //指定NameServer的地址  只用namesrv的地址就行，它会从namesrv上拿到broker的地址和topic信息
        producer.setNamesrvAddr(RocketMQConst.endpoints);
        //启动生产者
        producer.start();

        int num = 0;
        while (num < 20) {
            num++;
            /**
             * rocketmq封装了Message
             * String topic,
             * String tags, 标签（分类）---> 筛选
             * byte[] body
             */
            Message message = new Message(RocketMQConst.testTopic, "", ("hello rocketmq:" + num).getBytes());
            //同步发送  发送消息，拿到返回SendResult
            SendResult result = producer.send(message);
            System.out.println(result);
        }
        //关闭生产者
        producer.shutdown();
    }

}
