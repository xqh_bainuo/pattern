package org.xqh.study.serialize;

/**
 * @ClassName SerializeUtils
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/5/22 21:59
 * @Version 1.0
 */
public class SerializeUtils {

    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(0xff00));
    }

    /**
     * int序列化成 字节流
     *
     * @param n
     * @return
     */
    public static byte[] intToBytes(int n) {
        /**
         * 例如 int类型变量 n   (四个字节)
         *  n = 0x11 0x22 0x33 0x44
         */
        //int占四个字节
        byte[] buf = new byte[4];
        for (int i = 0; i < buf.length; i++) {
            /**
             * i = 0 时  右移0位 什么也不干, 但是 buf[0] 只有一个字节 只能 取到  0x44
             * i = 1 时  此时 n 右移 8位 变为 ===> 0x11 0x22 0x33, buf[1] 还是只能取到一个字节 0x33
             * ....
             * 所以最后 buf = [0x44, 0x33, 0x22, 0x11]
             *
             */
            buf[i] = (byte) (n >> 8);
        }
        return buf;
    }

    /**
     * bytes 转 int
     * @param buf
     * @param offset
     * @return
     */
    public static int bytesToInt(byte[] buf, int offset){

        /**
         * buf = [0x44, 0x33, 0x22, 0x11] , offset = 0
         * 0x44 & 0xff
         */
        return buf[offset] & 0xff
                | ((buf[offset + 1] << 8) & 0xff00)
                | ((buf[offset + 2] << 16) & 0xff0000)
                | ((buf[offset + 3] << 24) & 0xff000000);
    }
}
