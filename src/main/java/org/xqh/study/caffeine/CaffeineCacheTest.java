package org.xqh.study.caffeine;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;

import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName CaffeineCacheTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/11/18 9:44
 * @Version 1.0
 */
public class CaffeineCacheTest {

    public static String value = "%s_%s";

    /**
     * action Map 缓存
     */
    private static LoadingCache<String, String> actionCache = Caffeine.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(5, TimeUnit.SECONDS)
            .build(key -> loadActionMapByKey(key));

    private static String loadActionMapByKey(String key) {
        if(key.contains("null")){
            return null;
        }
        String result = String.format(value, key, UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println("load new-value==> "+ result);
        return result;
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println(actionCache.getIfPresent("null1"));
    }
}
