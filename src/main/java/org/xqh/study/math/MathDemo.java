package org.xqh.study.math;

/**
 * @ClassName MathDemo
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/3/5 10:00
 * @Version 1.0
 */
public class MathDemo {

    public static void main(String[] args) {
        System.out.println(Math.log(8));
        System.out.println(Math.log10(100));
    }
}
