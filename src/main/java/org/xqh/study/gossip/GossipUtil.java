package org.xqh.study.gossip;

import net.lvsq.jgossip.core.GossipService;
import net.lvsq.jgossip.core.GossipSettings;
import net.lvsq.jgossip.event.GossipListener;
import net.lvsq.jgossip.model.SeedMember;
import net.lvsq.jgossip.net.MsgService;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName JgossipUtil
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/12/3 10:03
 * @Version 1.0
 */
public class GossipUtil {

    /**
     * 获取 gossipService实例
     * @param gossip_port  gossip启动ip
     * @param cluster 集群名称
     * @param seedNodes 节点集合
     * @return
     */
    public static GossipService getGossipService(int gossip_port, String cluster, List<SeedMember> seedNodes, GossipListener listener, MsgService msgService) {
        GossipService gossipService = null;

        GossipSettings settings = new GossipSettings();
        settings.setGossipInterval(1000);
        settings.setMsgService(msgService);
        try {
            String myIpAddress = InetAddress.getLocalHost().getHostAddress();
            gossipService = new GossipService(cluster, myIpAddress, gossip_port, null, seedNodes, settings, listener);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        gossipService.start();
        return gossipService;
    }

    public static SeedMember getSeedMember(String cluster, String ip, int port){
        SeedMember seed = new SeedMember();
        seed.setCluster(cluster);
        seed.setIpAddress(ip);
        seed.setPort(port);
        return seed;
    }
}
