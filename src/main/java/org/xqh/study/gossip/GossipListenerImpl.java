package org.xqh.study.gossip;

import net.lvsq.jgossip.event.GossipListener;
import net.lvsq.jgossip.model.GossipMember;
import net.lvsq.jgossip.model.GossipState;

/**
 * @ClassName GossipListenerImpl
 * @Description gossip监听实现  目前支持三种事件监听 join, up, down
 * @Author xuqianghui
 * @Date 2020/12/3 10:07
 * @Version 1.0
 */
public class GossipListenerImpl implements GossipListener {

    @Override
    public void gossipEvent(GossipMember gossipMember, GossipState gossipState) {
        System.out.println("member1:" + gossipMember + "  state: " + gossipState);
    }
}
