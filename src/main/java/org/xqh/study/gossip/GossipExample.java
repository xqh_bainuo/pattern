package org.xqh.study.gossip;

import net.lvsq.jgossip.core.GossipService;
import net.lvsq.jgossip.model.SeedMember;
import net.lvsq.jgossip.net.MsgService;
import net.lvsq.jgossip.net.udp.UDPMsgService;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName GossipExample
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/12/2 18:25
 * @Version 1.0
 */
public class GossipExample {

    public static void main(String[] args) throws UnknownHostException {
        String myIpAddress = InetAddress.getLocalHost().getHostAddress();
        String cluster = "gossip_cluster";
        MsgService msgService = new UDPMsgService();
        List<SeedMember> seedNodes = new ArrayList<>();
        seedNodes.add(GossipUtil.getSeedMember(cluster, myIpAddress, 60001));
        GossipService service = GossipUtil.getGossipService(60001, cluster, seedNodes, new GossipListenerImpl(), msgService);
        service.start();
    }

}
