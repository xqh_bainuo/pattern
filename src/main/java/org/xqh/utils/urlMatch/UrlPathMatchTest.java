package org.xqh.utils.urlMatch;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.sun.media.sound.SoftTuning;
import org.springframework.util.AntPathMatcher;
import org.xqh.utils.encrypt.EncryptUtils;
import org.xqh.utils.http.FxHttpClientUtils;

/**
 * @ClassName UrlPathMatchTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/1/25 11:31
 * @Version 1.0
 */
public class UrlPathMatchTest {

    private static AntPathMatcher matcher = new AntPathMatcher();

    public static void main(String[] args) throws HttpProcessException {
        String pattern = "/api/**/feign/**";
        System.out.println(matcher.match(pattern, "/api/test/abc/feign"));
        System.out.println(matcher.match(pattern, "/api/test/abc/feign/abc"));
        System.out.println(matcher.match(pattern, "/api/feign/abc"));

//        HttpConfig config = FxHttpClientUtils.getHttpConfig("https://test.ugreen.cloud/v1/opt/1639644855286.mp4?requestUrl=/v1/link/download&key=DH-vEC100002006ReId-001205&file_id=1&uuid=9122cbcb-a93d-46c0-92ca-3923b35d6cba&path=%2F.ugreen_nas%2F6732%2Fxqh%2F1639644855286.mp4&responseType=stream&token=RjdEMjYxQjM4OEJFRTJFRkU0RjFEQkE4Njc0RjVDMjA4RDg0OTc4NTAxNzFEODhBNDYwQkJCMDkxMjE5QUYxRQ%3D%3D&%24time=1643091215622");
//
//        HttpResult result = HttpClientUtil.sendAndGetResp(config);
//        System.out.println(result.getResult());
        String path = "%2F.ugreen_nas%2F6732%2Fxqh%2Ftest%2F%E6%97%A7%E8%AE%BE%E5%A4%87%E6%BF%80%E6%B4%BB%E6%B5%81%E7%A8%8B.png";
        System.out.println(EncryptUtils.urlDecode(EncryptUtils.urlDecode(path)));
    }
}
