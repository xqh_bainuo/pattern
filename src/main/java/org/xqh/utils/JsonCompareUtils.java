package org.xqh.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName JsonCompareUtils
 * @Description json内容对比工具
 * @Author xuqianghui
 * @Date 2019/5/6 11:26
 * @Version 1.0
 */
@Slf4j
public class JsonCompareUtils {

    // 不对比的 key
    private static final List<String> filterKeys = Arrays.asList("history", "responseId", "totalTime", "sessionId", "timeCosts", "updateTime");

    // 集合结果size 对比
    private static final List<String> arrayMatchKeys = Arrays.asList("musicinfo","news", "playlist");

    public static void recursionJson(JSONObject json, List<String> list) {
        if(java.util.Objects.isNull(json)){
            return ;
        }
        for(String key:json.keySet()){
            Object obj = json.get(key);
            if(java.util.Objects.isNull(obj)){
                continue;
            }
            if (obj.getClass().equals(JSONObject.class)) {
                recursionJson((JSONObject) obj, list);
            }else if(obj.getClass().equals(JSONArray.class)){
                JSONArray array = (JSONArray) obj;
                for(Object item:array){
                    if(item.getClass().equals(JSONObject.class)){
                        recursionJson((JSONObject) item, list);
                    }
                }
            }else if(obj.getClass().equals(String.class)){
                String value = (String)obj;
                if (!key.endsWith("_path") || value.contains("\\.")){
                    continue;
                }
                list.add(value);
            }
        }
    }

    /**
     * 对比 json 内容差异
     * @param json 对比Json对象
     * @param prefixKey json key
     * @param compareStr 被对比 字符
     * @param difMsg 差异内容
     */
    public static void compareJsonObj(JSONObject json, String prefixKey, String compareStr, StringBuilder difMsg){
        for(String key:json.keySet()){
            String currentKey = prefixKey.concat(".").concat(key);
            if(StringUtils.isEmpty(prefixKey)){
                currentKey = key;
            }
            Object obj = json.get(key);
            if (obj.getClass().equals(JSONObject.class)) {

                compareJsonObj(json.getJSONObject(key), currentKey, compareStr, difMsg);

            } else if (obj.getClass().equals(JSONArray.class)) {

                JSONArray array = json.getJSONArray(key);

                if(arrayMatchKeys.contains(key)){
                    // 只对比数组长度
                    compareArraySize(array.size(), compareStr, currentKey, difMsg);
                    continue;
                }

                for(int i=0;i<array.size();i++){
                    Object o = array.get(i);
                    String tmpKey = currentKey.concat("["+i+"]");
                    if(JSONObject.class.equals(o.getClass())){
                        JSONObject jsonObject = (JSONObject) o;
                        compareJsonObj(jsonObject, tmpKey, compareStr, difMsg);
                    }else{
                        // 其他数据类型
                        compareBaseType(o,  compareStr, tmpKey, difMsg);
                    }
                }
            } else {
                //其他类型 数据
                compareBaseType(obj, compareStr, currentKey, difMsg);
            }
        }
    }

    /**
     * 对比 数组长度
     * @param size
     * @param compareStr
     * @param jsonPath
     * @param difMsg
     */
    public static void compareArraySize(int size, String compareStr, String jsonPath, StringBuilder difMsg){
        int compareSize = 0;
        Object compareVal = JSONPath.read(compareStr, jsonPath);
        if(null != compareVal){
            JSONArray parseArray = null;
            try {
                parseArray = JSONArray.parseArray(String.valueOf(compareVal));
            }catch (Exception e){
                log.error("", e);
            }
            if(null != parseArray){
                compareSize = parseArray.size();
            }
        }
        if(size != compareSize){
            difMsg.append("差异key: "+jsonPath+" ,公有云数组长度: "+ size+" , 私有云数组长度: "+compareSize+" .'\n");
        }
    }

    /**
     * 对比 基础类型 内容差异
     * @param obj
     * @param compareStr
     * @param jsonPath
     * @param difMsg
     */
    public static void compareBaseType(Object obj, String compareStr, String jsonPath, StringBuilder difMsg){
        if(checkFilterKey(jsonPath)){
            return;
        }
        String val = String.valueOf(obj);
        Object compareVal = JSONPath.read(compareStr, jsonPath);
        String targetVal = String.valueOf(compareVal);
        if(!Objects.equal(val, targetVal)){
            difMsg.append("差异key: "+jsonPath+", 公有云内容: "+ val +", 私有云内容: "+targetVal+".\n");
        }
    }

    public static boolean checkFilterKey(String key){
        for(String s:key.split("\\.")){
            if(s.endsWith("]")){
                s = s.substring(0, s.indexOf("["));
            }
            if(filterKeys.contains(s)){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
//        String text1 = "{\"rc\":0,\"text\":\"开灯\",\"service\":\"cn.yunzhisheng.setting\",\"code\":\"SETTING_EXEC\",\"semantic\":{\"intent\":{\"operations\":[{\"operator\":\"ACT_OPEN_ALL\",\"deviceType\":\"OBJ_LIGHT\",\"deviceExpr\":\"灯\"}]}},\"general\":{\"quitDialog\":\"true\",\"type\":\"T\",\"text\":\"好的，正在为您打开所有灯\"},\"history\":\"cn.yunzhisheng.setting\",\"responseId\":\"861b5345349a46ce93dd344d2396d9dc\"}";
//        String text2 = "{\"rc\":0,\"text\":\"打开卧室壁灯\",\"service\":\"cn.yunzhisheng.setting\",\"code\":\"SETTING_EXEC\",\"semantic\":{\"intent\":{\"operations\":[{\"operator\":\"ACT_OPEN\",\"roomType\":\"BED_ROOM\",\"roomExpr\":\"卧室\",\"deviceType\":\"OBJ_LIGHT\",\"deviceExpr\":\"壁灯\"}]}},\"general\":{\"quitDialog\":\"true\",\"type\":\"T\",\"text\":\"好的，正在为您打开卧室壁灯\"},\"history\":\"cn.yunzhisheng.setting\",\"responseId\":\"4a7fa1d119114c5c92bfb4f425c8cc2f\"}";
//        JSONObject json = JSON.parseObject(text1);
//        StringBuilder diffMsg = new StringBuilder();
//        compareJsonObj(json, "", text2, diffMsg);
//        System.out.println("diff msg==>"+ diffMsg.toString());
        String jsonStr = "{\n" +
                "    \"adult\": false,\n" +
                "    \"backdrop_path\": \"/9juRmk8QjcsUcbrevVu5t8VZy5G.jpg\",\n" +
                "    \"belongs_to_collection\": null,\n" +
                "    \"budget\": 38400000,\n" +
                "    \"genres\": [\n" +
                "        {\n" +
                "            \"id\": 28,\n" +
                "            \"name\": \"动作\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 12,\n" +
                "            \"name\": \"冒险\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 80,\n" +
                "            \"name\": \"犯罪\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 53,\n" +
                "            \"name\": \"惊悚\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"homepage\": \"\",\n" +
                "    \"id\": 923667,\n" +
                "    \"imdb_id\": \"tt20316748\",\n" +
                "    \"origin_country\": [\n" +
                "        \"HK\"\n" +
                "    ],\n" +
                "    \"original_language\": \"cn\",\n" +
                "    \"original_title\": \"九龍城寨之圍城\",\n" +
                "    \"overview\": \"九龙城寨危机四起，守护之战一触即发！本片讲述了一个发生在80年代的香港的故事，一个落难青年误闯充满神秘气息的九龙城寨，因而卷入一场恶人夺城，众人殊死保卫栖息之地的风云之战。\",\n" +
                "    \"popularity\": 1608.095,\n" +
                "    \"poster_path\": \"/dS8C60iEHnuZEFgKFjRx0GCLVRf.jpg\",\n" +
                "    \"production_companies\": [\n" +
                "        {\n" +
                "            \"id\": 62894,\n" +
                "            \"logo_path\": \"/fIu9uAhKFyEZipwObaA9EjQJsS2.png\",\n" +
                "            \"name\": \"Entertaining Power\",\n" +
                "            \"origin_country\": \"HK\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 69437,\n" +
                "            \"logo_path\": null,\n" +
                "            \"name\": \"One Cool Pictures\",\n" +
                "            \"origin_country\": \"HK\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 5686,\n" +
                "            \"logo_path\": \"/o4aETXAP9HxOSRbGDWgBfkSzSJw.png\",\n" +
                "            \"name\": \"Sil-Metropole Organisation\",\n" +
                "            \"origin_country\": \"HK\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 214726,\n" +
                "            \"logo_path\": null,\n" +
                "            \"name\": \"HG Entertainment\",\n" +
                "            \"origin_country\": \"CN\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 197030,\n" +
                "            \"logo_path\": \"/z9xtT1e3HunOnZUm3uGYb59eL7v.png\",\n" +
                "            \"name\": \"Tao Piao Piao\",\n" +
                "            \"origin_country\": \"CN\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 5552,\n" +
                "            \"logo_path\": \"/wdtjT9x5OG7nLusPctACI3i7Im2.png\",\n" +
                "            \"name\": \"Media Asia Films\",\n" +
                "            \"origin_country\": \"HK\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 131587,\n" +
                "            \"logo_path\": \"/vtQSkfAuPHoBtlEbUpsjs5kVfG6.png\",\n" +
                "            \"name\": \"Lian Ray Pictures\",\n" +
                "            \"origin_country\": \"CN\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"production_countries\": [\n" +
                "        {\n" +
                "            \"iso_3166_1\": \"CN\",\n" +
                "            \"name\": \"China\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"iso_3166_1\": \"HK\",\n" +
                "            \"name\": \"Hong Kong\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"release_date\": \"2024-04-23\",\n" +
                "    \"revenue\": 46366411,\n" +
                "    \"runtime\": 126,\n" +
                "    \"spoken_languages\": [\n" +
                "        {\n" +
                "            \"english_name\": \"Cantonese\",\n" +
                "            \"iso_639_1\": \"cn\",\n" +
                "            \"name\": \"广州话 / 廣州話\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"status\": \"Released\",\n" +
                "    \"tagline\": \"\",\n" +
                "    \"title\": \"九龙城寨之围城\",\n" +
                "    \"video\": false,\n" +
                "    \"vote_average\": 7.188,\n" +
                "    \"vote_count\": 101,\n" +
                "    \"credits\": {\n" +
                "        \"cast\": [\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 21908,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"郭富城\",\n" +
                "                \"original_name\": \"Aaron Kwok\",\n" +
                "                \"popularity\": 24.706,\n" +
                "                \"profile_path\": \"/haCdKHKCBle20HkbHCapfYTibc0.jpg\",\n" +
                "                \"cast_id\": 25,\n" +
                "                \"character\": \"Jim\",\n" +
                "                \"credit_id\": \"662b2186b513a8011c3eeec5\",\n" +
                "                \"order\": 0\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 78875,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"古天乐\",\n" +
                "                \"original_name\": \"古天樂\",\n" +
                "                \"popularity\": 43.759,\n" +
                "                \"profile_path\": \"/yQuDBTzm7xWlJICNvl20OmcJP80.jpg\",\n" +
                "                \"cast_id\": 1,\n" +
                "                \"character\": \"Cyclone\",\n" +
                "                \"credit_id\": \"61d66a2f1b722c5dbb411de9\",\n" +
                "                \"order\": 1\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 64496,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"任贤齐\",\n" +
                "                \"original_name\": \"任賢齊\",\n" +
                "                \"popularity\": 21.902,\n" +
                "                \"profile_path\": \"/A4oidg4FY6or9TNHi2yz1TO7xeH.jpg\",\n" +
                "                \"cast_id\": 3,\n" +
                "                \"character\": \"Chau\",\n" +
                "                \"credit_id\": \"61d66a48e194b000649d044e\",\n" +
                "                \"order\": 2\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 70108,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"黄德斌\",\n" +
                "                \"original_name\": \"Kenny Wong Tak-Ban\",\n" +
                "                \"popularity\": 11.282,\n" +
                "                \"profile_path\": \"/fSSna59ChOc0OA8ju2xRf8jxOvT.jpg\",\n" +
                "                \"cast_id\": 17,\n" +
                "                \"character\": \"Tiger\",\n" +
                "                \"credit_id\": \"64361e4a94631800bdff3fc3\",\n" +
                "                \"order\": 3\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 62410,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"洪金宝\",\n" +
                "                \"original_name\": \"洪金寶\",\n" +
                "                \"popularity\": 45.096,\n" +
                "                \"profile_path\": \"/ltThjSeD7d2jdO9GSi9rZq1pbjB.jpg\",\n" +
                "                \"cast_id\": 2,\n" +
                "                \"character\": \"Mr. Big\",\n" +
                "                \"credit_id\": \"61d66a3fe10f460067790ee0\",\n" +
                "                \"order\": 4\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 100585,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"伍允龙\",\n" +
                "                \"original_name\": \"伍允龍\",\n" +
                "                \"popularity\": 29.102,\n" +
                "                \"profile_path\": \"/dUvw7dvUyfAiXhyzGWNV0Gd5ycN.jpg\",\n" +
                "                \"cast_id\": 6,\n" +
                "                \"character\": \"King\",\n" +
                "                \"credit_id\": \"61d66ac5e194b00041472414\",\n" +
                "                \"order\": 5\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 89409,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"林峰\",\n" +
                "                \"original_name\": \"Raymond Lam\",\n" +
                "                \"popularity\": 20.883,\n" +
                "                \"profile_path\": \"/Ak40P6TWthdQ9diRki5RIcJ6r95.jpg\",\n" +
                "                \"cast_id\": 4,\n" +
                "                \"character\": \"Chan Lok-kwun (Lok)\",\n" +
                "                \"credit_id\": \"61d66a51459ad6009255da88\",\n" +
                "                \"order\": 6\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2217481,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"刘俊谦\",\n" +
                "                \"original_name\": \"Terrance Lau\",\n" +
                "                \"popularity\": 17.151,\n" +
                "                \"profile_path\": \"/12UjFLBUTopW0O9UB9f2TK18dSl.jpg\",\n" +
                "                \"cast_id\": 14,\n" +
                "                \"character\": \"Shin\",\n" +
                "                \"credit_id\": \"64361dc0651fcf0095e438cb\",\n" +
                "                \"order\": 7\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1455929,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"张文杰\",\n" +
                "                \"original_name\": \"Man Kit Cheung\",\n" +
                "                \"popularity\": 10.881,\n" +
                "                \"profile_path\": \"/k8FKv1WgRQfsJlGi4l3J8YyeMYQ.jpg\",\n" +
                "                \"cast_id\": 16,\n" +
                "                \"character\": \"AV\",\n" +
                "                \"credit_id\": \"64361e2a94631800e075b018\",\n" +
                "                \"order\": 8\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 1638505,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"廖子妤\",\n" +
                "                \"original_name\": \"Fish Liew\",\n" +
                "                \"popularity\": 8.451,\n" +
                "                \"profile_path\": \"/wouDO99Pw2tCjhRYCbck9qPuGo1.jpg\",\n" +
                "                \"cast_id\": 20,\n" +
                "                \"character\": \"Fanny\",\n" +
                "                \"credit_id\": \"660b93da15dea0017c344b3f\",\n" +
                "                \"order\": 9\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2684219,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"朱栢康\",\n" +
                "                \"original_name\": \"Chu Pak-Hong\",\n" +
                "                \"popularity\": 4.025,\n" +
                "                \"profile_path\": \"/hGpToKjLUDXbz69NjCriRcBHYv8.jpg\",\n" +
                "                \"cast_id\": 21,\n" +
                "                \"character\": \"Woman Beater\",\n" +
                "                \"credit_id\": \"661375689408ec017d2abe1d\",\n" +
                "                \"order\": 10\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1674432,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"胡子彤\",\n" +
                "                \"original_name\": \"Tony Wu\",\n" +
                "                \"popularity\": 4.667,\n" +
                "                \"profile_path\": \"/bWYyRg8eFxrOdsgzuC1guPtrLoO.jpg\",\n" +
                "                \"cast_id\": 15,\n" +
                "                \"character\": \"Twelfth Master\",\n" +
                "                \"credit_id\": \"64361e1a945d360077bf778b\",\n" +
                "                \"order\": 11\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 1529401,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"蔡思韵\",\n" +
                "                \"original_name\": \"Cecilia Choi\",\n" +
                "                \"popularity\": 13.29,\n" +
                "                \"profile_path\": \"/gOCyKz6VaHf4E4qYk70Rf45lz0y.jpg\",\n" +
                "                \"cast_id\": 28,\n" +
                "                \"character\": \"Jim's Wife\",\n" +
                "                \"credit_id\": \"664de882c7aa0dab00b99682\",\n" +
                "                \"order\": 12\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1334855,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"张松枝\",\n" +
                "                \"original_name\": \"張松枝\",\n" +
                "                \"popularity\": 11.822,\n" +
                "                \"profile_path\": \"/oib9cyzPvcT07Fg6v9yFHxcZuVr.jpg\",\n" +
                "                \"cast_id\": 30,\n" +
                "                \"character\": \"Chief\",\n" +
                "                \"credit_id\": \"66b7ba8757cb3a90dd3c0b35\",\n" +
                "                \"order\": 13\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4684605,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"乔靖夫\",\n" +
                "                \"original_name\": \"Jozev Lau\",\n" +
                "                \"popularity\": 0.677,\n" +
                "                \"profile_path\": \"/fy16sADv6nhxu4kc2vDbGd6r6GF.jpg\",\n" +
                "                \"cast_id\": 27,\n" +
                "                \"character\": \"Double Blade\",\n" +
                "                \"credit_id\": \"6630e2dc6aa8e001291da739\",\n" +
                "                \"order\": 14\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 137143,\n" +
                "                \"known_for_department\": \"Directing\",\n" +
                "                \"name\": \"罗永昌\",\n" +
                "                \"original_name\": \"Law Wing-Cheong\",\n" +
                "                \"popularity\": 3.52,\n" +
                "                \"profile_path\": \"/3MXEBNMNgaOxATtmPwupVAgS3qI.jpg\",\n" +
                "                \"cast_id\": 31,\n" +
                "                \"character\": \"Uncle Ling\",\n" +
                "                \"credit_id\": \"66c020028c623db5279dbe86\",\n" +
                "                \"order\": 15\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 144513,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Kam Loi-Kwan\",\n" +
                "                \"original_name\": \"Kam Loi-Kwan\",\n" +
                "                \"popularity\": 2.31,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 32,\n" +
                "                \"character\": \"Watermelon\",\n" +
                "                \"credit_id\": \"66c0203b4bafe2aa5181ce22\",\n" +
                "                \"order\": 16\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1415784,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"廖骏雄\",\n" +
                "                \"original_name\": \"廖駿雄\",\n" +
                "                \"popularity\": 6.702,\n" +
                "                \"profile_path\": \"/bLtpSLTjnrkmV5ZKSfsChfuNYwU.jpg\",\n" +
                "                \"cast_id\": 50,\n" +
                "                \"character\": \"Uncle Grouper\",\n" +
                "                \"credit_id\": \"66c59a15626f3fb71cd5cc74\",\n" +
                "                \"order\": 17\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4897157,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lee Tse-Hei\",\n" +
                "                \"original_name\": \"Lee Tse-Hei\",\n" +
                "                \"popularity\": 0.103,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 51,\n" +
                "                \"character\": \"Woman Beater's Girlfriend\",\n" +
                "                \"credit_id\": \"66c59a61082e3f7161af5a9c\",\n" +
                "                \"order\": 18\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4897192,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Wong Wan-Ching\",\n" +
                "                \"original_name\": \"Wong Wan-Ching\",\n" +
                "                \"popularity\": 0.335,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 60,\n" +
                "                \"character\": \"Fishball Girl\",\n" +
                "                \"credit_id\": \"66c59f4def4106389724fbac\",\n" +
                "                \"order\": 19\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4923974,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"彭凯筠\",\n" +
                "                \"original_name\": \"彭凱筠\",\n" +
                "                \"popularity\": 0.98,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 35,\n" +
                "                \"character\": \"Third Auntie\",\n" +
                "                \"credit_id\": \"66c02266ddf73fcdaae74bb5\",\n" +
                "                \"order\": 20\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4897166,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Cheung Chung-Hang\",\n" +
                "                \"original_name\": \"Cheung Chung-Hang\",\n" +
                "                \"popularity\": 0.103,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 52,\n" +
                "                \"character\": \"Cyclone's Man\",\n" +
                "                \"credit_id\": \"66c59af5fa382cd66ad1b0a0\",\n" +
                "                \"order\": 21\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4877946,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lai Tsz-Chun\",\n" +
                "                \"original_name\": \"Lai Tsz-Chun\",\n" +
                "                \"popularity\": 0.012,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 36,\n" +
                "                \"character\": \"Cyclone's Man\",\n" +
                "                \"credit_id\": \"66c0227b142ef42c6581d46a\",\n" +
                "                \"order\": 22\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4897168,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"John Ho\",\n" +
                "                \"original_name\": \"John Ho\",\n" +
                "                \"popularity\": 0.271,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 53,\n" +
                "                \"character\": \"Cyclone's Man\",\n" +
                "                \"credit_id\": \"66c59b4566b85c5a15af5da6\",\n" +
                "                \"order\": 23\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4849574,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Leung Sze-Ching\",\n" +
                "                \"original_name\": \"Leung Sze-Ching\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 54,\n" +
                "                \"character\": \"Cyclone's Man\",\n" +
                "                \"credit_id\": \"66c59b73f93d653e51d1b025\",\n" +
                "                \"order\": 24\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3039838,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Alex Lee Yip-Kin\",\n" +
                "                \"original_name\": \"Alex Lee Yip-Kin\",\n" +
                "                \"popularity\": 0.09,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 34,\n" +
                "                \"character\": \"Cyclone's Man\",\n" +
                "                \"credit_id\": \"66c021fcca8130fce10d62cf\",\n" +
                "                \"order\": 25\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1139659,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lam Ling Yuen\",\n" +
                "                \"original_name\": \"Lam Ling Yuen\",\n" +
                "                \"popularity\": 0.786,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 55,\n" +
                "                \"character\": \"Cyclone's Man\",\n" +
                "                \"credit_id\": \"66c59b886792832621f7394a\",\n" +
                "                \"order\": 26\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3516999,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Mak Lok San\",\n" +
                "                \"original_name\": \"Mak Lok San\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 56,\n" +
                "                \"character\": \"Corner Store Owner\",\n" +
                "                \"credit_id\": \"66c59caef7f98729c2d1b20d\",\n" +
                "                \"order\": 27\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 2673398,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Yee-Yee Yeung\",\n" +
                "                \"original_name\": \"Yee-Yee Yeung\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 57,\n" +
                "                \"character\": \"Mary\",\n" +
                "                \"credit_id\": \"66c59d993f38c897ebdfddd9\",\n" +
                "                \"order\": 28\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 38285,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"朱芷莹\",\n" +
                "                \"original_name\": \"朱芷瑩\",\n" +
                "                \"popularity\": 14.301,\n" +
                "                \"profile_path\": \"/zJs10JAZSn3upwBSjMHbWPdcwDH.jpg\",\n" +
                "                \"cast_id\": 58,\n" +
                "                \"character\": \"Mama-san\",\n" +
                "                \"credit_id\": \"66c59dc73599c6f0c3f73919\",\n" +
                "                \"order\": 29\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4897190,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Bernard Fung Bun-Yu\",\n" +
                "                \"original_name\": \"Bernard Fung Bun-Yu\",\n" +
                "                \"popularity\": 0.122,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 59,\n" +
                "                \"character\": \"Underground Boxer\",\n" +
                "                \"credit_id\": \"66c59e669f457fe19ddfde0c\",\n" +
                "                \"order\": 30\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4890606,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Li Ching-Sum\",\n" +
                "                \"original_name\": \"Li Ching-Sum\",\n" +
                "                \"popularity\": 0.004,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 65,\n" +
                "                \"character\": \"Chau's Wife\",\n" +
                "                \"credit_id\": \"66d7faa8fc63845fa3f2075f\",\n" +
                "                \"order\": 31\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922454,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Sin Tsz-Ching\",\n" +
                "                \"original_name\": \"Sin Tsz-Ching\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 61,\n" +
                "                \"character\": \"Chau's Child\",\n" +
                "                \"credit_id\": \"66d7f96f652143c2b74f4b8b\",\n" +
                "                \"order\": 32\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922456,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Leung Cheuk-Hin\",\n" +
                "                \"original_name\": \"Leung Cheuk-Hin\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 62,\n" +
                "                \"character\": \"Chau's Child\",\n" +
                "                \"credit_id\": \"66d7f97c8cee68d3329d2fac\",\n" +
                "                \"order\": 33\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922457,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Man Noi-Tik\",\n" +
                "                \"original_name\": \"Man Noi-Tik\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 63,\n" +
                "                \"character\": \"Chau's Child\",\n" +
                "                \"credit_id\": \"66d7f98aa155e9fc080ee463\",\n" +
                "                \"order\": 34\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3575545,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Gallan So Chi-Chiu\",\n" +
                "                \"original_name\": \"Gallan So Chi-Chiu\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 33,\n" +
                "                \"character\": \"Cocaine Dealer\",\n" +
                "                \"credit_id\": \"66c021b7544595899249ff6f\",\n" +
                "                \"order\": 35\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922461,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Willy Yeung Wai\",\n" +
                "                \"original_name\": \"Willy Yeung Wai\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 64,\n" +
                "                \"character\": \"Cocaine Dealer\",\n" +
                "                \"credit_id\": \"66d7fa21e4153086e91ab603\",\n" +
                "                \"order\": 36\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922466,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lau Shet-Ming\",\n" +
                "                \"original_name\": \"Lau Shet-Ming\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 66,\n" +
                "                \"character\": \"Potential Renter\",\n" +
                "                \"credit_id\": \"66d7fb9e306d58aab7604778\",\n" +
                "                \"order\": 37\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1400222,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"苏伟南\",\n" +
                "                \"original_name\": \"蘇偉南\",\n" +
                "                \"popularity\": 2.975,\n" +
                "                \"profile_path\": \"/qw7aaQL4DfLyoY6a3VADSYkQHvT.jpg\",\n" +
                "                \"cast_id\": 37,\n" +
                "                \"character\": \"Chau's Bodyguard\",\n" +
                "                \"credit_id\": \"66c022e4ca8130fce10d62ea\",\n" +
                "                \"order\": 38\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922470,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Chu Kwok-Tung\",\n" +
                "                \"original_name\": \"Chu Kwok-Tung\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 67,\n" +
                "                \"character\": \"Chau's Bodyguard\",\n" +
                "                \"credit_id\": \"66d7fbf11bf0f83f3e8f0347\",\n" +
                "                \"order\": 39\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4813245,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Vincent Tam Yu-Hong\",\n" +
                "                \"original_name\": \"Vincent Tam Yu-Hong\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 38,\n" +
                "                \"character\": \"Food Stall Staff\",\n" +
                "                \"credit_id\": \"66c02306d40033c8dec30c35\",\n" +
                "                \"order\": 40\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4812234,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Jonas Cheung Chi-Hang\",\n" +
                "                \"original_name\": \"Jonas Cheung Chi-Hang\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 39,\n" +
                "                \"character\": \"Food Stall Staff\",\n" +
                "                \"credit_id\": \"66c0231937187d549781d343\",\n" +
                "                \"order\": 41\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 3836792,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Ivy Pang\",\n" +
                "                \"original_name\": \"Ivy Pang\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 40,\n" +
                "                \"character\": \"Kite Kid's Mother\",\n" +
                "                \"credit_id\": \"66c023321d7a0aff134a0061\",\n" +
                "                \"order\": 42\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3270592,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"黄梓乐\",\n" +
                "                \"original_name\": \"Sean Wong Tsz-lok\",\n" +
                "                \"popularity\": 2.995,\n" +
                "                \"profile_path\": \"/LzqwpXTKzDk4ZCC1laiJcr0ufv.jpg\",\n" +
                "                \"cast_id\": 26,\n" +
                "                \"character\": \"Kite Kid\",\n" +
                "                \"credit_id\": \"6630e2689661fc012d656e04\",\n" +
                "                \"order\": 43\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922472,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Ma Yuen-Ching\",\n" +
                "                \"original_name\": \"Ma Yuen-Ching\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 68,\n" +
                "                \"character\": \"King's Man\",\n" +
                "                \"credit_id\": \"66d7fc6106d231eed4096657\",\n" +
                "                \"order\": 44\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922474,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Raymond Li Man-Young\",\n" +
                "                \"original_name\": \"Raymond Li Man-Young\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 69,\n" +
                "                \"character\": \"King's Man\",\n" +
                "                \"credit_id\": \"66d7fc90ed918364606ce29a\",\n" +
                "                \"order\": 45\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4897045,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lam Ting-Hin\",\n" +
                "                \"original_name\": \"Lam Ting-Hin\",\n" +
                "                \"popularity\": 0.008,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 70,\n" +
                "                \"character\": \"King's Man\",\n" +
                "                \"credit_id\": \"66d7fc9b818c4481480ee5c0\",\n" +
                "                \"order\": 46\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922475,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Wilson Tam Wai-Keung\",\n" +
                "                \"original_name\": \"Wilson Tam Wai-Keung\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 71,\n" +
                "                \"character\": \"King's Man\",\n" +
                "                \"credit_id\": \"66d7fd4218fc37104926ebea\",\n" +
                "                \"order\": 47\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922476,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Che Chun-Hoi\",\n" +
                "                \"original_name\": \"Che Chun-Hoi\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 72,\n" +
                "                \"character\": \"King's Man\",\n" +
                "                \"credit_id\": \"66d7fe0e3d66f12f92f20765\",\n" +
                "                \"order\": 48\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4891010,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Kelvin Wong Ting-Kong\",\n" +
                "                \"original_name\": \"Kelvin Wong Ting-Kong\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 73,\n" +
                "                \"character\": \"King's Man\",\n" +
                "                \"credit_id\": \"66d7fe7828871ee4404f4813\",\n" +
                "                \"order\": 49\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3094651,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Samson Tsang Chung-Yuen\",\n" +
                "                \"original_name\": \"Samson Tsang Chung-Yuen\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 41,\n" +
                "                \"character\": \"Tiger's Man\",\n" +
                "                \"credit_id\": \"66c02392bb9e174a820d62f4\",\n" +
                "                \"order\": 50\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1723883,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Law Chi-Sing\",\n" +
                "                \"original_name\": \"Law Chi-Sing\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 74,\n" +
                "                \"character\": \"Tiger's Man\",\n" +
                "                \"credit_id\": \"66d7ff6f86c6b4cd3829e136\",\n" +
                "                \"order\": 51\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922483,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Tong Sik-Ming\",\n" +
                "                \"original_name\": \"Tong Sik-Ming\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 75,\n" +
                "                \"character\": \"Tiger's Man\",\n" +
                "                \"credit_id\": \"66d7ffcad7c19ea866b8afa1\",\n" +
                "                \"order\": 52\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922489,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Man Kwok-Hei\",\n" +
                "                \"original_name\": \"Man Kwok-Hei\",\n" +
                "                \"popularity\": 0.84,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 76,\n" +
                "                \"character\": \"Tiger's Man\",\n" +
                "                \"credit_id\": \"66d8001b306d58aab76047a0\",\n" +
                "                \"order\": 53\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1629648,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Rodney Wong\",\n" +
                "                \"original_name\": \"Rodney Wong\",\n" +
                "                \"popularity\": 2.237,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 42,\n" +
                "                \"character\": \"Tiger's Man\",\n" +
                "                \"credit_id\": \"66c023aa18e6622adcd31e5d\",\n" +
                "                \"order\": 54\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2774515,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Albert Leung\",\n" +
                "                \"original_name\": \"Albert Leung\",\n" +
                "                \"popularity\": 1.376,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 77,\n" +
                "                \"character\": \"Tiger's Man\",\n" +
                "                \"credit_id\": \"66d8004fdf474cf8778f028b\",\n" +
                "                \"order\": 55\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4043705,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Juliana Wong Pui-Chun\",\n" +
                "                \"original_name\": \"Juliana Wong Pui-Chun\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 78,\n" +
                "                \"character\": \"Double Blade's Wife\",\n" +
                "                \"credit_id\": \"66d800c25c5a6b2d049d2eaa\",\n" +
                "                \"order\": 56\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3282633,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Wai Sum Lai\",\n" +
                "                \"original_name\": \"Wai Sum Lai\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 79,\n" +
                "                \"character\": \"Bonesetter\",\n" +
                "                \"credit_id\": \"66d801186b4c464887b8ae26\",\n" +
                "                \"order\": 57\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3040520,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Jason Yip Wan-Keung\",\n" +
                "                \"original_name\": \"Jason Yip Wan-Keung\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 80,\n" +
                "                \"character\": \"Bus Driver\",\n" +
                "                \"credit_id\": \"66d8015c3d66f12f92f20786\",\n" +
                "                \"order\": 58\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922493,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Chan Fu-Shing\",\n" +
                "                \"original_name\": \"Chan Fu-Shing\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 81,\n" +
                "                \"character\": \"BBQ Meat Restaurant Staff\",\n" +
                "                \"credit_id\": \"66d801967a7985a620604638\",\n" +
                "                \"order\": 59\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2491527,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Toby Wong Chun-Lam\",\n" +
                "                \"original_name\": \"Toby Wong Chun-Lam\",\n" +
                "                \"popularity\": 1.14,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 82,\n" +
                "                \"character\": \"BBQ Meat Restaurant Staff\",\n" +
                "                \"credit_id\": \"66d801b9fc63845fa3f20780\",\n" +
                "                \"order\": 60\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4879066,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Suen Tsz-Wai\",\n" +
                "                \"original_name\": \"Suen Tsz-Wai\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 83,\n" +
                "                \"character\": \"BBQ Meat Restaurant Staff\",\n" +
                "                \"credit_id\": \"66d801d1463a1e11b0604802\",\n" +
                "                \"order\": 61\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4922495,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Natalie Yu Ka-Nam\",\n" +
                "                \"original_name\": \"Natalie Yu Ka-Nam\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 84,\n" +
                "                \"character\": \"Soda Pop Girl\",\n" +
                "                \"credit_id\": \"66d801f7d225c7113cf2078d\",\n" +
                "                \"order\": 62\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3055710,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Jason Li\",\n" +
                "                \"original_name\": \"Jason Li\",\n" +
                "                \"popularity\": 1.352,\n" +
                "                \"profile_path\": \"/1pHciFVfOGappfxhvxBeAQsSKZ.jpg\",\n" +
                "                \"cast_id\": 85,\n" +
                "                \"character\": \"Cocaine Joint Minion\",\n" +
                "                \"credit_id\": \"66d80295a2a821c71126e78c\",\n" +
                "                \"order\": 63\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922509,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Chan Chi-Sing\",\n" +
                "                \"original_name\": \"Chan Chi-Sing\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 86,\n" +
                "                \"character\": \"Cocaine Joint Minion\",\n" +
                "                \"credit_id\": \"66d804edb228ac48180ee379\",\n" +
                "                \"order\": 64\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922515,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Tsang Tsz-Kit\",\n" +
                "                \"original_name\": \"Tsang Tsz-Kit\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 87,\n" +
                "                \"character\": \"Cocaine Joint Minion\",\n" +
                "                \"credit_id\": \"66d8058ab542d47f8047452f\",\n" +
                "                \"order\": 65\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4671915,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Mak Ka-Ho\",\n" +
                "                \"original_name\": \"Mak Ka-Ho\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 43,\n" +
                "                \"character\": \"Cocaine Joint Minion\",\n" +
                "                \"credit_id\": \"66c023f9a632513e671f4f2e\",\n" +
                "                \"order\": 66\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4645787,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Chung Wai-Kit\",\n" +
                "                \"original_name\": \"Chung Wai-Kit\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 88,\n" +
                "                \"character\": \"Cocaine Joint Minion\",\n" +
                "                \"credit_id\": \"66d805f2be7c82e3eeb8aeb9\",\n" +
                "                \"order\": 67\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922522,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Andrew Chan\",\n" +
                "                \"original_name\": \"Andrew Chan\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 89,\n" +
                "                \"character\": \"Spirit Power Ceremony Assistant\",\n" +
                "                \"credit_id\": \"66d8063c0c711cc77429e0bc\",\n" +
                "                \"order\": 68\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922523,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Cheung Lok-Man\",\n" +
                "                \"original_name\": \"Cheung Lok-Man\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 90,\n" +
                "                \"character\": \"Spirit Power Ceremony Assistant\",\n" +
                "                \"credit_id\": \"66d806517666822ac19d7f9f\",\n" +
                "                \"order\": 69\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3040529,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Yu Tat Chi\",\n" +
                "                \"original_name\": \"Yu Tat Chi\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 91,\n" +
                "                \"character\": \"Hair Salon Staff\",\n" +
                "                \"credit_id\": \"66d806b0d7c19ea866b8afca\",\n" +
                "                \"order\": 70\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4849594,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Cheuk Yan\",\n" +
                "                \"original_name\": \"Cheuk Yan\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 92,\n" +
                "                \"character\": \"Hair Salon Staff\",\n" +
                "                \"credit_id\": \"66d806d528871ee4404f484b\",\n" +
                "                \"order\": 71\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1357559,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"刘宗基\",\n" +
                "                \"original_name\": \"Frank Liu Zong-Ji\",\n" +
                "                \"popularity\": 1.29,\n" +
                "                \"profile_path\": \"/qcKUMh2Rn7n9f2fBQ5ZsJNwi25s.jpg\",\n" +
                "                \"cast_id\": 44,\n" +
                "                \"character\": \"Immigration Department Staff\",\n" +
                "                \"credit_id\": \"66c0242670c6709c0b3acdff\",\n" +
                "                \"order\": 72\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3191697,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Fung King-Chung\",\n" +
                "                \"original_name\": \"Fung King-Chung\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 93,\n" +
                "                \"character\": \"Immigration Department Staff\",\n" +
                "                \"credit_id\": \"66d806fba155e9fc080ee4b1\",\n" +
                "                \"order\": 73\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922559,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Chau Huat-Tian\",\n" +
                "                \"original_name\": \"Chau Huat-Tian\",\n" +
                "                \"popularity\": 1.62,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 94,\n" +
                "                \"character\": \"Chiu Chow Musician\",\n" +
                "                \"credit_id\": \"66d809dbbcfa3188619d2f6b\",\n" +
                "                \"order\": 74\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922560,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Wong Chi-Fei\",\n" +
                "                \"original_name\": \"Wong Chi-Fei\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 95,\n" +
                "                \"character\": \"Chiu Chow Musician\",\n" +
                "                \"credit_id\": \"66d809e83e7e2814f08f0078\",\n" +
                "                \"order\": 75\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922562,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lo Sau-Chun\",\n" +
                "                \"original_name\": \"Lo Sau-Chun\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 96,\n" +
                "                \"character\": \"Chiu Chow Musician\",\n" +
                "                \"credit_id\": \"66d809f3867ff3c0ad60441f\",\n" +
                "                \"order\": 76\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922563,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Tang Siu-Cheung\",\n" +
                "                \"original_name\": \"Tang Siu-Cheung\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 97,\n" +
                "                \"character\": \"Chiu Chow Musician\",\n" +
                "                \"credit_id\": \"66d80a02709fbebe421ab6dc\",\n" +
                "                \"order\": 77\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922565,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Ma Yuk-Lam\",\n" +
                "                \"original_name\": \"Ma Yuk-Lam\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 98,\n" +
                "                \"character\": \"Chiu Chow Musician\",\n" +
                "                \"credit_id\": \"66d80a1076b129b261b8b200\",\n" +
                "                \"order\": 78\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4849531,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Zhang Wanfen\",\n" +
                "                \"original_name\": \"Zhang Wanfen\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 99,\n" +
                "                \"character\": \"Temple Lady\",\n" +
                "                \"credit_id\": \"66d80a6b878cfc5bf70ee0ff\",\n" +
                "                \"order\": 79\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 3190617,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Emil Chan Cheuk-Wah\",\n" +
                "                \"original_name\": \"Emil Chan Cheuk-Wah\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 100,\n" +
                "                \"character\": \"Tiger's Man\",\n" +
                "                \"credit_id\": \"66d80aa0f41610bde2604ab7\",\n" +
                "                \"order\": 80\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922576,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Poon Hing-Yan\",\n" +
                "                \"original_name\": \"Poon Hing-Yan\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 101,\n" +
                "                \"character\": \"Tiger's Man\",\n" +
                "                \"credit_id\": \"66d80ae23a94a49c1626ea1b\",\n" +
                "                \"order\": 81\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922580,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Fung Chi-Ngong\",\n" +
                "                \"original_name\": \"Fung Chi-Ngong\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 102,\n" +
                "                \"character\": \"Prison Staff\",\n" +
                "                \"credit_id\": \"66d80b1b06d231eed40966c4\",\n" +
                "                \"order\": 82\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2479295,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Stanly Ma\",\n" +
                "                \"original_name\": \"Stanly Ma\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 45,\n" +
                "                \"character\": \"Prison Staff\",\n" +
                "                \"credit_id\": \"66c0249d02f320957a1f512b\",\n" +
                "                \"order\": 83\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1172955,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"黄华和\",\n" +
                "                \"original_name\": \"Jimmy Wong Wa-Wo\",\n" +
                "                \"popularity\": 6.623,\n" +
                "                \"profile_path\": \"/yMslRhiLFNyZlEFJDH4aARVNut5.jpg\",\n" +
                "                \"cast_id\": 46,\n" +
                "                \"character\": \"Walled City Landlord\",\n" +
                "                \"credit_id\": \"66c024c42a735b66790d6321\",\n" +
                "                \"order\": 84\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2732132,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Clifford Tsang Man-Wai\",\n" +
                "                \"original_name\": \"Clifford Tsang Man-Wai\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 47,\n" +
                "                \"character\": \"Walled City Landlord\",\n" +
                "                \"credit_id\": \"66c024d1434b97eafdc30d52\",\n" +
                "                \"order\": 85\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4803838,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lam Wai Nin\",\n" +
                "                \"original_name\": \"Lam Wai Nin\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 48,\n" +
                "                \"character\": \"Walled City Landlord\",\n" +
                "                \"credit_id\": \"66c024ec768b03c06aad8dd6\",\n" +
                "                \"order\": 86\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4672235,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Wong Chi-Kong\",\n" +
                "                \"original_name\": \"Wong Chi-Kong\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 103,\n" +
                "                \"character\": \"TV Program Expert\",\n" +
                "                \"credit_id\": \"66d80b8d2fb264914d4745df\",\n" +
                "                \"order\": 87\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2158983,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Johnny Lam Ming-Yiu\",\n" +
                "                \"original_name\": \"Johnny Lam Ming-Yiu\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 104,\n" +
                "                \"character\": \"TV Program Expert\",\n" +
                "                \"credit_id\": \"66d80b9806d231eed40966cd\",\n" +
                "                \"order\": 88\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2916350,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Wong Ngar-Man\",\n" +
                "                \"original_name\": \"Wong Ngar-Man\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 105,\n" +
                "                \"character\": \"TV Program Host\",\n" +
                "                \"credit_id\": \"66d80bdccc1479a16029ddc2\",\n" +
                "                \"order\": 89\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4922601,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Zhang Da\",\n" +
                "                \"original_name\": \"Zhang Da\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 106,\n" +
                "                \"character\": \"Armed Robber\",\n" +
                "                \"credit_id\": \"66d80c436c474277338f0585\",\n" +
                "                \"order\": 90\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 2850257,\n" +
                "                \"known_for_department\": \"Crew\",\n" +
                "                \"name\": \"Tommy Leung Pok-Yan\",\n" +
                "                \"original_name\": \"Tommy Leung Pok-Yan\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 107,\n" +
                "                \"character\": \"Armed Robber\",\n" +
                "                \"credit_id\": \"66d80c60bcfa3188619d2f78\",\n" +
                "                \"order\": 91\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922603,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Chan Kwan-Fung\",\n" +
                "                \"original_name\": \"Chan Kwan-Fung\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 108,\n" +
                "                \"character\": \"Armed Robber\",\n" +
                "                \"credit_id\": \"66d80c7f3ed64d469f29e072\",\n" +
                "                \"order\": 92\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1972778,\n" +
                "                \"known_for_department\": \"Crew\",\n" +
                "                \"name\": \"Shun Yamashita\",\n" +
                "                \"original_name\": \"Shun Yamashita\",\n" +
                "                \"popularity\": 5.837,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 109,\n" +
                "                \"character\": \"Armed Robber\",\n" +
                "                \"credit_id\": \"66d80cda3ed64d469f29e076\",\n" +
                "                \"order\": 93\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4922604,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Chung Shiu-Wah\",\n" +
                "                \"original_name\": \"Chung Shiu-Wah\",\n" +
                "                \"popularity\": 1.62,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 110,\n" +
                "                \"character\": \"Chau's Maid\",\n" +
                "                \"credit_id\": \"66d80d0fdbbc782074182826\",\n" +
                "                \"order\": 94\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4812097,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Tang Lu\",\n" +
                "                \"original_name\": \"Tang Lu\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 111,\n" +
                "                \"character\": \"Chau's Maid\",\n" +
                "                \"credit_id\": \"66d80d1bb05536ab69b8b574\",\n" +
                "                \"order\": 95\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1619792,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Wong Sze-Yan\",\n" +
                "                \"original_name\": \"Wong Sze-Yan\",\n" +
                "                \"popularity\": 1.054,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 112,\n" +
                "                \"character\": \"Chinese Medicine Physician\",\n" +
                "                \"credit_id\": \"66d80d431e030516ca29e37f\",\n" +
                "                \"order\": 96\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4043728,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lai Chai-ming\",\n" +
                "                \"original_name\": \"Lai Chai-ming\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 113,\n" +
                "                \"character\": \"Western Doctor\",\n" +
                "                \"credit_id\": \"66d80d69d51a81c66826e998\",\n" +
                "                \"order\": 97\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4922605,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Benjamin Sze-Ma Shing-Hei\",\n" +
                "                \"original_name\": \"Benjamin Sze-Ma Shing-Hei\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 114,\n" +
                "                \"character\": \"Jim's Son\",\n" +
                "                \"credit_id\": \"66d80d9ca6bde0d9ad26e9cc\",\n" +
                "                \"order\": 98\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 1356773,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Giselle Lam\",\n" +
                "                \"original_name\": \"Giselle Lam\",\n" +
                "                \"popularity\": 4.022,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 49,\n" +
                "                \"character\": \"News Anchor\",\n" +
                "                \"credit_id\": \"66c02533768b03c06aad8dde\",\n" +
                "                \"order\": 99\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 4922608,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Yeung Ching-Man\",\n" +
                "                \"original_name\": \"Yeung Ching-Man\",\n" +
                "                \"popularity\": 1.22,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 115,\n" +
                "                \"character\": \"Private Nurse\",\n" +
                "                \"credit_id\": \"66d80e07a2a821c71126e7e3\",\n" +
                "                \"order\": 100\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 4693321,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"Lam Chun-Kit\",\n" +
                "                \"original_name\": \"Lam Chun-Kit\",\n" +
                "                \"popularity\": 0.001,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 116,\n" +
                "                \"character\": \"Food Stall Staff\",\n" +
                "                \"credit_id\": \"66d80e3511a5a563859d2be4\",\n" +
                "                \"order\": 101\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 2061289,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"梁卓美\",\n" +
                "                \"original_name\": \"梁卓美\",\n" +
                "                \"popularity\": 4.974,\n" +
                "                \"profile_path\": null,\n" +
                "                \"cast_id\": 117,\n" +
                "                \"character\": \"Temple Old Lady\",\n" +
                "                \"credit_id\": \"66d80e6b09b40837110965c0\",\n" +
                "                \"order\": 102\n" +
                "            }\n" +
                "        ],\n" +
                "        \"crew\": [\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1113443,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"谷垣健治\",\n" +
                "                \"original_name\": \"Kenji Tanigaki\",\n" +
                "                \"popularity\": 4.828,\n" +
                "                \"profile_path\": null,\n" +
                "                \"credit_id\": \"62e252a2da10f00135312e6d\",\n" +
                "                \"department\": \"Directing\",\n" +
                "                \"job\": \"Action Director\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 1,\n" +
                "                \"id\": 135830,\n" +
                "                \"known_for_department\": \"Writing\",\n" +
                "                \"name\": \"欧健儿\",\n" +
                "                \"original_name\": \"Au Kin-yee\",\n" +
                "                \"popularity\": 6.204,\n" +
                "                \"profile_path\": \"/aT09fkIAKVe18p6SKwsjOC35QLe.jpg\",\n" +
                "                \"credit_id\": \"61d66af1c15f8900686626fd\",\n" +
                "                \"department\": \"Writing\",\n" +
                "                \"job\": \"Screenplay\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1536908,\n" +
                "                \"known_for_department\": \"Writing\",\n" +
                "                \"name\": \"陈大利\",\n" +
                "                \"original_name\": \"Tai-lee Chan\",\n" +
                "                \"popularity\": 9.799,\n" +
                "                \"profile_path\": \"/jENyP86Zm1FZXxWhqro34Owf9cl.jpg\",\n" +
                "                \"credit_id\": \"61d66b04e194b0008cfb60f6\",\n" +
                "                \"department\": \"Writing\",\n" +
                "                \"job\": \"Screenplay\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 64487,\n" +
                "                \"known_for_department\": \"Production\",\n" +
                "                \"name\": \"庄澄\",\n" +
                "                \"original_name\": \"John Chong\",\n" +
                "                \"popularity\": 3.985,\n" +
                "                \"profile_path\": \"/du89FDdXNfxyp20afB3tZ1kAOtk.jpg\",\n" +
                "                \"credit_id\": \"61d66b3f899da2006771e062\",\n" +
                "                \"department\": \"Production\",\n" +
                "                \"job\": \"Producer\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 93991,\n" +
                "                \"known_for_department\": \"Directing\",\n" +
                "                \"name\": \"郑保瑞\",\n" +
                "                \"original_name\": \"鄭保瑞\",\n" +
                "                \"popularity\": 22.346,\n" +
                "                \"profile_path\": \"/5PKTT0o0Sbkuo2m2I9iooOALYug.jpg\",\n" +
                "                \"credit_id\": \"61d66adcbb260200850e0dd9\",\n" +
                "                \"department\": \"Directing\",\n" +
                "                \"job\": \"Director\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 63571,\n" +
                "                \"known_for_department\": \"Directing\",\n" +
                "                \"name\": \"叶伟信\",\n" +
                "                \"original_name\": \"葉偉信\",\n" +
                "                \"popularity\": 16.685,\n" +
                "                \"profile_path\": \"/eGl16WEAQs78qMBJE1CHSDZgYvq.jpg\",\n" +
                "                \"credit_id\": \"61d66b4c1b722c008bfd8f3f\",\n" +
                "                \"department\": \"Production\",\n" +
                "                \"job\": \"Producer\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 25240,\n" +
                "                \"known_for_department\": \"Camera\",\n" +
                "                \"name\": \"郑兆强\",\n" +
                "                \"original_name\": \"Cheng Siu-keung\",\n" +
                "                \"popularity\": 5.951,\n" +
                "                \"profile_path\": \"/b5AaGHDthGXTNF5RFlsk0WphhC0.jpg\",\n" +
                "                \"credit_id\": \"61d66b5dc15f8900686628ab\",\n" +
                "                \"department\": \"Crew\",\n" +
                "                \"job\": \"Cinematography\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 1654224,\n" +
                "                \"known_for_department\": \"Writing\",\n" +
                "                \"name\": \"岑君茜\",\n" +
                "                \"original_name\": \"Sam Shum Kwan-sin\",\n" +
                "                \"popularity\": 2.241,\n" +
                "                \"profile_path\": null,\n" +
                "                \"credit_id\": \"660b91e435818f017c3a6081\",\n" +
                "                \"department\": \"Writing\",\n" +
                "                \"job\": \"Screenplay\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 0,\n" +
                "                \"id\": 4629067,\n" +
                "                \"known_for_department\": \"Writing\",\n" +
                "                \"name\": \"Lai Chun\",\n" +
                "                \"original_name\": \"Lai Chun\",\n" +
                "                \"popularity\": 1.235,\n" +
                "                \"profile_path\": null,\n" +
                "                \"credit_id\": \"660b920bc8a5ac017c791fd2\",\n" +
                "                \"department\": \"Writing\",\n" +
                "                \"job\": \"Screenplay\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 1113443,\n" +
                "                \"known_for_department\": \"Acting\",\n" +
                "                \"name\": \"谷垣健治\",\n" +
                "                \"original_name\": \"Kenji Tanigaki\",\n" +
                "                \"popularity\": 4.828,\n" +
                "                \"profile_path\": null,\n" +
                "                \"credit_id\": \"66229bd10231f2017c1361d9\",\n" +
                "                \"department\": \"Crew\",\n" +
                "                \"job\": \"Choreographer\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 57304,\n" +
                "                \"known_for_department\": \"Sound\",\n" +
                "                \"name\": \"川井宪次\",\n" +
                "                \"original_name\": \"川井憲次\",\n" +
                "                \"popularity\": 8.253,\n" +
                "                \"profile_path\": \"/uKjPUmOMmYYMFYtLBNXBDPAtcOz.jpg\",\n" +
                "                \"credit_id\": \"6622b2feccde04014b054e47\",\n" +
                "                \"department\": \"Sound\",\n" +
                "                \"job\": \"Original Music Composer\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"adult\": false,\n" +
                "                \"gender\": 2,\n" +
                "                \"id\": 77705,\n" +
                "                \"known_for_department\": \"Art\",\n" +
                "                \"name\": \"麦国强\",\n" +
                "                \"original_name\": \"麥國強\",\n" +
                "                \"popularity\": 7.201,\n" +
                "                \"profile_path\": null,\n" +
                "                \"credit_id\": \"66214be77a3c52017d4ca0cb\",\n" +
                "                \"department\": \"Art\",\n" +
                "                \"job\": \"Art Direction\"\n" +
                "            }\n" +
                "        ]\n" +
                "    }\n" +
                "}";
        JSONObject json = JSON.parseObject(jsonStr);
        List<String> lists = Lists.newArrayList();
        recursionJson(json, lists);
        System.out.println(JSON.toJSONString(lists));
    }
}
