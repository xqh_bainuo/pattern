package org.xqh.utils.oss;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

/**
 * @ClassName AmazonS3Utils
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/3/27 21:09
 * @Version 1.0
 */
public class AmazonS3Utils {

    public static String accessKey = "";
    public static String secretKey = "";
    public static String region = "";

    public static void main(String[] args) {
        BasicAWSCredentials creds = new BasicAWSCredentials(accessKey, secretKey);
        AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(creds)).build();
    }
}
