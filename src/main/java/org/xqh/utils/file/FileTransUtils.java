package org.xqh.utils.file;

import org.apache.commons.io.FileUtils;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;
import org.xqh.utils.encrypt.RSAUtils;

import java.io.*;

/**
 * @ClassName FileTransUtils
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/5/17 16:54
 * @Version 1.0
 */
public class FileTransUtils {

    public static File multipartFileTransToFile(MultipartFile mFile, String path) throws IOException {
        File file = new File(path);

        FileUtils.copyInputStreamToFile(mFile.getInputStream(), file);
        return file;
    }

    public static MultipartFile fileTransToMultipartFile(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);
        MultipartFile multipartFile = new MockMultipartFile(file.getName(), inputStream);
        return multipartFile;
    }

    public static void main111(String[] args) throws IOException {
        File f = new File("D:\\work\\test\\test.jpg");
        MultipartFile file = fileTransToMultipartFile(f);
        File f2 = new File("D:\\work\\test\\test2.jpg");
        FileUtils.writeByteArrayToFile(f2, file.getBytes());

    }

    public static void main(String[] args) {
        String str = "eyJhY3RpdmF0ZVRpbWUiOjE2MjEwNzAwODMwMDAsImRlc2NyaXB0aW9uIjoiMjIyMjIyMiIsImV4cGlyZVRpbWUiOjE2NTI3ODA5NDgwMDAsIm5hbWUiOiIz54K55LqG6aWu6Iy25YWIIiwic3RhdHVzIjowLCJzeXNTZXJ2ZXJMaXN0IjpbeyJzZXJ2ZXJDb2RlIjoicHJpY2VfY29kZSIsInNlcnZlck5hbWUiOiIiLCJzeXN0ZW1OYW1lIjoiRUhN57O757ufIiwidW5pdCI6IiIsInZhbHVlIjoiMSJ9LHsic2VydmVyQ29kZSI6Im1zZ19saW1pdCIsInNlcnZlck5hbWUiOiIiLCJzeXN0ZW1OYW1lIjoiRUhN57O757ufIiwidW5pdCI6IiIsInZhbHVlIjoiMSJ9LHsic2VydmVyQ29kZSI6ImRldmljZV9saW1pdCIsInNlcnZlck5hbWUiOiIiLCJzeXN0ZW1OYW1lIjoiRUhN57O757ufIiwidW5pdCI6IiIsInZhbHVlIjoiMSJ9XX0=";
        String dstr = RSAUtils.base64DecodeStr(str);
        System.out.println(dstr);

        String s2 = "{\"activateTime\":1621070083000,\"description\":\"2222222\",\"expireTime\":1652780948000,\"name\":\"3点了饮茶先\",\"status\":1,\"sysServerList\":[{\"serverCode\":\"price_code\",\"serverName\":\"\",\"systemName\":\"EHM系统\",\"unit\":\"\",\"value\":\"1\"},{\"serverCode\":\"msg_limit\",\"serverName\":\"\",\"systemName\":\"EHM系统\",\"unit\":\"\",\"value\":\"1\"},{\"serverCode\":\"device_limit\",\"serverName\":\"\",\"systemName\":\"EHM系统\",\"unit\":\"\",\"value\":\"1\"}]}";
        System.out.println(RSAUtils.base64EncodeStr(s2));
    }
}
