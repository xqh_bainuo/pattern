package org.xqh.utils;

/**
 * @ClassName CharacterFilterUtils
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/5/17 10:15
 * @Version 1.0
 */
public class CharacterFilterUtils {

    /**
     * ascii 码范围
     * 中文  [19968,40869]
     * 数字
     * @param str
     * @param asciiStart
     * @param asciiEnd
     * @return
     */
    public static String characterFilter(String str, int asciiStart, int asciiEnd){
        return null;
    }

    public static void main(String[] args) {
        String text = "中文abc123,.";
        char[] chars = text.toCharArray();
        for(char c:chars){
            System.out.println("ascii码: " + (int)c);
        }

        char i = 19968;
        System.out.println(asciiToString("40870"));
    }

    /**
     * 字符串转换为Ascii
     * @param value
     * @return
     */
    public static String stringToAscii(String value)
    {
        StringBuffer sbu = new StringBuffer();
        char[] chars = value.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if(i != chars.length - 1)
            {
                sbu.append((int)chars[i]).append(",");
            }
            else {
                sbu.append((int)chars[i]);
            }
        }
        return sbu.toString();
    }

    /**
     * Ascii转换为字符串
     * @param value
     * @return
     */
    public static String asciiToString(String value)
    {
        StringBuffer sbu = new StringBuffer();
        String[] chars = value.split(",");
        for (int i = 0; i < chars.length; i++) {
            sbu.append((char) Integer.parseInt(chars[i]));
        }
        return sbu.toString();
    }
}
