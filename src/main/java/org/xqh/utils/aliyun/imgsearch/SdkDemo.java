package org.xqh.utils.aliyun.imgsearch;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.imagesearch.model.v20190325.*;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hq90172 on 2019/3/25
 */
public class SdkDemo {
    private IAcsClient client;

    private static final String INSTANCE = "xqhimagesearch0627";

    private static final String ADD_IMG = "E:\\document\\images\\light\\微信图片_20191028094343.jpg";

    private static final String SEARCH_IMG = "E:\\document\\images\\light\\微信图片_20191028094430.jpg";

    private static final String default_product_id = "test";

    private static final String batch_upload_dir = "E:\\document\\light\\test";//批量上传图片目录

    public SdkDemo() {
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");

        IClientProfile profile = DefaultProfile.getProfile("cn-shanghai",
                "LTAI4Fdp3zMdKxeuXAGgQ8ZV", "lo8EghU8oVnpS0S4y15pxKoPxS6wfU");
        DefaultProfile.addEndpoint("cn-shanghai", "ImageSearch", "imagesearch.cn-shanghai.aliyuncs.com");
        this.client = new DefaultAcsClient(profile);
    }

    public static void main(String[] args) throws Exception {
        SdkDemo demo = new SdkDemo();
//        demo.batchAddFile();

        // 添加图片
//        demo.add();

//        // 新添加的图片及时生效，通常1s左右即可搜索到
//        Thread.sleep(1000);
//        // 根据图片搜索图片
        String result = demo.batchSearch();
//        // 根据已入库的图片（ProductId+PicName）搜索图片
        demo.parseResult(result);
//        demo.searchByName();
//
//        // 删除图片
//        demo.delete();
    }

    public void parseResult(String searchRet){
//        String searchRet = "[{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test001.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test001.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test001.jpg\\\"},{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test011.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test011.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.95078182220459;172\\\",\\\"strAttr\\\":\\\"test011.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":143},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":88888888},\\\"requestId\\\":\\\"ADD50192-5D66-4E13-9312-B072D6CE5A59\\\",\\\"success\\\":true}\",\"searchPic\":\"test001.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test002.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test002.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test002.jpg\\\"},{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test014.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test014.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"6.07930326461792;150\\\",\\\"strAttr\\\":\\\"test014.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":139},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":88888888},\\\"requestId\\\":\\\"59FD31F5-6DE1-416A-AAC2-126D9CDB2F08\\\",\\\"success\\\":true}\",\"searchPic\":\"test002.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test003.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test003.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test003.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test009.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test009.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"4.14222621917725;183\\\",\\\"strAttr\\\":\\\"test009.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":107},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"41384EC9-E5B3-4A3E-9226-C0ECA3AFF764\\\",\\\"success\\\":true}\",\"searchPic\":\"test003.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test004.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test004.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test004.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test006.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test006.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"10.9075546264648;94\\\",\\\"strAttr\\\":\\\"test006.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":119},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"4C9612A8-F2D4-4298-B647-60AE341A6B52\\\",\\\"success\\\":true}\",\"searchPic\":\"test004.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test005.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test005.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test005.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test004.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test004.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"10.644097328186;99\\\",\\\"strAttr\\\":\\\"test004.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":105},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"5AEF04C9-1E76-4A47-B985-9AF7989C912F\\\",\\\"success\\\":true}\",\"searchPic\":\"test005.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test006.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test006.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test006.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test007.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test007.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"19.6551303863525;93\\\",\\\"strAttr\\\":\\\"test007.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":110},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"27BE4391-2DD3-43A0-A0E8-011565E27E0E\\\",\\\"success\\\":true}\",\"searchPic\":\"test006.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test007.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test007.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test007.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test006.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test006.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"19.6551303863525;93\\\",\\\"strAttr\\\":\\\"test006.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":114},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"65638E48-E6FA-4A10-895B-C8DBE5E4E7DA\\\",\\\"success\\\":true}\",\"searchPic\":\"test007.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test008.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test008.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test008.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test013.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test013.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"8.28189945220947;154\\\",\\\"strAttr\\\":\\\"test013.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":116},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"4847F2D6-35E6-45E1-AF1F-AE3EE56FFB43\\\",\\\"success\\\":true}\",\"searchPic\":\"test008.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test009.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test009.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test009.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test006.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test006.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"16.0743465423584;97\\\",\\\"strAttr\\\":\\\"test006.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":108},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"303027AB-DD6C-4D7E-A584-91726FFF6073\\\",\\\"success\\\":true}\",\"searchPic\":\"test009.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test010.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test010.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test010.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test005.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test005.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.48848152160645;172\\\",\\\"strAttr\\\":\\\"test005.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":108},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"0D4D406E-1B97-4FB1-834F-CA3D954F1431\\\",\\\"success\\\":true}\",\"searchPic\":\"test010.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test011.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test011.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test011.jpg\\\"},{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test001.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test001.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.95078182220459;172\\\",\\\"strAttr\\\":\\\"test001.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":133},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":88888888},\\\"requestId\\\":\\\"913E154C-6179-4B9E-B886-2602B62FDE46\\\",\\\"success\\\":true}\",\"searchPic\":\"test011.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test012.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test012.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test012.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test006.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test006.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.54451656341553;151\\\",\\\"strAttr\\\":\\\"test006.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":116},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"32385D65-49EB-4CA5-89C7-EE1269530A5E\\\",\\\"success\\\":true}\",\"searchPic\":\"test012.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test013.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test013.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test013.jpg\\\"},{\\\"categoryId\\\":5,\\\"customContent\\\":\\\"test008.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test008.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"8.28189945220947;154\\\",\\\"strAttr\\\":\\\"test008.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":126},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":5},\\\"requestId\\\":\\\"D3013A27-3E40-45DC-9626-8C124462CA2E\\\",\\\"success\\\":true}\",\"searchPic\":\"test013.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test014.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test014.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test014.jpg\\\"},{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test002.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test002.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"6.07930326461792;150\\\",\\\"strAttr\\\":\\\"test002.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":176},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":88888888},\\\"requestId\\\":\\\"EFEC21F0-DB76-4BB7-B0D4-981273B3C95D\\\",\\\"success\\\":true}\",\"searchPic\":\"test014.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test015.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test015.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test015.jpg\\\"},{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test016.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test016.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"161.613220214844;24\\\",\\\"strAttr\\\":\\\"test016.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":158},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":88888888},\\\"requestId\\\":\\\"0235852D-4CE2-48FD-BED0-6F79E8E87DAE\\\",\\\"success\\\":true}\",\"searchPic\":\"test015.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test016.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test016.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test016.jpg\\\"},{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test015.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test015.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"161.613220214844;24\\\",\\\"strAttr\\\":\\\"test015.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":145},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":88888888},\\\"requestId\\\":\\\"CEEF0844-4205-4D68-A9C0-6E891A8428F4\\\",\\\"success\\\":true}\",\"searchPic\":\"test016.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test017.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test017.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test017.jpg\\\"},{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test018.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test018.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"18.6483993530273;101\\\",\\\"strAttr\\\":\\\"test018.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":147},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":88888888},\\\"requestId\\\":\\\"AF3FAE4B-9B6F-4714-BED2-6A04AE6568CB\\\",\\\"success\\\":true}\",\"searchPic\":\"test017.jpg\"},{\"searchRet\":\"{\\\"auctions\\\":[{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test018.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test018.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"5.37633353624177e+24;0\\\",\\\"strAttr\\\":\\\"test018.jpg\\\"},{\\\"categoryId\\\":88888888,\\\"customContent\\\":\\\"test017.jpg custom content\\\",\\\"intAttr\\\":1,\\\"picName\\\":\\\"test017.jpg\\\",\\\"productId\\\":\\\"test\\\",\\\"sortExprValues\\\":\\\"18.6483993530273;101\\\",\\\"strAttr\\\":\\\"test017.jpg\\\"}],\\\"code\\\":0,\\\"head\\\":{\\\"docsFound\\\":10,\\\"docsReturn\\\":2,\\\"searchTime\\\":153},\\\"msg\\\":\\\"success\\\",\\\"picInfo\\\":{\\\"allCategories\\\":[{\\\"id\\\":0,\\\"name\\\":\\\"Tops\\\"},{\\\"id\\\":1,\\\"name\\\":\\\"Dress\\\"},{\\\"id\\\":2,\\\"name\\\":\\\"Bottoms\\\"},{\\\"id\\\":3,\\\"name\\\":\\\"Bag\\\"},{\\\"id\\\":4,\\\"name\\\":\\\"Shoes\\\"},{\\\"id\\\":5,\\\"name\\\":\\\"Accessories\\\"},{\\\"id\\\":6,\\\"name\\\":\\\"Snack\\\"},{\\\"id\\\":7,\\\"name\\\":\\\"Makeup\\\"},{\\\"id\\\":8,\\\"name\\\":\\\"Bottle\\\"},{\\\"id\\\":9,\\\"name\\\":\\\"Furniture\\\"},{\\\"id\\\":20,\\\"name\\\":\\\"Toy\\\"},{\\\"id\\\":21,\\\"name\\\":\\\"Underwear\\\"},{\\\"id\\\":22,\\\"name\\\":\\\"Digital device\\\"},{\\\"id\\\":88888888,\\\"name\\\":\\\"Other\\\"}],\\\"categoryId\\\":88888888},\\\"requestId\\\":\\\"0548E9AD-24BF-46BB-8237-06275A741E3C\\\",\\\"success\\\":true}\",\"searchPic\":\"test018.jpg\"}]";
        JSONArray array = JSONArray.parseArray(searchRet);
        List<JSONObject> outList = new ArrayList<>();
        for(Object obj:array){
            JSONObject json = (JSONObject) obj;
            JSONObject out = new JSONObject();
            JSONArray retList = JSONArray.parseArray(json.getJSONObject("searchRet").getString("auctions"));
            String searchPicNames = "";
            for(Object robj:retList){
                JSONObject rJson = (JSONObject) robj;
                searchPicNames += rJson.getString("picName") + ",";
            }
            out.put("matchResult", searchPicNames);
            out.put("searchPic", json.getString("searchPic"));
            outList.add(out);
        }
        System.out.println(JSON.toJSONString(outList));
    }

    public String batchSearch(){
        File batchDir = new File(batch_upload_dir);
        List<JSONObject> searchRet = new ArrayList<>();
        try {
            for (File f : batchDir.listFiles()) {
                JSONObject json = new JSONObject();
                String ret = searchByPic(f);
                json.put("searchRet", ret);
                json.put("searchPic", f.getName());
                searchRet.add(json);
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("==============搜索结果如下============");
        return JSON.toJSONString(searchRet);
    }

    /**
     * 批量上传文件
     */
    public void batchAddFile() {
        File batchDir = new File(batch_upload_dir);
        try {
            for (File f : batchDir.listFiles()) {
                add(f);
                Thread.sleep(2000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void add(File file) {
        AddImageRequest request = new AddImageRequest();

        // 必填，图像搜索实例名称。
        request.setInstanceName(INSTANCE);

        // 必填，商品id，最多支持 512个字符。
        // 一个商品可有多张图片。
        request.setProductId(default_product_id);

        // 必填，图片名称，最多支持 512个字符。
        // 1. ProductId + PicName唯一确定一张图片。
        // 2. 如果多次添加图片具有相同的ProductId + PicName，以最后一次添加为准，前面添加的的图片将被覆盖。
        request.setPicName(file.getName());

        // 选填，图片类目。
        // 1. 对于商品搜索：若设置类目，则以设置的为准；若不设置类目，将由系统进行类目预测，预测的类目结果可在Response中获取 。
        // 2. 对于通用搜索：不论是否设置类目，系统会将类目设置为88888888。
//        request.setCategoryId(1);

        byte[] bytes2 = getBytes(file);
        Base64 base64 = new Base64();
        String encodePicContent = base64.encodeToString(bytes2);
        // 必填，图片内容，Base64编码。
        // 最多支持 2MB大小图片以及5s的传输等待时间。当前仅支持jpg和png格式图片；图片
        // 长和宽的像素必须都大于等于200，并且小于等于1024；图像中不能带有旋转信息。
        request.setPicContent(encodePicContent);


        // 选填，是否需要进行主体识别，默认为true。
        // 1.为true时，由系统进行主体识别，以识别的主体进行搜索，主体识别结果可在Response中获取。
        // 2. 为false时，则不进行主体识别，以整张图进行搜索。
        request.setCrop(false);
        // 选填，图片的主体区域，格式为 x1,x2,y1,y2, 其中 x1,y1 是左上角的点，x2，y2是右下角的点。
        // 若用户设置了Region，则不论Crop参数为何值，都将以用户输入Region进行搜索。
//        request.setRegion("280,486,232,351");


        // 选填，整数类型属性，可用于查询时过滤，查询时会返回该字段。
        //  例如不同的站点的图片/不同用户的图片，可以设置不同的IntAttr，查询时通过过滤来达到隔离的目的
        request.setIntAttr(1);
        // 选填，字符串类型属性，最多支持 128个字符。可用于查询时过滤，查询时会返回该字段。
        request.setStrAttr(file.getName());
        // 选填，用户自定义的内容，最多支持 4096个字符。
        // 查询时会返回该字段。例如可添加图片的描述等文本。
        request.setCustomContent(file.getName() + " custom content");

        try {
            AddImageResponse response = client.getAcsResponse(request);
            System.out.println(JSON.toJSONString(response));
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    public void delete() {
        DeleteImageRequest request = new DeleteImageRequest();
        // 必填，图像搜索实例名称。
        request.setInstanceName(INSTANCE);

        // 必填，商品id。
        request.setProductId("test");

        // 选填，图片名称。若不指定本参数，则删除ProductId下所有图片；若指定本参数，则删除ProductId+PicName指定的图片。
        request.setPicName("test-4");

        try {
            DeleteImageResponse response = client.getAcsResponse(request);
            System.out.println(JSON.toJSONString(response));
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    public String searchByPic(File file) {
        SearchImageRequest request = new SearchImageRequest();
        // 必填，图像搜索实例名称。
        request.setInstanceName(INSTANCE);

        // 选填，搜索类型，取值范围：
        // 1. SearchByPic（默认）：根据图片搜索相似图片。
        // 2. SearchByName，根据已添加的图片搜索相似图片。
        // request.setType("SearchByPic");


        byte[] bytes2 = getBytes(file);
        Base64 base64 = new Base64();
        String encodePicContent = base64.encodeToString(bytes2);
        // 图片内容，Base64编码。最多支持 2MB大小图片以及5s的传输等待时间。当前仅支持jpg和png格式图片；
        // 图片长和宽的像素必须都大于等于200，并且小于等于1024；图像中不能带有旋转信息。
        // 1. Type=SearchByPic时，必填
        // 2. Type=SearchByName时，无需填写。
        request.setPicContent(encodePicContent);

        // 选填，商品类目。
        // 1. 对于商品搜索：若设置类目，则以设置的为准；若不设置类目，将由系统进行类目预测，预测的类目结果可在Response中获取 。
        // 2. 对于通用搜索：不论是否设置类目，系统会将类目设置为88888888。
//        request.setCategoryId(1);

        // 选填，是否需要进行主体识别，默认为true。
        // 1.为true时，由系统进行主体识别，以识别的主体进行搜索，主体识别结果可在Response中获取。
        // 2. 为false时，则不进行主体识别，以整张图进行搜索。
        request.setCrop(false);

        // 选填，图片的主体区域，格式为 x1,x2,y1,y2, 其中 x1,y1 是左上角的点，x2，y2是右下角的点。
        // 若用户设置了Region，则不论Crop参数为何值，都将以用户输入Region进行搜索。
//        request.setRegion("280,486,232,351");

        // 选填，返回结果的数目。取值范围：1-100。默认值：10。
        request.setNum(20);
        // 选填，返回结果的起始位置。取值范围：0-499。默认值：0。
        request.setStart(0);

        // 选填，过滤条件
        // int_attr支持的操作符有>、>=、<、<=、=，str_attr支持的操作符有=和!=，多个条件之支持AND和OR进行连接。
        // 示例:
        //  1. 根据IntAttr过滤结果，int_attr>=100
        //  2. 根据StrAttr过滤结果，str_attr!="value1"
        //  3. 根据IntAttr和StrAttr联合过滤结果，int_attr=1000 AND str_attr="value1"
        request.setFilter("int_attr=1");

        try {
            SearchImageResponse response = client.getAcsResponse(request);
            return JSON.toJSONString(response);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void searchByName() {
        SearchImageRequest request = new SearchImageRequest();
        // 必填，图像搜索实例名称。
        request.setInstanceName(INSTANCE);

        // 选填，搜索类型，取值范围：
        // 1. SearchByPic（默认）：根据图片搜索相似图片。
        // 2. SearchByName，根据已添加的图片搜索相似图片。
        request.setType("SearchByName");

        // 商品id。
        // 1. Type=SearchByPic时，无需填写
        // 2. Type=SearchByName时，必填，已添加图片的ProductId。
        request.setProductId("test");

        // 图片名称。
        // 1. Type=SearchByPic时，无需填写
        // 2. Type=SearchByName时，必填，已添加图片的PicName。
        request.setPicName("test-1");

        // 选填，商品类目。
        // 1. 对于商品搜索：若设置类目，则以设置的为准；若不设置类目，将由系统进行类目预测，预测的类目结果可在Response中获取 。
        // 2. 对于通用搜索：不论是否设置类目，系统会将类目设置为88888888。
//        request.setCategoryId(1);

        // 选填，是否需要进行主体识别，默认为true。
        // 1.为true时，由系统进行主体识别，以识别的主体进行搜索，主体识别结果可在Response中获取。
        // 2. 为false时，则不进行主体识别，以整张图进行搜索。
        request.setCrop(false);

        // 选填，图片的主体区域，格式为 x1,x2,y1,y2, 其中 x1,y1 是左上角的点，x2，y2是右下角的点。
        // 若用户设置了Region，则不论Crop参数为何值，都将以用户输入Region进行搜索。
//        request.setRegion("280,486,232,351");

        // 选填，返回结果的数目。取值范围：1-100。默认值：10。
        request.setNum(2);
        // 选填，返回结果的起始位置。取值范围：0-499。默认值：0。
        request.setStart(0);

        // 选填，过滤条件
        // int_attr支持的操作符有>、>=、<、<=、=，str_attr支持的操作符有=和!=，多个条件之支持AND和OR进行连接。
        // 示例:
        //  1. 根据IntAttr过滤结果，int_attr>=100
        //  2. 根据StrAttr过滤结果，str_attr!="value1"
        //  3. 根据IntAttr和StrAttr联合过滤结果，int_attr=1000 AND str_attr="value1"
        request.setFilter("int_attr=0");

        try {
            SearchImageResponse response = client.getAcsResponse(request);
            System.out.println(JSON.toJSONString(response));
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }

    private static byte[] getBytes(File file) {
        byte[] buffer = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            // picture max size is 2MB
            ByteArrayOutputStream bos = new ByteArrayOutputStream(2000 * 1024);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }

    private static byte[] getBytes(String filePath) {
        return getBytes(new File(filePath));
    }
}
