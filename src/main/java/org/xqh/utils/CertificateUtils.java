package org.xqh.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.arronlong.httpclientutil.common.HttpHeader;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.Header;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.*;

/**
 * yzs
 */
public class CertificateUtils {

    private final static Charset CHARSET = StandardCharsets.UTF_8;

	private final static String SIGNATURE = "signature";

	private final static String TIMESTAMP = "timestamp";

	private final static String BRAND = "brand";

    /**
     * 创建签名
     * 注意：外部不要将timestamp、accessKey和sign这4个参数添加到方法参数params集合中
     * params就是请求的参数
     */
    public static String createSign(final HashMap<String, String> params, String accessKey, String secretKey, String timestamp) {
        //1、除去空值请求参数
        HashMap<String, String> newRequestParams = removeEmptyParam(params);

        //2、按照请求参数名的字母升序排列非空请求参数（包含AccessKey,Timestamp)，使用URL键值对的格式（即key1=value1&key2=value2…）拼接成字符串
        newRequestParams.put("AccessKey", accessKey);
        newRequestParams.put("Timestamp", timestamp);
        String strUrlParams = parseUrlString(newRequestParams);

        //4、最后拼接上secretKey得到字符串stringSignTemp
        String stringSignTemp = MessageFormat.format("{0}&SecretKey={1}", strUrlParams, secretKey);

        //5、对stringSignTemp进行MD5运算，并将得到的字符串所有字符转换为大写，得到sign值
        return DigestUtils.md5Hex(stringSignTemp.getBytes(CHARSET)).toUpperCase();
    }

	/**
	 * 获取请求 连接平 请求头对象
	 * @param reqJson 请求json
	 * @param appKey
	 * @param secret
	 * @param brand
	 * @return
	 */
	public static Header[] getReqHeaders(String reqJson, String appKey, String secret, String brand){
		HashMap<String, String> hashMap = JSON.parseObject(Objects.requireNonNull(reqJson), new TypeReference<HashMap<String, String>>() {
		});
		String timestamp = String.valueOf(System.currentTimeMillis());
		String sign = createSign(hashMap, appKey, secret, timestamp);
		return HttpHeader.custom()
			.other(TIMESTAMP, timestamp)
			.other(BRAND, brand)
			.other(SIGNATURE, sign)
			.build();
	}


    /**
     * 移除空请求参数
     */
    private static HashMap<String, String> removeEmptyParam(final HashMap<String, String> params) {
        HashMap<String, String> newParams = new HashMap<>(10);
        if (params == null || params.size() <= 0) {
            return newParams;
        }
        for (String key : params.keySet()) {
            String value = params.get(key);
            if (value == null || "".equals(value)) {
                continue;
            }
            newParams.put(key, value);
        }
        return newParams;
    }

    /**
     * 使用URL键值对的格式（即key1=value1&key2=value2…）将参数列表拼接成字符串
     */
    private static String parseUrlString(final HashMap<String, String> requestMap) {
        List<String> keyList = new ArrayList<>(requestMap.keySet());
        Collections.sort(keyList);

        List<String> entryList = new ArrayList<>();
        for (String key : keyList) {
            String value = requestMap.get(key);
            try {
                value = URLEncoder.encode(value, CHARSET.name());
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            entryList.add(MessageFormat.format("{0}={1}", key, value));
        }
        return String.join("&", entryList);
    }

    /**
     * 测试main 方法
     *
     * @param args
     */
    public static void main(String[] args) {
        // Body
        String params ="{\"deviceCode\":\"yzs82193\"}";
        HashMap<String, String> hashMap = JSON.parseObject(Objects.requireNonNull(params), new TypeReference<HashMap<String, String>>() {
        });
        String timestamp = String.valueOf(System.currentTimeMillis());
        String sign = createSign(hashMap, "shimao62ae0b32d0", "f6339f4974cacdeca4037489", timestamp);
        System.out.println("sign : " + sign);
        System.out.println("timeMillis : " + timestamp);
    }

}
