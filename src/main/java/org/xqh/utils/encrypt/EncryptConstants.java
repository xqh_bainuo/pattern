package org.xqh.utils.encrypt;

/**
 * @ClassName EncryptConstants
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/4/22 15:11
 * @Version 1.0
 */
public class EncryptConstants {

    public final static String ENCRYPT_AES = "AES";

    public static final String CHAR_ENCODING = "UTF-8";
    public static final String ALGORITHM_ECB_PKCS5 = "AES/ECB/PKCS5Padding";
    public static final String ALGORITHM_CBC_PKCS5 = "AES/CBC/PKCS5Padding";
    public static final String ALGORITHM_CBC_PKCS7 = "AES/CBC/PKCS7Padding";
    public static final String ALGORITHM_AES_CBC = "AES/CBC/PKCS5Padding";
    public static final String ALGORITHM_RSA_ECB = "RSA/ECB/PKCS1Padding";

    public static final String ALGORITHM_AES_GCM = "AES/GCM/NoPadding";

    public static final int AES_KEY_SIZE = 256;
    public static final int GCM_TAG_LENGTH = 128; // 16 bytes
    public static final int GCM_IV_LENGTH = 128;  // 12 bytes
}
