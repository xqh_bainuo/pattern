package org.xqh.utils.encrypt;

import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * ClassName:DESUtils2 <br/>
 * description:  <br/>
 * Date:     2020/4/13、14:32 <br/>
 *
 * @author jie.xu
 */
public class DESUtils {

    public static String encryptString(String keySrc, String passWord) throws Exception {
        if (StringUtils.isBlank(passWord)) {
            return "";
        }
        byte[] key = keySrc.getBytes(); // 长度最少要8个字符
        String encBase64Content = null;
        byte[] encContent = encrypt(key, passWord);
        encBase64Content = Base64.getEncoder().encodeToString(encContent);
        return encBase64Content;
    }

    public static byte[] encrypt(byte[] rawKeyData, String str) throws Exception {
        byte[] iv = {1, 2, 3, 4, 5, 6, 7, 8};
        IvParameterSpec zeroIv = new IvParameterSpec(iv);
        DESKeySpec dks = new DESKeySpec(rawKeyData);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        SecretKey key = keyFactory.generateSecret(dks);
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
        byte data[] = str.getBytes();
        byte[] encryptedData = cipher.doFinal(data);
        return encryptedData;
    }


    public static String decryptString(String keySrc, String passWord) throws Exception {
        if (StringUtils.isBlank(passWord)) {
            return "";
        }
        byte[] key = Base64.getDecoder().decode(passWord); // 长度最少要8个字符
        byte[] encContent = decrypt(key, keySrc);
        return new String(encContent, "UTF-8");
    }

    public static byte[] decrypt(byte[] rawKeyData, String password) throws Exception {
        byte[] iv = {1, 2, 3, 4, 5, 6, 7, 8};
        IvParameterSpec zeroIv = new IvParameterSpec(iv);
        // 创建一个DESKeySpec对象
        DESKeySpec desKey = new DESKeySpec(password.getBytes());
        // 创建一个密匙工厂
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
        // 将DESKeySpec对象转换成SecretKey对象
        SecretKey securekey = keyFactory.generateSecret(desKey);
        // Cipher对象实际完成解密操作
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        // 用密匙初始化Cipher对象
        cipher.init(Cipher.DECRYPT_MODE, securekey, zeroIv);
        // 真正开始解密操作
        return cipher.doFinal(rawKeyData);
    }

    public static String sessionKey = "tiihtNczf5v6AKRyjwEUhQ==";
    public static String appId = "wx4f4bc4dec97d474b";

    public static void main(String[] args) throws Exception {
        String secretKey = "t8ulwy8l";
        String password = "getech@1234";
        System.out.println(DESUtils.encryptString(secretKey, password));
        String test = "{\"tenantType\":\"2\",\"allOrgCode\":[\"testgroup\"],\"isTenantManager\":true,\"mobile\":\"18929305694\",\"language\":\"zh_CN\",\"uid\":\"admin\",\"empNum\":null,\"postionCodes\":[\"defaultNull\"],\"busOrgMap\":{\"poros-manage\":\"abdghhh\",\"iot\":\"abdghhh\"},\"name\":\"管理员\",\"tenantId\":\"geek\",\"allOrgCodePath\":[\"testgroup\"],\"email\":\"admin@getech.cn\"}";

//        String encryptData = "CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZMQmRzooG2xrDcvSnxIMXFufNstNGTyaGS9uT5geRa0W4oTOb1WT7fJlAC+oNPdbB+3hVbJSRgv+4lGOETKUQz6OYStslQ142dNCuabNPGBzlooOmB231qMM85d2/fV6ChevvXvQP8Hkue1poOFtnEtpyxVLW1zAo6/1Xx1COxFvrc2d7UL/lmHInNlxuacJXwu0fjpXfz/YqYzBIBzD6WUfTIF9GRHpOn/Hz7saL8xz+W//FRAUid1OksQaQx4CMs8LOddcQhULW4ucetDf96JcR3g0gfRK4PC7E/r7Z6xNrXd2UIeorGj5Ef7b1pJAYB6Y5anaHqZ9J6nKEBvB4DnNLIVWSgARns/8wR2SiRS7MNACwTyrGvt9ts8p12PKFdlqYTopNHR1Vf7XjfhQlVsAJdNiKdYmYVoKlaRv85IfVunYzO0IKXsyl7JCUjCpoG20f0a04COwfneQAGGwd5oa+T8yO5hzuyDb/XcxxmK01EpqOyuxINew==";
//        String iv = "r7BXXKkLb8qrSNn05n0qiA==";


//        System.out.println(Base64.getEncoder().encodeToString("super_admin:123456".getBytes()));

    }
}
