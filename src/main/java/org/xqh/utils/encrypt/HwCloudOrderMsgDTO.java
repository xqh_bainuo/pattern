package org.xqh.utils.encrypt;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import org.xqh.utils.DateUtil;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName HwCloudOrderMsgDTO
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/7/19 18:24
 * @Version 1.0
 */

@Data
public class HwCloudOrderMsgDTO {

    private String systemId;

    private String tenantId;

    private String productComboId;

    private String activity;

    private Date expireTime;

    public static void main(String[] args) {
//        String json = "{\"systemId\":\"systemIdsystemIdsystemId\",\"expireTime\":1626746231764,\"activity\":\"abcdefg\",\"tenantId\":\"geek\",\"productComboId\":\"sdfgggggg\"}";
//        HwCloudOrderMsgDTO msg = JSON.parseObject(json, HwCloudOrderMsgDTO.class);
//        System.out.println(JSON.toJSONString(msg));
        System.out.println(DateUtil.plusDate(new Date(), 10, ChronoUnit.YEARS).getTime());
    }
}
