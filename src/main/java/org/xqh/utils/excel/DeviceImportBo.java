package org.xqh.utils.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceImportBo implements Serializable {

	@NotNull
	@Excel(name = "设备品牌")
	private String brandName;

	@NotNull
	@Excel(name = "设备模型")
	private String typeName;

	@NotNull
	@Excel(name = "设备名称")
	private String entityName;

	@NotNull
	@Excel(name = "设备编码")
	private String entityCode;

	@Excel(name = "设备别名")
	private String tag;

	@Excel(name = "设备描述")
	private String remark;

	@Excel(name = "空间ID")
	private Long spaceId;

	@Excel(name = "空间名称")
	private String spaceName;

	@Excel(name = "空间类型")
	private String spaceType;

	@Excel(name = "上级空间")
	private String upSpace;

}
