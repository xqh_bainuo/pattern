package org.xqh.utils.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.IOException;

/**
 * @ClassName HttpRetModel
 * @Description http请求 响应结果封装
 * @Author wxqq
 * @Date 2020/3/2 17:03
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HttpRetModel {

    private String statusKey = "Code";// 请求状态码key

    private String successVal = "200"; // 请求成功状态值

    private String dataKey = "Data"; // 请求响应结果key

    private String msgKey = "Text";// 响应信息描述key

    public static void main(String[] args) throws IOException {
        FxHttpClientUtils.downloadFile("http://localhost:8020/device/v1/web/office/downloadFile?fileId=6662236351&fileVersion=0&fileMd5=12333", new File("D://test.docx"));
    }
}
