package org.xqh.utils.http;

import com.arronlong.httpclientutil.builder.HCB;
import com.arronlong.httpclientutil.common.SSLs.SSLProtocolVersion;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;

/**
 * @ClassName FxHttpPool
 * @Description http client pool
 * @Author wxqq
 * @Date 2019/7/13 17:36
 * @Version 1.0
 */
@Slf4j
public class FxHttpPool {

    private static HCB hcb;

    public static int timeOut;

    static {
        try {
            int max = 200;
            int per = 50;
            int retry = 3;
            timeOut = 30000;//默认五秒超时时间
            hcb = HCB.custom()
                    .pool(max, per)    	//启用连接池，每个路由最大创建120个链接，总连接数限制为1000个
                    .sslpv(SSLProtocolVersion.TLSv1_2) 	//可设置ssl版本号，默认SSLv3，用于ssl，也可以调用sslpv("TLSv1.2")
                    .ssl()  			   		//https，支持自定义ssl证书路径和密码，ssl(String keyStorePath, String keyStorepass)
                    .retry(retry);
        } catch (HttpProcessException e) {
            log.error("", e);
        }
    }

    public static HttpClient client = hcb.build();
}
