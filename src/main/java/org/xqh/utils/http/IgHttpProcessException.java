package org.xqh.utils.http;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName IgHttpProcessException
 * @Description Ig http请求异常
 * @Author wxqq
 * @Date 2020/4/17 13:51
 * @Version 1.0
 */
@Data
@NoArgsConstructor
public class IgHttpProcessException extends RuntimeException{

    private int statusCode;

    private String apiRet;

    /**
     * http响应正常
     * 接口响应异常
     * @param apiRet  接口响应异常结果
     */
    public IgHttpProcessException(String apiRet){
        super(apiRet);
        this.statusCode = 200;// http状态码正常
        this.apiRet = apiRet;
    }

    /**
     * http请求异常 构造函数
     * @param statusCode http 响应状态码
     * @param msg
     */
    public IgHttpProcessException(int statusCode, String msg){
        super(msg);
        this.statusCode = statusCode;
    }

}
