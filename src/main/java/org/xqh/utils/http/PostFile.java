package org.xqh.utils.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;

/**
 * @ClassName PostFile
 * @Description post请求 带文件参数
 * @Date 2020/7/27 13:31
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostFile {

    /**
     * 请求文件
     */
    private File file;

    /**
     * 请求key
     */
    private String key;
}
