package org.xqh.utils;

import com.github.yitter.contract.IdGeneratorOptions;
import com.github.yitter.idgen.YitIdHelper;

/**
 * @ClassName IdGeneratorUtil
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/11/30 20:20
 * @Version 1.0
 */
public class IdGeneratorUtil {

    static {
        IdGeneratorOptions options = new IdGeneratorOptions((short) 11);
        YitIdHelper.setIdGenerator(options);
    }

    public static void main(String[] args) {
        System.out.println(YitIdHelper.nextId());
        System.out.println(YitIdHelper.nextId());
        System.out.println(YitIdHelper.nextId());
        System.out.println(YitIdHelper.nextId());
        System.out.println(YitIdHelper.nextId());
    }
}
