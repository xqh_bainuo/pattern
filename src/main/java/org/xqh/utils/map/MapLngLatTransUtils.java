package org.xqh.utils.map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName MapLngLatTransUtils
 * @Description 地图经纬度 互转
 * @Author xuqianghui
 * @Date 2020/12/21 17:20
 * @Version 1.0
 */
public class MapLngLatTransUtils {

    public final static double x_pi = 3.14159265358979324 * 3000.0 / 180.0;

    /**
     * 高德 / 百度 地图经纬度 验证.
     * http://api.map.baidu.com/lbsapi/getpoint/index.html
     * https://lbs.amap.com/console/show/picker
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(JSON.toJSONString(bMapTransGdMap(118.136639,30.294517)));
//        System.out.println(JSON.toJSONString(gdMapTransBMap(118.042155,24.615560)));
        System.out.println(JSON.toJSONString(gdMapTransBMap(118.136639,30.294517)));
//        gdMapTransBMap(118.0421555209464, 24.61556070741811);
//        System.out.println(subString(24.61556070741811, 6));
//        String text = "{\"appKey\":\"45leaptxsb6qm7sqgkzlxo3ky62es3mtfildmyyo\",\"appCd\":\"YD2020092701\",\"appSecret\":\"196144cf83a4494393b5bde9bd70558d\",\"parkCd\":\"03\",\"size\":\"20\",\"url\":\"https://www.xmsoft.com\",\"PZT_WZFL002\":{\"name\":\"通知公告\",\"categoryKey\":\"ZPfw5md9\"},\"PZT_WZFL003\":{\"name\":\"园区动态\",\"categoryKey\":\"lLqx5Kee\"},\"PZT_WZFL004\":{\"name\":\"办事项目\",\"categoryKey\":\"Rz8LZzrZ\"},\"PZT_WZFL005\":{\"name\":\"办事指南\",\"categoryKey\":\"BSZN\"},\"PZT_WZFL009\":{\"name\":\"公维金使用情况公示\",\"categoryKey\":\"\"},\"PZT_ZCJB001\":{\"name\":\"国家级政策\",\"categoryKey\":\"7VWU5xn6\"},\"PZT_ZCJB002\":{\"name\":\"省级政策\",\"categoryKey\":\"klzSB13H\"},\"PZT_ZCJB003\":{\"name\":\"市级政策\",\"categoryKey\":\"1Y2nWrgv\"},\"PZT_ZCJB004\":{\"name\":\"区级政策\",\"categoryKey\":\"2K583mWF\"},\"entSetup\":{\"token\":\"xmxxg\",\"accountId\":\"8a8092f96eb54f3c016f42603d551824\",\"size\": 5000,\"name\":\"企业信息配置\",\"address\":\"厦门软件园三期\",\"categoryKey\":\"QYCX\",\"location\":{\"A03\":\"118.056699,24.611892\",\"A04\":\"118.056029,24.61282\",\"A05\":\"118.056918,24.61337\",\"A06\":\"118.058176,24.613945\",\"B01\":\"118.056318,24.617686\",\"B02\":\"118.055694,24.618167\",\"B03\":\"118.053558,24.617181\",\"B04\":\"118.053415,24.61636\",\"B08\":\"118.052678,24.62063\",\"B09\":\"118.0524,24.621496\",\"B10\":\"118.05116,24.621767\",\"B11\":\"118.051301,24.619201\",\"B12\":\"118.05169,24.619526\",\"B13\":\"118.051411,24.620048\",\"B14\":\"118.05065,24.621207\",\"B15\":\"118.050208,24.620446\",\"B19\":\"118.05112,24.622488\",\"B20\":\"118.050103,24.621401\",\"B21\":\"118.048699,24.621423\",\"D03\":\"118.049395,24.615479\",\"B02裙楼\":\"118.055039,24.618228\",\"D08\":\"118.003763,24.533353\"}}}";
//        System.out.println(transJsonMapJWD(text));
    }

    /**
     * 转换content-service 服务 配置表  企业服务 配置数据
     * @param json
     * @return
     */
    public static String transJsonMapJWD(String json){
        JSONObject obj = JSON.parseObject(json);
        JSONObject location = obj.getJSONObject("entSetup").getJSONObject("location");//拿到经纬度
        for(String key : location.keySet()){
            String val = location.getString(key);
            String[] arr = val.split(",");
            LngLatModel m = bMapTransGdMap(Double.valueOf(arr[0].trim()), Double.valueOf(arr[1].trim()));
            String nVal = m.getLng()+","+m.getLat();
            location.put(key, nVal);
        }
        return obj.toJSONString();
    }

    public static String subString(double num, int scale){
        String str = String.valueOf(num);
        return str.substring(0, str.indexOf(".") + scale + 1);
    }

    /**
     * 腾讯&高德 转 百度
     * @param lng
     * @param lat
     * @return
     */
    public static LngLatModel gdMapTransBMap(double lng, double lat) {
        double x = lng;
        double y = lat;
        double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * x_pi);
        double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * x_pi);
        double lngs = z * Math.cos(theta) + 0.0065;
        double lats = z * Math.sin(theta) + 0.006;
        return LngLatModel.builder()
                .lng(subString(lngs, 6))
                .lat(subString(lats, 6))
                .build();
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class LngLatModel{
        private String lng;//经度
        private String lat;//维度
    }

    /**
     * 百度经纬度 转 腾讯/高德
     * @param lng
     * @param lat
     * @return
     */
    public static LngLatModel bMapTransGdMap(double lng, double lat) {
        double x = lng - 0.0065;
        double y = lat - 0.006;
        double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * x_pi);
        double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * x_pi);
        double lngs = z * Math.cos(theta);
        double lats = z * Math.sin(theta);
        return LngLatModel.builder()
                .lng(subString(lngs, 6))
                .lat(subString(lats, 6))
                .build();
    }
}
