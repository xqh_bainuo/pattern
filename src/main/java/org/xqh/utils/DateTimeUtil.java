package org.xqh.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {
    private static DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static DateFormat dateFormatMill=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    /***
     * 将时间戳转为时间字符串
     * @param stamp
     * @return
     */
    public static String stamp2Str(Long stamp){
        try {
            return dateFormat.format(new Date(stamp));
        } catch (Exception e) {
            throw new RuntimeException("查询时间格式错误");
        }
    }
    public static String stamp2StrMill(Long stamp){
        try {
            return dateFormatMill.format(new Date(stamp));
        } catch (Exception e) {
            throw new RuntimeException("查询时间格式错误");
        }
    }

    /***
     * 将时间字符串转为时间戳
     * @param dateTime
     * @return
     */
    public static Long str2Stamp(String dateTime){
        try {
            return dateFormat.parse(dateTime).getTime();
        } catch (ParseException e) {
            throw new RuntimeException("查询时间格式错误");
        }
    }
    /***
     * 将时间字符串转为时间戳Mill
     * @param dateTime
     * @return
     */
    public static Long str2StampMill(String dateTime){
        try {
            return dateFormatMill.parse(dateTime).getTime();
        } catch (ParseException e) {
            throw new RuntimeException("查询时间格式错误");
        }
    }

    public static Long str2Nanos(String dateTime){
        try {
            return dateFormatMill.parse(dateTime).getTime()*1000000L;
        } catch (ParseException e) {
            throw new RuntimeException("查询时间格式错误");
        }
    }
}
