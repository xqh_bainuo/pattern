package org.xqh.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.RandomStringUtils;
import org.xqh.test.yzs.shimao.hongxu.SpaceTreeDto;
import org.xqh.utils.encrypt.EncryptUtils;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.excel.ExcelReader;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.http.Header;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by leo on 2017/12/15.
 */
public class MainTest {

    public static void main(String[] args) {
        System.out.println(EncryptUtils.getMd5("{\"msg\":\"success\",\"code\":200,\"data\":[{\"releaseTime\":1693278956,\"pkgType\":\"deb\",\"icon\":\"https://ugreen-pro.oss-cn-shenzhen.aliyuncs.com/applicationMgr/icon/应用1.jpg\",\"accessList\":[\"admin\"],\"isNew\":false,\"serviceName\":\"download_serv.service\",\"removeAble\":true,\"version\":{\"importantVersion\":0,\"models\":[\"DXP2800\"],\"gray\":false,\"size\":11227568,\"lowVersion\":\"1.00.0000\",\"devices\":[],\"versionNum\":1000000,\"recommend\":false,\"version\":\"1.00.0000\",\"beta\":false},\"i18n\":[{\"protocol\":\"手动阀沙发手动阀阿凡达\",\"author\":\"UGREEN\",\"name\":\"下载中心\",\"description\":\"download center\",\"langName\":\"zh-CN\",\"type\":\"0\",\"releaseNote\":\"阿是发送到发是打发是的三大啊\",\"thumbs\":[\"https://ugreen-pro.oss-cn-shenzhen.aliyuncs.com/applicationMgr/thumbs/zh/5a433ad18ab72.jpg\",\"https://ugreen-pro.oss-cn-shenzhen.aliyuncs.com/applicationMgr/thumbs/zh/1 (9).jpg\"],\"tags\":[\"系统应用\"]}],\"url\":\"nUprC1Rqgz9pdVKAPc5RAkYD5iijFItICH1bAJqz8mNfsw3m1zRigdKJzCuWBOcR/4zinCwnikxN6XRcR3eXaI2DDhITM8vEEZIhQyUQ4ZKiHqfSkHWghK9+9JgMLVxd8ZyHrX4KXGZ6JY7ytH8AxLEkSTBKsNgWQNiGlW/KgaSXMq05auzXAHj0HRFrgm34xGojmV17rH++77bKIqbfvg==\",\"daemon\":true,\"aloneModule\":true,\"route\":\"/downloadCenter\",\"open_path\":false,\"appType\":1,\"appId\":\"com.ugreen.downloadCenter\",\"arch\":\"amd64\",\"languageList\":[\"zh-CN\"],\"hash\":\"6e6de24d8312900ab99e1e2422366975\"},{\"pkgType\":\"deb\",\"icon\":\"/ugreen/static/icons/music.png\",\"accessList\":[\"admin\",\"users\"],\"serviceName\":\"music_serv.service\",\"removeAble\":true,\"version\":{\"importantVersion\":0,\"models\":[\"DXP2800\",\"DXP4800\",\"DXP480T\"],\"gray\":false,\"size\":20645653,\"buildTime\":1692841784,\"lowVersion\":\"1.00.0000\",\"versionNum\":1000000,\"recommend\":false,\"version\":\"1.00.0000\",\"beta\":false},\"i18n\":[{\"help\":\"\",\"protocol\":\"\",\"author\":\"UGREEN\",\"name\":\"音乐服务\",\"description\":\"为用户提供音乐相关功能!\",\"langName\":\"zh-CN\",\"official\":\"https://www.ugreen.com\",\"releaseNote\":\"\",\"tags\":[\"官方应用\"]}],\"daemon\":true,\"aloneModule\":true,\"route\":\"/music\",\"open_path\":false,\"appType\":1,\"appId\":\"com.ugreen.music\",\"arch\":\"amd64\",\"languageList\":[\"zh-CN\"],\"hash\":\"54c0781e15d6e54fac545f148891fdb8\"}],\"time\":1.7686E-5}"));
        System.out.println(EncryptUtils.base64Decode("ddffgg"));
//        List<JsonModel> list = Lists.newArrayList(new JsonModel(1), new JsonModel(11));
//        JSONObject json = new JSONObject();
//        json.put("code", 200);
//        json.put("data", list);
//        json.put("gray", true);
//        JSONObject newJson = JSON.parseObject(json.toJSONString());
//        JSONArray array = newJson.getJSONArray("data");
//        for(Object obj : array){
//            System.out.println(obj);
//        }
//        Boolean gray = newJson.getBoolean("gray");
//        boolean flag = true;
//        System.out.println("gray===> "+ (Objects.nonNull(gray) && gray == flag));
//        JSONObject obj = newJson.getJSONObject("test");
//        System.out.println(newJson.getString("abc"));
//        System.out.println();
//        System.out.println(Objects.isNull(obj));
        Boolean gray = true;
        System.out.println(Objects.equals(gray, false));
    }

    @Data
    public static class JsonModel {
        private int val;
        public JsonModel(int val){
            this.val = val;
        }
    }

    public static void main111(String[] args) {
        System.out.println(RandomStringUtils.randomAlphanumeric(18));
        System.out.println(new Date().getTime());
    }

    public static void main11(String[] args) {
        JSONObject json = JSON.parseObject("{\n" +
                "    \"retcode\": 0,\n" +
                "    \"msg\": \"success\",\n" +
                "    \"data\": {\n" +
                "        \"id\": 435,\n" +
                "        \"spaceName\": \"酒店管理平台\",\n" +
                "        \"measure\": null,\n" +
                "        \"spaceType\": null,\n" +
                "        \"typeCode\": null,\n" +
                "        \"typeName\": null,\n" +
                "        \"typeRemark\": null,\n" +
                "        \"remarks\": \"8\",\n" +
                "        \"rootId\": 435,\n" +
                "        \"pid\": 435,\n" +
                "        \"orderShow\": 1,\n" +
                "        \"createTime\": \"2020-01-02 22:33:30\",\n" +
                "        \"updateTime\": \"2020-10-26 16:06:06\",\n" +
                "        \"createName\": \"59\",\n" +
                "        \"updateName\": null,\n" +
                "        \"isSuite\": false,\n" +
                "        \"isSpecialMode\": false,\n" +
                "        \"status\": 1,\n" +
                "        \"isProject\": false,\n" +
                "        \"spacePath\": null,\n" +
                "        \"provinceCode\": null,\n" +
                "        \"cityCode\": null,\n" +
                "        \"icon\": null,\n" +
                "        \"isExistSubspace\": true,\n" +
                "        \"spaceTreeInfo\": [\n" +
                "            435\n" +
                "        ],\n" +
                "        \"spaceExtensionEntities\": null,\n" +
                "        \"spaceEntityList\": [\n" +
                "            {\n" +
                "                \"id\": 2004417,\n" +
                "                \"spaceName\": \"喜来登酒店\",\n" +
                "                \"measure\": null,\n" +
                "                \"spaceType\": 28,\n" +
                "                \"typeCode\": \"hotel\",\n" +
                "                \"typeName\": \"酒店\",\n" +
                "                \"typeRemark\": \"酒店类型\",\n" +
                "                \"remarks\": null,\n" +
                "                \"rootId\": 435,\n" +
                "                \"pid\": 435,\n" +
                "                \"orderShow\": 99,\n" +
                "                \"createTime\": \"2020-11-12 15:53:02\",\n" +
                "                \"updateTime\": \"2020-11-12 15:53:16\",\n" +
                "                \"createName\": \"888\",\n" +
                "                \"updateName\": null,\n" +
                "                \"isSuite\": false,\n" +
                "                \"isSpecialMode\": false,\n" +
                "                \"status\": 1,\n" +
                "                \"isProject\": true,\n" +
                "                \"spacePath\": null,\n" +
                "                \"provinceCode\": null,\n" +
                "                \"cityCode\": null,\n" +
                "                \"icon\": \"&#xeb61;\",\n" +
                "                \"isExistSubspace\": true,\n" +
                "                \"spaceTreeInfo\": [\n" +
                "                    435,\n" +
                "                    2004417\n" +
                "                ],\n" +
                "                \"spaceExtensionEntities\": null,\n" +
                "                \"spaceEntityList\": [\n" +
                "                    {\n" +
                "                        \"id\": 2004418,\n" +
                "                        \"spaceName\": \"A栋\",\n" +
                "                        \"measure\": null,\n" +
                "                        \"spaceType\": 2,\n" +
                "                        \"typeCode\": \"building\",\n" +
                "                        \"typeName\": \"楼栋\",\n" +
                "                        \"typeRemark\": \"楼栋\",\n" +
                "                        \"remarks\": null,\n" +
                "                        \"rootId\": 435,\n" +
                "                        \"pid\": 2004417,\n" +
                "                        \"orderShow\": 99,\n" +
                "                        \"createTime\": \"2020-11-12 15:53:16\",\n" +
                "                        \"updateTime\": \"2020-11-12 15:53:33\",\n" +
                "                        \"createName\": \"888\",\n" +
                "                        \"updateName\": null,\n" +
                "                        \"isSuite\": false,\n" +
                "                        \"isSpecialMode\": false,\n" +
                "                        \"status\": 1,\n" +
                "                        \"isProject\": false,\n" +
                "                        \"spacePath\": null,\n" +
                "                        \"provinceCode\": null,\n" +
                "                        \"cityCode\": null,\n" +
                "                        \"icon\": \"&#xeb61;\",\n" +
                "                        \"isExistSubspace\": true,\n" +
                "                        \"spaceTreeInfo\": [\n" +
                "                            435,\n" +
                "                            2004417,\n" +
                "                            2004418\n" +
                "                        ],\n" +
                "                        \"spaceExtensionEntities\": null,\n" +
                "                        \"spaceEntityList\": [\n" +
                "                            {\n" +
                "                                \"id\": 2004419,\n" +
                "                                \"spaceName\": \"1层\",\n" +
                "                                \"measure\": null,\n" +
                "                                \"spaceType\": 18,\n" +
                "                                \"typeCode\": \"floor\",\n" +
                "                                \"typeName\": \"楼层\",\n" +
                "                                \"typeRemark\": null,\n" +
                "                                \"remarks\": null,\n" +
                "                                \"rootId\": 435,\n" +
                "                                \"pid\": 2004418,\n" +
                "                                \"orderShow\": 99,\n" +
                "                                \"createTime\": \"2020-11-12 15:53:33\",\n" +
                "                                \"updateTime\": \"2020-11-17 21:41:16\",\n" +
                "                                \"createName\": \"888\",\n" +
                "                                \"updateName\": \"256\",\n" +
                "                                \"isSuite\": false,\n" +
                "                                \"isSpecialMode\": false,\n" +
                "                                \"status\": 1,\n" +
                "                                \"isProject\": false,\n" +
                "                                \"spacePath\": null,\n" +
                "                                \"provinceCode\": null,\n" +
                "                                \"cityCode\": null,\n" +
                "                                \"icon\": \"&#xeb61;\",\n" +
                "                                \"isExistSubspace\": true,\n" +
                "                                \"spaceTreeInfo\": [\n" +
                "                                    435,\n" +
                "                                    2004417,\n" +
                "                                    2004418,\n" +
                "                                    2004419\n" +
                "                                ],\n" +
                "                                \"spaceExtensionEntities\": null,\n" +
                "                                \"spaceEntityList\": [\n" +
                "                                    {\n" +
                "                                        \"id\": 2004420,\n" +
                "                                        \"spaceName\": \"888\",\n" +
                "                                        \"measure\": null,\n" +
                "                                        \"spaceType\": 6,\n" +
                "                                        \"typeCode\": \"hotel_room\",\n" +
                "                                        \"typeName\": \"酒店房\",\n" +
                "                                        \"typeRemark\": \"酒店房\",\n" +
                "                                        \"remarks\": \"888\",\n" +
                "                                        \"rootId\": 435,\n" +
                "                                        \"pid\": 2004419,\n" +
                "                                        \"orderShow\": 99,\n" +
                "                                        \"createTime\": \"2020-11-12 16:00:02\",\n" +
                "                                        \"updateTime\": \"2020-11-17 23:05:45\",\n" +
                "                                        \"createName\": \"904\",\n" +
                "                                        \"updateName\": \"256\",\n" +
                "                                        \"isSuite\": true,\n" +
                "                                        \"isSpecialMode\": false,\n" +
                "                                        \"status\": 1,\n" +
                "                                        \"isProject\": false,\n" +
                "                                        \"spacePath\": null,\n" +
                "                                        \"provinceCode\": null,\n" +
                "                                        \"cityCode\": null,\n" +
                "                                        \"icon\": null,\n" +
                "                                        \"isExistSubspace\": true,\n" +
                "                                        \"spaceTreeInfo\": [\n" +
                "                                            435,\n" +
                "                                            2004417,\n" +
                "                                            2004418,\n" +
                "                                            2004419,\n" +
                "                                            2004420\n" +
                "                                        ],\n" +
                "                                        \"spaceExtensionEntities\": null,\n" +
                "                                        \"spaceEntityList\": [\n" +
                "                                            {\n" +
                "                                                \"id\": 2004526,\n" +
                "                                                \"spaceName\": \"主卧\",\n" +
                "                                                \"measure\": null,\n" +
                "                                                \"spaceType\": 29,\n" +
                "                                                \"typeCode\": \"master_bedroom\",\n" +
                "                                                \"typeName\": \"主卧\",\n" +
                "                                                \"typeRemark\": null,\n" +
                "                                                \"remarks\": null,\n" +
                "                                                \"rootId\": 435,\n" +
                "                                                \"pid\": 2004420,\n" +
                "                                                \"orderShow\": 99,\n" +
                "                                                \"createTime\": \"2020-11-17 23:05:45\",\n" +
                "                                                \"updateTime\": \"2020-11-17 23:05:45\",\n" +
                "                                                \"createName\": \"256\",\n" +
                "                                                \"updateName\": null,\n" +
                "                                                \"isSuite\": false,\n" +
                "                                                \"isSpecialMode\": false,\n" +
                "                                                \"status\": 1,\n" +
                "                                                \"isProject\": false,\n" +
                "                                                \"spacePath\": null,\n" +
                "                                                \"provinceCode\": null,\n" +
                "                                                \"cityCode\": null,\n" +
                "                                                \"icon\": \"&#xeb61;\",\n" +
                "                                                \"isExistSubspace\": false,\n" +
                "                                                \"spaceTreeInfo\": [\n" +
                "                                                    435,\n" +
                "                                                    2004417,\n" +
                "                                                    2004418,\n" +
                "                                                    2004419,\n" +
                "                                                    2004420,\n" +
                "                                                    2004526\n" +
                "                                                ],\n" +
                "                                                \"spaceExtensionEntities\": null,\n" +
                "                                                \"spaceEntityList\": null,\n" +
                "                                                \"address\": \"\",\n" +
                "                                                \"spaceIconEntities\": null\n" +
                "                                            }\n" +
                "                                        ],\n" +
                "                                        \"address\": null,\n" +
                "                                        \"spaceIconEntities\": null\n" +
                "                                    }\n" +
                "                                ],\n" +
                "                                \"address\": \"\",\n" +
                "                                \"spaceIconEntities\": null\n" +
                "                            }\n" +
                "                        ],\n" +
                "                        \"address\": \"\",\n" +
                "                        \"spaceIconEntities\": null\n" +
                "                    }\n" +
                "                ],\n" +
                "                \"address\": \"\",\n" +
                "                \"spaceIconEntities\": null\n" +
                "            }\n" +
                "        ],\n" +
                "        \"address\": null,\n" +
                "        \"spaceIconEntities\": null\n" +
                "    }\n" +
                "}");
        SpaceTreeDto dto = json.getObject("data", SpaceTreeDto.class);
        SpaceTreeDto dt = recursionFindSuiteSpace(dto);
        if(Objects.nonNull(dt)){
            System.out.println(dt.getSpaceName() + dt.getId());
        }
    }

    public static SpaceTreeDto recursionFindSuiteSpace(SpaceTreeDto tree){
        if(Objects.nonNull(tree.getIsSuite()) && tree.getIsSuite()){
            return tree;
        }
        if(!CollectionUtils.isEmpty(tree.getSpaceEntityList())){
            for(SpaceTreeDto c:tree.getSpaceEntityList()){
                SpaceTreeDto cRet = recursionFindSuiteSpace(c);
                if(Objects.nonNull(cRet)){
                    return cRet;
                }
            }
        }
        return null;
    }

    public static String handleHttpsUrl(String realServerName){
        //https://route.igaicloud.cn/admin-center/login
        String httpsFilter = "route.igaicloud.cn:-1,ro22ute.igaicloud.cn:-3";
        String[] filterArray = httpsFilter.split(",");
        for(String f:filterArray){
            if(realServerName.endsWith(f)){
                if(realServerName.endsWith(":-1")){
                    realServerName = realServerName.substring(0, realServerName.length() - 3);
                }
                if(realServerName.startsWith("http://")){
                    realServerName = realServerName.replaceFirst("http://", "https://");
                }
                break;
            }
        }
        return realServerName;
    }
    public static void mainggg(String[] args) throws Exception {
//        String key="obj.objItem.key";
//        System.out.println(key.substring(key.indexOf(".")+1));
        /**
         * 局部变量表 slot 复用 对 垃圾回收的影响
         *
         */
//        {
//            byte[] placeHolder=new byte[64*1024*1024];
//            //虽然  已经出了变量placeHolder的作用域,但是 其对应的slot还没有被其他变量复用
//            //所以作为GC Roots一部分的局部变量表仍然保持着对它的关联.
//        }
//        int a=0;
//        System.gc();
//        System.out.println(EncryUtils.getMd5("123456789"));

//        String url = "http://localhost:8800/pay?test=abc&name=hello&amount=122.0";
//        Long a = 5L;
//        Long b = 5L;
//        BigDecimal bd = NumberUtils.divide(new BigDecimal(a * b), new BigDecimal(100));
//
//        System.out.println(bd.longValue());
////        System.out.println(JSON.toJSONString(url.substring(url.indexOf("?")).split("&")));
////        switchTest(2);
////
//        String filePath = "D:\\testfile\\ffff\\sdfasdf\\";
//        filePathMkdirs(filePath);

//          File file = new File("D:\\chromeDownload\\nlu指令对比结果 (22).xlsx");
//        System.out.println(file.getName());
//        System.out.println(file.getName().contains("."));

//        String test = "/fasdf";
//        System.out.println(test.substring(1));

//        String url = "http://scv2.sh.hivoice.cn:80/service/iss?appendLength=1&wakeupword=%E5%B0%8F%E8%8C%82%E5%B0%8F%E8%8C%82&city=%E6%B7%B1%E5%9C%B3%E5%B8%82&appver=1.0.1&filterName=nlu3&screen=&platform=&audioUrl=false&viewid=&scenario=&udid=LTY2OTg5Mjk4NTAwNWE3YmU1ZTE3YmUxqq&dpi=&filterUrl=http%3A%2F%2F47.107.47.68%3A19998%2Fprocess%2Ftr%2FdataProcess&ver=3.2&method=iss.getTalk&gps=22.554349%2C113.948661&history=&oneshotKeyProperty=wakeup&additionalService=athenaAppService&voiceid=2fa3cfabce6ff4640889f5236ce5028f&appsig=7FF47E6F31169EDFCC4CA58E01613DC3A164E42F&fullDuplex=false&time=2019-03-2110%3A25%3A20&req_nlu_length=1&returnType=json";
//        String params = url.substring(0, url.indexOf("?"));
////        for(String p:params.split("&")){
////            System.out.println(p);
////        }
//        System.out.println(params.substring(0, params.length()-1));

//        File file = new File("E:\\document\\yzs\\program\\班课学生上传模板.xlsx");
//        List<String[]> list = ExcelReader.getExcelData(file, 1);
//        for(String[] a:list){
//            System.out.println(a[1]);
//        }
//        String str = "阔四你好\n" +
//                "科视你好\n" +
//                "你好科视\n" +
//                "你好阔四\n";
//        System.out.println(str.contains("\n"));
//        System.out.println(JSON.toJSONString(str.split("\n")));
//        String str = "D:\\TestSpace\\out.pcm";
//        System.out.println(str.substring(str.lastIndexOf("\\")+1).replaceAll(".pcm", ""));

//        String t = "最终幻想7";
//        System.out.println(t.substring(0, t.lastIndexOf("7")));
//        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
//        long start = System.currentTimeMillis();
//        Thread.sleep(1000);
//        long end = System.currentTimeMillis();
//        System.out.println(start);
//        System.out.println(end);
//        JSONObject json = new JSONObject();
//        System.out.println(json.getString("a"));

//        System.out.println(EncryptUtils.base64Encode("object://5WcujHkJLW-TD3Otug9c80Ji4mOpJe1IhSTTboWpZN0="));
//        downloadTest();

//        System.out.println(IdWorker.getId());
//
//        System.out.println(IdWorker.getIdStr());
//
//        String str = "#";
//
//        String test = "abcd".concat(str).concat("sdafsdgg");
//        System.out.println(test);
//        System.out.println(test.split(str)[1]);
//        System.out.println(EncryptUtils.urlEncode("%E4%BA%91%E7%9F%A5%E5%A3%B0%E5%B0%8F%E5%8C%BA"));
//        System.out.println(EncryptUtils.urlEncode("云知声小区"));

//        Map<String, Object> reqMap = Maps.newHashMap();
//        reqMap.put("token", "tokenddasdf");
//
//        Map<String, Object> jsonMap = Maps.newHashMap();
//        reqMap.put("name", "1111");
//        reqMap.put("name2", "2222");
//        HttpConfig config = HttpConfig.custom()
//                .method(HttpMethods.POST)
//                .url("http://localhost:8813/test")
//                .json(JSON.toJSONString(jsonMap))
//                .map(reqMap)
//                ;
//        System.out.println("========>"+JSON.toJSONString(config.map()));
//        HttpClientUtil.sendAndGetResp(config);

//        AtomicInteger ato = new AtomicInteger(0);
//        ato.addAndGet(100);
//        System.out.println(ato.intValue());
//        CountSyncSpace space = CountSyncSpace.builder()
//                .dbCount(new AtomicInteger(0))
//                .esCount(new AtomicInteger(0))
//                .build();
//
//        for(int i = 0; i< 100; i++){
//            new Thread(new TestThread2(space, 100)).start();
//        }
//        System.out.println(JSON.toJSONString(space));

        String v1 = "382,407,408,409,387,157,410,403,404,406,444,452,494,537,155,154,156,1983,459,458,460,441,440,388,428,431,522,509,521,524,523,518,517,515,520,519,549,547,546,543,542,541,544,448,449,446,450,451,447,430,429,540,513,512,511,510,152,150,151,1000228,144,399,141,145,146,506,507,485,484,848,1909,1910,1878,1911,1877,1881,1882,1879,1880,1885,1883,1884,652,656,650,657,658,651,649,553,659,563,572,570,569,571,1076,566,565,595,594,593,600,599,558,557,556,660,664,730,732,731,714,715,716,733,719,720,741,729,724,723,722,718,735,1004084,804,1004085,1004091,801,736,747,763,742,764,765,756,766,767,768,787,783,788,782,784,789,785,786,1506,1387,930,842,1165,846,843,754,755,887,779,772,771,778,773,818,808,770,837,1004338,475,618,616,622,619,625,623,627,1000059,617,641,620,621,1000061,624,626,1000060,1000058,628,629,630,631,453,1004337,798,797,665,454,685,686,687,526,527,3402,3401,635,640,529,533,530,535,534,614,531,532,632,633,536,528,607,663,646,611,612,610,1235,1700,1234,1002,1524,939,690,695,821,1000063,1123,1216,1532,1236,940,933,1523,1000065,1127,1545,1001,1000,689,932,934,800,1000064,1000062,1702,1525,688,1713,1712,829,828,831,830,822,1988,1987,1986,3405,1962,1963,1964,1965,1966,1967,1961,1691,1693,1690,3422,2041,2036,3421,2034,2035,3498,2037,2038,2039,3458,2033,465,606,482,602,464,615,604,605,645,545,481,669,585,466,661,601,438,603,436,654,636,462,1507,582,608,609,638,811,810,809,805,806,781,780,580,662,1000068,463,637,760,761,439,777,807,655,1000067,1000066,1508,437,2001,1999,2000,847,792,793,1004104,1004103,1004098,2065,817,815,814,813,486,931,1948,993,1199,994,819,311,324,321,412,376,355,340,308,309,310,334,325,346,380,342,343,344,339,341,345,351,350,352,347,322,354,358,317,316,401,402,400,359,434,432,433,360,361,318,362,363,364,365,366,367,368,369,370,371,372,373,374,375,377,378,379,319,348,349,357,356,327,328,353,330,336,329,323,312,337,320,332,326,313,331,333,335,338,315,314,307,418,419,420,426,421,424,415,740,739,123,386,383,384,147,148,127,1526,1420,876,883,907,874,1319,849,938,852,1570,1569,840,841,839,854,1085,1084,947,1173,1039,853,1520,886,1439,1438,1559,1556,1560,1557,1688,1687,1561,1558,1695,1694,1697,1696,1577,1572,1578,1571,838,711,708,709,707,710,706,1167,1174,1224,1171,1172,1169,1168,1166,385,1321,673,676,674,677,675,678,672,422,1282,423,414,704,705,703,701,702,700,1968,1683,1689,1226,1685,1686,1684,1225,751,1680,1681,2040,1679,1389,858,905,1509,1316,1181,992,1529,1317,834,1914,1510,1388,1913,833,3388,3387,3386,3396,3417,3416,3415,981,978,982,977,979,983,1180,980,976,988,985,989,984,986,990,987,944,1391,1392,1390,1899,1903,1900,1901,1904,1906,1905,1894,1266,1268,1270,1264,1265,1269,1267,1271,960,943,443,1274,1276,1278,1272,1273,1277,1275,1279,955,959,3400,1280,684,958,1437,1414,1417,1435,1436,1412,1410,1941,1938,1942,1937,1939,1943,1940,1936,1299,442,864,867,865,820,692,1990,750,749,693,752,1929,613,1950,1949,3379,3378,1315,1385,1386,1384,1981,1980,1977,1383,1957,1958,1956,1959,1955,1314,1568,1251,1252,1249,1254,1250,1253,1255,952,1258,1260,1262,1256,1257,1261,1259,1263,949,1285,1287,1289,1283,1284,1288,1286,1290,953,1530,1519,2057,1302,1304,1306,1300,1301,1305,1308,1307,948,721,3494,3493,1004335,3495,1973,2046,1972,1000014,1000015,1982,1971,1969,1293,1295,1297,1291,1292,1296,1294,950,1872,1871,1876,1873,1875,1870,1869,790,954,1244,957,1896,1874,1951,1128,1223,1013,1012,1014,1018,1101,1238,1011,762,1500,1502,1501,1497,1503,1975,1504,1491,1538,1535,1539,1555,1699,1536,1540,1907,1537,1533,928,1867,1866,1868,1865,1864,869,946,909,871,855,832,951,1381,896,894,895,1379,1380,1149,1155,1156,1151,1152,1153,1154,1148,997,995,1003,998,1161,1007,1005,1006,999,1004,996,904,902,903,900,898,899,1546,1726,1563,1564,1727,1891,1729,1863,1860,1889,1862,1861,1895,1908,1912,1985,1984,1399,1425,1426,1409,1421,1402,1424,1725,1423,1553,1400,1422,1401,1427,1429,1430,1431,1527,1528,1395,1397,1083,1893,1543,1542,1541,1544,1547,1398,1022,1406,1548,1549,1550,1551,1552,1574,1403,1407,1404,1575,1573,1707,1698,1714,1708,1706,1408,1405,3406,1976,1000027,1918,1917,1715,1717,1718,1719,1716,1720,1721,1722,1723,1724,1710,1705,1709,1711,1920,1919,1922,1921,1923,1925,1944,1927,1928,1947,1926,1952,1978,1930,1979,2010,2044,2047,2045,3393,3394,1000057,1000070,1000047,1000120,3500,3499,1000139,1004310,1004329,1000001,1000002,1000004,1000003,1000138,1000123,1004074,1000129,1000246,1839,1840,1837,1842,1841,1838,1799,1846,1847,1844,1848,1849,1845,1843,1804,1802,1815,1816,1803,1800,1801,1809,1810,1811,1812,1813,1814,1798,1000162,2053,2056,2052,2051,2049,2050,2054,2055,2048,1819,1820,1822,1823,1004326,1004325,1004324,1004327,1004323,1000229,1004080,1000225,1000224,1000209,1004321,1004320,1824,1825,1826,1827,1818,1829,1830,1831,1832,1833,1834,1835,1836,1828,1817,1996,1997,2011,2012,2013,2014,2016,2015,2019,2020,2021,2022,2009,2017,2018,1004318,1000212,1000200,1000197,1000196,1000198,1000199,1000192,1004331,1004330,1004096,1004095,1004083,1004328,2023,2024,1994,2025,1995,1992,2026,1998,1993,1991,1004317,1004316,1004315,1000112,1000111,1000102,1000145,1004082,1004081,1000144,1004333,1004334,1000093,1000098,1000099,1000113,1000114,1000154,1000223,1004019,1004093,1004094,1004099,1004100,1004118,1004119,1004307,1004308,1000194,1000205,1000226,1000195,1000206,1000217,1000218,1000227,1000216,1000124,1000166,1000176,1000179,1000189,1000238,1000242,1000247,1000250,1000259,1000268,1000273,1004006,1000210,1000213,1000211,1000201,1000186,1000193,1000075,2230,2261,2231,2232,2262,2079,2233,2263,2234,2235,2264,2080,2236,2265,2237,2238,2266,2081,2239,2267,2240,2268,2082,2241,2242,2083,2243,2244,2084,2245,2269,2246,2270,2085,2203,2247,2204,2205,2248,2070,2206,2207,2208,2071,2209,2249,2210,2211,2250,2072,2212,2213,2214,2073,2215,2251,2216,2217,2252,2074,2218,2253,2219,2220,2254,2075,2221,2255,2222,2223,2256,2076,2224,2257,2225,2226,2258,2077,2227,2259,2228,2229,2260,2078,2061,2064,2063,2316,2416,2317,2417,2318,2418,2319,2419,2320,2420,2095,2321,2421,2322,2422,2323,2423,2324,2424,2325,2425,2096,2326,2426,2327,2427,2328,2428,2329,2429,2330,2430,2097,2331,2431,2332,2432,2333,2433,2334,2434,2335,2435,2098,2336,2436,2337,2437,2338,2438,2339,2439,2340,2440,2099,2341,2441,2342,2442,2343,2443,2344,2444,2345,2445,2100,2346,2446,2347,2447,2348,2448,2349,2449,2350,2450,2101,2351,2451,2352,2452,2353,2453,2354,2454,2355,2455,2102,2356,2456,2357,2457,2358,2458,2359,2459,2360,2460,2103,2361,2461,2486,2362,2462,2487,2363,2463,2488,2364,2464,2489,2365,2465,2490,2104,2271,2371,2471,2272,2372,2472,2273,2373,2473,2274,2374,2474,2275,2375,2475,2086,2366,2466,2367,2467,2368,2468,2369,2469,2370,2470,2105,2276,2376,2277,2377,2278,2378,2279,2379,2280,2380,2087,2281,2381,2476,2282,2382,2477,2283,2383,2478,2284,2384,2479,2285,2385,2480,2088,2286,2386,2481,2491,2287,2387,2482,2492,2288,2388,2483,2493,2289,2389,2484,2494,2290,2390,2485,2495,2089,2291,2391,2292,2392,2293,2393,2294,2394,2295,2395,2090,2296,2396,2297,2397,2298,2398,2299,2399,2300,2400,2091,2301,2401,2302,2402,2303,2403,2304,2404,2305,2405,2092,2306,2406,2307,2407,2308,2408,2309,2409,2310,2410,2093,2311,2411,2312,2412,2313,2413,2314,2414,2315,2415,2094,2060,2550,2836,3116,2551,2837,3117,2552,2838,3118,2553,2839,3119,2554,2840,3120,2555,2841,3121,2115,2556,2842,3122,2557,2843,3123,2558,2844,3124,2559,2845,3125,2560,2846,3126,2561,2847,3127,2116,2562,2848,3128,2563,2849,3129,2564,2850,3130,2565,2851,3131,2566,2852,3132,2567,2853,3133,2117,2568,2854,3134,2569,2855,3135,2570,2856,3136,2571,2857,3137,2572,2858,3138,2573,2859,3139,2118,2574,2860,3140,2575,2861,3141,2576,2862,3142,2577,2863,3143,2578,2864,3144,2579,2865,3145,2119,2580,2866,3146,2581,2867,3147,2582,2868,3148,2583,2869,3149,2584,2870,3150,2585,2871,3151,2120,2586,2872,3152,2587,2873,3153,2588,2874,3154,2589,2875,3155,2590,2876,3156,2591,2877,3157,2121,2592,2878,3158,3342,2593,2879,3159,3343,2594,2880,3160,3344,2595,2881,3161,3345,2596,2882,3162,3346,2597,2883,3163,3347,2122,2598,2884,3164,2599,2885,3165,2600,2886,3166,2601,2887,3167,2602,2888,3168,2603,2889,3169,2123,2604,2890,3170,2605,2891,3171,2606,2892,3172,2607,2893,3173,2608,2894,3174,2609,2895,3175,2124,2496,2782,3062,2497,2783,3063,2498,2784,3064,2499,2785,3065,2500,2786,3066,2501,2787,3067,2106,2610,2896,3176,2611,2897,3177,2612,2898,3178,2613,2899,3179,2614,2900,3180,2615,2901,3181,2125,2616,2902,3182,2617,2903,3183,2618,2904,3184,2619,2905,3185,2620,2906,3186,2621,2907,3187,2126,2622,2908,3188,2623,2909,3189,2624,2910,3190,2625,2911,3191,2626,2912,3192,2627,2913,3193,2127,2628,2914,3194,2629,2915,3195,2630,2916,3196,2631,2917,3197,2632,2918,3198,2633,2919,3199,2128,2634,2920,3200,2635,2921,3201,2636,2922,3202,2637,2923,3203,2638,2924,3204,2639,2925,3205,2129,2640,2926,3206,2641,2927,3207,2642,2928,3208,2643,2929,3209,2644,2930,3210,2645,2931,3211,2130,2646,2932,3212,2647,2933,3213,2648,2934,3214,2649,2935,3215,2650,2936,3216,2651,2937,3217,2131,2652,2938,3218,2653,2939,3219,2654,2940,3220,2655,2941,3221,2656,2942,3222,2657,2943,3223,2132,2658,2944,3224,3348,2659,2945,3225,3349,2660,2946,3226,3350,2661,2947,3227,3351,2662,2948,3228,3352,2663,2949,3229,3353,2133,2664,2950,3230,2665,2951,3231,2666,2952,3232,2667,2953,3233,2668,2954,3234,2669,2955,3235,2134,2502,2788,3068,2503,2789,3069,2504,2790,3070,2505,2791,3071,2506,2792,3072,2507,2793,3073,2107,2670,2956,3236,2671,2957,3237,2672,2958,3238,2673,2959,3239,2674,2960,3240,2675,2961,3241,2135,2676,2962,3242,2677,2963,3243,2678,2964,3244,2679,2965,3245,2680,2966,3246,2681,2967,3247,2136,2682,2968,3248,2683,2969,3249,2684,2970,3250,2685,2971,3251,2686,2972,3252,2687,2973,3253,2137,2688,2974,3254,2689,2975,3255,2690,2976,3256,2691,2977,3257,2692,2978,3258,2693,2979,3259,2138,2694,2980,3260,2695,2981,3261,2696,2982,3262,2697,2983,3263,2698,2984,3264,2699,2985,3265,2139,2700,2986,3266,3354,2701,2987,3267,3355,2702,2988,3268,3356,2703,2989,3269,3357,2704,2990,3270,3358,2705,2991,3271,3359,2140,2706,2992,3272,2707,2993,3273,2708,2994,3274,2709,2995,3275,2710,2996,3276,2711,2997,3277,2141,2712,2998,3278,2713,2999,3279,2714,3000,3280,2715,3001,3281,2716,3002,3282,2717,3003,3283,2142,2718,3004,3284,2719,3005,3285,2720,3006,3286,2721,3007,3287,2722,3008,3288,2723,3009,3289,2143,2724,3010,3290,2725,3011,3291,2726,3012,3292,2727,3013,3293,2728,3014,3294,2729,3015,3295,2144,2508,2794,3074,2509,2795,3075,2510,2796,3076,2511,2797,3077,2512,2798,3078,2513,2799,3079,2108,2730,3016,3296,3360,2731,3017,3297,3361,2732,3018,3298,3362,2733,3019,3299,3363,2734,3020,3300,3364,2735,3021,3301,3365,2145,2736,3022,3302,3366,2737,3023,3303,3367,2738,3024,3304,3368,2739,3025,3305,3369,2740,3026,3306,3370,2741,3027,3307,3371,2146,2742,3028,3308,2743,3029,3309,2744,3030,3310,2745,3031,3311,2746,3032,3312,2747,3033,3313,2147,2748,3034,3314,2749,3035,3315,2750,3036,3316,2751,3037,3317,2752,3038,3318,2753,3039,3319,2148,2754,3040,3320,2755,3041,3321,2756,3042,3322,2757,3043,3323,2758,3044,3324,2759,3045,3325,2149,2760,3046,3326,3372,2761,3047,3327,3373,2762,3048,3328,3374,2763,3049,3329,3375,2764,3050,3330,3376,2765,3051,3331,3377,2150,2766,3052,3332,2767,3053,3333,2768,3054,3334,2769,3055,3335,2151,2770,3056,2771,3057,2772,3058,2773,3059,2774,3060,2775,3061,2152,2153,2776,2777,2778,2779,2780,2781,2154,2514,2800,3080,3336,2515,2801,3081,3337,2516,2802,3082,3338,2517,2803,3083,3339,2518,2804,3084,3340,2519,2805,3085,3341,2109,2520,2806,3086,2521,2807,3087,2522,2808,3088,2523,2809,3089,2524,2810,3090,2525,2811,3091,2110,2526,2812,3092,2527,2813,3093,2528,2814,3094,2529,2815,3095,2530,2816,3096,2531,2817,3097,2111,2532,2818,3098,2533,2819,3099,2534,2820,3100,2535,2821,3101,2536,2822,3102,2537,2823,3103,2112,2538,2824,3104,2539,2825,3105,2540,2826,3106,2541,2827,3107,2542,2828,3108,2543,2829,3109,2113,2544,2830,3110,2545,2831,3111,2546,2832,3112,2547,2833,3113,2548,2834,3114,2549,2835,3115,2114,2059,2155,2179,2156,2180,2157,2181,2158,2182,2159,2183,2160,2184,2066,2161,2185,2162,2186,2163,2187,2164,2188,2165,2189,2166,2190,2067,2167,2191,2168,2192,2169,2193,2170,2194,2171,2195,2172,2196,2068,2173,2197,2174,2198,2175,2199,2176,2200,2177,2201,2178,3382,3381,2202,2069,2062";
        String v2 = "406,403,382,157,410,408,387,409,407,404,381,388,440,441,153,154,155,156,140,141,144,145,146,399,1000228,149,150,151,152,537,494,444,452,483,484,485,445,446,448,449,447,450,451,505,506,507,508,509,456,848,1877,1878,1879,1880,1881,1882,1883,1884,1885,1909,1910,1911,427,428,429,430,431,552,553,457,458,459,1983,460,495,510,511,512,513,515,517,518,519,520,521,522,523,524,540,541,542,543,544,546,547,549,649,658,650,657,651,652,656,659,561,563,564,565,566,577,593,594,595,599,600,568,569,570,572,571,1076,555,556,557,558,660,715,732,731,730,714,716,733,664,717,741,712,718,722,723,724,729,713,719,720,691,801,1004085,1004091,804,1004084,747,735,736,766,764,763,765,756,734,742,767,743,842,843,846,930,1165,1387,1506,738,887,757,782,783,784,785,786,787,788,789,753,754,755,768,773,772,778,771,779,808,818,837,803,2065,1004098,1004103,1004104,769,770,791,792,793,812,813,814,815,817,858,774,1999,2000,2001,836,847,992,905,1181,1316,1389,1317,1509,1529,897,898,899,900,857,995,997,1148,1151,1155,1156,1152,1153,1154,996,998,999,1003,1004,1005,1006,1007,1161,1149,1379,1380,893,894,895,896,1170,1381,901,902,903,904,435,436,438,464,465,466,481,482,545,585,601,602,603,604,605,606,615,645,661,669,437,462,636,654,1507,1508,1000066,1000067,439,760,761,463,637,655,777,807,1000068,580,780,781,126,582,127,123,147,383,384,608,386,609,148,638,307,662,308,805,809,309,310,311,312,313,314,315,316,317,810,811,318,319,320,321,322,323,324,325,326,327,328,329,330,331,806,332,453,475,333,334,335,336,337,338,339,340,341,342,1004338,343,616,344,618,345,619,346,622,347,625,348,349,350,351,628,352,629,630,353,354,355,356,357,358,359,400,401,631,1000058,402,360,432,433,1000059,434,623,361,362,363,364,365,366,367,368,369,370,627,371,372,1000060,624,373,626,374,375,1000061,617,620,621,641,454,665,797,798,1004337,526,685,686,687,527,376,377,378,379,380,528,412,529,530,531,532,533,534,535,536,415,614,418,632,419,420,421,424,426,739,740,385,414,633,422,635,423,1282,486,672,673,674,675,676,677,678,1321,700,701,702,703,704,705,706,707,708,640,709,607,710,711,751,819,931,993,994,1199,1948,838,839,840,1569,1570,841,849,852,938,1319,853,854,947,1039,1173,1084,1085,886,1520,1438,1439,1556,1559,1557,1560,1558,1561,1571,1578,610,611,612,646,663,688,1235,1525,1700,1702,1000062,1000063,690,695,821,939,1002,1234,1524,1000064,689,800,932,934,1000,1001,1127,1545,1000065,933,940,1572,1577,1687,1688,1694,1695,1123,1216,1236,1523,1532,822,828,829,1712,1713,830,831,1690,1691,1696,1697,874,876,883,907,1166,1693,1167,1961,1168,1962,1169,3405,1171,1963,1174,1964,1965,1966,1967,1986,1987,1224,1988,1172,2033,1225,2034,3421,2035,2036,2041,2037,2038,2039,3422,3458,3498,3401,3402,1226,1683,1684,1685,1686,1689,1420,1526,1679,1680,1681,2040,1968,1564,1862,1729,1546,1726,1727,1860,1861,1863,1563,1889,1857,1984,1985,1019,1022,1398,1083,1397,1395,1399,1400,1401,1402,1409,1421,1422,1423,1424,1425,1426,1427,1429,1430,1431,1527,1528,1553,1725,1403,1406,1404,1407,1405,1408,1917,1918,1976,3406,1000027,411,442,443,943,684,1280,3400,721,762,790,820,864,865,867,832,855,869,871,909,946,833,834,1388,1510,1913,1914,928,944,984,985,986,987,988,989,990,948,1300,1301,1302,1304,1305,1306,1307,1308,949,1256,1257,1258,1259,1260,1261,1262,1263,950,1291,1292,1293,1294,1295,1296,1297,951,952,1249,1250,1251,1252,1253,1254,1255,953,1283,1284,1285,1286,1287,1288,1289,1290,954,955,1272,1273,1274,1275,1276,1277,1278,1279,957,1244,958,959,960,1264,1265,1266,1267,1268,1269,1270,1271,976,977,978,979,980,981,982,983,1180,1011,1012,1013,1014,1018,1101,1223,1238,1128,1299,1314,1315,613,692,693,749,750,752,1929,1990,1949,1950,3378,3379,1383,1384,1385,1386,1977,1980,1981,1955,1956,1957,1958,1959,1390,1391,1392,1410,1412,1414,1417,1435,1436,1437,1491,1497,1500,1501,1502,1503,1504,1975,1519,1530,1533,1535,1536,1537,1538,1539,1540,1555,1699,1907,1568,1864,1865,1868,1866,1867,1869,1870,1873,1875,1871,1872,1876,1874,1894,1899,1900,1901,1903,1904,1905,1906,1896,1936,1937,1938,1939,1940,1941,1942,1943,1951,1969,1971,1972,1973,3493,3494,3495,1004335,2046,1982,1000014,1000015,2057,3386,3387,3388,3396,3415,3416,3417,1891,1908,1919,1895,1921,1912,1920,1925,1922,1927,1928,1923,1926,1531,1541,1542,1543,1544,1547,1548,1549,1550,1551,1552,1573,1574,1575,1698,1705,1706,1707,1708,1709,1710,1711,1714,1715,1892,1893,1716,1717,1718,1719,1720,1721,1722,1723,1724,1979,3394,2010,1952,1930,1947,1944,2047,3393,2045,2044,1978,3398,3380,3499,3500,1000070,1000047,1000057,1000120,1000139,1004310,1004329,1004333,1000159,1000162,1000137,1000138,1000245,1000246,3501,1000001,1000002,1000003,1000004,1000122,1000123,1000129,1004074,1004334,1000097,1000145,1000102,1000111,1000112,1004315,1004316,1004317,1004318,1000100,1000209,1000224,1000225,1000229,1004080,1004320,1004321,1004323,1004324,1004325,1004326,1004327,1000101,1000144,1004081,1004082,1004083,1004095,1004096,1004328,1000192,1000196,1000197,1000198,1000199,1000200,1797,1798,1800,1000212,1004330,1004331,1802,1804,1803,1815,1816,1801,1809,1810,1811,1812,1813,1814,1799,1837,1839,1840,1838,1841,1842,1817,1818,1819,1820,1822,1823,1824,1825,1826,1827,1828,1829,1830,1831,1832,1833,1834,1835,1836,1843,1844,1846,1847,1845,1848,1849,1991,1992,1994,1996,1997,2009,2011,2012,2013,2014,2015,2016,2017,2018,2019,2020,2021,2022,2023,2024,1995,2025,1993,1998,2026,2048,2049,2050,2051,2052,2053,2056,2054,2055,1000074,1000075,1000093,1000098,1000099,1000113,1000114,1000124,1000154,1000166,1000176,1000179,1000186,1000201,1000189,1000193,1000194,1000195,1000205,1000206,1000210,1000211,1000213,1000216,1000218,1000217,1000223,1000226,1000227,1000238,1000242,1000247,1000250,1000259,1000268,1000273,1004006,1004019,1004093,1004094,1004099,1004100,1004118,1004119,1004307,1004308,2058,2059,2106,2496,2497,2498,2499,2500,2501,2782,2783,2784,2785,2786,2787,3062,3063,3064,3065,3066,3067,2107,2502,2503,2504,2505,2506,2507,2788,2789,2790,2791,2792,2793,3068,3069,3070,3071,3072,3073,2108,2508,2509,2510,2511,2512,2513,2794,2795,2796,2797,2798,2799,3074,3075,3076,3077,3078,3079,2109,2514,2515,2516,2517,2518,2519,2800,2801,2802,2803,2804,2805,3080,3081,3082,3083,3084,3085,3336,3337,3338,3339,3340,3341,2110,2520,2521,2522,2523,2524,2525,2806,2807,2808,2809,2810,2811,3086,3087,3088,3089,3090,3091,2111,2526,2527,2528,2529,2530,2531,2812,2813,2814,2815,2816,2817,3092,3093,3094,3095,3096,3097,2112,2532,2533,2534,2535,2536,2537,2818,2819,2820,2821,2822,2823,3098,3099,3100,3101,3102,3103,2113,2538,2539,2540,2541,2542,2543,2824,2825,2826,2827,2828,2829,3104,3105,3106,3107,3108,3109,2114,2544,2545,2546,2547,2548,2549,2830,2831,2832,2833,2834,2835,3110,3111,3112,3113,3114,3115,2115,2550,2551,2552,2553,2554,2555,2836,2837,2838,2839,2840,2841,3116,3117,3118,3119,3120,3121,2116,2556,2557,2558,2559,2560,2561,2842,2843,2844,2845,2846,2847,3122,3123,3124,3125,3126,3127,2117,2562,2563,2564,2565,2566,2567,2848,2849,2850,2851,2852,2853,3128,3129,3130,3131,3132,3133,2118,2568,2569,2570,2571,2572,2573,2854,2855,2856,2857,2858,2859,3134,3135,3136,3137,3138,3139,2119,2574,2575,2576,2577,2578,2579,2860,2861,2862,2863,2864,2865,3140,3141,3142,3143,3144,3145,2120,2580,2581,2582,2583,2584,2585,2866,2867,2868,2869,2870,2871,3146,3147,3148,3149,3150,3151,2121,2586,2587,2588,2589,2590,2591,2872,2873,2874,2875,2876,2877,3152,3153,3154,3155,3156,3157,2122,2592,2593,2594,2595,2596,2597,2878,2879,2880,2881,2882,2883,3158,3159,3160,3161,3162,3163,3342,3343,3344,3345,3346,3347,2123,2598,2599,2600,2601,2602,2603,2884,2885,2886,2887,2888,2889,3164,3165,3166,3167,3168,3169,2124,2604,2605,2606,2607,2608,2609,2890,2891,2892,2893,2894,2895,3170,3171,3172,3173,3174,3175,2125,2610,2611,2612,2613,2614,2615,2896,2897,2898,2899,2900,2901,3176,3177,3178,3179,3180,3181,2126,2616,2617,2618,2619,2620,2621,2902,2903,2904,2905,2906,2907,3182,3183,3184,3185,3186,3187,2127,2622,2623,2624,2625,2626,2627,2908,2909,2910,2911,2912,2913,3188,3189,3190,3191,3192,3193,2128,2628,2629,2630,2631,2632,2633,2914,2915,2916,2917,2918,2919,3194,3195,3196,3197,3198,3199,2129,2634,2635,2636,2637,2638,2639,2920,2921,2922,2923,2924,2925,3200,3201,3202,3203,3204,3205,2130,2640,2641,2642,2643,2644,2645,2926,2927,2928,2929,2930,2931,3206,3207,3208,3209,3210,3211,2131,2646,2647,2648,2649,2650,2651,2932,2933,2934,2935,2936,2937,3212,3213,3214,3215,3216,3217,2132,2652,2653,2654,2655,2656,2657,2938,2939,2940,2941,2942,2943,3218,3219,3220,3221,3222,3223,2133,2658,2659,2660,2661,2662,2663,2944,2945,2946,2947,2948,2949,3224,3225,3226,3227,3228,3229,3348,3349,3350,3351,3352,3353,2134,2664,2665,2666,2667,2668,2669,2950,2951,2952,2953,2954,2955,3230,3231,3232,3233,3234,3235,2135,2670,2671,2672,2673,2674,2675,2956,2957,2958,2959,2960,2961,3236,3237,3238,3239,3240,3241,2136,2676,2677,2678,2679,2680,2681,2962,2963,2964,2965,2966,2967,3242,3243,3244,3245,3246,3247,2137,2682,2683,2684,2685,2686,2687,2968,2969,2970,2971,2972,2973,3248,3249,3250,3251,3252,3253,2138,2688,2689,2690,2691,2692,2693,2974,2975,2976,2977,2978,2979,3254,3255,3256,3257,3258,3259,2139,2694,2695,2696,2697,2698,2699,2980,2981,2982,2983,2984,2985,3260,3261,3262,3263,3264,3265,2140,2700,2701,2702,2703,2704,2705,2986,2987,2988,2989,2990,2991,3266,3267,3268,3269,3270,3271,3354,3355,3356,3357,3358,3359,2141,2706,2707,2708,2709,2710,2711,2992,2993,2994,2995,2996,2997,3272,3273,3274,3275,3276,3277,2142,2712,2713,2714,2715,2716,2717,2998,2999,3000,3001,3002,3003,3278,3279,3280,3281,3282,3283,2143,2718,2719,2720,2721,2722,2723,3004,3005,3006,3007,3008,3009,3284,3285,3286,3287,3288,3289,2144,2724,2725,2726,2727,2728,2729,3010,3011,3012,3013,3014,3015,3290,3291,3292,3293,3294,3295,2145,2730,2731,2732,2733,2734,2735,3016,3017,3018,3019,3020,3021,3296,3297,3298,3299,3300,3301,3360,3361,3362,3363,3364,3365,2146,2736,2737,2738,2739,2740,2741,3022,3023,3024,3025,3026,3027,3302,3303,3304,3305,3306,3307,3366,3367,3368,3369,3370,3371,2147,2742,2743,2744,2745,2746,2747,3028,3029,3030,3031,3032,3033,3308,3309,3310,3311,3312,3313,2148,2748,2749,2750,2751,2752,2753,3034,3035,3036,3037,3038,3039,3314,3315,3316,3317,3318,3319,2149,2754,2755,2756,2757,2758,2759,3040,3041,3042,3043,3044,3045,3320,3321,3322,3323,3324,3325,2150,2760,2761,2762,2763,2764,2765,3046,3047,3048,3049,3050,3051,3326,3327,3328,3329,3330,3331,3372,3373,3374,3375,3376,3377,2151,2766,2767,2768,2769,3052,3053,3054,3055,3332,3333,3334,3335,2152,2770,2771,2772,2773,2774,2775,3056,3057,3058,3059,3060,3061,2153,2154,2776,2777,2778,2779,2780,2781,2060,2086,2271,2272,2273,2274,2275,2371,2372,2373,2374,2375,2471,2472,2473,2474,2475,2087,2276,2277,2278,2279,2280,2376,2377,2378,2379,2380,2088,2281,2282,2283,2284,2285,2381,2382,2383,2384,2385,2476,2477,2478,2479,2480,2089,2286,2287,2288,2289,2290,2386,2387,2388,2389,2390,2481,2482,2483,2484,2485,2491,2492,2493,2494,2495,2090,2291,2292,2293,2294,2295,2391,2392,2393,2394,2395,2091,2296,2297,2298,2299,2300,2396,2397,2398,2399,2400,2092,2301,2302,2303,2304,2305,2401,2402,2403,2404,2405,2093,2306,2307,2308,2309,2310,2406,2407,2408,2409,2410,2094,2311,2312,2313,2314,2315,2411,2412,2413,2414,2415,2095,2316,2317,2318,2319,2320,2416,2417,2418,2419,2420,2096,2321,2322,2323,2324,2325,2421,2422,2423,2424,2425,2097,2326,2327,2328,2329,2330,2426,2427,2428,2429,2430,2098,2331,2332,2333,2334,2335,2431,2432,2433,2434,2435,2099,2336,2337,2338,2339,2340,2436,2437,2438,2439,2440,2100,2341,2342,2343,2344,2345,2441,2442,2443,2444,2445,2101,2346,2347,2348,2349,2350,2446,2447,2448,2449,2450,2102,2351,2352,2353,2354,2355,2451,2452,2453,2454,2455,2103,2356,2357,2358,2359,2360,2456,2457,2458,2459,2460,2104,2361,2362,2363,2364,2365,2461,2462,2463,2464,2465,2486,2487,2488,2489,2490,2105,2366,2367,2368,2369,2370,2466,2467,2468,2469,2470,2061,2070,2203,2204,2205,2247,2248,2071,2206,2207,2208,2072,2209,2210,2211,2249,2250,2073,2212,2213,2214,2074,2215,2216,2217,2251,2252,2075,2218,2219,2220,2253,2254,2076,2221,2222,2223,2255,2256,2077,2224,2225,2226,2257,2258,2078,2227,2228,2229,2259,2260,2079,2230,2231,2232,2261,2262,2080,2233,2234,2235,2263,2264,2081,2236,2237,2238,2265,2266,2082,2239,2240,2267,2268,2083,2241,2242,2084,2243,2244,2085,2245,2246,2269,2270,2062,2066,2155,2156,2157,2158,2159,2160,2179,2180,2181,2182,2183,2184,2067,2161,2162,2163,2164,2165,2166,2185,2186,2187,2188,2189,2190,2068,2167,2168,2169,2170,2171,2172,2191,2192,2193,2194,2195,2196,2069,2173,2174,2175,2176,2177,2178,2197,2198,2199,2200,2201,2202,3381,3382,2063,2064";
        Set<String> set1 = new HashSet<>();
        for(String v:v1.split(",")){
            set1.add(v);
        }
        for(String v:v2.split(",")){
            if(!set1.contains(v)){
                System.out.print(v+",");
            }
        }
    }


    public static class TestThread2 implements Runnable{

        private CountSyncSpace space;

        private int step;

        public TestThread2(CountSyncSpace space, int step){
            this.step = step;
            this.space = space;
        }

        @Override
        public void run() {
            for(int i = 0 ;i <step; i++){
                space.getDbCount().addAndGet(10);
                space.getEsCount().incrementAndGet();
            }
        }
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CountSyncSpace{

        private String uuid;

        private AtomicInteger dbCount;

        private AtomicInteger esCount;
    }

    public static void maineee(String[] args) {
        String repeat = "_1";
        String codePath = "E:\\code\\gitee\\wx-protocol-java\\com\\wechat";
        String itPath = codePath + "\\capital\\";
        File dir = new File(codePath);
        for (File f : dir.listFiles()) {
            String fname = f.getName();
            if (fname.indexOf(repeat) != -1) {
                fname = fname.replaceAll(repeat, "");
                File nf = new File(itPath.concat(fname));
                try {
                    FileUtils.copyFile(f, nf);
                    FileUtils.deleteQuietly(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public static void main222(String[] args) {
        int a = 0x112;
        System.out.println(a);
    }

    public static void main11111111111(String[] args) {
//        System.out.println(RandomStringUtils.randomAlphanumeric(18));
//        System.out.println(RandomStringUtils.randomAlphanumeric(18));
//        JSONObject j = new JSONObject();
//        j.put("idx", 11);
//        System.out.println(j.getString("idx"));
//        List<String> list = Arrays.asList("e2b763586d78e1ae","21223adfgg");
//        System.out.println(new Date().getTime() / 1000);
//        System.out.println(JSON.toJSONString(list));

        System.out.println(-5 >>> 2);
        System.out.println(-5 >> 2);
        System.out.println(5 >>> 2);
        /**
         * -5 >>> 2
         * -5  的补码 :  1111 1111 1111 1111 1111 1111 1111 1011
         * 右移两位 高位 补 0  -- >  0011 1111 1111 1111 1111 1111 1111 1110   直接计算 = 1,073,741,822
         *
         * -5 >> 2
         * 右移两位 高位 补 1,  1111 1111 1111 1111 1111 1111 1111 1110
         *  减1 取反 :  0010 = -2
         *
         * 5 >>> 2
         * 5 的补码:  0101 右移两位 01 = 1
         *
         */
    }

    private static String cur_path = "E:\\document\\yzs\\program\\疫情排查\\";
    public static void main3333(String[] args) {
        String file = cur_path+"数据对比.xlsx";
        List<String[]> list = ExcelReader.getExcelData(new File(file), 1);

        List<ExcelM> eList = Lists.newArrayList();
        for(String[] array:list){
            if(Objects.nonNull(array) && array.length >= 8 && StringUtils.hasText(array[0])){
                ExcelM e = new ExcelM();
                e.setCodeJd(array[0]);
                e.setNameJd(array[1]);
                e.setCodeJw(array[2]);
                e.setNameJw(array[3]);
                e.setOurCodeJd(array[4]);
                e.setOurNameJd(array[5]);
                e.setOurCodeJw(array[6]);
                e.setOurNameJw(array[7]);

                if(e.getCodeJw().equals(e.getOurCodeJw())){
                    e.setSameCode("是");
                }else{
                    e.setSameCode("否");
                }
                eList.add(e);

            }else {
                break;
            }
        }
        String out = cur_path + "out.xlsx";
        String[] headers = {"街道编码", "街道名称", "居委编码", "居委名称", "街道编码", "街道名称", "居委编码", "居委名称", "编码是否一致"};
        String[] properties = {"codeJd", "nameJd", "codeJw", "nameJw", "ourCodeJd", "ourNameJd", "ourCodeJw", "ourNameJw", "sameCode"};
        ExcelExportUtil.export(headers, properties, eList, false, out);

    }

    @Data
    public static class ExcelM{
        private String codeJd;

        private String nameJd;

        private String codeJw;

        private String nameJw;

        private String ourCodeJd;

        private String ourNameJd;

        private String ourCodeJw;

        private String ourNameJw;

        private String sameCode;
    }

    public static void main1233(String[] args) {
//        System.out.println(IdWorker.get32UUID());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis()-1000*60);
        System.out.println(timestamp.getTime());
        System.out.println(System.currentTimeMillis());

//        String str = "我是一个兵";
//        String unicode = "\\u597D\\u7684\\uFF0C\\u5DF2\\u4E3A\\u60A8\\u8054\\u7CFB\\u524D\\u53F0";
//        try {
//            System.out.println(EncryptUtils.urlDecode("%E4%BD%A0%E6%98%AF%E5%82%BB%E9%80%BC%E5%90%97%EF%BC%8C"));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        int yearStart = 2000;
//        int yearEnd = 2099;
//
//        List<DateModel> retList = new ArrayList<>();// 输出 json 对象
//        int idx = 0;
//        // 循环年
//        for(int i = yearStart; i <= yearEnd; i++){
//            idx ++;
//            String year = String.valueOf(i);
//            DateModel c = DateModel.builder()
//                    .id(idx)
//                    .value(year)
//                    .build();
//            // 处理 月份
//            List<DateModel> months = new ArrayList<>();
//            for(int j = 1; j <= 12; j ++){
//                String mStr = j < 10 ? ("0"+j) : String.valueOf(j); //月份
//                DateModel m = DateModel.builder()
//                        .id(j)
//                        .value(mStr)
//                        .build();
//                months.add(m);
//
//                // 处理天
//                List<DateModel> days = new ArrayList<>();
//                // 获取 当月 有多少天
//                String lastDay = DateUtil.getLastDayOfMonth(year, mStr);
//                int last = Integer.parseInt(lastDay.substring(lastDay.lastIndexOf("-")+1));
//                for(int k = 1; k <= last; k++){
//                    String dStr = k < 10 ? ("0"+k) : String.valueOf(k);
//                    DateModel d = DateModel.builder()
//                            .id(k)
//                            .value(dStr)
//                            .build();
//                    days.add(d);
//                }
//                m.setChilds(days);
//
//
//            }
//
//            c.setChilds(months);
//            retList.add(c);
//        }
//
//        ReadTxtFileUtils.writeToTxt(Arrays.asList(JSON.toJSONString(retList)), "E:\\secureCRT_file\\download\\result.asrcheck");
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DateModel{
        private int id;

        private String value;

        private List<DateModel> childs;
    }

    /**
     * 获取请求头
     * @return
     */
    public static Header[] getReqHeaders(){
        return HttpHeader.custom()
                .other("RbaToken", "vDr2DXrJDn1YYiGze2uGi26QujKKgGZUAjPvTDzgGGPbR5+omOwDOHizFf2MRl5xIUBCLJGvx4ymAQvYeg9C+RjbawCMYEQRUF9pr1luZww6w4sgTaraj3wGBbtECq7p")
                .other("UserName", "unisound_test")
                .build();
    }

    public static void downloadTest() {
        String url = "http://192.168.3.248:58830/apiRba/entrancePassRecord/getImageByUri/hitImage??uriBase64=b2JqZWN0Oi8vNVdjdWpIa0pMVy1URDNPdHVnOWM4NktuWUs3Z0Rjb3JNaXhIbXV4RzFKWT0";
        File downloadFile = new File("D:\\temp\\eeeee.png");
        try {
            HttpConfig config = HttpConfig.custom()
                    .method(HttpMethods.GET)
                    .url(url)
                    .headers(getReqHeaders())
                    .out(new FileOutputStream(downloadFile));
            HttpClientUtil.down(config);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (HttpProcessException e) {
            e.printStackTrace();
        }
    }


        @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ExcelModel{

        private String udid;

        private String sessionId;
    }




    public static void callableThreadTest(){
        TestThread t = new TestThread();
        TestThread t2 = new TestThread();
        FutureTask<String> f1 = new FutureTask<>(t);
        FutureTask<String> f2 = new FutureTask<>(t2);
        new Thread(f1).start();
        new Thread(f2).start();
    }


    public static class TestThread implements Callable<String>{
        @Override
        public String call() throws Exception {
            System.out.println(Thread.currentThread().getName()+" in....");
            Thread.sleep(3000);
            return "success";
        }
    }

    public static void filePathMkdirs(String targetPath){
        File file = new File(targetPath.concat("mp3"));
        file.mkdirs();
        file = new File(targetPath.concat("pcm"));
        file.mkdirs();
    }

    public static int switchTest(int s){

        switch (s){
            case 1:
                System.out.println("1111111");
                break;
            case 2:
                System.out.println("22222222");
                break;
            case 3:
                System.out.println("333333333");
                break;
        }
        return -1;
    }
}
