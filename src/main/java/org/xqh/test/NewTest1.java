package org.xqh.test;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.xqh.test.gc.securitybcrypt.TestInterface;
import org.xqh.utils.encrypt.EncryptUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName NewTest1
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/8/6 14:04
 * @Version 1.0
 */
@Slf4j
public class NewTest1 {

    public static volatile boolean switchBtn = false;

    public static LoadingCache<String, String> cache = CacheBuilder.newBuilder().maximumSize(1000)
            .refreshAfterWrite(3, TimeUnit.SECONDS)// 3秒刷新数据, 一个线程去获取最新值, 其他可以先获取旧的缓存值 返回.
//            .expireAfterWrite(3, TimeUnit.SECONDS) // 3秒刷新数据, 一个线程获取最新值 其他所有线程阻塞等待
//            .expireAfterAccess(10, TimeUnit.SECONDS) // 限定时间内 没有 访问 该数据, 会 执行异步 刷新缓存数据方法.
            .build(
                    new CacheLoader<String, String>() {
                        @Override
                        public String load(String s) throws Exception {
                            return loadVal(s);
                        }
                    }
            );
//
    public static String loadVal(String s) {
        if(switchBtn){
            return null;
        }
        String result = String.valueOf(System.currentTimeMillis());
        return result;
    }

    private static String splitYearFromDateStr(String date){
        if(StringUtils.hasText(date) && date.length() == 10){
            return date.substring(0, 4);
        }
        return null;
    }


    public static void main222(String[] args) throws InterruptedException {
        String key = "123";
        System.out.println(cache.getUnchecked(key));
        Thread.sleep(2000);
        System.out.println(cache.getUnchecked(key));
        Thread.sleep(2000);
        System.out.println(cache.getUnchecked(key));
        switchBtn = true;
        Thread.sleep(2000);
        System.out.println(cache.getUnchecked(key));
        Thread.sleep(3100);
        System.out.println(cache.getUnchecked(key));
//        String jwt = "eyJhbGciOiJIUzI1NiJ9.eyJ0ZW5hbnRUeXBlIjoiMyIsInVpZCI6ImFkbWluIiwiYWxsT3JnQ29kZSI6W10sImVtcE51bSI6bnVsbCwicG9zdGlvbkNvZGVzIjpbXSwiaXNUZW5hbnRNYW5hZ2VyIjp0cnVlLCJuYW1lIjoi566h55CG5ZGYIiwibW9iaWxlIjoiMTg5MjkzMDU2OTQiLCJ0ZW5hbnRJZCI6IjFvdG44Ymd3dHByYyIsImxhbmd1YWdlIjoiemgtQ04iLCJhbGxPcmdDb2RlUGF0aCI6W10sImVtYWlsIjoiYWRtaW5AZ2V0ZWNoLmNuIn0.u7nygtgx89NMT7zYGymsNta7p8LKLYTQ6yagHs81UiU";
//        System.out.println(EncryptUtils.base64Decode(jwt));
    }

    /**
     *
     */
    public static void pushMsg(){

    }

    public static void main(String[] args) {
//        TestInterface test = new TestInterface() {
//            @Override
//            public void test() {
//                System.out.println("test");
//            }
//        };
//
//        test.test();
//        System.out.println("next");

//        String msg = "ak=EC671JJ212409D4A&data=整机主板测试结果报告\\r\\n————产品基本信息————\\r\\n测试时间：2024-09-14 16:12:13\\r\\n工位：008\\r\\n产品型号：DXP4800\\r\\n设备型号：DXP4800 \\r\\n固件版本号：1.00.1305 \\r\\nDDR容量：8G \\r\\nMCU版本：version:V1.0.23 \\r\\nBIOS版本：AD0X0190 \\r\\n固件完整性：c67aed95db53fd47260067613fad5eed \\r\\nEMMC寿命：1 1 \\r\\n授权状态：已授权 \\r\\n————人工确认项————\\r\\nHDMI接口：有图像\\r\\nHDMI接口：有声音\\r\\n指示灯：是否全部闪烁\\r\\n————烧号确认项————\\r\\n注意：该设备产测配置为MAC地址自动填写\\r\\nModel：DXP4800\\r\\nSN：EC671JJ212409D4A\\r\\nMAC(eth0)：6C:1F:F7:10:85:EA\\r\\nMAC(eth1)：6C:1F:F7:10:85:EB\\r\\n————自动测试项————\\r\\n————设备授权项目————\\r\\n设备激活结果【成功】&mac=6C:1F:F7:10:85:EA&ts=1726301534&SecretKey=wPrs4jow21cQRBjwNY6bvWkNhNtk6SXqhPGMiqLGbIzfzibBzxdEpgXr8LBAVmNd";
//        System.out.println(EncryptUtils.getSha256(msg));

//        System.out.println(splitYearFromDateStr("2021-10-12"));

        List<Integer> list = Lists.newArrayList(1, 2, 3, 4, 5, 6);
        System.out.println(list.subList(0, 4));
    }
}
