package org.xqh.test.randomstr;


import org.apache.commons.lang3.RandomStringUtils;
import org.xqh.utils.encrypt.EncryptUtils;
import org.xqh.utils.encrypt.RSAUtils;

import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.*;

/**
 * @ClassName RandomStrTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/2/14 10:56
 * @Version 1.0
 */
public class RandomStrTest {

    public static void main121111(String[] args) {
        // 不带数字 全字母f17wkdpsrnuh
        //  FmwYhh2b6z5TiVYcvncRuQdbJs2P0Nsj   ugreen
//        System.out.println(RandomStringUtils.randomAlphabetic(16));
        String key = "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDDd6eh8FEadARU\ndifjufpBiZQEemDnUrztw58brleTJLUWsoJzZW0EXqEnOboqUPkvQX5RuB9Z+eR5\nFimYrICwoNJNZbfdO/Az+VUyhxwhntkTM/Rk5djJa7lUV/EtjD2q3XOOOnkDYncz\njVz2NusFglp1fT7nbRVarEMoPnRI9C9GKNYd7sbGwubmZmdAqOb0I9CkcLCpjkS/\n3vhGW2U34WhxFEbNMcgo4kB0P425iSVMseJa7/6ZwVIWZuhNXskyseK/YZPNJaKd\nlLWkwqnl3AGcRDydukweqJJCoabwQfxWFboVFjBxFM4vN5TnrwN2oaye3cfgIK/r\nmPukFcCvAgMBAAECggEAQTAoE01CtxhHE2X+Yqe1CuNXnnCz3aHNyVvKteAca8G7\ntRCeid6jD2iBxUOuzwsHN5fXiWhm6Ep7RFi8UaMCHiXcTRylx54EO0klfT7rBllk\nGs/iaz3uYSpQeg0ELYUOUewd7CqbDiJqD6LPBHwHQGSbR4CYGHVcQEuf8A14GZXk\nM6Ipr5aaJqqoQWWinzufKW56cUCx+7q5BARsw0jPSwoNQGkTXYMXTz3IbDFFZ5UP\nGM4NeC/LC+2rUPCcc2cfxy4fHOhzLaMaV2lER1kV4WPqKP52ay9cHoRqBRY3HNuo\niHtCI5k4L4ZIAthehgNCX09BCY0EUNfeVQaqSv99UQKBgQDKYKyYV4ibZoXGgS0K\nRe2Jv/1mYSFADEapVJkgNPrfQ0Q5adCAsV6GehsP0FqjXTDM3Ec2ULaNs2ottKIh\nt4Prjf9buSq3nVhNKAl1RzJXfTDGCqw6KA2pLbZhDn2MCcN0FDYR7/DIarFaGKzR\n4fMiysSWxrE+Cz+bV6ydoNzfsQKBgQD3QkIB7/ndVKU4Afit4wo7st/pXfwJUgXW\nSllUrXFiBEUZrqviqFOAb5GvJmnBL6xM6lVFBHznXywuxRT/amBUoraWnRXtvL6L\n9V6zIDPCVhhHJdo9jf+eK3BwEmlQPssqDkgIn/NCvAlIR+UpliqhBfxptEkSf2o2\nG1onMGgeXwKBgAUD0EMWKDRxne8FVHBkuzYmo8jdr00/AKFBj2nU5kKSECa8EIaf\n3o/GhtUlfJl/va3V543ZAi0QvRjU2Q+s1IAJzMIhHIXgSEfhLhrR4NV6LLM7oHjw\ndSY0E8jj6wiFVWqS9aQMJhNRUYPCCMZKqxO5opV55vp6nODkk0p0qgSBAoGAUtnY\n1F1dFFTwp7vBYELbgvWFIeVkNptNaWiwVjdn7rgPj3OAFQriL/8yicYNWbZnvb4n\npGDEmaLCY0mWp581eCatl/o7BtZf4vXtLYh0z4GZ0P1WC5oxNQbJPkRtFX51BTqT\n2JAAKzRmx9mPaEfxMNOYqLLIaXJYXJj9tHP773ECgYAmpzNRvK4blNflXszZBXrN\nPIiX4ZG+xHzYeXrHgslNJvjpI/J9yTGsKQtCnA7HQgkKz9LglI8sNSCil1lOB80q\nJX9B+W+8M647nrVqPlyIltRsvxuXpXhUQSqWlCX84PxB9Wt9Xqe7IBNYD3XfZJL3\nzyaO18E2An9ZzqnWn1cLDQ==\n-----END PRIVATE KEY-----\n";

        String secret = "";



        System.out.println(EncryptUtils.base64Encode("VtwBGOoRnuchu6KbWWQWXLODBxqznFGvhAtB2WIJj31XrMH4rNR3Qmt7yPHY7hPm"));
        System.out.println(EncryptUtils.base64Decode("VnR3QkdPb1JudWNodTZLYldXUVdYTE9EQnhxem5GR3ZoQXRCMldJSmozMVhyTUg0ck5SM1FtdDd5UEhZN2hQbQ"));
        System.out.println(EncryptUtils.base64Decode("VGhpc0lzTXlDdXN0b21TZWNyZXRLZXkwMTIzNDU2Nzg="));

//        System.out.println("DrAVZZLJTwhhM2eV5cjkUnxWdHguXK0n".length());
//        for(int i = 0; i < 20; i ++){
//            System.out.println(RandomStringUtils.randomAlphanumeric(32));
//        }
//        //字母+数字  9gpsrfwtr3ie0mvw 5t1k38ij5kmrhesv
        System.out.println(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
        System.out.println(RandomStringUtils.randomAlphanumeric(10).toLowerCase());
        System.out.println(RandomStringUtils.randomAlphanumeric(32));
        System.out.println(RandomStringUtils.randomAlphanumeric(32));
        System.out.println(RandomStringUtils.randomAlphanumeric(16).toLowerCase());
        System.out.println(RandomStringUtils.randomAlphanumeric(16));
        System.out.println(RandomStringUtils.randomAlphanumeric(16));
        System.out.println(RandomStringUtils.randomAlphanumeric(16));
    }

    public static void main(String[] args) {
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
    }

    public static void main333333(String[] args) throws Exception {
        //{"id":743768431924518912,"code":8000,"content":"get phone success","exID":null,"phone":"F17wkdpsrNuHc5y22hcYa1BXZLkKWIvFPr3Ik9HRO6duZdznoTZExaPQw3kjAfGM/0g886BPVNKZsTAfHsnf6TczTOL47V3yiJdN1bbvvPgpR/qcvuZq4MwWS3WumAFMtiwu7Wlw8ZdVp2s1w/L4M4Def7TnLecDj4a3ttNHCvg="}
        String encrypt = "F17wkdpsrNuHc5y22hcYa1BXZLkKWIvFPr3Ik9HRO6duZdznoTZExaPQw3kjAfGM/0g886BPVNKZsTAfHsnf6TczTOL47V3yiJdN1bbvvPgpR/qcvuZq4MwWS3WumAFMtiwu7Wlw8ZdVp2s1w/L4M4Def7TnLecDj4a3ttNHCvg=";
        String priKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMDOqbFeG6nO1Ssy\n" +
                "pOuxKVhzLjpUIdLRTQLvHrUzcna+t6PZFj/PVuXUx1/aV4rWnqr+re4UXTGs5/17\n" +
                "WjM++KDdhm7kfKZOReeYFrspoQRtVbslLtCbgaxtUbXPYiaQaDQyMS3+ofP/aKoE\n" +
                "cI0Snq4cYlWY0PdpqBe+66JwF4ePAgMBAAECgYBHaELQvdng+glZBliuMd7uMzpb\n" +
                "v+0G9x1eiNEGqxcpg1jgBfaHi3LTjAqLJYJmmTYKfmhMVJ5vV2pUJusxL5FE3jOX\n" +
                "RLmbVpYy6FWySru3x5+vFMCTGhNoTA0ZUF2ZtXgwQbcMICbktPMzQYjCbx0EIJda\n" +
                "9RjD/4/V9JmviLA3AQJBAOB8KwhLywg1035rc/7GXk4RN+X0A4HYIndExGZfnqDY\n" +
                "FQGEx0xsot3kc9ML8p9ip0J3MfcSRr2GGHQ6UzFmXpcCQQDb4ATTvnDJrMA+tEdU\n" +
                "5RXNV9lDyijKR3e9zGwfBczBTejqZQp6/murTh+y3pIZICXtX3UtbxxrHz0tgH76\n" +
                "LjXJAkAtzoiFZ9nlMf6XFFwWB+pkabULUlCI5U6bIcglz6sxzQX0SmDUonJVbvGH\n" +
                "hi+bAGl68ZGbhfozYDKLPP+O3AG/AkEAy6yETDDJAchIfM06lmVvALwzwhvuFRKG\n" +
                "1QYb0dDpjtbAvv/B6ZTr38+KhYKvCx9AgtY+EUwvovtZDGFPPii4IQJALOcGwBuX\n" +
                "jZ9rIjtKS2tKhL8Lxk48xc96j+iTq9lk7nvMc7bfqwg9dkiz6oBqqBaJJT3v6M4y\n" +
                "e2MjoaUmP1oDgQ==";
        System.out.println(RSAUtils.decrypt(encrypt, priKey));
        System.out.println(Base64.getEncoder().encodeToString("48b6572ccd1b99c5be782105:c9c48fbb39d4fe616597010f".getBytes()));
        System.out.println(EncryptUtils.base64Encode("48b6572ccd1b99c5be782105:c9c48fbb39d4fe616597010f"));
        System.out.println(EncryptUtils.base64Encode(""));
        System.out.println(EncryptUtils.base64Decode("PG-sS3BCpVPf1Vv4pVQfQnlkzwvGR2oFAkvs7muHVIlZ1T8B7ne0oIhckFKF704shVwFShmpsUnh_ORQ5FngYbGd1lrTWFaYUARbCP5xwUE-aV2I5-M4Yj82oxNgSNaiwj6pHHhXp87QFkL5xtNoCVraPGp6CcwjamogoWgqn1NretXjpDHqJoWMbZCE16UWluAAufkEbwjSepdYLyWOkysPqdAfsCahvtSozz7SNtqekXvhjWa6Ecs_EXwh-TkbE3B2ocS3sxTtXjuoUNg04fMbXPQyCgvO10_-iMBDrymfNjkFN8PUwUzD7i6X07NmzDlgItUZ37BqcRVkIJjglhbKSPFnbmQOsrdKhEzt-KxA4Z1_LcWVrajOxP6wkf2xx7KNK730BQi6c20c48btQK69JxCZnYxQZoL8Cdon5MjrfR3PBR-673AKJ36A-t9n"));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
        System.out.println(System.currentTimeMillis()/1000);

        System.out.println(RandomStringUtils.randomAscii(10));
        System.out.println(RandomStringUtils.random(10));
        System.out.println(EncryptUtils.getMd5("RriglSdy5W6EjEre3333"));
//
//        int size = 10000000;
//        Set<String> set = new HashSet<>(size);
//        int repeat = 0;
//        for(int i = 0; i< size; i++){
//            String str = RandomStringUtils.randomAlphanumeric(16);
//            if(set.contains(str)){
//                repeat ++;
//            }else {
//                set.add(str);
//            }
//            // 字母 + 数字
////            System.out.print("\""+RandomStringUtils.randomAlphanumeric(10)+"\",");
//        }
//        System.out.println("重复个数 "+repeat);

    }

    public static String decrypt(String cryptograph, String prikey) throws Exception {
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(prikey));
        PrivateKey privateKey = KeyFactory.getInstance("RSA").generatePrivate(keySpec);

        Cipher cipher=Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte [] b = Base64.getDecoder().decode(cryptograph);
        return new String(cipher.doFinal(b));
    }

    public static void main222(String[] args) throws InterruptedException {
//        JSONObject j = new JSONObject();
//        j.put("Text", "ddd'");
//        System.out.println(j.toJSONString());
        //1598683768
        //1598756840283
//        System.out.println(new Date().getTime());
//        String buffer = "PFRpbWVsaW5lT2JqZWN0PjxpZD4xMzQxMTY2OTA5MjI0OTM4MzA4MTwvaWQ+PHVzZXJuYW1lPnd4aWRfb25mcDAzd3d2ZDA2MjI8L3VzZXJuYW1lPjxjcmVhdGVUaW1lPjE1OTg3OTU1NDQ8L2NyZWF0ZVRpbWU+PGNvbnRlbnREZXNjPjwvY29udGVudERlc2M+PGNvbnRlbnREZXNjU2hvd1R5cGU+MDwvY29udGVudERlc2NTaG93VHlwZT48Y29udGVudERlc2NTY2VuZT4zPC9jb250ZW50RGVzY1NjZW5lPjxwcml2YXRlPjA8L3ByaXZhdGU+PHNpZ2h0Rm9sZGVkPjA8L3NpZ2h0Rm9sZGVkPjxzaG93RmxhZz4wPC9zaG93RmxhZz48YXBwSW5mbz48aWQ+PC9pZD48dmVyc2lvbj48L3ZlcnNpb24+PGFwcE5hbWU+PC9hcHBOYW1lPjxpbnN0YWxsVXJsPjwvaW5zdGFsbFVybD48ZnJvbVVybD48L2Zyb21Vcmw+PGlzRm9yY2VVcGRhdGU+MDwvaXNGb3JjZVVwZGF0ZT48L2FwcEluZm8+PHNvdXJjZVVzZXJOYW1lPjwvc291cmNlVXNlck5hbWU+PHNvdXJjZU5pY2tOYW1lPjwvc291cmNlTmlja05hbWU+PHN0YXRpc3RpY3NEYXRhPjwvc3RhdGlzdGljc0RhdGE+PHN0YXRFeHRTdHI+PC9zdGF0RXh0U3RyPjxDb250ZW50T2JqZWN0Pjxjb250ZW50U3R5bGU+MzwvY29udGVudFN0eWxlPjx0aXRsZT4mI3gwQTsmI3gwQTsmI3gwQTvkuaDov5HlubM8L3RpdGxlPjxkZXNjcmlwdGlvbj48L2Rlc2NyaXB0aW9uPjxtZWRpYUxpc3Q+PG1lZGlhPjxpZD4xMzQxMTY2OTA5MjU5NjY1ODMzMDwvaWQ+PHR5cGU+MjwvdHlwZT48dGl0bGU+5LuA5LmI5omN5piv55yf5q2j55qE5p625p6E6K6+6K6h77yfPC90aXRsZT48ZGVzY3JpcHRpb24+LSAgICAg5p625p6E55qE5a6a5LmJ5ZKM5p625p6E5pys6LSoICAgICAt5Zyo6L2v5Lu26KGM5Lia77yM5a+55LqO5LuA5LmI5piv5p625p6E77yM6YO95pyJ5b6I5aSa55qE5LqJ6K6677yM5q+P5Liq5Lq66YO95pyJ6Ieq5bex55qE55CG6KejPC9kZXNjcmlwdGlvbj48cHJpdmF0ZT4wPC9wcml2YXRlPjx1c2VyRGF0YT48L3VzZXJEYXRhPjxzdWJUeXBlPjA8L3N1YlR5cGU+PHZpZGVvU2l6ZSB3aWR0aD0iIiBoZWlnaHQ9IiI+PC92aWRlb1NpemU+PHVybCB0eXBlPSIxIiBtZDU9IiIgdmlkZW9tZDU9IiI+aHR0cDovL21wLndlaXhpbi5xcS5jb20vcz9fX2Jpej1Nekl6T0RJek56RTBOUT09bWlkPTI2NTQ0MjU1ODBpZHg9MnNuPTQ4ZWZlZTEwNWRmODc4ZmQyYjZlMTY3MThlNzU0MDc5Y2hrc209ZjJmZjk1NWFjNTg4MWM0YzQzYmYxNmRjMjBjYTlhMzg3NTY0ZTllOWE5Mjc4NDIyNDM2ZDc4OWZmZTc1ZTk2Yjk1ZjVlMDE4OWE1OW1wc2hhcmU9MXNjZW5lPTI0c3JjaWQ9MDgzMGh3MW84b2RYZndJNlMwU3ljMG84c2hhcmVyX3NoYXJldGltZT0xNTk4Nzk1MDc3MTc1c2hhcmVyX3NoYXJlaWQ9ZTE0MjQxODdmZTZlMzMzYjVlY2RkN2Q4NDE2OWVkZWYjcmQ8L3VybD48dGh1bWIgdHlwZT0iMSI+aHR0cHM6Ly9tbWJpei5xbG9nby5jbi9tbWJpel9qcGcvOVRQbjY2SFQ5MzFHbUJ0eGxVUUNpYUluMlBFcGF0a0lXT2hacEtRWjR3V1V0WENBSnp5V3g5TnZ4cGlhcGtsUmNiWTRaNWV5bHhNTW9VaG1WQnBudkpJdy8zMDA/d3hfZm10PWpwZWd3eGZyb209NDwvdGh1bWI+PHNpemUgd2lkdGg9IiIgaGVpZ2h0PSIiIHRvdGFsU2l6ZT0iIj48L3NpemU+PC9tZWRpYT48L21lZGlhTGlzdD48Y29udGVudFVybD48L2NvbnRlbnRVcmw+PC9Db250ZW50T2JqZWN0PjxhY3Rpb25JbmZvPjxhcHBNc2c+PG1lc3NhZ2VBY3Rpb24+PC9tZXNzYWdlQWN0aW9uPjwvYXBwTXNnPjwvYWN0aW9uSW5mbz48bG9jYXRpb24gcG9pQ2xhc3NpZnlJZD0iIiBwb2lOYW1lPSIiIHBvaUFkZHJlc3M9IiIgcG9pQ2xhc3NpZnlUeXBlPSIwIiBjaXR5PSIiPjwvbG9jYXRpb24+PHB1YmxpY1VzZXJOYW1lPjwvcHVibGljVXNlck5hbWU+PHN0cmVhbXZpZGVvPjxzdHJlYW12aWRlb3VybD48L3N0cmVhbXZpZGVvdXJsPjxzdHJlYW12aWRlb3RodW1idXJsPjwvc3RyZWFtdmlkZW90aHVtYnVybD48c3RyZWFtdmlkZW93ZWJ1cmw+PC9zdHJlYW12aWRlb3dlYnVybD48L3N0cmVhbXZpZGVvPjwvVGltZWxpbmVPYmplY3Q+";
//        System.out.println(EncryptUtils.base64Decode(buffer));

//        Long time = (1598783684 * 1000L);
//        Date date = new Date(time);
//        System.out.println(DateUtil.formatDate(date, "yyyy-MM-dd HH:mm:ss"));
        int size = 51;
        for(int i = 0; i < size; i++){
            Thread.sleep(100);
            if(i >= 9 && (i+1) % 10 == 0){
                System.out.println(String.format("当前清除: %s个, 还剩%s 个", (i+1), (size - i - 1)));
            }
        }

    }


}
