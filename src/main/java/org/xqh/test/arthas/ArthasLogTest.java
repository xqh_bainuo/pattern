package org.xqh.test.arthas;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import org.xqh.utils.file.ReadTxtFileUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName ArthasLogTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/5/15 15:35
 * @Version 1.0
 */
public class ArthasLogTest {

    public final static String prefix = "`---ts=";//日志前缀 标识 一次新的请求

    private final static AtomicInteger index = new AtomicInteger(0);

    public static void main(String[] args) {
        List<LogLine> logs = parseTraceLog("E:\\secureCRT_file\\download\\arthas.log");
        System.out.println(JSON.toJSONString(logs));
    }

    public static List<LogLine> parseTraceLog(String filePath){
        List<String> list = ReadTxtFileUtils.readTxt(new File(filePath));
        List<LogLine> logList = Lists.newArrayList();
        LogLine tmp = null;
        for(String l:list){
            if(l.startsWith(prefix)){
                tmp = null;
                List<String> llist = Lists.newArrayList();
                llist.add(l);
                LogLine log = LogLine.builder()
                        .idx(index.get())
                        .logList(llist)
                        .build();
                tmp = log;
                logList.add(log);
                index.incrementAndGet();
            }else {
                if(Objects.nonNull(tmp))
                tmp.getLogList().add(l);
            }
        }
        return logList;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class LogLine{

        private int idx;

        private List<String> logList;

    }
}
