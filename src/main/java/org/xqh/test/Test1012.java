package org.xqh.test;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

/**
 * @ClassName Test1012
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/10/12 19:56
 * @Version 1.0
 */
@Slf4j
public class Test1012 {


    public static void main(String[] args) {
        String num = "1.1.22.0012";
        String[] array = num.split("\\.");
        System.out.println(JSON.toJSONString(array));

        System.out.println(transFirmwareVerToNum(num));
//        System.out.println(Integer.parseInt(num));
//        System.out.println(NumberUtils.isPositiveInteger(num));
    }

    private static int transFirmwareVerToNum(String version){
        if(StringUtils.isEmpty(version)){
            return -1;
        }
        String[] array = version.split("\\.");
        int verNum = 0;
        for(int i = 0; i < array.length; i ++){
            int item = parseInteger(array[i]);
            if (item < 0) {
                return -1;
            }
            if (i == 0) {
                verNum += item * 100000000;
            }else if(i == 1){
                verNum += item * 1000000;
            }else if(i == 2){
                verNum += item * 10000;
            }else if(i == 3){
                verNum += item;
            }
        }
        return verNum;
    }

    private static int parseInteger(String val){
        try {
            return Integer.parseInt(val);
        }catch (Exception e){
            log.error("parse integer err.", e);
        }
        return -1;
    }
}
