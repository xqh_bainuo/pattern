package org.xqh.test;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.concurrent.Semaphore;
import java.util.stream.Collectors;

/**
 * @ClassName TestNew
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/12/15 17:03
 * @Version 1.0
 */
public class TestNew {

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TestOrg{
        private String orgCode;
        private String orgName;
        private String posCode;
        private String posName;
    }


    public static void main(String[] args) throws InterruptedException {

        List<TestOrg> list = Lists.newArrayList();
        list.add(TestOrg.builder()
                .orgCode("defaultNull")
                .build());
        list.add(TestOrg.builder()
                .orgCode("long")
                .orgName("设计组")
                .build());
        list.add(TestOrg.builder()
                .posCode("pcode")
                .posName("设计师")
                .build());
        list =list.stream().filter(l-> StringUtils.hasText(l.getOrgCode())).collect(Collectors.collectingAndThen(
                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(c -> c.getOrgCode()))), ArrayList::new
        ));
        System.out.println(JSON.toJSONString(list));

//        Semaphore semaphore = new Semaphore(1);
//        new Thread(new TestThread(semaphore), "t1").start();
//        new Thread(new TestThread(semaphore), "t2").start();
//        new Thread(new TestThread(semaphore), "t3").start();
//        Thread.sleep(3000);
//        semaphore.release(1);
//        Thread.sleep(2000);
//
//        System.out.println("main executed."+semaphore.getQueueLength());
//        char a = '3';
//        System.out.println((int)a);
//        System.out.println(a - '0');
//        int i = 3;
//        System.out.println(i/3 * 3);
//        for(int i = 0, j = 0; i < 10; ){
//            System.out.println("i:"+ i++);

//            System.out.println("j:"+ ++j);
//        }

//        String str = "abcdggggg\nfffgggggg";
//        System.out.println(JSON.toJSONString(str.split("\n")));
    }

    public static class TestThread implements Runnable{

        private Semaphore semaphore;

        public TestThread(Semaphore semaphore){
            this.semaphore = semaphore;
        }

        @Override
        public void run() {

            try {
                semaphore.acquire();
                System.out.println(Thread.currentThread().getName()+" executed...");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
