//package org.xqh.test.gc.poroslog;
//
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONException;
//import com.alibaba.fastjson.JSONObject;
//import org.apache.commons.lang.StringEscapeUtils;
//import org.apache.commons.lang.StringUtils;
//import org.apache.poi.ss.formula.functions.T;
//import org.xqh.utils.file.ReadTxtFileUtils;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Objects;
//
///**
// * @ClassName CheckLogResult
// * @Description TODO
// * @Author xuqianghui
// * @Date 2021/6/8 21:09
// * @Version 1.0
// */
//public class CheckLogResult {
//
//    public static void main(String[] args) {
//        String str = ReadTxtFileUtils.readJson(new File("E:\\work\\program\\poros\\poros-log\\log.txt"));
//        JSONObject json = JSON.parseObject(str);
//        JSONArray jsonResult = json.getJSONArray("result");
//        List<T> res = new ArrayList<T>();
//        jsonResult.stream().forEach(stream->{
//            JSONObject st = (JSONObject)stream;
//            JSONArray values = (JSONArray)st.get("values");
//            values.stream().forEach(log->{
//                JSONArray logArrayMap = (JSONArray)log;
//                T logObj = handleLogs( (String) logArrayMap.get(1) );
//                if(null != queryLogType ){
//                    logObj.setLog_msg_time_str(DateTimeUtil.stamp2StrMill(logObj.getLog_msg_time()));
//                    if(logObj.getLog_msg_type().equalsIgnoreCase(queryLogType)){
//                        boolean filterFlag = filterMsg(param,logObj);
//                        if(!filterFlag){
//                            res.add(logObj);
//                        }
//                    }
//                }
//            });
//        });
//        res.sort((o1, o2) -> (int) (o2.getLog_msg_time()-o1.getLog_msg_time()));
//    }
//
//    public static  <T extends PorosBaseLog> T handleLogs(String logContent){
//        T result = null;
//        JSONObject jsonObject = new JSONObject();
//        String[] logs=logContent.split("‖");
//        //解析LOG_TYPE
//        String log_type="";
//        if(logs!=null&&logs.length>=6){
//            log_type=logs[3];
//            extCommonInfo(jsonObject, logs);
//        }
//        if(StringUtils.isNotEmpty(log_type)) {
//            String LogMsg = "";
//            if("BIZ_LOG".equals(log_type)&&logs != null && logs.length >= 9){
//                //业务日志处理
//                log.debug("业务日志--"+logs[logs.length - 1]);
//                LogMsg = parseBizLogMsg(logs[logs.length - 1]);
//                jsonObject.set("log_msg", StringEscapeUtils.unescapeJavaScript(LogMsg));
//                result = (T) JSONUtil.toBean(jsonObject, PorosBizLog.class);
//            } else {
//                //处理登录日志
//                if ("LOGIN_LOG".equals(log_type)&&logs != null && logs.length >= 9) {
//                    LogMsg = parserPorosLoginLogMsg(logs[logs.length - 1]);
//                    com.alibaba.fastjson.JSONObject msg = null ;
//                    try{
//                        if (JSONUtil.isJson(LogMsg)) {
//                            msg = com.alibaba.fastjson.JSONObject.parseObject(LogMsg);
//                        }
//                    }catch (JSONException e){
//                        log.error("The login parserPoroLoginLogMsg log is : " + LogMsg );
//                    }
//                    if(msg!=null) {
//                        log.debug("登录日志--"+msg);
//                        jsonObject.set("log_msg_username", msg.get("username"));
//                        jsonObject.set("log_msg_login_type", msg.get("loginType"));
//                        jsonObject.set("log_msg_success", msg.get("success"));
//                        jsonObject.set("log_msg_result", msg.get("result"));
//                        jsonObject.set("log_msg_ip", msg.get("ip"));
//                        jsonObject.set("log_msg_platformType", msg.get("platformType"));
//                    }
//                    result = (T) JSONUtil.toBean(jsonObject, PorosLoginLog.class);
//                }else{
//                    //处理操作日志
//                    if ("AUDIT_LOG".equals(log_type)&&logs != null && logs.length >= 9) {
//                        LogMsg = parserPorosAuditLogMsg(logs[logs.length - 1]);
//                        com.alibaba.fastjson.JSONObject msg = null ;
//                        try{
//                            if (JSONUtil.isJson(LogMsg)) {
//                                msg = com.alibaba.fastjson.JSONObject.parseObject(LogMsg);
//                            }
//                        }catch (JSONException e){
//                            log.error("The audit parserPorosAuditLogMsg log is : " + LogMsg );
//                        }
//                        if(Objects.nonNull(msg)) {
//                            log.debug("操作日志--"+msg);
//                            OperLog operLog = JSONUtil.toBean(msg.toJSONString(),OperLog.class);
//                            jsonObject.set("log_msg_username", operLog.getUsername());
//                            jsonObject.set("log_msg_sername", operLog.getSername());
//                            jsonObject.set("log_msg_result", operLog.getResult());
//                            jsonObject.set("log_msg_parameter", operLog.getParameter());
//                            jsonObject.set("log_msg_business_type", operLog.getBusinessType());
//                            jsonObject.set("log_msg_business_title", operLog.getTitle());
//                            jsonObject.set("log_msg_description", operLog.getDescription());
//                            jsonObject.set("log_msg_method",operLog.getMethod());
//                            jsonObject.set("log_msg_ip", operLog.getIp());
//                            jsonObject.set("log_msg_userAgent", operLog.getUserAgent());
//                            jsonObject.set("log_msg_url", operLog.getUrl());
//                            jsonObject.set("log_msg_start_time", dateFormat.format(DateUtil.date(operLog.getStartTime())));
//                        }else {
//                            jsonObject.set("log_msg_username", "");
//                            jsonObject.set("log_msg_sername", logs[1]);
//                            handleAuditLogParameter(jsonObject, logs[8]);
//                        }
//                        result = (T) JSONUtil.toBean(jsonObject, PorosAuditLog.class);
//                    }
//                }
//            }
//        }
//        return result;
//    }
//}
