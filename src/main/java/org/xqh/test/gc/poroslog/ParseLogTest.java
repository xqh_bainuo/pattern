package org.xqh.test.gc.poroslog;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @ClassName ParseLogTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/6/4 13:52
 * @Version 1.0
 */
public class ParseLogTest {

    public static String path = "E:\\download\\chrome\\";


    private static Set<String> paramSet = Sets.newHashSet("请求参数：");
    private static Set<String> resultSet = Sets.newHashSet("请求应答：");
    private static String responseStr = "\"msg\":";
    private static String restResponse = "RestResponse";
    private static String split_str = ", ";


    public static void main(String[] args) {

//        List<String> list = ReadTxtFileUtils.readTxt(new File(path + "container (13).log"));
//
//        for (String str : list) {
//            if (str.contains("log-content")) {
//                String log = str.substring(str.indexOf("log-content") + 13);
//                String[] arr = log.split("‖");
//                JSONObject jsonObject = new JSONObject();
//                handleAuditLogParameter(jsonObject, arr[8]);
//                System.out.println(jsonObject);
//            }
//        }
        Boolean flag = null;
        if(flag){
            System.out.println("dddddd");
        }

    }

    private static final String lokiStr = "\",\"stream\":\"stdout\",\"time\"";

    private final static String change_line = "\n";

    public static void handleAuditLogParameter(JSONObject jsonObject, String content) {
        if (StringUtils.isEmpty(content)) {
            return;
        }
        content = StringEscapeUtils.unescapeJava(content);
        if (content.contains(change_line)) {
            content = content.replaceAll(change_line, "");
        }
        if (content.contains(lokiStr)) {
            content = content.split(lokiStr)[0];
        }
        int idx = -1;
        for (String res : paramSet) {
            if (content.contains(res)) {
                idx = content.indexOf(res) + res.length();
                jsonObject.put("log_msg_parameter", content.substring(idx));
                return;
            }
        }

        for (String res : resultSet) {
            if (content.contains(res)) {
                idx = content.indexOf(res) + res.length();
                handleShowResult(jsonObject, content.substring(idx));
                return;
            }
        }
        handleShowResult(jsonObject, content);
    }

    public static void handleShowResult(JSONObject jsonObject, String content) {
        if (content.contains(responseStr)) {
            jsonObject.put("log_msg_result", content);
        } else if (content.contains(restResponse)) {
            String[] arr = content.split(restResponse);
            jsonObject.put("log_msg_business_type", arr[0]);
            putResultData(jsonObject, arr[1]);
        } else {
            jsonObject.put("log_msg_business_type", content);
        }
    }

    public static void putResultData(JSONObject jsonObject, String msg) {
        JSONObject result = new JSONObject();
        int idx = msg.indexOf(split_str);
        String code = msg.substring(0, idx);
        msg = msg.substring(idx + split_str.length());
        putToJsonObject(result, code);
        idx = msg.indexOf(split_str);
        String message = msg.substring(0, idx);
        putToJsonObject(result, message);
        msg = msg.substring(idx + split_str.length());
        putToJsonObject(result, msg);

        jsonObject.put("log_msg_result", result);
    }

    public static void putToJsonObject(JSONObject jsonObject, String str){
        if(StringUtils.isEmpty(str)){
            return ;
        }
        if(!str.contains("=")){
            return ;
        }
        if(str.startsWith("(")){
            str = str.substring(1);
        }
        if(str.endsWith(")")){
            str = str.substring(0, str.length() - 1);
        }
        int firstIdx = str.indexOf("=");
        jsonObject.put(str.substring(0, firstIdx), str.substring(firstIdx + 1));
    }

    public static void main3333(String[] args) {
        String content = "访问系统服务，查询所有的系统信息，响应结果为：RestResponse(code=0, msg=success, data=[SystemDto(id=027fb58341721fc3f5de388a18e7bfdb, remark=null, systemName=CPMS系统, englishName=CPMS, systemCode=cpms-service, systemLogo=null, systemUrl=/cpms-service, parentId=0, gentlyAppId=null, description=CPMS, createBy=admin, createName=null, managers=null, defaultProduct=false, createTime=Fri Apr 02 15:08:51 CST 2021), SystemDto(id=080bf17a5e0ef4ec507b791f986038b9, remark=null, systemName=xixi_test, englishName=xixi_test, systemCode=xixi_test, systemLogo=/minio/poros/1391694950673252353.png, systemUrl=/index, parentId=0, gentlyAppId=null, description=, createBy=admin, createName=null, managers=null, defaultProduct=false, createTime=Mon May 10 18:01:50 CST 2021), SystemDto(id=0c22e6e1f71c36eb27485c554a664d0b, remark=null, systemName=系统创建产品, englishName=system-product, systemCode=system-product, systemLogo=/minio/poros/1390630618833190913.png, systemUrl=null, parentId=0, gentlyAppId=null, description=system_product, createBy=admin, createName=null, managers=null, defaultProduct=false, createTime=Fri May 07 19:33:07 CST 2021), SystemDto(id=12b53e1f6066722390e4d707f64297d3, remark=null, systemName=平台管理, englishName=poros-manage, systemCode=poros-manage, systemLogo=/minio/poros/1390943107328344066.png, systemUrl=/poros-manage, parentId=0, gentlyAppId=null, description=, createBy=admin, createName=null, managers=null, defaultProduct=false, createTime=Tue Nov 10 13:51:53 CST 2020), SystemDto(id=1bd89e2b0e193c315abe6844038f9cee, remark=null, systemName=GeekMind, englishName=GeekMind, systemCode=geekmin\"";
        String desc = content.substring(0, content.indexOf("RestResponse"));
        System.out.println(desc);
        String value = content.substring(content.indexOf("RestResponse") + "RestResponse".length());
        JSONObject result = new JSONObject();
        if (value.startsWith("(code=0,")) {
//            value = value.substring()
            result.put("code", 0);
        }
        System.out.println(value);
    }

    public static void main22222222222222(String[] args) {
//        String str = "(code=0, msg=success, data=[abcdefg=aaa])";
        String str = "(code=3002, msg=操作出错误了, data=[abcdefg=aaa])";

        int idx = str.indexOf(", ");
        String code = str.substring(0, idx);
        str = str.substring(idx + 2);
        System.out.println(code);

        idx = str.indexOf(", ");
        String msg = str.substring(0, idx);
        str = str.substring(idx + 2);
        System.out.println(msg);
        System.out.println(str);

    }

    /**
     * 从 文本中截取 内容
     *
     * @param key
     * @param content
     * @return
     */
    public String getValueByKey(String key, String content) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }

        String[] array = content.split(", ");
        if (Objects.isNull(array) || array.length == 0) {
            return null;
        }

        for (String str : array) {
            if (str.startsWith(key)) {
                return str.replace(key, "");
            }
        }
        return null;
    }
}
