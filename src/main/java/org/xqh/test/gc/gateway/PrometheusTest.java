package org.xqh.test.gc.gateway;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import org.xqh.utils.encrypt.EncryptUtils;
import org.xqh.utils.http.FxHttpClientUtils;

/**
 * @ClassName PrometheusTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/9/4 15:04
 * @Version 1.0
 */
public class PrometheusTest {

    private static String url = "http://10.74.166.198:32428/api/v1/query?query=%s&start=1630684800&end=1630771199&step=119";

    private static String query = "increase(%s{appMark=\"%s\",apiCode=\"poros_appCreate\"}[86399s])";

    private static String appMark = "1621327982554";

    public static void main(String[] args) throws HttpProcessException {
        queryMonthCallCount();
//        System.out.println(254/4);
    }

    /**
     * 查询本月调用次数
     * @return
     */
    public static String queryMonthCallCount() throws HttpProcessException {
        String params = String.format(query, "gateway_request_count_total", appMark);
        String reqUrl = String.format(url, EncryptUtils.urlEncode(params));
        HttpConfig config = FxHttpClientUtils.getHttpConfig(reqUrl);
        config.method(HttpMethods.GET);
        HttpResult result = HttpClientUtil.sendAndGetResp(config);
        System.out.println(result.getResult());
        return null;
    }
}
