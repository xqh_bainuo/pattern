package org.xqh.test.gc.Tlink;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import org.apache.http.Header;
import org.xqh.utils.encrypt.EncryptUtils;
import org.xqh.utils.encrypt.RSAUtils;
import org.xqh.utils.http.FxHttpClientUtils;

import java.util.UUID;

/**
 * @ClassName TlinkTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/6/11 10:51
 * @Version 1.0
 */
public class TlinkTest {
//
//    public static final String appId = "e11s10242ac11s00ssv2s";
//    public static final String appSecret = "f1211ebbf0242ac122130f2";

    public static final String appId = "8298e311ebbc6e0242ac11s0002";
    public static final String appSecret = "e98e311ebbc6e0242ac110sf";

    public static final String tlinkUrl = "https://mtest.getech.cn/hydra/app/push/send.do";
//    public static final String tlinkUrl = "https://teamwork.getech.cn/hydra/app/push/send.do";

    public static void main(String[] args) throws HttpProcessException {

        String cid = UUID.randomUUID().toString().replaceAll("-", "");
        String toDept = "";
        String toUser = "guitang.he";
        String summary = "呵呵哒";
        pushMsg(cid, toDept, toUser, summary);
    }

    public static void pushMsg(String cid, String toDept, String toUser, String summary) throws HttpProcessException {
        String msgJson = getMsgJson(cid, toDept, toUser, summary);
        HttpConfig config = FxHttpClientUtils.getHttpConfig(tlinkUrl);
        config.method(HttpMethods.POST);
        config.json(msgJson);
        config.headers(getReqHeaders());
        HttpResult httpRet = HttpClientUtil.sendAndGetResp(config);
        System.out.println(httpRet.getResult());
    }

    public static Header[] getReqHeaders(){
        String ts = String.valueOf(System.currentTimeMillis());
        return HttpHeader.custom()
                .other("appId", appId)
                .other("timestamp", ts)
                .other("sign", RSAUtils.base64EncodeStr(EncryptUtils.getMd5(appSecret+ts)))
                .build();
    }

    public static String getMsgJson(String cid, String toDept, String toUser, String summary){
        String msg = "{\n" +
                "    \"cid\":\"%s\",\n" +
                "    \"pluginId\":\"com.getech.shenpi\",\n" +
                "    \"serToken\":\"\",\n" +
                "    \"filter\":{\n" +
                "        \"toDept\":\"%s\",\n" +
                "        \"toUser\":\"%s\"\n" +
                "    },\n" +
                "    \"summary\":\"%s\",\n" +
                "    \"targetUrl\":\"\",\n" +
                "    \"items\":[\n" +
                "        {\n" +
                "            \"type\":2,\n" +
                "            \"value\":[\n" +
                "                {\n" +
                "                    \"str\":\"通知\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"str\":\"2019-01-01\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"str\":\"各单位请注意！完毕\"\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    ],\n" +
                "    \"btn\":{\n" +
                "        \"value\":[\n" +
                "            {\n" +
                "                \"str\":\"详情\",\n" +
                "                \"targetUrl\":\"https://ip/detail\"\n" +
                "            }\n" +
                "        ]\n" +
                "    },\n" +
                "    \"extras\":\"\"\n" +
                "}";
        return String.format(msg, cid, toDept, toUser, summary);
    }

}
