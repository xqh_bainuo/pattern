package org.xqh.test.gc.notify;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.Header;
import org.xqh.utils.http.FxHttpClientUtils;

import java.util.List;

/**
 * @ClassName CallNotifyWechatApiTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/7/15 16:55
 * @Version 1.0
 */
public class CallNotifyWechatApiTest {


    public static String url = "https://uat.poros.getech.cn/api/poros-notify/wechat/wxOauth/code2Session";

    public static String code = "091M2NFa13KboB0ALhGa1NIBJc3M2NF8";

    public static String sysCode = "geekworks";

    public static void main(String[] args) {
//        callApi();
        List<TestModel> list = Lists.newArrayList();
        list.add(TestModel.builder()
                .flag(false)
                .num(10)
                .build());
        list.add(TestModel.builder()
                .flag(false)
                .num(9)
                .build());
        list.add(TestModel.builder()
                .flag(false)
                .num(8)
                .build());
        list.add(TestModel.builder()
                .flag(true)
                .num(3)
                .build());
        list.add(TestModel.builder()
                .flag(false)
                .num(15)
                .build());
        list.add(TestModel.builder()
                .flag(false)
                .num(22)
                .build());

        list.sort((a,b)-> Boolean.valueOf(a.isFlag()).compareTo(Boolean.valueOf(b.isFlag())));
        System.out.println(JSON.toJSONString(list));
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TestModel{
        private boolean flag;
        private Integer num;
    }


    public static void callApi(){
        JSONObject json = new JSONObject();
        json.put("code", code);
        json.put("systemCode", sysCode);
        HttpConfig config = FxHttpClientUtils.getHttpConfig(url);
        config.method(HttpMethods.POST);
        config.json(json.toJSONString());
        Header[] headers = HttpHeader.custom()
                .other("authorization", "bearer 845b3cbe-b67a-40b4-a4be-fdff0618c1c9")
                .build();
        config.headers(headers);
        try {
            HttpResult result = HttpClientUtil.sendAndGetResp(config);
            System.out.println("请求结果===>"+result.getResult());
        } catch (HttpProcessException e) {
            e.printStackTrace();
        }
    }
}
