package org.xqh.test.gc.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.xqh.utils.http.FxHttpClientUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName WechatTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/6/22 17:01
 * @Version 1.0
 */
@Slf4j
public class WechatTest {

    public static String appId = "wx99ca881ea7566aca";
    public static String appSecret = "d90fac70f6a185342c095e7c9714ba24";

    public static String tokenUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

    public static String wx_access_token = "47_eHje1NAVEPrIwHQoc9NBAHoDxIfTq1ET6XSMwZA3IyYR2C1GbidxbjSQsDD_Quo-hB_ai-SMg3Hl_Z3IWd-ZvxVjIbMiK_JcW8G0fIYw7y-8qlgGByXfNhdqqlmF3u5oeh9jpFz0uV0u4MeB_RQQcAGAJII";
    public static void main(String[] args) {
//        System.out.println(callWxApiForAccessToken());

//        downloadQrCode();
        Integer a = 40001;
        Integer b = 40001;
        System.out.println( a == b);

//        String str = "{\"auto_color\":false,\"hyaline\":true,\"line_color\":{\"r\":\"0\",\"b\":\"0\",\"g\":\"0\"},\"page\":\"/subpackages/worksheet/public-form/nnJ6q8p1\",\"scene\":\"nnJ6q8p1\",\"systemCode\":\"geekworks\",\"width\":300}";
//
//        WxQRCodeReq req = JSON.parseObject(str, WxQRCodeReq.class);
//        System.out.println(JSON.toJSONString(req));
    }

    /**
     * 请求微信 接口 获取accessToken
     * @return
     */
    public static JSONObject callWxApiForAccessToken(){
        String reqUrl = String.format(tokenUrl, appId, appSecret);
        HttpConfig httpConfig = FxHttpClientUtils.getHttpConfig(reqUrl);
        httpConfig.method(HttpMethods.GET);
        try {
            HttpResult httpRet = HttpClientUtil.sendAndGetResp(httpConfig);
            log.info("call wx-api for access-token result: {}", httpRet.getResult());
            if(httpRet.getStatusCode() == HttpStatus.SC_OK){
                return  JSON.parseObject(httpRet.getResult());
            }
        } catch (HttpProcessException e) {
            log.error("调用微信api, 获取accessToken异常", e);
        }
        return null;
    }

    public static void downloadQrCode(){
        String reqUrl = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token="+wx_access_token;
        Map<String, Object> reqMap = new HashMap<>();
        /**
         * {"auto_color":false,"hyaline":true,"line_color":{"r":"0","b":"0","g":"0"},"page":"/subpackages/worksheet/public-form/nnJ6q8p1","scene":"nnJ6q8p1","systemCode":"geekworks","width":300}
         */
        reqMap.put("auto_color", false);
        reqMap.put("is_hyaline", true);
        reqMap.put("page", "subpackages/worksheet/public-form");
        reqMap.put("scene", "nnJ6q8p1");
        reqMap.put("width", 300);
        Map<String, String> line_color = new HashMap<>();
        line_color.put("r", "0");
        line_color.put("r", "0");
        line_color.put("r", "0");
        reqMap.put("line_color", line_color);
        String reqJson = JSON.toJSONString(reqMap);
        HttpConfig config = FxHttpClientUtils.getHttpConfig(reqUrl);

        config.method(HttpMethods.POST);
        config.json(reqJson);
        try {
            HttpResult result = HttpClientUtil.sendAndGetResp(config);
            System.out.println(result.getResult());
        } catch (HttpProcessException e) {
            e.printStackTrace();
        }

//        File qrCodeFile = new File("D:\\documents\\test1.png");
//        try {
//            FxHttpClientUtils.downloadFile(reqUrl, qrCodeFile, null, reqJson);
//        } catch (IOException e) {
//            log.error("调用微信下载小程序码异常", e);
//        }
    }
}
