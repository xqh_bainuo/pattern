package org.xqh.test.gc.wechat;

import org.springframework.util.StringUtils;
import org.xqh.utils.encrypt.Base64;
import org.xqh.utils.encrypt.RSAUtils;
import sun.misc.BASE64Decoder;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.spec.AlgorithmParameterSpec;

/**
 * @ClassName WxDecryptTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/6/29 18:49
 * @Version 1.0
 */
public class WxDecryptTest {

    /**
     * 密钥  AES加密要求key必须要128个比特位（这里需要长度为16，否则会报错）
     */
    private static final String KEY = "1234567887654344";

    /**
     * 算法，CB模式（默认）：
     * 电码本模式    Electronic Codebook Book
     * CBC模式：
     * 密码分组链接模式    Cipher Block Chaining
     * CTR模式：
     * 计算器模式    Counter
     * CFB模式：
     * 密码反馈模式    Cipher FeedBack
     * OFB模式：
     * 输出反馈模式    Output FeedBack
     */
    private static final String ALGORITHMSTR = "AES/CBC/PKCS5Padding";

    private static String sessionKey ="tiihtNczf5v6AKRyjwEUhQ==";
    private static String encryptData = "CiyLU1Aw2KjvrjMdj8YKliAjtP4gsMZMQmRzooG2xrDcvSnxIMXFufNstNGTyaGS9uT5geRa0W4oTOb1WT7fJlAC+oNPdbB+3hVbJSRgv+4lGOETKUQz6OYStslQ142dNCuabNPGBzlooOmB231qMM85d2/fV6ChevvXvQP8Hkue1poOFtnEtpyxVLW1zAo6/1Xx1COxFvrc2d7UL/lmHInNlxuacJXwu0fjpXfz/YqYzBIBzD6WUfTIF9GRHpOn/Hz7saL8xz+W//FRAUid1OksQaQx4CMs8LOddcQhULW4ucetDf96JcR3g0gfRK4PC7E/r7Z6xNrXd2UIeorGj5Ef7b1pJAYB6Y5anaHqZ9J6nKEBvB4DnNLIVWSgARns/8wR2SiRS7MNACwTyrGvt9ts8p12PKFdlqYTopNHR1Vf7XjfhQlVsAJdNiKdYmYVoKlaRv85IfVunYzO0IKXsyl7JCUjCpoG20f0a04COwfneQAGGwd5oa+T8yO5hzuyDb/XcxxmK01EpqOyuxINew==";
    private static String iv = "r7BXXKkLb8qrSNn05n0qiA==";

    public static void main221233(String[] args) {
        System.out.println(RSAUtils.base64DecodeStr("eyJhbGciOiJIUzI1NiJ9.eyJ0ZW5hbnRUeXBlIjoiMyIsInVpZCI6IjE4NTg4MjkwMzE0IiwiYWxsT3JnQ29kZSI6WyJvQHlsc3hjYmhzenUiLCJvQGFla2tudXFhb3MiXSwiZW1wTnVtIjpudWxsLCJwb3N0aW9uQ29kZXMiOltudWxsLCJwQHVpbHZ5ZmZzb2ciXSwiaXNUZW5hbnRNYW5hZ2VyIjpmYWxzZSwibmFtZSI6IuiDoea1qSIsIm1vYmlsZSI6IjE4NTg4MjkwMzE0IiwidGVuYW50SWQiOiIzIiwibGFuZ3VhZ2UiOiJ6aC1DTiIsImFsbE9yZ0NvZGVQYXRoIjpbIm9AeWxzeGNiaHN6dSIsIjAwMDAwMDAwL29AYWVra251cWFvcyJdLCJlbWFpbCI6bnVsbH0.WX3dZoEAy7LVZrjOTcQtGDBX3RUta13NHpLAGEG6deg"));
    }

    public static void main111(String[] args) throws Exception {
//        String content = "url：findNames.action";
//        System.out.println("加密前：" + content);
//
//        System.out.println("加密密钥和解密密钥：" + KEY);
//
//        String encrypt = aesEncrypt(content, KEY);
//        System.out.println("加密后：" + encrypt);
//
//        String decrypt = aesDecrypt(encrypt, KEY, iv);
//
//        System.out.println("解密后：" + decrypt);

//        byte[] encryptBytes = org.apache.commons.codec.binary.Base64.decodeBase64(encryptData);
//        byte[] decryptKeys = org.apache.commons.codec.binary.Base64.decodeBase64(sessionKey);
//        System.out.println(aesDecryptByBytes(encryptBytes,decryptKeys , iv));
        String str = "abc,abbd,";
        System.out.println(str.split(",").length);
        System.out.println(RSAUtils.base64DecodeStr("eyJhbGciOiJIUzI1NiJ9.eyJ0ZW5hbnRUeXBlIjoiMiIsInVpZCI6ImFkbWluIiwiYWxsT3JnQ29kZSI6WyJ0ZXN0MyIsImRlZmF1bHROdWxsIiwiYnVnb3JnIiwidGVzdGNvZGUiXSwiZW1wTnVtIjpudWxsLCJwb3N0aW9uQ29kZXMiOltudWxsLCJwQGtwa3JqY3F5aWgiLG51bGwsbnVsbF0sImlzVGVuYW50TWFuYWdlciI6dHJ1ZSwibmFtZSI6IueuoeeQhuWRmCIsIm1vYmlsZSI6IjE4OTI5MzA1Njk0IiwidGVuYW50SWQiOiJnZWVrIiwibGFuZ3VhZ2UiOiJ6aC1DTiIsImFsbE9yZ0NvZGVQYXRoIjpbInRlc3QzIixudWxsLCJidWdvcmciLCJ0ZXN0Y29kZSJdLCJlbWFpbCI6ImFkbWluQGdldGVjaC5jbiJ9.JsERdtUp8AmqEByYmV1T9AlwrssbP5un4Mp3QqTYrUs"));
    }


    /**
     * 解密微信小程序 数据
     * @param sessionKey
     * @param encryptedData
     * @param iv
     * @return
     * @throws Exception
     */
    public static String decryptWXAppletInfo(String sessionKey, String encryptedData, String iv)
            throws Exception {
        byte[] encryptData = org.apache.commons.codec.binary.Base64.decodeBase64(encryptedData);
        byte[] ivData = org.apache.commons.codec.binary.Base64.decodeBase64(iv);
        byte[] sessionKeyB = org.apache.commons.codec.binary.Base64.decodeBase64(sessionKey);
        KeyGenerator keygen= KeyGenerator.getInstance("AES");
        keygen.init(128);
        AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivData);
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(sessionKeyB, "AES");
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
        byte[] doFinal = cipher.doFinal(encryptData);
        return new String(doFinal);
    }

    public static void main(String[] args) throws Exception {
//        String encryptData = "Ae3Q/qNila2Ooy3yMsaiq57+G4xL2OMZEto5paI0V0sToKU22r9qlr6VnhJPa+IQdiNAVNYSmrOz7nLIQ/UH/Xyx7Jw4DU4qOxmxy7PkPqP+G19R3/S+LGYMnTCyDvFHSqLuqGW8hXK5ZlrabtLWjYR7xc1Cw5hpt4qAJhIwF/q2ncpFwpwL3gC/27/KiYR6flEHkR1iIABOaZai9//PNA==";
//        String sessionKey = "40F/EACdunF7kAl5ynwOjw==";
//        String iv = "uph7p0zrYdqF1xBIWG6BEQ==";

        String encryptData = "F/xsf1+ik/ZxbZCdL59x3uFcnnq7pUiUhenJpZperBiRgnBHOF4PrT0PB3Z7bvjgIUX4ixBj8CIF7ZHXC+f9R1844T9JOJacGfDnNv1CPSjpA2AbXdeVXZfTIcRse1BgALC10wZoaI9j8UZ2dl6WKOSDXA5mzqQbH4yXXOx+qFPEeFumu0cvd1sPfxcLEB7CwIAldk2F4mVuKsYYX0QUyA==";
        String sessionKey = "T1oBIEf8euQ+t+wSeXt+zg==";
        String iv = "zfOJnsO0whxmDaaTns5L0g==";
        System.out.println(decryptWXAppletInfo(sessionKey, encryptData, iv));
    }

    /**
     * base 64 encode
     * @param bytes 待编码的byte[]
     * @return 编码后的base 64 code
     */
    private static String base64Encode(byte[] bytes){
        return Base64.encodeBase64String(bytes);
    }

    /**
     * base 64 decode
     * @param base64Code 待解码的base 64 code
     * @return 解码后的byte[]
     * @throws Exception 抛出异常
     */
    private static byte[] base64Decode(String base64Code) throws Exception{
        return StringUtils.isEmpty(base64Code) ? null : new BASE64Decoder().decodeBuffer(base64Code);
    }


    /**
     * AES加密
     * @param content 待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的byte[]
     */
    private static byte[] aesEncryptToBytes(String content, String encryptKey) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);
        Cipher cipher = Cipher.getInstance(ALGORITHMSTR);
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(encryptKey.getBytes(), "AES"));

        return cipher.doFinal(content.getBytes("utf-8"));
    }


    /**
     * AES加密为base 64 code
     *
     * @param content 待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的base 64 code
     */
    private static String aesEncrypt(String content, String encryptKey) throws Exception {
        return base64Encode(aesEncryptToBytes(content, encryptKey));
    }

    /**
     * AES解密
     *
     * @param encryptBytes 待解密的byte[]
     * @param decryptKey 解密密钥
     * @return 解密后的String
     */
    private static String aesDecryptByBytes(byte[] encryptBytes, byte[] decryptKey, String iv) throws Exception {
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        kgen.init(128);

        byte[] ivData = org.apache.commons.codec.binary.Base64.decodeBase64(iv);
        AlgorithmParameterSpec ivSpec = new IvParameterSpec(ivData);

        Cipher cipher = Cipher.getInstance(ALGORITHMSTR);
        cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(decryptKey, "AES"), ivSpec);
        byte[] decryptBytes = cipher.doFinal(encryptBytes);

        return new String(decryptBytes);
    }

}
