package org.xqh.test.gc.wechat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Map;

/**
 * @ClassName WxQRCodeReq
 * @Author xuqianghui
 * @Date 2021/6/22 13:47
 * @Version 1.0
 */
@Data
@ApiModel(value = "获取微信小程序码请求参数")
public class WxQRCodeReq {

    @ApiModelProperty(value = "系统标识")
    private String systemCode;

    @ApiModelProperty(value = "必须是已经发布的小程序存在的页面（否则报错），例如 pages/index/index, 根路径前不要填加 /,不能携带参数（参数请放在scene字段里），如果不填写这个字段，默认跳主页面", required = true)
    private String page;

    @ApiModelProperty(value = "二维码宽度, 默认430")
    private Integer width;

    @ApiModelProperty(required = true, value = "最大32个可见字符，只支持数字，大小写英文以及部分特殊字符：!#$&'()*+,/:;=?@-._~，其它字符请自行编码为合法字符（因不支持%，中文无法使用 urlencode 处理，请使用其他编码方式）")
    private String scene;

    @ApiModelProperty(value = "自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调，默认 false")
    private boolean auto_color;

    @ApiModelProperty(value = "auto_color 为 false 时生效，使用 rgb 设置颜色 例如 {\"r\":\"xxx\",\"g\":\"xxx\",\"b\":\"xxx\"} 十进制表示")
    private Map<String, String> line_color;
    
    @ApiModelProperty(value = "是否需要透明底色，为 true 时，生成透明底色的小程序")
    private boolean is_hyaline;

    public boolean getIs_hyaline() {
        return is_hyaline;
    }
}
