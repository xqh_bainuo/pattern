package org.xqh.test.gc;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xqh.utils.DateUtil;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName LicenceModel
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/4/27 15:32
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LicenceModel {

    private String machineCode;//客户机器唯一标识

    private int roleType;//角色类型 0：试用版, 1: 正式版, 2: 终身版

    private Long generateTime;//生成时间

    private Long expireTime;//失效时间

    private String extInfo;//拓展信息

    public static void main(String[] args) {
        LicenceModel licence = LicenceModel.builder()
                .machineCode("uniqueMachineCode")
                .roleType(1)
                .generateTime(new Date().getTime())
                .expireTime(DateUtil.plusDate(new Date(), 1L, ChronoUnit.YEARS).getTime())
                .extInfo("{}")
                .build();
        System.out.println(JSON.toJSONString(licence));
    }
}
