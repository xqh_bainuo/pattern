package org.xqh.test.gc.securitybcrypt;

import com.alibaba.fastjson.JSON;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.MessageDigestPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.StopWatch;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName SpringSecurityBCryptTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/8/4 20:38
 * @Version 1.0
 */
public class SpringSecurityBCryptTest {

    public static LoadingCache<PwdMatchCacheKey, String> sha256Cache = CacheBuilder.newBuilder().maximumSize(1000)
            .refreshAfterWrite(1, TimeUnit.DAYS)// 缓存一天
            .build(
                    new CacheLoader<PwdMatchCacheKey, String>() {
                        @Override
                        public String load(PwdMatchCacheKey pwd) throws Exception {
                            return getSha256Val(pwd);
                        }
                    }
            );

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class PwdMatchCacheKey{
        private String pwd;//前端传输明文
        private String dbHashedPwd;//数据库存储 hash后密码
    }

    public static String getSha256Val(PwdMatchCacheKey pwd){
        System.out.println("重新匹配:"+ JSON.toJSONString(pwd));
        return String.valueOf(passwordEncoder2().matches(pwd.getPwd(), pwd.getDbHashedPwd()));
    }

    public static String getCacheVal(PwdMatchCacheKey pwd){
        return sha256Cache.getUnchecked(pwd);
    }

    public static PasswordEncoder passwordEncoder2(){
        DelegatingPasswordEncoder delegatingPasswordEncoder = (DelegatingPasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder();
        /**设置defaultPasswordEncoderForMatches为BCryptPasswordEncoder，如果需要兼容,则设计为兼容就好*/
        delegatingPasswordEncoder.setDefaultPasswordEncoderForMatches(new MessageDigestPasswordEncoder("MD5"));
        return  delegatingPasswordEncoder;
    }

    public static PasswordEncoder passwordEncoder(){
        DelegatingPasswordEncoder delegatingPasswordEncoder = (DelegatingPasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder();
        /**设置defaultPasswordEncoderForMatches为BCryptPasswordEncoder，如果需要兼容,则设计为兼容就好*/
        delegatingPasswordEncoder.setDefaultPasswordEncoderForMatches(new BCryptPasswordEncoder());
        return  delegatingPasswordEncoder;
    }

    public static String def_pwd = "PoRos#$Init56";

    public static void main33(String[] args) {

        System.out.println(passwordEncoder2().encode(def_pwd));
    }

    public static void main(String[] args) {
        PwdMatchCacheKey pwd1 = PwdMatchCacheKey.builder()
                .pwd(def_pwd)
                .dbHashedPwd("{bcrypt}$2a$10$oB5dIsC0jS/0oKF5av6wU..9U75rtqslVg4ov0QFiiFAAOVjUNpiG")
                .build();

        PwdMatchCacheKey pwd2 = PwdMatchCacheKey.builder()
                .pwd("PoRos#$Init56")
                .dbHashedPwd("{bcrypt}$2a$10$oB5dIsC0jS/0oKF5av6wU..9U75rtqslVg4ov0QFiiFAAOVjUNpiG")
                .build();

        PwdMatchCacheKey pwd3 = PwdMatchCacheKey.builder()
                .pwd("PoRos#$Init56")
                .dbHashedPwd("{bcrypt}$2a$10$1QG3mhNOIOiYpWhnabugt.Uc/2wHayLV.EOwD528KnNUD8ruwluwm")
                .build();

        PwdMatchCacheKey pwd4 = PwdMatchCacheKey.builder()
                .pwd("PoRos#$Init56")
                .dbHashedPwd("{bcrypt}$2a$10$W/fpch5Wac.ewwlrWP92Wuty1Y9viBNJSuFBHAV2aRmPk3ZtUuPM2")
                .build();

        PwdMatchCacheKey pwd5 = PwdMatchCacheKey.builder()
                .pwd("PoRos#$Init56")
                .dbHashedPwd("{bcrypt}$2a$10$Fb6zFceUcJB8Vr30czJQfu8Iph3vvyfqkfv6IZptC.GzMnZgnFwga")
                .build();
        System.out.println(getCacheVal(pwd1));
        System.out.println(getCacheVal(pwd2));
        System.out.println(getCacheVal(pwd3));
        System.out.println(getCacheVal(pwd4));
        System.out.println(getCacheVal(pwd5));
        PasswordEncoder encoder = passwordEncoder();
        int i = 100;
        StopWatch watch = new StopWatch("encode test");
        Map<String, String> map = new HashMap<>();
        String str = "PoRos#$Init56";
        for(int j = 0; j < i; j++){
            String pass = str;
            final String passHash = encoder.encode(pass);
            map.put(pass, passHash);
            System.out.println(passHash);
        }
        watch.start("test job");
        for(int j = 0; j < i; j++){
            String pass = str;
            final boolean matches = encoder.matches(pass, "{bcrypt}$2a$10$1QG3mhNOIOiYpWhnabugt.Uc/2wHayLV.EOwD528KnNUD8ruwluwm");
            System.out.println(j+"====>"+matches);
        }
        watch.stop();
        System.out.println(watch.prettyPrint());
    }
}
