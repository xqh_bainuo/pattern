package org.xqh.test.gc.pulsar;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @ClassName HwCloudTenantComboDTO
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/7/27 11:01
 * @Version 1.0
 */
@Data
@ApiModel(value = "华为云租户套餐")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HwCloudTenantComboDTO {

    private String systemId;

    private String systemCode;

    private String tenantId;

    private String productComboId;

    private Date expireTime;

    private String activity;

    /**
     * 计费属性
     */
    private List<TenantComboAttrDTO> extendInfo;
}
