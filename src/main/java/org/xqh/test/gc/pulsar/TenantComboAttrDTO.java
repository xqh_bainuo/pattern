package org.xqh.test.gc.pulsar;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName TenantComboAttrDTO
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/7/27 14:14
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TenantComboAttrDTO {

    /**
     * 计费属性
     */
    private String chargeUnit;

    /**
     * 属性值
     */
    private Integer chargeQuantity;
}
