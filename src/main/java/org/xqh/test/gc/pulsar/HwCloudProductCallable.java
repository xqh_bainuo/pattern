//package org.xqh.test.gc.pulsar;
//
//import cn.getech.poros.pulsar.utils.PulsarCallable;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Component;
//
///**
// * @ClassName HwCloudProductCallable
// * @Description 接收华为云 购买套餐 mq 消息
// * @Author xuqianghui
// * @Date 2021/7/19 18:21
// * @Version 1.0
// */
//@Component
//@Slf4j
//public class HwCloudProductCallable extends PulsarCallable {
//
//
//    @Override
//    public Object call() throws Exception {
//        String msgStr = new String(this.msg.getData());
//        log.info("receive hw-cloud combo-sku msg. {}, msgId: {}", msgStr, this.msg.getMessageId());
//        return super.call();
//    }
//
//
//}
