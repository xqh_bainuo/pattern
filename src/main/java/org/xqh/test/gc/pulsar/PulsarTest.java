//\package org.xqh.test.gc.pulsar;
//
//import cn.getech.poros.pulsar.utils.POROS_PULSAR;
//import org.apache.pulsar.client.api.PulsarClientException;
//
///**
// * @ClassName PulsarTest
// * @Description TODO
// * @Author xuqianghui
// * @Date 2021/7/27 12:43
// * @Version 1.0
// */
//public class PulsarTest {
//
//    private final static String schema = "10.74.143.19:32676";
//
//    private final static boolean enable = true;
//
//    private final static boolean ssl = false;
//
//    private final static String topic = "tenant_combo_IOT";
//
//    public static void main(String[] args) throws PulsarClientException {
//        pulsarInit();
////        String msg = "{\"activity\":\"newInstance\",\"expireTime\":1942309969533,\"extendInfo\":\"[{\\\"chargeQuantity\\\":33,\\\"chargeUnit\\\":\\\"deviceLocationNum\\\"},{\\\"chargeQuantity\\\":308,\\\"chargeUnit\\\":\\\"vpParameterNum\\\"},{\\\"chargeQuantity\\\":2800,\\\"chargeUnit\\\":\\\"deviceNum\\\"},{\\\"chargeQuantity\\\":100,\\\"chargeUnit\\\":\\\"ruleNum\\\"},{\\\"chargeQuantity\\\":30,\\\"chargeUnit\\\":\\\"dataStorageTime\\\"},{\\\"chargeQuantity\\\":7,\\\"chargeUnit\\\":\\\"exportTime\\\"}]\",\"productComboId\":\"9c4801e1b9bf1208b62c7486f6b3001e\",\"systemCode\":\"IOT\",\"systemId\":\"12b96dc1cd2102d168862dc49b151602\",\"tenantId\":\"bdbtntbtjwvo\"}";
////        HwCloudTenantComboDTO dto = JSON.parseObject(msg, HwCloudTenantComboDTO.class);
////
////        System.out.println(JSON.toJSONString(dto));
//    }
//
//
//
//    public static void pulsarInit() throws PulsarClientException {
//        //初始化
//        POROS_PULSAR.pulsarInit(enable, ssl, schema);
//        //订阅 华为云 套餐购买 消息
//        POROS_PULSAR.INSTANCE()
//                .persistent()
//                .subTopic(topic, "poros-licence-server".concat(String.valueOf(System.currentTimeMillis())))
//                .sub(new HwCloudProductCallable());
//    }
//
//}
