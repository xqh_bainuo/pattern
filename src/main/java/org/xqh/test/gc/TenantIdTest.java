package org.xqh.test.gc;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Map;
import java.util.Set;

/**
 * @ClassName TenantIdTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/7/5 15:40
 * @Version 1.0
 */
public class TenantIdTest {

    public static void main(String[] args) {
        int siz = 20000000;
        Map<String, Integer> set = Maps.newHashMap();
        for(int i = 0; i < siz; i++){
            String key = getRandomTenantId(12);
            if(set.containsKey(key)){
                int count = set.getOrDefault(key, 0);
                set.put(key, count + 1);
                System.out.println("重复的key: "+key +", 重复次数: "+(count+1));
            }else {
                set.put(key, 1);
            }
        }
    }


    public static String getRandomTenantId(int len){
        return RandomStringUtils.randomAlphanumeric(len);
    }
}
