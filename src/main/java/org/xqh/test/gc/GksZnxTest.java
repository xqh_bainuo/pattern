package org.xqh.test.gc;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.digest.HMac;
import cn.hutool.crypto.digest.HmacAlgorithm;
import com.alibaba.fastjson.JSON;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.Header;
import org.xqh.utils.http.FxHttpClientUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @ClassName GksZnxTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/6/28 10:23
 * @Version 1.0
 */
public class GksZnxTest {

    private static String api = "http://10.74.166.27:8000/api/gks-message-center/v1/messageInfo/poros";


    public static void main(String[] args) {
//        String c = "<p>8122</p>";
//        System.out.println(c.substring(3, c.length() - 4));
//        sendMsg();
        buildGksSign("123456");
    }

    public static String buildGksSign(String secret){
// 注意：时间只能是GMT格式
        DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss'GMT'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        String signDate = dateFormat.format(Calendar.getInstance().getTime());
        //用于生成签名的字符串测试结果
        String signingString = "x-date: " + signDate + "\nGET " + "/api/poroslog/porosLog/getBizLogList HTTP/1.1";
        HMac hMac = new HMac(HmacAlgorithm.HmacSHA256.getValue(),secret.getBytes());
        //生成对应签名
        byte[] digest = hMac.digest(signingString.getBytes());
        String base64Digest = Base64.encode(digest);
        System.out.println(digest);
        System.out.println(signDate);
        System.out.println(base64Digest);
        return null;
    }

    public static void sendMsg(){
        GksZnx znx = GksZnx.builder()
                .activateCode(UUID.randomUUID().toString().replaceAll("-", ""))
                .fromUser("admin")
                .fromUserName("管理员")
                .templateId(100L)
                .tenantId("geek")
                .tenantName("租户名称")
                .toUser(Lists.newArrayList("13893181900"))
                .build();


        HttpConfig config = FxHttpClientUtils.getHttpConfig(api);
        config.method(HttpMethods.POST);
        config.json(JSON.toJSONString(znx));
        Header[] headers = HttpHeader.custom()
                .other("Authorization", "bearer 5bbd548c-bcdc-405b-b704-cff2f40538d4")
                .build();
        config.headers(headers);
        try {
            HttpResult result = HttpClientUtil.sendAndGetResp(config);
            System.out.println(result.getResult());
        } catch (HttpProcessException e) {
            e.printStackTrace();
        }
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class GksZnx{

        private String activateCode;

        private String fromUser;

        private String fromUserName;

        private Long templateId;

        private String tenantId;

        private String tenantName;

        private List<String> toUser;
    }
}
