package org.xqh.test.gc;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import org.apache.http.Header;
import org.xqh.utils.encrypt.DESUtils;
import org.xqh.utils.encrypt.DESUtils22;
import org.xqh.utils.encrypt.RSAUtils;
import org.xqh.utils.http.FxHttpClientUtils;
import org.xqh.utils.http.HttpRetModel;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @ClassName GcLoginTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/5/31 19:21
 * @Version 1.0
 */
public class PorosLoginTest {

    public final static String url = "http://kong.poros-platform.10.74.158.69.nip.io/api/poros-authcenter";
    public final static String loginUrl = url.concat("/login");

//    public final static String url = "http://localhost:8030/api/poros-authcenter";
//    public final static String loginUrl = url.concat("/custom/login");

    public static void main(String[] args) throws Exception {
//        testLogin();
        printUserInfo();

    }

    public static void testLogin() throws Exception {
        String result = getSecret("admin");
        String secret = JSON.parseObject(result).getString("data");
        System.out.println(RSAUtils.base64EncodeStr(secret));
        login(secret);
    }


    public static void printUserInfo() {
        String token = "eyJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiJhZG1pbiIsImFsbE9yZ0NvZGUiOlsiQ1NPVDAwMDAiLCJ0ZXN0b3JncGVybWlzc2lvbiJdLCJlbXBOdW0iOiJhZG1pbiIsInBvc3Rpb25Db2RlcyI6W251bGwsbnVsbF0sImlzVGVuYW50TWFuYWdlciI6dHJ1ZSwibmFtZSI6Iuenn-aIt-euoeeQhuWRmC1hZG1pbiIsIm1vYmlsZSI6IjE4OTI5MzA1Njk0IiwidGVuYW50SWQiOiJnZWVrIiwibGFuZ3VhZ2UiOiJlbmciLCJhbGxPcmdDb2RlUGF0aCI6W251bGwsInRlc3RvcmdwZXJtaXNzaW9uIl0sImVtYWlsIjoiYWRtaW5AZ2V0ZWNoLmNuIn0.TG3JaPMb8GUUxp9TKYQESr2V7k0AfXM4s_JJREuupPo";

        System.out.println(RSAUtils.base64DecodeStr(token));
    }

    public static String getSecret(String username) {
        String reqUrl = url.concat("/secret/%s");
        reqUrl = String.format(reqUrl, username);
        HttpConfig config = FxHttpClientUtils.getHttpConfig(reqUrl);
        config.method(HttpMethods.GET);
        return FxHttpClientUtils.requestReturnString(config, getReqModel());
    }

    public static void login(String secret) throws Exception {
        String pwd = DESUtils.encryptString(secret, "123456");
        String reqUrl = loginUrl;
        HttpConfig config = FxHttpClientUtils.getHttpConfig(reqUrl);

        config.method(HttpMethods.POST);
        config.headers(getReqHeaders());
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("grant_type", "password");
        reqMap.put("username", "admin");
        reqMap.put("language", "eng");
        reqMap.put("password", pwd);
        reqMap.put("isSerialize", "true");
        config.map(reqMap);
        String result = FxHttpClientUtils.requestReturnString(config, getReqModel());
        System.out.println(result);

    }

    public static Header[] getReqHeaders() {

        HttpHeader httpHead = HttpHeader.custom()
                .contentType("application/x-www-form-urlencoded")
                .accept("*/*")
                .cacheControl("no-cache");
//        String h = "{\n" +
//                "    \"x-request-id\":\"29f57eac432e6614ba4607a3a7eeab5d\",\n" +
//                "    \"x-forwarded-proto\":\"http\",\n" +
//                "    \"x-forwarded-port\":\"80\",\n" +
//                "    \"accept\":\"*/*\",\n" +
//                "    \"authorization\":\"Basic ZGV2OjEyMzQ1Ng==\",\n" +
//                "    \"x-real-ip\":\"10.8.4.162\",\n" +
//                "    \"x-forwarded-host\":\"kong.poros-platform.10.74.158.69.nip.io\",\n" +
//                "    \"host\":\"kong.poros-platform.10.74.158.69.nip.io\",\n" +
//                "    \"x-original-uri\":\"/api/poros-authcenter/login\",\n" +
//                "    \"connection\":\"keep-alive\",\n" +
//                "    \"content-type\":\"application/x-www-form-urlencoded\",\n" +
//                "    \"x-scheme\":\"http\",\n" +
//                "    \"cache-control\":\"no-cache\",\n" +
//                "    \"accept-encoding\":\"gzip,deflate\",\n" +
//                "    \"real-ip\":\"10.8.4.162\",\n" +
//                "    \"user-agent\":\"Apache-HttpClient/4.5.3 (Java/1.8.0_152)\"\n" +
//                "}";
//        Map<String, String> map = JSON.parseObject(h, Map.class);
//
//        for(Entry<String, String> entry:map.entrySet()){
//            httpHead.other(entry.getKey(), entry.getValue());
//        }
        Header[] heads = httpHead.build();
        return heads;
    }


    public static HttpRetModel getReqModel() {
        return HttpRetModel.builder()
                .statusKey("code")
                .successVal("0")
                .dataKey("data")
                .msgKey("msg")
                .build();
    }
}
