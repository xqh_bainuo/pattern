package org.xqh.test.ugreen.apitest;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName UGreenApiTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/8/9 17:23
 * @Version 1.0
 */
public class UGreenApiTest {

    public final static String unbind_api = "/api/device/v2/sa/admin/unbind";

    public static void main(String[] args) {
        unbindDevice("UG549HH08229999A", "dev");
    }

    public static void unbindDevice(String deviceSn, String env){
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("sn", deviceSn);
        reqMap.put("username", "admintest");
        reqMap.put("remark", "售后设备测试");
        RedisOptTest.callCloudApi(reqMap, RedisOptTest.envMap.get(env).getDomain().concat(unbind_api), env);
    }
}
