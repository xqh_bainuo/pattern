package org.xqh.test.ugreen.apitest;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.xqh.test.ugreen.CertificateUtils;
import org.xqh.test.ugreen.UGreenApiCallUtils;
import org.xqh.test.ugreen.application.ApplicationRelDataSql;
import org.xqh.test.ugreen.dto.RedisOptParams;
import org.xqh.utils.http.FxHttpClientUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.xqh.test.ugreen.UGreenApiCallUtils.callCloudApiWithSign;

/**
 * @ClassName RedisOptTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/11/11 11:39
 * @Version 1.0
 */
@Slf4j
public class RedisOptTest {

    public static void main(String[] args) {
//        deleteRedisKey("p2p:device:conn-err:UG670UU372300029", "test");
//        queryRedis("p2p:device:conn-err:EC671JJ15240CEF0", "prod");
//        queryRedis("p2p:device:conn-err:UG670UU372300029", "test");
//        queryRedis("p2p:node:info:EC752JJ20240F747", "prod");
//        queryRedis("p2p:device:uglink-cfg:EC671JJ15240CEF0", "prod");
//        queryRedis("p2p:connect:auth:7b9b33b5d7ae4d599583b1361d012345", "test");/**
        /**
         * 55a738dbcc4f451e9d3ae713e9234377
         * c2d0ddf20fd7409d8a38c950f53e5a09
         *
         * 89ca181f81b845fa970191b047c88b54
         * c0f2b61f1575487facd19c08c60b432d
         *
         * 51f52ae55e6a4ed49d2bf92a4013aa65
         * c828930efe0a41728a0c07218a5a31cc
         *
         * 7cfec213ac554d99afb38447a5ab268f
         * 1362891970514adc965e786a27b7880f
         */

        /**
         * d8487974fb6f4a868062729267bc5125
         * 34c7b01cbdc94a429a54af074891484b
         * 7d416a11f7e84392a535c7b27064fb2a
         * e26ed9517cd644fdb128a95134583cbd
         * a8f97502aea64e19b9536375d06d111d
         * 5d6230eb9a84494299de5b66d5be562b
         * 72a23ceae1eb415184079a333559b91b
         * d27fb1dfcdca4a50a86ef586bfa7002c
         */
//        setRedis("p2p:connect:auth:55a738dbcc4f451e9d3ae713e9234377", "\"UG670UU372308101\"", "test");//testappclient4
//        setRedis("p2p:connect:auth:c2d0ddf20fd7409d8a38c950f53e5a09", "\"UG670UU372308101\"", "test");
//
//        setRedis("p2p:connect:auth:89ca181f81b845fa970191b047c88b54", "\"UG670UU372308102\"", "test");//testappclient5
//        setRedis("p2p:connect:auth:c0f2b61f1575487facd19c08c60b432d", "\"UG670UU372308102\"", "test");
//
//        setRedis("p2p:connect:auth:51f52ae55e6a4ed49d2bf92a4013aa65", "\"UG670UU372308103\"", "test");//testappclient6
//        setRedis("p2p:connect:auth:c828930efe0a41728a0c07218a5a31cc", "\"UG670UU372308103\"", "test");

//        setRedis("p2p:connect:auth:d8487974fb6f4a868062729267bc5125", "\"UG670UU372308100\"", "test");
        setRedis("p2p:connect:auth-app:7b9b33b5d7ae4d599583b1361d012345", "\"UG670UU372308888\"", "test");
        setRedis("p2p:connect:auth-app:593c9756214d432b89c10ddddc9e5768", "\"UG670UU372308999\"", "test");
        setRedis("p2p:connect:auth-app:1ff3459ef8234d4abbd1913ea6f5e9f6", "\"UG670UU372308100\"", "test");
        setRedis("p2p:connect:auth-app:d8487974fb6f4a868062729267bc5125", "\"UG670UU372308100\"", "test");
        setRedis("p2p:connect:auth-app:34c7b01cbdc94a429a54af074891484b", "\"UG670UU372308100\"", "test");
        setRedis("p2p:connect:auth-app:c2d0ddf20fd7409d8a38c950f53e5a09", "\"UG670UU372308101\"", "test");
        setRedis("p2p:connect:auth-app:c0f2b61f1575487facd19c08c60b432d", "\"UG670UU372308102\"", "test");
        setRedis("p2p:connect:auth-app:c828930efe0a41728a0c07218a5a31cc", "\"UG670UU372308103\"", "test");
        setRedis("p2p:connect:auth-app:1362891970514adc965e786a27b7880f", "\"UG670UU372308104\"", "test");
//        setRedis("p2p:connect:auth:9acf087ece944a73abfe242eecfc758a", "\"UG671UU32230A887\"", "prod");
//        setRedis("p2p:relay:alias:johntest", "\"UG671UU32230A887\"", "prod");


//        String test = "\"{\\\"bindRelayId\\\":7,\\\"bindSignalId\\\":6,\\\"connKey\\\":\\\"7b9b33b5d7ae4d599583b1361d06fd88\\\",\\\"deviceSn\\\":\\\"UG670UU372300029\\\",\\\"muxType\\\":0,\\\"relayAddr\\\":\\\"58.49.151.18:8024\\\",\\\"relayDomain\\\":\\\"cn2-test.ug.link:8088\\\",\\\"relayHost\\\":\\\"58.49.151.18:8024\\\",\\\"relayName\\\":\\\"relay_server_18\\\",\\\"signalAddr\\\":\\\"58.49.151.18:8023\\\",\\\"signalHost\\\":\\\"58.49.151.18:8023\\\",\\\"signalName\\\":\\\"signal_server_18\\\"}\"";
//        setRedis("p2p:node:info:UG670UU372300029", "\"{\\\"bindRelayId\\\":7,\\\"bindSignalId\\\":6,\\\"connKey\\\":\\\"7b9b33b5d7ae4d599583b1361d06fd8811\\\",\\\"deviceSn\\\":\\\"UG670UU372300029\\\",\\\"muxType\\\":0,\\\"relayAddr\\\":\\\"58.49.151.18:8024\\\",\\\"relayDomain\\\":\\\"cn3-test.ug.link:8088\\\",\\\"relayHost\\\":\\\"58.49.151.18:8024\\\",\\\"relayName\\\":\\\"relay_server_18\\\",\\\"signalAddr\\\":\\\"58.49.151.18:8023\\\",\\\"signalHost\\\":\\\"58.49.151.18:8023\\\",\\\"signalName\\\":\\\"signal_server_18\\\"}\"", "test");
//        queryRedis("p2p:device:uglink-cfg:UG670UU372300029", "test");//查询设备uglink配置
//        deleteRedisKey("p2p:device:uglink-cfg:U60120U48N10ABF5", "test");//查询设备uglink配置
//        queryRedis("p2p:device:uglink-cfg:EC671JJ15240CEF0", "prod");//查询设备uglink配置
////        deleteRedisKey("p2p:device:uglink-cfg:EC671JJ15240CEF0", "prod");
////        deleteRedisKey("p2p:device:conn-err:EC671JJ202402232", "prod");
////        deleteRedisKey("p2p:node:info:TC718HH142438295", "prod-us");
//        deleteRedisKey("p2p:relay:alias:dxp4800plus-fil", "prod-us");
//        queryRedis("p2p:relay:alias:dxp4800plus-fil", "prod-us");
//        deleteRedisKey("p2p:relay:alias:abc111", "test");
//        queryRedis("p2p:node:info:U60120U48N10ABF5", "test");
//        deleteRedisKey("p2p:node:info:U60120U48N10ABF5", "test");
//        queryRedis("p2p:node:info:U60120U48N10ABF5", "test");
//        queryRedis("p2p:relay:alias:abc111", "test");

//        deleteRedisKey("p2p:node:info:U60120U48N10ABF5", "test");
//        deleteRedisKey("p2p:relay:alias:abc111", "test");
//        deleteRedisKey("p2p:gray:skip:sn:EC671JJ202402232", "prod");
//        deleteRedisKey("p2p:node:info:EC554JJ472301A89", "prod");
//        deleteRedisKey("p2p:gray:skip:sn:EE720JJ2224052E8", "prod");
//        queryRedis("p2p:gray:skip:sn:EC671JJ202402232", "prod");
//        queryRedis("p2p:device:conn-err:EE720JJ2224052E8", "prod");
//        queryRedis("p2p:node:info:UG752JJ522304428", "prod");
//        queryRedis("p2p:node:info:U60120U48N10ABF5", "test");
//        queryRedis("p2p:relay:alias:TC718HH142438295", "prod-us");

//        queryRedis("p2p:gray:skip:sn:UG752JJ52230120B", "test");
//        queryRedis("p2p:gray:skip:sn:TC718HH04241C8FE", "prod");
//        queryRedis("p2p:gray:skip:sn:UG671UU32230A887", "prod");
//        queryRedis("p2p:gray:skip:sn:EC671JJ15240CEF0", "prod");
//        deleteRedisKey("p2p:device:conn-err:UG720JJ022408BED", "prod");
//        deleteRedisKey("p2p:device:conn-err:TC718HH04241C8FE", "prod");
//        deleteRedisKey("p2p:gray:skip:sn:UG720JJ022408BED", "prod");
//        deleteRedisKey("p2p:gray:skip:sn:EC660JJ292308498", "prod");
//        deleteRedisKey("p2p:gray:skip:sn:UG752JJ522304105", "prod");
//        queryRedis("p2p:gray:skip:sn:UG752JJ52230FCB6", "prod");
//        queryRedis("p2p:gray:skip:sn:UG752JJ52230FCB6", "prod");
//        queryRedis("p2p:gray:on", "prod");
//        setRedis("p2p:gray:skip:sn:UG752JJ52230FCB6", "true", "prod");
//        setRedis("p2p:gray:skip:sn:UG671UU32230A887", "true", "prod-eur");
//        setRedis("p2p:gray:skip:sn:UG671UU32230A887", "true", "prod");
//        setRedis("p2p:gray:skip:sn:EC671JJ15240CEF0", "", "prod");
//        setRedis("p2p:device:conn-err:EC752JJ1324181FA", "\"[{\\\"data\\\":\\\"107.20.234.92:8024\\\",\\\"errCode\\\":1001,\\\"originalStatus\\\":0,\\\"sn\\\":\\\"EC752JJ1324181FA\\\",\\\"status\\\":-1,\\\"type\\\":1}]\"", "prod-us");
//        setRedis("p2p:device:conn-err:EC752JJ1324181FA", "\"[{\\\"data\\\":\\\"107.20.234.92:8023\\\",\\\"errCode\\\":1001,\\\"originalStatus\\\":0,\\\"sn\\\":\\\"EC752JJ1324181FA\\\",\\\"status\\\":-1,\\\"type\\\":1}]\"", "prod-us");
        //[{\"data\":\"47.106.245.32:8023\",\"errCode\":1001,\"originalStatus\":0,\"sn\":\"EC752JJ21240D13C\",\"status\":-1,\"type\":1}]
        //"[{\"data\":\"47.106.245.32:8023\",\"errCode\":1001,\"originalStatus\":0,\"sn\":\"EC752JJ21240D13C\",\"status\":-1,\"type\":1}]"
        //f75545320b074dc6897bcdf01ae72787   UG671UU32230A888  xqhceshiceshi
        //f75545320b074dc6897bcdf01ae72788   UG671UU32230A889  xqhceshiceshi2
//        addNewTestUglinkDevice("UG671UU32230A889", "f75545320b074dc6897bcdf01ae72788", "johntest111", "prod-aar", 10);
//        addNewTestUglinkDevice("DH00000000999999", "3dee6ecdeb434970a482ae41eba64e4c", "johntest123456", "prod-us", 10);
//        addNewTestUglinkDevice("UG671UU32230A887", "9acf087ece944a73abfe242eecfc758a", "johntest12345678", "prod-eur", 10);


        /**
         * p2p:node:info:UG752JJ522304105  设备uglink连接信息
         * p2p:device:conn-err:HB718HH462301E5A  设备异常连接信息
         */
//        queryRedis("p2p:node:info:HB718HH462301E5A");//查询异常连接信息
//        queryRedis("p2p:relay:alias:16888");//查询异常连接信息
//        deleteRedisKey("p2p:node:info:HB718HH462301E5A");
//        queryUGLinkInfo("EC752JJ20240E631");
//        queryUGLinkInfo("DH00000000999999");

//        userEncryptTest();

//        Map<String, Object> params = new HashMap<>();
//        params.put("uglinkId", "5a105e8b9d40e1329780d62ea2265d8aa");
//        params.put("deviceSn", "EC660JJ292305F51");
//        String sn = "{\"0\":\"CN\",\"1\":\"US\",\"2\":\"UK\",\"3\":\"DE\"}";
//        System.out.println(JSON.parseObject(sn).toJSONString());

//        syncDeviceToCluster("EC660JJ29230495A"); //生产设备
//        syncDeviceToCluster("UG671UU33230058E");
//        syncDeviceToCluster("UG670UU372300039");
//        syncDeviceToCluster("HB718HH4623033EC");
//        syncDeviceToCluster("UG670UU37230005A");
//        userEncryptTest();
//        DelApplicationCacheOpt();  //删除应用中心 相关缓存
//        doRedisOptForCenter(params);//数据中心接口



//        syncDeviceToCluster("U60110U48N10F87F", "test");
//        syncDeviceToCluster("HB607EE202400710", "prod-eur");
//        syncDeviceToCluster("UG671UU332304FF6", "prod");
//        syncDeviceToCluster("HB670EE222400013", "prod-us");
//        syncDeviceToCluster("HB670EE222400019", "prod-us");
//        syncDeviceToCluster("HB670EE22240001C", "prod-us");
//        syncDeviceToCluster("HB670EE182400324", "prod-us");
//        syncDeviceToCluster("HB670EE18240034C", "prod-us");

//        syncDeviceToCluster("EF721JJ20240064C", "test");

//        dataCenterTmdbJob("test", "movie_relate_upd");
//        dataCenterTmdbJob("test", "movie_relate_upd");
//        dataCenterTmdbJob("test", "movie_relate_upd");
//        dataCenterTmdbJob("test", "movie_relate_upd");
//        dataCenterTmdbJob("test", "sync-movie");
////        dataCenterTmdbJob("test", "sync-relate");
////        dataCenterTmdbJob("test", "tv");
//        dataCenterTmdbJob("test", "sync-tv");
//        dataCenterTmdbJob("test", "sync-movie");
//        dataCenterTmdbChangeJob("test");
    }

    private static void setRedis(String cacheKey, String value, String env) {
        RedisOptParams params = RedisOptParams.builder()
                .optType("set")//set , get , del, expire
                .optKey(cacheKey)
                .optVal(value)
                .expire(1440000000)
//                .db(12)//国内uglink redis
                .db(env.contains("us") || env.contains("eur") || env.contains("test") ? 10 : 12)//国内uglink redis
                .build();
        doRedisOpt(params, env);//cloud接口
    }


    private static void queryRedis(String cacheKey, String env) {

        RedisOptParams params = RedisOptParams.builder()
                .optType("get")//set , get , del, expire
                .optKey(cacheKey)
//                .db(12)//国内uglink redis
                .db(env.contains("us") || env.contains("eur") || env.equals("test") ? 10 : 12)//国内uglink redis
                .build();
        doRedisOpt(params, env);//cloud接口
    }


    /**
     * redis操作
     */
    private static void deleteRedisKey(String cacheKey, String env) {

        RedisOptParams params = RedisOptParams.builder()
                .optType("del")//set , get , del, expire
                .optKey(cacheKey)
                .db(env.contains("us") || env.contains("eur") || env.equals("test") ? 10 : 12)//国内uglink redis
                .build();
        doRedisOpt(params, env);//cloud接口
        //        device:secset:UG670UU37230005A:false  设备秘钥
//        p2p:connect:auth:7b9b33b5d7ae4d599583b1361d06fd88  p2p连接认证
//        p2p:relay:alias:johntest  uglink别名
//        固件升级 system:fwVer:online-list:DXP4800
//        device:p2p:tunnel:DC670UU3723000AA
//        RedisOptParams params = RedisOptParams.builder()
//                .optType("set")//set , get , del, expire
////                .optKey("p2p:connect:auth:9acf087ece944a73abfe242eecfc758a")
////                .optKey("p2p:connect:auth:f75545320b074dc6897bcdf01ae72787")
//                .optKey("p2p:connect:auth:6cc2b92bab424ae98acde37802d48585")
////                .optKey("p2p:node:info:UG752JJ522304105")
////                .optKey(String.format("p2p:node:info:%s", deviceSn))
////                .optKey("p2p:device:conn-err:EC752JJ2124058E8")
////                .optKey("dataCenter:third:data:2.56.171.1")
////                .optKey("dataCenter:device:secret:UG721JJ02240E55C")
////                .optKey("device:p2p:tunnel:UG670UU37230005F")
////                .optKey("device:status-info:HB718HH4623045AC")
////                .optKey("device:network:ip:113.118.134.75")
////                .optKey("application:check:version:DX4700")
////                .optKey("p2p:node:info:HB670EE20240067D")
////                .optKey("device:status-info:UG721JJ02240E55C")
////                .optKey("p2p:relay:alias:johntest")
////                .optKey("p2p:relay:alias:xqhceshiceshi")
////                .optKey("p2p:gray:skip:sn:EC671JJ15240CEF0")
////                .optKey("user:ugreenno:nowNo")
////                .optKey("p2p:relay:alias:dxp4800")
////                .optVal("DH00000000999999")//美国uglink测试设备号
//                .optVal("EC752JJ2124058E8")//国内uglink测试设备号
////                .optVal("UG671UU32230A887")//德国uglink测试设备号
////                .optVal("\"{\\\"bindRelayId\\\":3,\\\"bindSignalId\\\":1,\\\"connKey\\\":\\\"5bcd254142a84ccf8d7338da5e334ffe\\\",\\\"relayAddr\\\":\\\"183.193.69.85:8024\\\",\\\"relayDomain\\\":\\\"cn1.ug.link\\\",\\\"relayName\\\":\\\"relay_server_85\\\",\\\"signalAddr\\\":\\\"47.106.245.32:8023\\\",\\\"signalName\\\":\\\"signal_server_32\\\"}\"")
//                .db(12)//国内uglink redis
////                .db(10) //美国环境uglink
////                .db(5)//hk
//                .expire(52560000)
//                .build();

    }


    /**
     * 新增测试uglink设备
     *
     * @param deviceSn
     * @param connKey
     * @param alias
     */
    private static void addNewTestUglinkDevice(String deviceSn, String connKey, String alias, String env, int db) {
        //  p2p:connect:auth:f75545320b074dc6897bcdf01ae72787   UG671UU32230A888
        //p2p:relay:alias:xqhceshiceshi
        RedisOptParams params = RedisOptParams.builder()
                .optType("set")//set , get , del, expire
                .optKey("p2p:connect:auth:" + connKey)
                .optVal(deviceSn)
                .db(db)//国内uglink redis
                .expire(1000000000)
                .build();
        doRedisOpt(params, env);//cloud接口
        RedisOptParams params2 = RedisOptParams.builder()
                .optType("set")//set , get , del, expire
                .optKey("p2p:relay:alias:" + alias)
                .optVal(deviceSn)
                .db(db)//国内uglink redis
                .expire(1000000000)
                .build();
        doRedisOpt(params2, env);//cloud接口
    }


    private static void DelApplicationCacheOpt(String env) {
        List<String> cacheKeyList = ApplicationRelDataSql.getCacheKeyList();
        for (String key : cacheKeyList) {
            RedisOptParams params = RedisOptParams.builder()
                    .optType("del")//set , get , del, expire
                    .optKey(key)
                    .db(3)
                    .expire(600000)
                    .build();
            doRedisOpt(params, env);//cloud接口
        }
    }

    public static void userEncryptTest() {
//        String url1 = "http://dev.ugreengroup.com/api/user/v2/sa/admin/encrypt/cfg/update";//更新秘钥
//        String url2 = "http://dev.ugreengroup.com/api/user/v2/sa/admin/encrypt/user/job/execute";//执行数据更新操作

        String url1 = "https://cloud2.ugreengroup.com/api/user/v2/sa/admin/encrypt/cfg/update";//更新秘钥
        String url2 = "https://cloud2.ugreengroup.com/api/user/v2/sa/admin/encrypt/user/job/execute";//执行数据更新操作
        DeviceSyncDto dto = DeviceSyncDto.builder()
                .deviceSn("dd")
                .ugreenNo(55000L)
                .build();
        callCloudApi(dto, url1, "test");

        //qm4Fx8YwCOUnM2ytOUyzeBxi/81FrsMj7NkxYwhKL3Q=
    }


    public static void syncDeviceToCluster(String sn, String env) {
//        String url = "https://api-test1.ugnas.com/api/device/v2/sa/admin/sync/device";
        String url = "https://api.ugnas.com/api/device/v2/sa/admin/sync/device";
        DeviceSyncDto dto = DeviceSyncDto.builder().deviceSn(sn).build();
        callCloudApi(dto, url, env);
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class DeviceSyncDto {
        private String deviceSn;
        private Long ugreenNo;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RateLimitReq {
        private String device_sn;
        private int rate_limit;
        private long ts;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UgRelayDto {

        private String deviceSn;

        private int msgType;//具体的业务消息类型

        private String serverId;

        private String data;//发送的消息

        private String sign;

        private String cmd;

        private long ts;

        /**
         * DeviceSn string `json:"deviceSn"`
         * 	MsgType  int    `json:"msgType"`
         * 	ServerId string `json:"serverId"` //服务器节点ID
         * 	Data     string `json:"data"`
         */
    }

    /**
     * 调用数据中心服务
     *
     * @param params
     */
    public static void doRedisOptForCenter(Object params, String env) {
//        callCloudApiWithSign(params, center_api_uglink_check, center_secret_key, "ugreen", "nas");
        callCloudApiWithSign(params, envMap.get(env).getCenterRedisOpt(), envMap.get(env).getCenterSecret(), "ugreen", "nas");
    }

    public static void dataCenterTmdbJob(String env, String jobType) {
        Map<String, Object> params = new HashMap<>();
        params.put("jobType", jobType);
//        callCloudApiWithSign(params, center_api_uglink_check, center_secret_key, "ugreen", "nas");
//        callCloudApiWithSign(params, "https://center-test.ugnas.com/data/cluster/v2/sa/external/tmdb/job", envMap.get(env).getCenterSecret(), "ugreen", "nas");
        callCloudApiWithSign(params, "http://8.217.199.112:8071/spider/v2/sa/external/tmdb/job", envMap.get(env).getCenterSecret(), "ugreen", "nas");
    }

    public static void dataCenterTmdbChangeJob(String env) {
        Map<String, Object> params = new HashMap<>();
//        callCloudApiWithSign(params, center_api_uglink_check, center_secret_key, "ugreen", "nas");
        callCloudApiWithSign(params, "https://center-test.ugnas.com/data/cluster/v2/sa/external/tmdb/change/job", envMap.get(env).getCenterSecret(), "ugreen", "nas");
    }

    public static <T> UGreenApiCallUtils.CallResultModel doRedisOpt(T dto, String env) {

        return callCloudApi(dto, envMap.get(env).getRedisOptUrl(), env);
    }


    /**
     * 封装调用 cloud通用逻辑
     *
     * @param dto
     * @param <T>
     * @return
     */
    public static <T> UGreenApiCallUtils.CallResultModel callCloudApi(T dto, String url, String env) {
        HttpConfig config = FxHttpClientUtils.getHttpConfig(url);
        config.method(HttpMethods.POST);
        String json = JSON.toJSONString(dto);
        JSONObject jsonPar = JSON.parseObject(json);
        Map<String, String> signMap = handleReqHeader2(dto, env);
        jsonPar.putAll(signMap);
        config.json(jsonPar.toJSONString());
        log.info("redis opt api start. {}, {}", url, jsonPar.toJSONString());

        HttpRequest httpRequest = HttpUtil.createPost(url);
        httpRequest.body(jsonPar.toJSONString());
        httpRequest.contentType("application/json");

        HttpResponse response = httpRequest.execute();
        log.info("call api result: {}", response.body());
        UGreenApiCallUtils.CallResultModel checkResult = UGreenApiCallUtils.checkCallResult(response);
        return checkResult;
    }

    private static Header[] buildReqHeaders(String ts, String sign, String env) {
        Header[] result = HttpHeader.custom()
                .other(REQUEST_HEAD_ACCESS_KEY, envMap.get(env).getAccessKey())
                .other(REQUEST_HEAD_ACCESS_TYPE, access_type)
                .other(REQUEST_HEAD_TS, ts)
                .other(REQUEST_HEAD_SIGN, sign)
                .build();
        return result;
    }

    private static Map<String, String> buildReqHeadersMap(String ts, String sign, String env) {
        Map<String, String> signMap = new HashMap<>();
//        config.header(REQUEST_HEAD_ACCESS_KEY, envMap.get(env).getAccessKey());
//        config.header(REQUEST_HEAD_ACCESS_TYPE, access_type);
//        config.header(REQUEST_HEAD_TS, ts);
//        config.header(REQUEST_HEAD_SIGN, sign);
//
        signMap.put(REQUEST_HEAD_ACCESS_KEY, envMap.get(env).getAccessKey());
        signMap.put(REQUEST_HEAD_ACCESS_TYPE, access_type);
        signMap.put(REQUEST_HEAD_TS, ts);
        signMap.put(REQUEST_HEAD_SIGN, sign);
        return signMap;
    }

    //

    private static String getReqTimestamp() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    private static <T> String buildReqSign(T params, String ts, String env) {
        Map<String, Object> reqMap = JSONObject.parseObject(JSON.toJSONString(params), Map.class);
        reqMap.put(REQUEST_HEAD_ACCESS_KEY, envMap.get(env).getAccessKey());
        reqMap.put(REQUEST_HEAD_ACCESS_TYPE, access_type);
        reqMap.put(REQUEST_HEAD_TS, ts);
        System.out.println(JSON.toJSONString(reqMap));
        String sign = CertificateUtils.createSignNew(reqMap, envMap.get(env).getAccessSecret());
        System.out.println(sign);
        return sign;
    }

    public static <T> void handleReqHeader(T params, HttpConfig config, String env) {
        String ts = getReqTimestamp();
        String sign = buildReqSign(params, ts, env);
        Header[] headers = buildReqHeaders(ts, sign, env);
        config.headers(headers);//设置请求头
    }

    public static <T> Map<String, String> handleReqHeader2(T params, String env) {
        String ts = getReqTimestamp();
        String sign = buildReqSign(params, ts, env);
        Map<String, String> signMap = buildReqHeadersMap(ts, sign, env);
        return signMap;
    }

    /***
     *
     * @param inputPwd 输入密码
     * @param userPwd 用户实际密码
     * @return
     */
    public static boolean comparePwd(String inputPwd, String userPwd, Long ugreenNo) {
        if (StringUtils.isBlank(inputPwd) || StringUtils.isBlank(userPwd)) {
            return false;
        }
        if (inputPwd.length() == 32) {
            //说明前前端还未修改算法，前端传入的密码是明文密码两次md5无加盐，后端进行一次md5,使用ugreenno前面加盐
            String realPwd = DigestUtils.md5Hex(ugreenNo + inputPwd);
            return realPwd.equals(userPwd);
        } else {
            //前端已经修改算法，前端传入的密码是明文，后端先进行两次md5无加盐,再进行一次md5,使用ugreenno前面加盐
            String doubleMd5 = DigestUtils.md5Hex(DigestUtils.md5Hex(inputPwd));
            String realPwd = DigestUtils.md5Hex(ugreenNo + doubleMd5);
            if (!realPwd.equals(userPwd)) {
                //有可能是使用了新算法sha256的用户
                realPwd = DigestUtils.sha256Hex(ugreenNo + inputPwd);
                return realPwd.equals(userPwd);
            }
        }
        return true;
    }
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CallApiParams {
        private String accessKey;
        private String domain;
        private String accessSecret;
        private String centerSecret;
        private String centerRedisOpt;
        private String issueCmdUrl;
        private String redisOptUrl;
        private String uglinkQueryUrl;
    }

    public static Map<String, CallApiParams> envMap = new HashMap<>();

    static {
        envMap.put("dev",
                CallApiParams.builder()
                        .accessKey("CcAhdNrns3YYJwcd")
                        .accessSecret("KJ5NYMzzHSQ6dTDuNpKpnYye8LdcwR96")
                        .domain("https://dev.ugreengroup.com")
                        .redisOptUrl("https://dev.ugreengroup.com/api/system/v2/sa/admin/redis/opt")
                        .issueCmdUrl("https://dev.ugreengroup.com/api/p2p/v2/sa/admin/device/uglink/setup")
                        .uglinkQueryUrl("https://dev.ugreengroup.com/api/p2p/v2/sa/admin/uglink/query")
                        .build()
        );

        envMap.put("test",
                CallApiParams.builder()
                        .accessKey("tNRuGaWBVhJxWoSj")
                        .accessSecret("8e754d65684e4e79aa3e1d106dff3dc5")
                        .domain("https://api-test1.ugnas.com")
                        .centerSecret("7T4GZHG05NRFQSL4")
                        .redisOptUrl("https://api-test1.ugnas.com/api/system/v2/sa/admin/redis/opt")
                        .issueCmdUrl("https://api-test1.ugnas.com/api/p2p/v2/sa/admin/device/uglink/setup")
                        .uglinkQueryUrl("https://api-test1.ugnas.com/api/p2p/v2/sa/admin/uglink/query")
                        .build()
        );

        envMap.put("prod",
                CallApiParams.builder()
                        .accessKey("PWbK6NNtRKyEpbSg")
                        .accessSecret("zuGRPTHMWQUjcVm7VzLUxMc3A87YQCCu")
                        .centerSecret("VJ3q6KNmB69QEf5fHb1HiOAs7QYMpSBJ")
                        .domain("https://api-zh.ugnas.com")
                        .redisOptUrl("https://api-zh.ugnas.com/api/system/v2/sa/admin/redis/opt")
                        .issueCmdUrl("https://api-zh.ugnas.com/api/p2p/v2/sa/admin/device/uglink/setup")
                        .centerRedisOpt("https://center.ugnas.com/data/cluster/v2/sa/external/redis/opt")
                        .uglinkQueryUrl("https://api-zh.ugnas.com/api/p2p/v2/sa/admin/uglink/query")
                        .build()
        );

        envMap.put("prod-aar",
                CallApiParams.builder()
                        .accessKey("PWbK6NNtRKyEpbSg")
                        .accessSecret("zuGRPTHMWQUjcVm7VzLUxMc3A87YQCCu")
                        .centerSecret("VJ3q6KNmB69QEf5fHb1HiOAs7QYMpSBJ")
                        .domain("https://api-aar.ugnas.com")
                        .redisOptUrl("https://api-aar.ugnas.com/api/system/v2/sa/admin/redis/opt")
                        .issueCmdUrl("https://api-aar.ugnas.com/api/p2p/v2/sa/admin/device/uglink/setup")
                        .centerRedisOpt("https://center.ugnas.com/data/cluster/v2/sa/external/redis/opt")
                        .uglinkQueryUrl("https://api-aar.ugnas.com/api/p2p/v2/sa/admin/uglink/query")
                        .build()
        );

        envMap.put("prod-us",
                CallApiParams.builder()
                        .accessKey("PWbK6NNtRKyEpbSg")
                        .accessSecret("zuGRPTHMWQUjcVm7VzLUxMc3A87YQCCu")
                        .centerSecret("VJ3q6KNmB69QEf5fHb1HiOAs7QYMpSBJ")
                        .domain("https://api-us.ugnas.com")
                        .centerRedisOpt("https://center.ugnas.com/data/cluster/v2/sa/external/redis/opt")
                        .issueCmdUrl("https://api-us.ugnas.com/api/p2p/v2/sa/admin/device/uglink/setup")
                        .redisOptUrl("https://api-us.ugnas.com/api/system/v2/sa/admin/redis/opt")
                        .uglinkQueryUrl("https://api-us.ugnas.com/api/p2p/v2/sa/admin/uglink/query")
                        .build()
        );

        envMap.put("prod-eur",
                CallApiParams.builder()
                        .accessKey("PWbK6NNtRKyEpbSg")
                        .accessSecret("zuGRPTHMWQUjcVm7VzLUxMc3A87YQCCu")
                        .domain("https://api-eur.ugnas.com")
                        .centerRedisOpt("https://center.ugnas.com/data/cluster/v2/sa/external/redis/opt")
                        .centerSecret("VJ3q6KNmB69QEf5fHb1HiOAs7QYMpSBJ")
                        .issueCmdUrl("https://api-eur.ugnas.com/api/p2p/v2/sa/admin/device/uglink/setup")
                        .redisOptUrl("https://api-eur.ugnas.com/api/system/v2/sa/admin/redis/opt")
                        .uglinkQueryUrl("https://api-eur.ugnas.com/api/p2p/v2/sa/admin/uglink/query")
                        .build()
        );
    }

    public final static String access_type = "ugreen";
    public final static String REQUEST_HEAD_SIGN = "sign";
    public final static String REQUEST_HEAD_TS = "ts";
    public final static String REQUEST_HEAD_ACCESS_TYPE = "at";
    public final static String REQUEST_HEAD_ACCESS_KEY = "ak";

}
