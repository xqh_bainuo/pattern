package org.xqh.test.ugreen.apitest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.xqh.test.ugreen.CertificateUtils;
import org.xqh.utils.file.ReadTxtFileUtils;
import org.xqh.utils.http.FxHttpClientUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName RelayApiTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/7/25 9:36
 * @Version 1.0
 */
@Slf4j
public class RelayApiTest {

    /**
     * 开发换 ugRelay api
     */
    public final static String relay_api_url = "%s";

    public final static String relay_secret_key = "";

    public final static Map<String, RelayApiPar> envMap = new HashMap<>();

    static {
        envMap.put("dev", RelayApiPar.builder().url("http://183.240.139.230:8066").secretKey("DrAVZZLJTwhhM2eV5cjkUnxWdHguXK0n").build());
        envMap.put("test", RelayApiPar.builder().url("http://58.49.151.55:8078").secretKey("DrAVZZLJTwhhM2eV5cjkUnxWdHguXK0n").build());
        envMap.put("test2", RelayApiPar.builder().url("http://58.49.151.18:8078").secretKey("DrAVZZLJTwhhM2eV5cjkUnxWdHguXK0n").build());
        envMap.put("prod-cn5", RelayApiPar.builder().url("http://58.49.151.17:8078").secretKey("VncPJwBXOfaFiUrhBwkQfuoSc5ETnMha").build());
        envMap.put("prod-cn6", RelayApiPar.builder().url("http://58.49.151.54:8078").secretKey("VncPJwBXOfaFiUrhBwkQfuoSc5ETnMha").build());
        envMap.put("prod-cn7", RelayApiPar.builder().url("http://183.193.69.72:8078").secretKey("VncPJwBXOfaFiUrhBwkQfuoSc5ETnMha").build());
        envMap.put("prod-cn8", RelayApiPar.builder().url("http://183.193.69.73:8078").secretKey("VncPJwBXOfaFiUrhBwkQfuoSc5ETnMha").build());
        envMap.put("prod-cn9", RelayApiPar.builder().url("http://110.42.42.151:8078").secretKey("VncPJwBXOfaFiUrhBwkQfuoSc5ETnMha").build());
        envMap.put("prod-cn10", RelayApiPar.builder().url("http://110.42.42.149:8078").secretKey("VncPJwBXOfaFiUrhBwkQfuoSc5ETnMha").build());
        envMap.put("prod-us", RelayApiPar.builder().url("http://183.240.139.230:8078").secretKey("DrAVZZLJTwhhM2eV5cjkUnxWdHguXK0n").build());
        envMap.put("prod-eur", RelayApiPar.builder().url("http://183.240.139.230:8078").secretKey("DrAVZZLJTwhhM2eV5cjkUnxWdHguXK0n").build());
    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RelayApiPar {
        private String url;
        private String secretKey;
    }

    public static void main(String[] args) throws InterruptedException {
//        pushCloudMsgByUgRelay("EC671JJ15240CEF0", "ursh", "prod");
//        issueCmdByUgRelay("EC671JJ15240CEF0", "ursh", "prod");
//        changeRateLimit("UG670UU372300029", 2048, "test2");
//        changeRateLimit("EF721JJ20240064C", 8192, "test2");
//        changeRateLimit("HB670EE202400404", 4096, "prod-cn9");//james设备

//        List<String> grayList = ReadTxtFileUtils.readTxt(new File("D:\\work\\program\\项目迭代\\发布记录\\pro20241112\\uglink_gray.txt"));
//        for(String sn:grayList){
//            changeRateLimit(sn, 4096, "prod-cn9");//我的设备
//            Thread.sleep(100);
//        }
//        changeRateLimit("HB670EE2024007E1", 8192, "prod");//韩东设备
        changeRateLimit("EC671JJ15240CEF0", 8192, "prod-cn10");//韩东设备
//        changeRateLimit("TC718HH1924047E1", 8192, "prod");//杜茗煜 设备
    }

    /**
     * uglink下发指令
     *
     * @param deviceSn
     * @param rateLimit
     */
    private static void changeRateLimit(String deviceSn, int rateLimit, String env) {
        RedisOptTest.RateLimitReq req = new RedisOptTest.RateLimitReq();
        long timestamp = System.currentTimeMillis() / 1000;
        req.setTs(timestamp);
        req.setDevice_sn(deviceSn);
        req.setRate_limit(rateLimit);

        doCallUgRelayApi(req, getReqUrl(env, "/api/device/rate"), envMap.get(env).getSecretKey());
    }


    /**
     * uglink下发指令
     *
     * @param deviceSn
     * @param cmd
     */
    private static void issueCmdByUgRelay(String deviceSn, String cmd, String env) {
        RedisOptTest.UgRelayDto req = new RedisOptTest.UgRelayDto();
        long timestamp = System.currentTimeMillis() / 1000;
        req.setTs(timestamp);
        req.setDeviceSn(deviceSn);
        req.setCmd(cmd);

        doCallUgRelayApi(req, getReqUrl(env, "/api/issue/cmd"), envMap.get(env).getSecretKey());
    }

    /**
     * uglink推送消息
     *
     *
     * @param deviceSn
     */
    private static void pushCloudMsgByUgRelay(String deviceSn, String msgContent, String env) {
        Map<String, Object> map = new HashMap<>();
        map.put("sn", deviceSn);
        map.put("msg", msgContent);
        String msg = JSON.toJSONString(map);
        RedisOptTest.UgRelayDto req = new RedisOptTest.UgRelayDto();
        long timestamp = System.currentTimeMillis() / 1000;
        req.setTs(timestamp);
        req.setDeviceSn(deviceSn);
        req.setMsgType(10);
        req.setData(msg);
        req.setServerId("signal_test_server_1");

        doCallUgRelayApi(req, getReqUrl(env, "/api/msg/push"), envMap.get(env).getSecretKey());
    }

    private static String getReqUrl(String env, String apiPath) {
        return envMap.get(env).getUrl() + apiPath;
    }

    /**
     * 调用ugRelay服务接口
     */
    public static void doCallUgRelayApi(Object req, String relayServerAddr, String secretKey) {
        Map<String, Object> signMap = JSON.parseObject(JSON.toJSONString(req), Map.class);
        String sign = CertificateUtils.createSignForCloud(signMap, secretKey);
        signMap.put("sign", sign);
        String reqJson = JSON.toJSONString(signMap);
        log.info("call relay-server: {} open-ssh api start. params: {}", relayServerAddr, reqJson);
        HttpConfig config = FxHttpClientUtils.getPostJsonConfig(relayServerAddr, null, reqJson);
        try {
            HttpResult httpRet = HttpClientUtil.sendAndGetResp(config);
            log.info("call relay-server: {} open-ssh api result: {}", relayServerAddr, httpRet.getResult());
            if (StringUtils.isNotEmpty(httpRet.getResult())) {
                JSONObject apiRet = JSON.parseObject(httpRet.getResult());
                int retCode = apiRet.getInteger("code");
            }
        } catch (HttpProcessException e) {
            log.error("", e);
        }
    }
}
