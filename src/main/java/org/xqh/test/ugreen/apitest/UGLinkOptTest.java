package org.xqh.test.ugreen.apitest;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson2.JSONPath;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;
import org.xqh.test.ugreen.UGreenApiCallUtils;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.util.*;

/**
 * @ClassName QueryUGLinkInfo
 * @Description TODO
 * @Author xuqianghui
 * 我的设备号: EC671JJ15240CEF0
 * @Date 2024/7/25 9:48
 * @Version 1.0
 */
public class UGLinkOptTest {

    public static final String uglink_gray_api = "/api/p2p/v2/sa/admin/uglink/gray/opt";

    public static final String secret_add_api = "/api/device/v2/sa/admin/fetch/secret/device/save";

    public final static String GRAY_MAIN_SWITCH = "gray_main_switch";//灰度总开关
    public final static String GRAY_SN = "gray_sn";
    public final static String GRAY_IP = "gray_ip";
    public final static String GRAY_SN_HASH = "gray_sn_hash";

    /**
     *        signal_server_32	cn1-signal.ugnas.com
     *      * signal_server_199	cn2-signal.ugnas.com
     *      * relay_server_85	cn1.ug.link
     *      * relay_server_225	cn2.ug.link
     *      * relay_server_cn3	cn3.ug.link
     *      * relay_server_cn4	cn4.ug.link
     *      * relay_server_cn5	cn5.ug.link
     *      * relay_server_cn6	cn6.ug.link
     *      * relay_server_cn7	cn7.ug.link
     *      * relay_server_cn8	cn8.ug.link
     *      * relay_server_cn9	cn9.ug.link
     *      * signal_server_cn3
     * @param args  EC671JJ15240CEF0 我的设备
     */
    public static void main(String[] args) {
        //查询Uglink节点数据   DXP4800PLUS-AFC9
//        queryUGLinkInfo("UG671UU33230A329", "test");
//        queryUGLinkInfo("EC554JJ11230A1B4", "prod-aar");
//        queryUGLinkInfo("HB549HH21220BAD5", "test");
//        queryUGLinkInfo("EC752JJ20240DDC6", "prod");
//        queryUGLinkInfo("EC752JJ392405360", "prod");
//        queryUGLinkInfo("EC671JJ172404763", "prod");//
//        queryUGLinkInfo("EC671JJ15240CEF0", "prod");//我的设备
//        queryUGLinkInfo("EC554JJ44220CB32", "prod");
//        issueCmd2Device("EC752JJ20240DDC6", "prod", "relay_server_cn13", "signal_server_199");// signal_server_199   UG721JJ022408614 前端测试设备
//        issueCmd2Device("EC752JJ20240DDC6", "prod", "relay_server_bgp-cn2", "signal_server_cn3");// signal_server_199   UG721JJ022408614 前端测试设备
//        issueCmd2Device("HB670EE20240071E", "prod", "relay_server_cn5", "signal_server_cn3");// signal_server_199   UG721JJ022408614 前端测试设备
//        issueCmd2Device("EC671JJ172404763", "prod", "relay_server_cn21", "signal_server_199");// signal_server_199, signal_server_32
//        issueCmd2Device("EC671JJ15240CEF0", "prod", "relay_server_cn21", "signal_server_cn3");// signal_server_199, signal_server_32
//        issueCmd2Device("HB670EE202400404", "prod", "relay_server_cn22", "signal_server_cn3");// signal_server_199, signal_server_32
//        issueCmd2Device("EC671JJ15240CEF0", "prod", "relay_server_cn21", "signal_server_32");// signal_server_199, signal_server_32
//        issueCmd2Device("UG671UU332304FF6", "prod-aar", "relay_server_aar1", "signal_server_aar1");
//        issueCmd2Device("EC660JJ51230B5AA", "prod", "relay_server_cn17", "signal_server_32");// signal_server_199, signal_server_32
//        issueCmd2Device("EF721JJ20240064C", "test", "relay_server_55", "signal_server_55");// signal_server_199
//        issueCmd2Device("UG752JJ522300664", "test", "relay_server_18", "signal_server_55");// signal_server_199
//        issueCmd2Device("EC752JJ12243680A", "prod-eur", "relay_server_eur5", "signal_server_eur3");// signal_server_199
//        issueCmd2Device("UG752JJ522300664", "test", "relay_server_18", "signal_server_55");// signal_server_199
//        issueCmd2Device("EC671JJ15240CEF0", "prod", "relay_server_bgp-cn1", "signal_server_cn3");
//        issueCmd2Device("EC660JJ442304555", "prod", "relay_server_cn6", "signal_server_32");

        //relay_server_85 -- signal_server_32  relay_server_85
//        issueCmd2Device("EC671JJ15240CEF0", "prod", "relay_server_cn9", "signal_server_cn3");
//        issueCmd2Device("HB718HH462307F5E", "prod", "relay_server_cn13", "signal_server_32");
//        List<String> list = ReadTxtFileUtils.readTxt(new File("D:\\work\\program\\项目迭代\\需求\\uglink-p2p\\cn5.txt"));
//        for(String sn:list){
//            issueCmd2Device(sn, "prod", "relay_server_cn9", "signal_server_cn3");
//        }
//        queryUGLinkInfo("EC752JJ2124058E8");
//        queryUGLinkInfo("UG671UU32230A889");
//        queryUGLinkInfo("TC718HH19240267E");
//        queryUGLinkInfo("EC671JJ172407F69");
//        queryUGLinkInfo("EE720JJ192401D9E");


        //新增uglink 灰度设备
//        addUGLinkGrayItems("D:\\work\\program\\项目迭代\\发布记录\\pro20250114\\uglink_grey.txt", "prod");
//        addUGLinkGrayItems("D:\\work\\program\\项目迭代\\发布记录\\pro20241112\\uglink_gray.txt", "prod");
//        removeUGLinkGrayItems("D:\\work\\program\\发布记录\\pro20240730版本\\公司内测设备.txt", "dev");
//        removeUGLinkGrayItems("D:\\work\\program\\项目迭代\\发布记录\\pro20241112\\uglink_gray.txt", "prod");
//        enableUGLinkGrayRule(GRAY_SN_HASH, "dev");
//        updateGrayVal(GRAY_SN_HASH, "65", "dev");
//        doUGLinkGrayItems(Lists.newArrayList("EC752JJ12243680A"), "prod-eur");
//        doUGLinkGrayItems(Lists.newArrayList("EC752JJ20240DDC6"), "prod");
//        doUGLinkGrayItems(Lists.newArrayList("EC671JJ15240CEF0", "HB670EE202400404", "TC718HH1924047E1"), "prod");
//        doUGLinkGrayItems(Lists.newArrayList("UG752JJ52230120B"), "test");
        doRemoveUGLinkGrayItems(Lists.newArrayList("EC752JJ20240DDC6"), "prod");
        saveSecret("EC752JJ0924322FB", "prod-eur");
//        doRemoveUGLinkGrayItems(Lists.newArrayList("EC752JJ2024082F7","EC671JJ21240E672","EE719JJ20240AEFA","EE720JJ19240A911","TC718HH1924047E1","UG671UU332304FF6","HB670EE202400404","UG671UU332304FF6"), "prod");
//        switchUglinkNode();
    }

    public static void switchUglinkNode(){
        List<String> list = ReadTxtFileUtils.readTxt(new File("D:\\work\\program\\项目迭代\\需求\\uglink-p2p\\切换节点设备.txt"));
        for(String str:list){
            if(!StringUtils.isEmpty(str)&&str.contains(",")){
                String[] array = str.split(",");
                issueCmd2Device(array[0], "prod", "relay_server_cn21", "signal_server_199");// signal_server_199, signal_server_32
            }
        }
    }

    //新增uglink灰度名单
    public static void addUGLinkGrayItems(String filepath, String env){
        List<String> items = ReadTxtFileUtils.readTxt(new File(filepath));
        doUGLinkGrayItems(items, env);
    }

    public static void doUGLinkGrayItems(List<String> items, String env){
        UGLinkGrayDTO dto = UGLinkGrayDTO.builder()
                .items(items)
                .grayType("gray_sn")
                .build();
        UGreenApiCallUtils.CallResultModel result = RedisOptTest.callCloudApi(dto, RedisOptTest.envMap.get(env).getDomain().concat(uglink_gray_api), env);
    }

    public static void updateGrayVal(String grayType, String grayVal, String env){
        UGLinkGrayDTO dto = UGLinkGrayDTO.builder()
                .grayType(grayType)
                .grayVal(grayVal)
                .build();
        UGreenApiCallUtils.CallResultModel result = RedisOptTest.callCloudApi(dto, RedisOptTest.envMap.get(env).getDomain().concat(uglink_gray_api), env);
    }

    //新增uglink灰度名单
    public static void removeUGLinkGrayItems(String filepath, String env){
        List<String> items = ReadTxtFileUtils.readTxt(new File(filepath));
        doRemoveUGLinkGrayItems(items, env);
    }

    public static void doRemoveUGLinkGrayItems(List<String> items, String env){
        UGLinkGrayDTO dto = UGLinkGrayDTO.builder()
                .items(items)
                .grayType("gray_sn")
                .optType(2)//删除数据
                .build();
        UGreenApiCallUtils.CallResultModel result = RedisOptTest.callCloudApi(dto, RedisOptTest.envMap.get(env).getDomain().concat(uglink_gray_api), env);
    }

    public static void saveSecret(String deviceSn, String env ){
        Map<String,Object> par = new HashMap<>();
        par.put("configVal", deviceSn);
        par.put("configKey", "FETCH_SECRET_DEVICE");
        UGreenApiCallUtils.CallResultModel result = RedisOptTest.callCloudApi(par, RedisOptTest.envMap.get(env).getDomain().concat(secret_add_api), env);
    }

    //secret_add_api

    public static void enableUGLinkGrayRule(String grayType, String env){
        UGLinkGrayDTO dto = UGLinkGrayDTO.builder()
                .grayType(grayType)
                .status(1)//删除数据
                .build();
        UGreenApiCallUtils.CallResultModel result = RedisOptTest.callCloudApi(dto, RedisOptTest.envMap.get(env).getDomain().concat(uglink_gray_api), env);
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UGLinkGrayDTO {

        private List<String> items;//"灰度明细列表"

        private Integer optType;//"操作类型(1: 新增, 2: 删除)"

        private String grayType;//"灰度类型"

        private String grayVal;//"灰度值"

        private Integer status;//不传旧不更新状态  "状态(0: 禁用, 1: 启用)"
    }


    public static void issueCmd2Device(String sn, String env, String relay, String signal){
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("deviceSn", sn);
        reqMap.put("sdkExtPar", "{\"cpt\":2,\"bct\":\"tcp\",\"dt\":5,\"cel\":6,\"cesc\":\"-1,-5\",\"czel\":5,\"cem\":\"\",\"mka\":-1,\"mws\":-1,\"oft\":true,\"rit\":30,\"czrd\":false}");
        reqMap.put("relayServer", relay);
        reqMap.put("signalServer", signal);
        RedisOptTest.callCloudApi(reqMap, RedisOptTest.envMap.get(env).getIssueCmdUrl(), env);
    }

    public static void queryUGLinkInfo(String sn, String env) {
        RedisOptTest.DeviceSyncDto dto = RedisOptTest.DeviceSyncDto.builder().deviceSn(sn).build();
        UGreenApiCallUtils.CallResultModel result = RedisOptTest.callCloudApi(dto, RedisOptTest.envMap.get(env).getUglinkQueryUrl(), env);
        if(result.isSuccess()){
            JSONObject json = JSON.parseObject(result.getData());
            int relaySum = 0;
            int signalSum = 0;
            int grayRelaySum = 0;
            int graySignalSum = 0;
            String deviceInfo = null;
            Map<String, Integer> groupNum = new HashMap<>();
            for(Map.Entry<String, Object> entry:json.entrySet()){
                JSONObject node = json.getJSONObject(entry.getKey());
                if(Objects.equals(entry.getKey(), "device")){
                    deviceInfo = String.format("sn: %s, 信令连接状态: %s, 信令节点: %s, 信令连接时间: %s, 转发连接状态: %s, 转发节点: %s, 转发连接时间: %s",
                            node.getString("sn"), node.getString("isConnect"), node.getString("signalServerId"), node.getString("lastTime")
                            , node.getString("relayIsConn"), node.getString("relayServerId"), node.getString("relayLastTime"));
                }else {
                    String nodeType = node.getString("nodeType");
                    if(!Objects.equals(nodeType, "signal") && !Objects.equals(nodeType, "relay")){
                        continue;
                    }
                    //处理信令转发节点数据
                    int num = node.getInteger("curConnections");
                    groupNum.put(node.getString("nodeName"), num);
                    if(Objects.equals(node.getString("nodeType"), "signal")){
                        if(node.getInteger("grayNode") == 1) {
                            //灰度节点
                            graySignalSum += num;
                        }else{
                            signalSum += num;
                        }
                    }else if(Objects.equals(node.getString("nodeType"), "relay")){
                        if(node.getInteger("grayNode") == 1) {
                            //灰度节点
                            grayRelaySum += num;
                        }else{
                            relaySum += num;
                        }
                    }
                }
            }
            System.out.println(deviceInfo);
            String connInfo = String.format("信令总连接数: %s, 转发总连接数: %s, 灰度信令连接数: %s, 灰度转发连接数: %s", signalSum, relaySum, graySignalSum, grayRelaySum);
            System.out.println(connInfo);
            System.out.println(JSON.toJSONString(groupNum));
        }
    }
}
