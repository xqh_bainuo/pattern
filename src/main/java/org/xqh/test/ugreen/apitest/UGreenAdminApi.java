package org.xqh.test.ugreen.apitest;

import akka.japi.pf.FI;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.xqh.test.ugreen.UGreenApiCallUtils;
import org.xqh.utils.http.FxHttpClientUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName UGreenAdminApi
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/20 16:34
 * @Version 1.0
 */
@Slf4j
public class UGreenAdminApi {

    private final static String token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjRiODMxZGFkLTE0MDItNDg5Ny04MzZiLTI3MmQ2ODdiOWJmYiJ9.GfNb5M_oSqIOaqoJYTBqhY6qQ88qRC5-EdjIhCQnyuC7N0koorHk5j5nKyN2vJNlppUvCS4BldoxoBP8sUMSaw";

    private final static String domain = "https://admin-test.ugnas.com/test-api";

    private final static String apiNodePage = "/uglink/node/page";
    private final static String apiNodeSave = "/uglink/node/save";
    private final static String apiNodeDel = "/uglink/node/delete";
    private final static String apiNodeModify = "/uglink/node/status/modify";
    private final static String apiGrayPage= "/uglink/gray/page";
    private final static String apiGraySave= "/uglink/gray/save";
    private final static String apiGrayDelete= "/uglink/gray/delete";
    private final static String apiGrayClear= "/uglink/gray/clear";

    public static void main(String[] args) {
//        uglinkNodePage(20, 1);

//        String nodeJson = "{\n" +
//                "            \"nodeName\": \"relay_server_110\",\n" +
//                "            \"maxConnection\": 10000,\n" +
//                "            \"connectIp\": \"58.49.151.110:8024\",\n" +
//                "            \"connectDomain\": \"58.49.151.110:8024\",\n" +
//                "            \"nodeType\": \"relay\",\n" +
//                "            \"nodeGroup\": \"sh_idc\",\n" +
//                "            \"operator\": \"relay-test.ugnas.com:8024\",\n" +
//                "            \"grayNode\": 0,\n" +
//                "            \"nodeDomain\": \"cn6-test.ug.link:8088\"" +
//                "        }";
//        uglinkNodeSave(nodeJson);//新增修改节点

//        uglinkNodeDel(17L);//删除节点

//        uglinkNodeModify(4, 0); //节点上下线

//        uglinkGrayPage(10, 1);
//        uglinkGraySave("testsn");

        uglinkGrayDelete("222222222222222");

    }

    public static void uglinkGrayDelete(String deviceSn){
        wrapperUGLinkGrayOpt(apiGrayDelete, deviceSn);
    }

    public static void wrapperUGLinkGrayOpt(String api, String deviceSn){
        String url = domain.concat(api);
        Map<String, Object> params = new HashMap<>();
        params.put("deviceSn", deviceSn);
        HttpRequest httpRequest = HttpUtil.createPost(url);
        httpRequest.header("Authorization", token);
        httpRequest.body(JSON.toJSONString(params));
        httpRequest.contentType("application/json");
        HttpResponse response = httpRequest.execute();
        log.info("call api result: {}", response.body());
    }

    public static void uglinkGraySave(String deviceSn){
        wrapperUGLinkGrayOpt(apiGraySave, deviceSn);
    }
    /**
     * 节点分页接口
     * @param pageSize
     * @param page
     * @return
     */
    public static void uglinkGrayPage(int pageSize, int page){
        String url = domain.concat(apiGrayPage).concat(String.format("?pageNum=%s&pageSize=%s", page, pageSize));
        HttpRequest httpRequest = HttpUtil.createGet(url);
        httpRequest.header("Authorization", token);

        HttpResponse response = httpRequest.execute();
        log.info("call api result: {}", response.body());
    }

    public static void uglinkNodeModify(int id, int status){
        String url = domain.concat(apiNodeModify);
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("status", status);
        HttpRequest httpRequest = HttpUtil.createPost(url);
        httpRequest.header("Authorization", token);
        httpRequest.body(JSON.toJSONString(params));
        httpRequest.contentType("application/json");
        HttpResponse response = httpRequest.execute();
        log.info("call api result: {}", response.body());
    }


    public static void uglinkNodeDel(Long id){
        String url = domain.concat(apiNodeDel);
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        HttpRequest httpRequest = HttpUtil.createPost(url);
        httpRequest.header("Authorization", token);
        httpRequest.body(JSON.toJSONString(params));
        httpRequest.contentType("application/json");
        HttpResponse response = httpRequest.execute();
        log.info("call api result: {}", response.body());
    }

    public static void uglinkNodeSave(String json){
        log.info(json);
        String url = domain.concat(apiNodeSave);
        HttpRequest httpRequest = HttpUtil.createPost(url);
        httpRequest.header("Authorization", token);
        httpRequest.body(json);
        httpRequest.contentType("application/json");
        HttpResponse response = httpRequest.execute();
        log.info("call api result: {}", response.body());
    }


    /**
     * 节点分页接口
     * @param pageSize
     * @param page
     * @return
     */
    public static String uglinkNodePage(int pageSize, int page){
        String url = domain.concat(apiNodePage).concat(String.format("?pageNum=%s&pageSize=%s", page, pageSize));
        HttpRequest httpRequest = HttpUtil.createGet(url);
        httpRequest.header("Authorization", token);

        HttpResponse response = httpRequest.execute();
        log.info("call api result: {}", response.body());
        return null;
    }

    public static void putRequestHeads(HttpConfig config){
        Header[] result = HttpHeader.custom()
                .other("authorization", token)
                .build();
        config.headers(result);
    }


}
