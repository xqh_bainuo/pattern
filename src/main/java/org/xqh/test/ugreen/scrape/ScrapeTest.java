package org.xqh.test.ugreen.scrape;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;
import org.xqh.test.NumberUtils;
import org.xqh.utils.BeanUtils;

import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName ScrapeTest
 * @Description 影视剧集刮削
 * @Author xuqianghui
 * @Date 2023/11/17 11:35
 * @Version 1.0
 */
public class ScrapeTest {

    private final static String type_number = "number";
    private final static String type_text = "text";

    public final static String media_tv = "tv";

    public final static String media_movie = "movie";

    public final static int replace_text = 1;

    public final static int replace_head_tail = 2;

    public final static int replace_regex = 3;//正则表达式替换

    public final static int replace_regex_fill = 4;//正则匹配后填充 内容

    public final static int split_regex_put_property = 5;//正则拆分 设置属性

    public final static int regex_just_put_property = 6;//仅仅是 设置对象属性

    public final static void main(String[] args) {
        
        System.out.println("approval_config_upd.sql".length());
        String config = "{\"audioCfg\":[\"AC3\",\"DDP\",\"AAC\",\"DTS-HD.MA\",\"DTS\",\"TrueHD\",\"LPCM\",\"PCM\"],\"captionCfg\":[\"CHS-ENG\",\"CHS\",\"CHT\",\"ENG\",\"JPN\",\"RUS\"],\"cfgVer\":\"v1.0.0\",\"editVerCfg\":[\"PROPER\",\"REPACK\",\"DUPE\",\"IMAX\",\"UNRATED\",\"R-RATE\",\"SE\",\"WITH EXTRAS\",\"RERIP\",\"SUBBED\",\"Director's Cut\",\"Directors Cut\",\"DC\",\"LIMITED\",\"Extended REMASTERED\",\"Extended Edition\",\"EXTENDED\",\"CC\",\"Remastered\"],\"encodeCfg\":[\"H.264\",\"H.265\",\"H264\",\"H265\",\"AV1\",\"VC-1\",\"Xvid\",\"MPEG-2\",\"MPEG2\",\"FLAC\",\"DD\"],\"matchItems\":[{\"key\":\"year\",\"type\":\"number\"},{\"key\":\"editVer\",\"type\":\"text\"},{\"key\":\"source\",\"type\":\"text\"},{\"key\":\"ratio\",\"type\":\"text\"},{\"key\":\"audio\",\"type\":\"text\"},{\"key\":\"sound\",\"type\":\"text\"},{\"key\":\"caption\",\"type\":\"text\"},{\"key\":\"encode\",\"type\":\"text\"}],\"nameReplaceCfg\":[{\"resource\":\"第([0-9]+)[话,集,季]\",\"type\":3},{\"resource\":\".\",\"type\":2},{\"resource\":\"[\",\"type\":2},{\"resource\":\"]\",\"type\":2},{\"resource\":\"-\",\"type\":2},{\"resource\":\"(\",\"type\":2},{\"resource\":\")\",\"type\":2},{\"resource\":\"([a-z]*[\\\\u4e00-\\\\u9fa5]+[\\\\u4e00-\\\\u9fa5,0-9,.,：]*)[.]([a-z,.,-]+)\",\"target\":\"{1,name}{2,engName}\",\"type\":5}],\"ratioCfg\":[\"1080p\",\"1080i\",\"720p\",\"2160p\",\"SD\",\"2K\",\"4K\"],\"replaceCfg\":[{\"resource\":\"[\\\\[,【][a-z, ,\\\\-,★,\\\\u4e00-\\\\u9fa5]*(Team|社|月新番|字幕组|字幕組|国漫)[],】]\",\"type\":3},{\"resource\":\"[★][a-z, ,\\\\-,★,\\\\u4e00-\\\\u9fa5]*月新番\",\"type\":3},{\"resource\":\"\\\\[[a-z, ,\\\\-,★,\\\\u4e00-\\\\u9fa5]*sub]\",\"type\":3},{\"resource\":\"[\\\\[]*(电影之家|最新电影|阳光电影).*\\\\.(com|me)[\\\\]]*\",\"type\":3},{\"resource\":\"(HD|BD)([\\\\u4e00-\\\\u9fa5]*)(中字|双字)\",\"target\":\"{.}{1}\",\"type\":4},{\"resource\":\"(HD|BD)([0-9]+)(p|i)\",\"target\":\"{.}{1}\",\"type\":4},{\"resource\":\"djjh10\",\"target\":\"电锯惊魂10\",\"type\":1},{\"resource\":\"([\\\\u4e00-\\\\u9fa5]*(中字|双字|字幕))\",\"target\":\"{1,caption}\",\"type\":6},{\"resource\":\"top[0-9]+[.]\",\"type\":3}],\"soundCfg\":[\"Atmos.7.1\",\"7.1\",\"5.1\",\"2.0\"],\"sourceCfg\":[\"UHD Blu-Ray\",\"Blu-ray\",\"BluRay\",\"HDDVD\",\"HR-HDTV\",\"HDRIP\",\"BDRIP\",\"DVD\",\"HDTV\",\"TV\",\"WEB-DL\",\"Encode\",\"REMUX\",\"CAM\",\"TS\",\"TC\",\"DVDSCR\",\"DVDRIP\",\"R5\",\"R6\",\"HD\",\"BD\"],\"splitCfg\":[\" \",\".\",\"-\",\"_\",\"[\",\"]\",\"(\",\")\",\",\"],\"tvRegexCfg\":[{\"eIdx\":2,\"regex\":\"[.,\\\\-,\\\" \\\"]s([0-9]+)e([0-9]+)[.,\\\\-,\\\" \\\"]\",\"sIdx\":1},{\"eIdx\":2,\"regex\":\"[.,\\\\-,\\\" \\\"]ep([0-9]+)\\\\-([0-9]+)[.,\\\\-,\\\" \\\"]\",\"sIdx\":1},{\"eIdx\":1,\"regex\":\"[.,\\\\-,\\\" \\\"]ep([0-9]+)[.,\\\\-,\\\" \\\"]\",\"sIdx\":-1},{\"eIdx\":2,\"regex\":\"\\\\[[a-z, ]+([0-9]+)th Season]\\\\[([0-9]+)]\",\"sIdx\":1},{\"eIdx\":2,\"regex\":\"第([0-9]+)[季]([0-9]+)[.,\\\\-,\\\" \\\"]\",\"sIdx\":1},{\"eIdx\":1,\"regex\":\"第([0-9]+)[话,集]\",\"sIdx\":-1},{\"eIdx\":1,\"regex\":\"\\\\[([0-9]+) end]\",\"sIdx\":-1},{\"eIdx\":2,\"regex\":\"\\\\[([0-9]+)-([0-9]+)fin]\",\"sIdx\":1},{\"eIdx\":1,\"regex\":\"\\\\[([0-9]+)]\",\"sIdx\":-1}],\"yearCfg\":[1930,2030]}";
        MediaConfig mediaCfg = new MediaConfig();
        System.out.println(JSON.toJSONString(mediaCfg));
        //djjh10.1080p.HD中英双字[最新电影www.dygangs.me].mp4
        //Taxi.4.2007.Directors.Cut.BluRay.1080p.x265.10bit.2Audio.MNHD-FRDS.mkv
        //Leon.1994.EXTENDED.REMASTERED.BluRay.1080p.2Audio.TrueHD.Atmos.7.1,X265,10bit-BeiTai.mkv
        //Stripes.1981.Extended.Cut.BluRay.1080p.DTS-HD.MA.5.1.AVC.REMUX-FraMeSToR.mkv
        List<String> testList = Lists.newArrayList(
                "X战警前传：金刚狼.X-Men.Origins.Wolverine.2009.BDRip.X264.AAC.iNT-XTM.mkv",
                "斗罗大陆第2集.mp4",
                "阳光电影www.ygdy8.com.调琴师.BD.720p.中文字幕.mkv",
                "쓸쓸하고 찬란하神 - 도깨비-03a.mp4",
                "蜥蜴伯伯里奥.1080p.BD中英双字[最新电影www.dygangs.me].mp4",
                "[电影之家www.dyzj18.com]新奥林巴斯-2023_BD意大利语中字.mp4",
                "island.of.lemurs.madagascar.2014.docu.1080p.bluray.x264-nodlabs.mkv",
                "Island.of.Lemurs.Madagascar.2014.IMAX.1080p.3D.BluRay.Half-SBS.x264.DTS-HD.MA.5.1-RARBG.mkv",
                "[电影之家www.dyzj18.com]流浪地球2-2023_BD国英双语双字修复版.mp4",
                "[电影之家www.dyzj18.com]止痛骗-2023_BD中英双字.mp4",
                "[电影之家www.dyzj18.com]人生大乱斗-2021_BD卡纳达语中字.mp4",
                "[电影之家www.dyzj18.com]驱魔人：信徒.2023.BD.1080P.英语中字.mkv",
                "[电影之家www.dyzj18.com]富江2.冤有头.Tomie.Replay.2000.HD720P.日语中字.mkv",
                "夜之门.Gates.of.the.Night.1946.BD1080P.法语中字.mp4",
                "The.War.S01E01.2007.Repack.1080p.BluRay.x265.10bit.DD.5.1-DDHDTV.mkv",
                "天生的标签.Stamped.from.the.Beginning.2023.HD1080P.英语中字.mp4",
                "奈德.Nyad.2023.HD1080P.英语中字.mp4",
                "进击的巨人 最终季 完结篇 后篇.2023.HD1080P.日语中字.mp4",
                "AI创世者.The.Creator.2023.HD1080P.英语中字.mp4",
                "忍者神龟：变种大乱斗.1080p.国粤英三语.BD中英双字[最新电影www.dygangs.me].mp4",
                "15.1080p.HD国语中字无水印[最新电影www.dygangs.me].mkv.bt", //治愈系恋人 第15集 (没有片名)
                "斗破苍穹 第5季11.1080p.HD国语中字无水印.mp4",
                "铁道人：1984博帕尔事件.The.Railway.Men.The.Untold.Story.Of.Bhopal.1984.S01E04.1080p.HD中英双字[最新电影www.dygangs.me].mp4",
                "djjh10.1080p.HD中英双字.[最新电影www.dygangs.me].mp4",
                "Leon.1994.EXTENDED.REMASTERED.BluRay.1080p.2Audio.TrueHD.Atmos.7.1,X265,10bit-BeiTai.mkv",
                "Stripes.1981.Extended.Cut.BluRay.1080p.DTS-HD.MA.5.1.AVC.REMUX-FraMeSToR.mkv",
                "X.Men.Apocalypse.2016.1080p.BluRay.x264.DTS-WiKi.MKV",
                "Taxi.4.2007.Directors.Cut.BluRay.1080p.x265.10bit.2Audio.MNHD-FRDS.mkv",
                "Game.of.Thrones.S08E05.1080p.WEB.H264-MEMENTO.mkv",
                "The.Walking.Dead.S05E05.720p.HDTV.x264-KILLERS.mkv",
                "Jade.Big WhiteDuel.Ep10.HDTV.720p.H264-CNHK.mkv",
                "Zhui.Qiu.2019.EP01-36.WEB-DL.1080p.H265.AAC-CHDWEB.mkv",
                "为全人类.for.all.mankind.s03e03.2160p.HD中英双字.mp4",
                "AHTV.Judge.of.Song.Dynasty.2005.EP01-02.HDTV.1080i.H264-XX.mp4",
                "王冠.The.Crown.S04E04.官方中字.WEBrip.720P-CS.mp4",
                "[最新电影www.dygangs.me]罪城苏布拉：风云再起.Suburraeterna.S01E06.Hoo-ha.1080p.HD中字.mp4",
                "Top016.教父.The.Godfather.1972.Bluray.1080p.x265.AAC(5.1).2Audios.GREENOTEA.mp4",
                "1Top001.肖申克的救赎.The.Shawshank.Redemption.1994.Bluray.REPACK.1080p.x265.AAC(5.1).5Audios.GREENOTEA.mp4",
                "人在囧途 (2010) - 720p.mkv",
                "人生路不熟.mp4",
                "人生路不熟-1080p.mp4",
                "第01话.龙的继承者.龙之家族.2022.1080P.H265.AAC.mkv",
                "第01话.2022.1080P.龙之家族.H265.AAC.mkv",
                "第01话.龙之家族.2022.1080P.H265.AAC.mkv",
                "第02话.龙之家族.2022.1080P.H265.AAC.mkv",
                "第03话.龙之家族.2022.1080P.H265.AAC.mkv",
                "[名侦探柯南][12 END][1080P][简体][MP4].mkv",
                "[数码宝贝][1-13Fin][1080P][MKV].mkv",
                "[GM-Team][国漫][择天记 第5季][Ze Tian Ji 5th Season][12][AVC][GB][1080P].mp4",
                "鹊刀门传奇第03集.mp4",
                "[ONE PIECE 海贼王][第10话][1080p][MKV].mkv",
                "[xx字幕組][海贼王][09][1080P][MP4].mkv",
                "【xx社】★x月新番[海贼王][09][1080P][MP4].mkv",
                "[xxsub][x月新番]海贼王[09][1080P][MP4].mkv"
        );

        for (String text : testList) {
            scrapeOneResource(text, mediaCfg);
        }
    }


    @Data
    public static class MediaConfig {

        private String cfgVer = "v1.0.0";//配置版本号

        private List<CfgItem> matchItems = initMatchCfgItems();

        private List<Integer> yearCfg = Lists.newArrayList(1930, 2030);//年份区间

        /**
         * ^	匹配字符串开头
         * $	匹配字符串结尾
         * 剪辑版本
         * 如果有的资源是特别的剪辑版，如导演剪辑版，会标识有“DC”/“Director's Cut”；其他的还有UNRATED（未分级版）、LIMITED等。
         */
        private List<String> editVerCfg = Lists.newArrayList("PROPER", "REPACK", "DUPE", "IMAX", "UNRATED",
                "R-RATE", "SE", "WITH EXTRAS", "RERIP", "SUBBED",
                "Director's Cut", "Directors Cut", "DC", "LIMITED",
                "Extended REMASTERED", "Extended Edition", "EXTENDED", "CC", "Remastered");// 剪辑版本配置

        private List<String> ratioCfg = Lists.newArrayList("1080p", "1080i", "720p", "2160p", "SD", "2K", "4K");//分辨率配置

        private List<String> sourceCfg = Lists.newArrayList("UHD Blu-Ray", "Blu-ray", "BluRay", "HDDVD", "HR-HDTV", "HDRIP", "BDRIP", "DVD", "HDTV", "TV", "WEB-DL", "Encode",
                "REMUX", "CAM", "TS", "TC", "DVDSCR", "DVDRIP", "R5", "R6", "HD", "BD");//片源

        private List<String> encodeCfg = Lists.newArrayList("H.264", "H.265", "H264", "H265", "AV1", "VC-1", "Xvid", "MPEG-2", "MPEG2", "FLAC", "DD");//编码

        private List<String> captionCfg = Lists.newArrayList("CHS-ENG", "CHS", "CHT", "ENG", "JPN", "RUS");//字幕语言

        private List<String> audioCfg = Lists.newArrayList("AC3", "DDP", "AAC", "DTS-HD.MA", "DTS", "TrueHD", "LPCM", "PCM");//音频编码

        private List<String> soundCfg = Lists.newArrayList("Atmos.7.1", "7.1", "5.1", "2.0");

        //全文匹配 替换的 配置
        private List<ReplaceCfg> replaceCfg = Lists.newArrayList(
                ReplaceCfg.builder().resource("[\\[,【][a-z, ,\\-,★,\\u4e00-\\u9fa5]*(Team|社|月新番|字幕组|字幕組|国漫)[],】]").type(replace_regex).build(),
                ReplaceCfg.builder().resource("[★][a-z, ,\\-,★,\\u4e00-\\u9fa5]*月新番").type(replace_regex).build(),
                ReplaceCfg.builder().resource("\\[[a-z, ,\\-,★,\\u4e00-\\u9fa5]*sub]").type(replace_regex).build(),
                ReplaceCfg.builder().resource("[\\[]*(电影之家|最新电影|阳光电影).*\\.(com|me)[\\]]*").type(replace_regex).build(),
                ReplaceCfg.builder().resource("(HD|BD)([\\u4e00-\\u9fa5]*)(中字|双字)").target("{.}{1}").type(replace_regex_fill).build(),
                ReplaceCfg.builder().resource("(HD|BD)([0-9]+)(p|i)").target("{.}{1}").type(replace_regex_fill).build(), // "{.}{1}" 表示  填充字符串为 ".", 1 表示填充第一个位置
                ReplaceCfg.builder().resource("djjh10").target("电锯惊魂10").type(replace_text).build(),
                ReplaceCfg.builder().resource("([\\u4e00-\\u9fa5]*(中字|双字|字幕))").target("{1,caption}").type(regex_just_put_property).build(), //兼容 中文字幕语言
                ReplaceCfg.builder().resource("^top[0-9]+[.]").type(replace_regex).build()
        );

        private List<ReplaceCfg> nameReplaceCfg = Lists.newArrayList( //针对资源名 首位 需要替换的字符串
                ReplaceCfg.builder().resource("第([0-9]+)[话,集,季]").type(replace_regex).build(),
                ReplaceCfg.builder().resource(".").type(replace_head_tail).build(),
                ReplaceCfg.builder().resource("[").type(replace_head_tail).build(),
                ReplaceCfg.builder().resource("]").type(replace_head_tail).build(),
                ReplaceCfg.builder().resource("-").type(replace_head_tail).build(),
                ReplaceCfg.builder().resource("(").type(replace_head_tail).build(),
                ReplaceCfg.builder().resource(")").type(replace_head_tail).build(),
                ReplaceCfg.builder().resource("([a-z]*[\\u4e00-\\u9fa5]+[\\u4e00-\\u9fa5,0-9,.,：]*)[.]([a-z,.,-]+)").target("{1,name}{2,engName}").type(split_regex_put_property).build()
        );

        //电视剧正则匹配
        private List<TvRegexCfg> tvRegexCfg = Lists.newArrayList(
                TvRegexCfg.builder().regex("[.,\\-,\" \"]s([0-9]+)e([0-9]+)[.,\\-,\" \"]").sIdx(1).eIdx(2).build(),
                TvRegexCfg.builder().regex("[.,\\-,\" \"]ep([0-9]+)\\-([0-9]+)[.,\\-,\" \"]").sIdx(1).eIdx(2).build(),
                TvRegexCfg.builder().regex("[.,\\-,\" \"]ep([0-9]+)[.,\\-,\" \"]").sIdx(-1).eIdx(1).build(),
                TvRegexCfg.builder().regex("\\[[a-z, ]+([0-9]+)th Season]\\[([0-9]+)]").sIdx(1).eIdx(2).build(),
                TvRegexCfg.builder().regex("第([0-9]+)[季]([0-9]+)[.,\\-,\" \"]").sIdx(1).eIdx(2).build(),
                TvRegexCfg.builder().regex("第([0-9]+)[话,集]").sIdx(-1).eIdx(1).build(),
                TvRegexCfg.builder().regex("\\[([0-9]+) end]").sIdx(-1).eIdx(1).build(),
                TvRegexCfg.builder().regex("\\[([0-9]+)-([0-9]+)fin]").sIdx(1).eIdx(2).build(),
                TvRegexCfg.builder().regex("\\[([0-9]+)]").sIdx(-1).eIdx(1).build(),
                TvRegexCfg.builder().regex("[-]{1}([0-9]{2})").sIdx(-1).eIdx(1).build()
        );

        private List<String> splitCfg = Lists.newArrayList(" ", ".", "-","_", "[", "]", "(", ")", ","); //分割匹配标识符
    }

    private static void scrapeOneResource(String text, MediaConfig mediaCfg) {
        if (StringUtils.isBlank(text)) {
            return;
        }
        Cursor cursor = initCursorObj();
        MediaModel media = new MediaModel();
        text = parseMediaFormat(text, media);//解析文件格式
        text = handleReplaceConfig(text, mediaCfg, media);//处理 需要单独替换的内容
        String[] array = text.split("");
        matchTvSeason(text, media, mediaCfg, cursor);
        for (CfgItem cfg : mediaCfg.getMatchItems()) {
            handleOneCfg(cursor, text, cfg, array, mediaCfg, media);
        }
        handleMediaObjName(text, media, mediaCfg, cursor);

        System.out.println(JSON.toJSONString(media));
//        System.out.println(JSON.toJSONString(cursor));
    }

    private static Cursor initCursorObj() {
        return Cursor.builder().start(0)
                .matchLst(Lists.newArrayList())
                .build();
    }

    private static List<CfgItem> initMatchCfgItems() {
        List<CfgItem> result = Lists.newArrayList();
        result.add(CfgItem.builder().key("year").type("number").build());
        result.add(CfgItem.builder().key("editVer").type("text").build());
        result.add(CfgItem.builder().key("source").type("text").build());
        result.add(CfgItem.builder().key("ratio").type("text").build());
        result.add(CfgItem.builder().key("audio").type("text").build());
        result.add(CfgItem.builder().key("sound").type("text").build());
        result.add(CfgItem.builder().key("caption").type("text").build());
        result.add(CfgItem.builder().key("encode").type("text").build());
        return result;
    }

    private static String handleReplaceConfig(String text, MediaConfig mediaCfg, MediaModel media) {
        List<ReplaceCfg> cfgs = mediaCfg.getReplaceCfg();
        for (ReplaceCfg c : cfgs) {
            text = wrapperReplaceLogic(text, c, media);
        }
        return text;
    }

    private static String wrapperReplaceLogic(String text, ReplaceCfg cfg, MediaModel media) {
        if (Objects.isNull(cfg.getType()) || cfg.getType() == replace_text) {
            //全文本替换
            return text.replaceAll(cfg.getResource(), cfg.getTarget());
        } else if (cfg.getType() == replace_head_tail) {
            //只替换 收尾
            if (text.startsWith(cfg.getResource())) {
                text = text.substring(cfg.getResource().length());
            }
            if (text.endsWith(cfg.getResource())) {
                text = text.substring(0, text.lastIndexOf(cfg.getResource()));
            }
        } else if (cfg.getType() == replace_regex) {
            text = regexReplaceText(text, cfg);
        } else if (cfg.getType() == replace_regex_fill) {//正则匹配 填充内容
            text = regexFillText(text, cfg);
        }else if (cfg.getType() == split_regex_put_property) {//正则匹配 设置属性
            text = regexSplitPutProperty(text, cfg, media);
        }else if(cfg.getType() == regex_just_put_property){
            regexJustPutProperty(text, cfg, media);
        }
        return text;
    }

    private static void regexJustPutProperty(String text, ReplaceCfg cfg, MediaModel media){
        List<RegexSplitItem> items = parseRegexSplitItem(cfg.getTarget());
        if(CollectionUtils.isEmpty(items)){
            return ;
        }
        Pattern pattern = Pattern.compile(cfg.getResource(), Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        a:while (matcher.find()) {//只匹配一次
            int maxSize = matcher.groupCount();
            for(int i = 0; i < items.size(); i ++){
                if(i > maxSize){
                    break a;
                }
                putObjPropertyConditionNull(media, items.get(i).getProperty(), matcher.group(items.get(i).getIndex()));
            }
        }
    }

    private static <T> void putObjPropertyConditionNull(T obj, String property, Object value){
        Object val = BeanUtils.getObjFieldPropertyVal(obj, property);
        if(Objects.nonNull(val)){
            return ;
        }
        BeanUtils.putObjFieldProperty(obj, property, value);
    }

    private static String regexSplitPutProperty(String text, ReplaceCfg cfg, MediaModel media){
        List<RegexSplitItem> items = parseRegexSplitItem(cfg.getTarget());
        if(CollectionUtils.isEmpty(items)){
            return text;
        }
        Pattern pattern = Pattern.compile(cfg.getResource(), Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        String copyText = text;
        a:while (matcher.find()) {//只匹配一次
            int maxSize = matcher.groupCount();
            copyText = matcher.group(items.get(0).getIndex());//第一个值 为响应结果
            for(int i = 1; i < items.size(); i ++){
                if(i > maxSize){
                    break a;
                }
                putObjPropertyConditionNull(media, items.get(i).getProperty(), matcher.group(items.get(i).getIndex()));
            }
        }
        return copyText;
    }

    /**
     * 正则匹配 内容填充
     *
     * @param text
     * @param cfg
     * @return
     */
    private static String regexFillText(String text, ReplaceCfg cfg) {
        RegexFillItem fillItem = parseRegexFillObj(cfg.getTarget());
        if (Objects.isNull(fillItem)) {
            return text;
        }
        Pattern pattern = Pattern.compile(cfg.getResource(), Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        String copyText = text;
        while (matcher.find()) {//只匹配一次
            String replace = matcher.group();
            StringBuffer sb = new StringBuffer();
            int maxSize = matcher.groupCount();
            for (int i = 1; i <= maxSize; i++) {
                sb.append(matcher.group(i));
                if (fillItem.getFillSlot().contains(i)) {
                    sb.append(fillItem.getFillStr());
                }
            }
//            System.out.println("match text => " + replace + ", replace to => " + sb.toString());
            copyText = copyText.replace(replace, sb.toString());
        }
        return copyText;
    }

    public static boolean checkRegexItemFormat(String txt){
        return !txt.startsWith("{") || !txt.endsWith("}");
    }

    public static List<RegexSplitItem> parseRegexSplitItem(String txt) {
        if (checkRegexItemFormat(txt)) {
            return null;
        }
        String[] array = txt.split("}");
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i].substring(1);
        }
        List<RegexSplitItem> result = Lists.newArrayList();
        //第一个槽位 为 替换字符串
        for (int i = 0; i < array.length; i++) {
            String[] items = array[i].split(",");
            if(!NumberUtils.isPositiveInteger(items[0])){
                return null;
            }
            RegexSplitItem obj = new RegexSplitItem();
            obj.setIndex(Integer.parseInt(items[0]));
            obj.setProperty(items[1]);
            result.add(obj);
        }
        result.sort((a, b) -> a.getIndex().compareTo(b.getIndex()));//序号排序
        return result;
    }

    public static RegexFillItem parseRegexFillObj(String txt) {
        if (checkRegexItemFormat(txt)) {
            return null;
        }
        String[] array = txt.split("}");
        if (array.length <= 1) {
            return null;//配置格式错误
        }
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i].substring(1);
        }
        RegexFillItem result = new RegexFillItem();
        //第一个槽位 为 替换字符串
        result.setFillStr(array[0]);
        result.setFillSlot(Lists.newArrayList());
        for (int i = 1; i < array.length; i++) {
            if (!NumberUtils.isPositiveInteger(array[i])) {
                return null;//配置个参数 类型 不是int
            }
            result.getFillSlot().add(Integer.parseInt(array[i]));
        }
        result.getFillSlot().sort((a, b) -> a.compareTo(b));//序号排序
        return result;
    }

    @Data
    public static class RegexSplitItem{
        private Integer index;//正则匹配的内容序号
        private String property;//设置的属性
    }

    @Data
    public static class RegexFillItem {
        private String fillStr;//填充字符串
        private List<Integer> fillSlot;//填充槽位
    }

    /**
     * 正则 文本替换
     *
     * @param text
     * @param cfg
     * @return
     */
    private static String regexReplaceText(String text, ReplaceCfg cfg) {
        //正则匹配替换
        Pattern pattern = Pattern.compile(cfg.getResource(), Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
        String textCopy = text;
        while (matcher.find()) {
            String group = matcher.group();
            if (group.startsWith("[")) {
                group = "\\".concat(group);
            }
            String target = StringUtils.isEmpty(cfg.getTarget()) ? "" : cfg.getTarget();
            textCopy = textCopy.replaceAll(group, target);
        }
        text = textCopy;
        return text;
    }


    private static void matchTvSeason(String text, MediaModel media, MediaConfig mediaCfg, Cursor cursor) {
        List<TvRegexCfg> cfgList = mediaCfg.getTvRegexCfg();
        boolean isTv = false;
        a:
        for (TvRegexCfg cfg : cfgList) {
            Pattern pattern = Pattern.compile(cfg.getRegex(), Pattern.CASE_INSENSITIVE);//不区分大小写 正则匹配
            Matcher matcher = pattern.matcher(text);
            while (matcher.find()) {
                //设置 电视剧 第几季  和 第几集
                if (cfg.getSIdx() >= 0) {
                    media.setSeason(matcher.group(cfg.getSIdx()));
                }
                if (cfg.getEIdx() >= 0) {
                    media.setEpisode(matcher.group(cfg.getEIdx()));
                }
                matchedContentWithRegex(cursor, text, matcher.group());
                isTv = true;
                break a;
            }
        }
        if (isTv) {
            media.setType(media_tv);
        } else {
            media.setType(media_movie);
        }
    }

    private static void matchedContentWithRegex(Cursor cursor, String text, String matchTxt) {
        int idx = text.indexOf(matchTxt);
        if (cursor.getMinMatch() == 0 || idx < cursor.getMinMatch()) {
            cursor.setMinMatch(idx + 1);
        }
        cursor.getMatchLst().add(
                CursorItem.builder()
                        .index(text.indexOf(matchTxt))
                        .length(matchTxt.length())
                        .build()
        );
    }

    private static void matchedContent(Cursor cursor, CfgItem cfg, MediaModel media, String subText) {
        putObjPropertyConditionNull(media, cfg.getKey(), subText);
        if (cursor.getMinMatch() == 0 || cursor.getStart() < cursor.getMinMatch()) {
            cursor.setMinMatch(cursor.getStart());
        }
        cursor.getMatchLst().add(
                CursorItem.builder()
                        .index(cursor.getStart())
                        .length(subText.length())
                        .build()
        );
        cursor.setStart(cursor.getStart() + subText.length() + 1);
    }

    private static boolean handleOneCfg(Cursor cursor, String text, CfgItem cfg, String[] array, MediaConfig mediaCfg, MediaModel media) {
//        int oldStart = cursor.getStart();
        boolean result = false;
        if (cfg.getType().equals(type_number)) {
            result = matchNumberContent(cursor, array, text, cfg, mediaCfg, media);
        } else if (cfg.getType().equals(type_text)) {//匹配文本
            List<String> txtLst = (List<String>) BeanUtils.getObjFieldPropertyVal(mediaCfg, cfg.getKey() + "Cfg");
            result = recursionMatchTxt(cursor, txtLst, text, cfg, array, media, mediaCfg);
        }
//        if (!result) {
//            cursor.setStart(oldStart);//没有匹配到结果 重置游标
//        }
        cursor.setStart(0);//重置游标
        return result;
    }

    /**
     * 处理资源名称
     *
     * @param text
     * @param media
     */
    private static void handleMediaObjName(String text, MediaModel media, MediaConfig mediaCfg, Cursor cursor) {
        String name = null;
        if (CollectionUtils.isEmpty(cursor.getMatchLst())) {
            //没有匹配到 任何一条规则
            name = text;
        } else {
            List<CursorItem> items = cursor.getMatchLst();
            items.sort((a, b) -> a.getIndex().compareTo(b.getIndex()));//对匹配到的 数据 序号进行排序
            //假如第一个匹配的 不是 第0个序号
            if (items.get(0).getIndex() != 0) {
                //直接截取前面的内容 设置为 资源名称
                name = text.substring(0, items.get(0).getIndex());
            } else {
                if (items.size() == 1) { //如果只匹配到了一条
                    name = text.substring(items.get(0).getIndex() + items.get(0).getLength());//直接截取到文本末尾
                } else {
                    for (int i = 1; i < items.size(); i++) {
                        CursorItem first = items.get(i - 1);
                        CursorItem next = items.get(i);
                        if (next.getIndex() > (first.getIndex() + first.getLength() + 1)) {
                            name = text.substring(first.getIndex() + first.getLength(), next.getIndex());
                        }
                    }
                }
            }
        }
        media.setName(replaceMediaNameStr(name, mediaCfg, media));
    }

    private static String replaceMediaNameStr(String name, MediaConfig mediaCfg, MediaModel media) {
        if (StringUtils.isBlank(name)) {
            return name;
        }

        for (ReplaceCfg cfg : mediaCfg.getNameReplaceCfg()) {
            name = wrapperReplaceLogic(name, cfg, media);
        }
        return name.trim();
    }

    private static boolean matchNumberContent(Cursor cursor, String[] array, String text, CfgItem cfg, MediaConfig mediaCfg, MediaModel media) {
        for (int i = cursor.getStart(); i < array.length; i++) {
            String t = array[i];
            if (checkIsSplitPoint(t, mediaCfg)) {
                String subText = text.substring(cursor.getStart(), i);
                //获取当前 截取的内容
                if (NumberUtils.isPositiveInteger(subText)) {
                    List<Integer> limitList = (List<Integer>) BeanUtils.getObjFieldPropertyVal(mediaCfg, cfg.getKey() + "Cfg");
                    int val = Integer.parseInt(subText);
                    if (val >= limitList.get(0) && val <= limitList.get(1)) {
                        matchedContent(cursor, cfg, media, subText);
                        return true;
                    }
                }
                cursor.setStart(i + 1);
            }
        }
        return false;
    }

    private static boolean checkIsSplitPoint(String t, MediaConfig mediaCfg) {
        for (String sp : mediaCfg.getSplitCfg()) {
            if (t.equals(sp)) {
                return true;
            }
        }
        return false;
    }

    private static boolean recursionMatchTxt(Cursor cursor, List<String> cfgList, String text, CfgItem cfg, String[] array, MediaModel media, MediaConfig mediaCfg) {
        if (cursor.getStart() >= text.length()) {
            return false;
        }
        boolean result = false;
        for (String txt : cfgList) {
            if (cursor.getStart() + txt.length() > text.length()) {
                continue;
            }
            //截取 和 该匹配字符一样长度的 内容
            String subText = text.substring(cursor.getStart(), cursor.getStart() + txt.length());
            if (compareContent(subText, txt)) {
                //判断 该内容后是 截取字符串
                if (array.length > cursor.getStart() + txt.length()) {
                    if (!checkIsSplitPoint(array[cursor.getStart() + txt.length()], mediaCfg)) {
                        continue;
                    }
                }
                matchedContent(cursor, cfg, media, subText);
                result = true;
                break;
            }
        }
        if (result) {
            return true;
        }
        //遍历到下一个 标识符
        boolean next = false;
        for (int i = cursor.getStart(); i < array.length; i++) {
            String t = array[i];
            if (checkIsSplitPoint(t, mediaCfg)) {
                cursor.setStart(i + 1);
                next = true;
                break;
            }
        }
        if (next) {
            return recursionMatchTxt(cursor, cfgList, text, cfg, array, media, mediaCfg);
        }
        return false;
    }

    private static boolean compareContent(String s1, String s2) {
        s1 = replaceCompareChar(s1.toLowerCase());
        s2 = replaceCompareChar(s2.toLowerCase());
        return s1.equals(s2);
    }

    private static String replaceCompareChar(String str) {
        return str.replaceAll("\\.", " ").replaceAll("-", " ");
    }

    /**
     * 遍历游标
     */
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Cursor {
        private int start;
        private int minMatch;//最小匹配索引
        private List<CursorItem> matchLst;//匹配到的 序号 集合  起始序号, 匹配内容长度
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CursorItem {
        private Integer index;
        private Integer length;
    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TvRegexCfg {
        private String regex;//正则表达式
        private int sIdx;//第几季序号
        private int eIdx;//第几集序号
    }

    /**
     * 替换内容配置
     */
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ReplaceCfg {

        private String resource;//原内容

        private String target;//替换内容

        /**
         * 替换类型 (1: 直接文本替换, 2: 首尾匹配替换, 3: 正则匹配替换)
         */
        private Integer type;
    }

    private static String parseMediaFormat(String text, MediaModel media) {
        int last = text.lastIndexOf(".");
        if (last < 0) {
            return null;
        }
        String format = text.substring(last + 1);
        media.setFormat(format);
        return text.substring(0, last);
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CfgItem {
        private String key;
        private String type;//匹配类型 text: 文本直接匹配, contain: 包含文本, regex: 正则表达式匹配, number: 数字匹配
        private String value;//对应配置内容 为空 取 总的配置项 MediaConfig, 不为空 则取该值, 如 正则表达式
    }


    @Data
    public static class MediaModel {

        private String type;//资源类型 (movie, tv)

        private String season;//第几季

        private String episode;//第几集

        private String originalName;

        private String name;//资源名

        private String engName;//资源名2

        private String year;//年份

        private String editVer;//剪辑版本

        private String ratio;//分辨率

        private String source;//来源

        private String encode;//资源编码

        private String caption;//字幕语言

        private String format;//资源格式

        private String audio;//音频格式

        private String sound;//声道

    }

}
