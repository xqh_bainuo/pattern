package org.xqh.test.ugreen;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.extension.api.R;
import com.google.common.collect.Maps;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.compress.utils.Lists;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * @ClassName GeoResolveTest
 * @Description 经纬度解析
 * @Author xuqianghui
 * @Date 2023/11/14 15:45
 * @Version 1.0
 */
public class GeoResolveTest {

    public static void main(String[] args) {

        String lat = "40.70390";
        String lng = "-73.98670";
//        GpsVo vo = requestGeoApiAndResolve(buildRadarParams(), lat, lng);
//        System.out.println(JSON.toJSONString(vo));
//        Random rand = new Random();
//        for(int i = 0 ;i  < 100; i ++){
//            System.out.println(rand.nextInt(10));
//        }

        Map<String, ResolveDto> cfgMap = Maps.newHashMap();
        cfgMap.put("google", buildGoogleParams());
        cfgMap.put("baidu", buildBaiduParams());
        cfgMap.put("locationIp", buildLocationIpParams());
        cfgMap.put("arcgis", buildArcgisParams());
        cfgMap.put("radar", buildRadarParams());
        String json = JSON.toJSONString(cfgMap);
        System.out.println(json);



//        String text = "$.results[0].address_components{array#types#administrative_area_level_1#long_name}";
//        int idx = text.indexOf("{array");
//        String[] list = text.substring(idx).split("#");
//        System.out.println(list[3].substring(0, list[3].length() - 1));
//        System.out.println();
//        System.out.println(text.substring(0, idx));
        /**
         * 40.70390,-73.98670
         * -117.20744088954731%2C34.0375903447169
         * 37.42159,-122.0837
         * 48.8584, 2.2945
         *
         * https://us1.locationiq.com/v1/reverse?key=pk.4cfc0d244ecba67ed0d542bc4d5ee56e&lat=37.42159&lon=-122.0837&format=json
         */
    }

    private static ResolveDto buildRadarParams() {
        String apiUrl = "https://api.radar.io/v1/geocode/reverse?coordinates=%s";
        ResolveDto bdDto = ResolveDto.builder()
                .codeKey("$.meta.code")
                .apiUrl(apiUrl)
                .successCode("200")
                .errKey("$.meta.code")
                .errVal("400")
                .headMap(new HashMap<String, String>() {{
                    put("Authorization", "prj_test_sk_4c0255174269643197dc650e8eaadefefe9655ed");
                }})
                .formattedAddrKey("$.addresses[0].formattedAddress")
                .countryKey("$.addresses[0].country")
                .countryCodeKey("$.addresses[0].countryCode")
                .provinceKey("$.addresses[0].state")
                .cityKey("$.addresses[0].county")
//                .cityCodeKey("$.addresses[0].")
                .districtKey("$.addresses[0].city")
                .townKey("$.addresses[0].neighborhood")
                .streetKey("$.addresses[0].street")
                .streetNumKey("$.addresses[0].number")
                .adcodeKey("$.addresses[0].postalCode")
                .build();

        return bdDto;
    }

    private static ResolveDto buildLocationIpParams() {
        // us1, eur1
        String apiUrl = "https://us1.locationiq.com/v1/reverse?key=pk.4cfc0d244ecba67ed0d542bc4d5ee56e&lat=%s&lon=%s&format=json";
        ResolveDto bdDto = ResolveDto.builder()
                .codeKey("$.address")
                .apiUrl(apiUrl)
                .successCode("obj_not_null")
                .errKey("$.error")
                .errVal("Invalid Request")
                .formattedAddrKey("$.display_name")
                .countryKey("$.address.country")
                .countryCodeKey("$.address.country_code")
                .provinceKey("$.address.state")
                .cityKey("$.address.county")
                .cityCodeKey("$.address.borough")
                .districtKey("$.address.city")
                .townKey("$.address.neighborhood")
                .streetKey("$.address.road")
                .streetNumKey("$.address.house_number")
                .adcodeKey("$.address.postcode")
                .latLngSplit(true) //经纬度参数 是拆分的.
                .build();

        return bdDto;
    }

    private static ResolveDto buildGoogleParams() {
        String apiUrl = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s&key=AIzaSyAF8CCwvxLx8PHRqvEFJR8Iy9FQK5cSGwU";
        ResolveDto bdDto = ResolveDto.builder()
                .codeKey("$.status")
                .apiUrl(apiUrl)
                .successCode("OK")
                .errKey("$.status")
                .errVal("INVALID_REQUEST")
                .formattedAddrKey("$.results[0].formatted_address")
                .countryKey("$.results[0].address_components{array#types#country#long_name}")
                .countryCodeKey("$.results[0].address_components{array#types#country#long_name}")
                .provinceKey("$.results[0].address_components{array#types#administrative_area_level_1#long_name}")
                .cityKey("$.results[0].address_components{array#types#administrative_area_level_2#long_name}")
//                .cityCodeKey("$.results[0].address_components{array#types#country#long_name}")
                .districtKey("$.results[0].address_components{array#types#locality@sublocality#long_name}") // types包含 locality  或者  sublocality_level_1
                .townKey("$.results[0].address_components{array#types#neighborhood#long_name}")
                .streetKey("$.results[0].address_components{array#types#route#long_name}")
                .streetNumKey("$.results[0].address_components{array#types#street_number#long_name}")
                .adcodeKey("$.results[0].address_components{array#types#postal_code#long_name}")
                .build();
        return bdDto;
    }


    private static ResolveDto buildArcgisParams() {
        String apiUrl = "https://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer/reverseGeocode?f=pjson&featureTypes=&location=%s";
        ResolveDto bdDto = ResolveDto.builder()
                .codeKey("$.address")
                .apiUrl(apiUrl)
                .successCode("obj_not_null")
                .errKey("$.address.CntryName")
                .errVal("")
                .formattedAddrKey("$.address.LongLabel")
                .countryKey("$.address.CntryName")
                .countryCodeKey("$.address.CountryCode")
                .provinceKey("$.address.Region")
                .cityKey("$.address.Subregion")
                .cityCodeKey("$.address.cityCode")
                .districtKey("$.address.City")
                .townKey("$.address.Neighborhood")
                .streetKey("$.address.Address")
                .streetNumKey("$.address.AddNum")
                .adcodeKey("$.address.Postal")
                .lngLat(true) //经纬度参数 顺序
                .build();
        return bdDto;
    }
    /**
     * 百度
     *
     * @return
     */
    private static ResolveDto buildBaiduParams() {
        String apiUrl = "https://api.map.baidu.com/reverse_geocoding/v3/?ak=nhW1IcO7aCccz2Z1Fb2I5nwvTq61wH2A&output=json&coordtype=wgs84ll&location=%s";
        ResolveDto bdDto = ResolveDto.builder()
                .codeKey("$.status")
                .apiUrl(apiUrl)
                .successCode("0")
                .errKey("$.result.addressComponent.country_code")
                .errVal("-1")
                .formattedAddrKey("$.result.formatted_address")
                .countryKey("$.result.addressComponent.country")
                .countryCodeKey("$.result.addressComponent.country_code")
                .provinceKey("$.result.addressComponent.province")
                .cityKey("$.result.addressComponent.city")
                .cityCodeKey("$.result.cityCode")
                .districtKey("$.result.addressComponent.district")
                .streetKey("$.result.addressComponent.street")
                .streetNumKey("$.result.addressComponent.street_number")
                .townKey("$.result.addressComponent.town")
                .adcodeKey("$.result.addressComponent.adcode")
                .build();
        return bdDto;
    }

    public static GpsVo requestGeoApiAndResolve(ResolveDto dto, String lat, String lng) {
        GpsVo vo = new GpsVo();
        String apiUrl = dto.getApiUrl();
        if(dto.isLatLngSplit()){//经纬度分开两个参数
            apiUrl = String.format(apiUrl, lat, lng);
        }else {
            String location = String.format("%s,%s", lat, lng);
            if(dto.isLngLat()){
                location = String.format("%s,%s", lng, lat);
            }
            apiUrl = String.format(apiUrl, location);
        }

        HttpRequest httpRequest = HttpUtil.createGet(apiUrl);
        if (!CollectionUtils.isEmpty(dto.getHeadMap())) {
            for (Map.Entry<String, String> entry : dto.getHeadMap().entrySet()) {
                httpRequest.header(entry.getKey(), entry.getValue());
            }
        }
        try (HttpResponse httpResponse = httpRequest.execute()) {
            System.out.println(httpResponse.body());
            if (httpResponse.getStatus() == 200) {
                String result = httpResponse.body();
                //判断 接口响应是否正常
                if (checkResponseIsSuccess(result, dto)) {
                    vo.setCountry(handleCountryName(transToString(result, dto.getCountryKey())));
                    vo.setProvince(transToString(result, dto.getProvinceKey()));
                    vo.setCity(transToString(result, dto.getCityKey()));
                    vo.setCityCode(transToString(result, dto.getCityCodeKey()));
                    vo.setDistrict(transToString(result, dto.getDistrictKey()));
                    vo.setAddrDetail(transToString(result, dto.getFormattedAddrKey()));
                    vo.setStreet(transToString(result, dto.getStreetKey()));
                    vo.setStreetNumber(transToString(result, dto.getStreetNumKey()));
                    vo.setTown(transToString(result, dto.getTownKey()));
                    vo.setAdCode(transToString(result, dto.getAdcodeKey()));
                }else {
                    //判断该请求的经纬度 是否 合法
                    if(checkResp4LatLngIsValid(result, dto)){
                        vo.setCountry("-1");
                    }
                }
            }
        }
        return vo;
    }

    public static String handleCountryName(String country){
        if(StringUtils.isEmpty(country)){
            return country;
        }
        if(country.toLowerCase().contains("united states")){
            return "United States";
        }
        return country;
    }

    public static boolean checkResp4LatLngIsValid(String result, ResolveDto dto){
        String errVal = transToString(result, dto.getErrKey());
        return Objects.equals(errVal, dto.getErrVal());
    }

    public static boolean checkResponseIsSuccess(String result, ResolveDto dto){
        if("obj_not_null".equalsIgnoreCase(dto.getSuccessCode())){
            Object obj = JSONPath.read(result, dto.getCodeKey());
            return Objects.nonNull(obj);
        }
        String apiCode = transToString(result, dto.getCodeKey());
        return Objects.equals(apiCode, dto.getSuccessCode());
    }

    public static String transToString(String json, String path) {
        if (StringUtils.isEmpty(path)) {
            return null;
        }
        if (path.contains("{array")) {//google数据独立处理 包含集合
            return handleGoogleTranslate(json, path);
        }
        Object obj = JSONPath.read(json, path);
        if (Objects.nonNull(obj)) {
            return obj.toString();
        }
        return null;
    }

    public static String handleGoogleTranslate(String json, String path) {
        int idx = path.indexOf("{array");
        String regex = path.substring(idx);
        String itemPath = path.substring(0, idx);
        String[] items = regex.split("#");
        String typesKey = items[1];
        String[] containsKeys = items[2].split("@");
        String putKey = items[3].substring(0, items[3].length() - 1);
        JSONArray array = JSON.parseArray(JSONPath.read(json, itemPath).toString());
        for (Object obj : array) {
            JSONObject jsonObj = (JSONObject) obj;
            List<String> types = JSON.parseArray(jsonObj.getString(typesKey), String.class);
            for(String ckey:containsKeys){
                if (types.contains(ckey)) {
                    return jsonObj.getString(putKey);
                }
            }
        }
        return null;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class GpsVo {

        /**
         * x坐标
         */
        @ApiModelProperty(value = "经度")
        private String gpsX;

        /**
         * y坐标
         */
        @ApiModelProperty(value = "维度")
        private String gpsY;

        /**
         * 国家
         */
        @ApiModelProperty(value = "国家")
        private String country;

        /**
         * 省份
         */
        @ApiModelProperty(value = "省份")
        private String province;

        /**
         * 城市
         */
        @ApiModelProperty(value = "市")
        private String city;

        /**
         * 区县
         */
        @ApiModelProperty(value = "地区")
        private String district;

        /**
         * 乡镇
         */
        @ApiModelProperty(value = "镇")
        private String town;

        /**
         * 街道
         */
        @ApiModelProperty(value = "街道")
        private String street;

        /**
         *
         */
        @ApiModelProperty(value = "门牌号")
        private String streetNumber;

        /**
         *
         */
        @ApiModelProperty(value = "城市代码")
        private String cityCode;

        /**
         *
         */
        @ApiModelProperty(value = "行政区域代码")
        private String adCode;

        /**
         * json串
         */
        @JSONField(serialize = false)
        private String ext;

        @ApiModelProperty(value = "详细地址")
        private String addrDetail;

    }

    /**
     * 经纬度解析
     */
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ResolveDto {

        private String apiUrl;

        private String httpMethod;

        private String errKey; //错误请求响应标识

        private String errVal; // 错误请求响应内容 用于错误的 经纬度信息不重复请求

        private Map<String, String> headMap;

        private String codeKey;// api响应code

        private String successCode;// api响应成功code 值

        private String formattedAddrKey; // 详细地址 key

        private String countryKey; //国家 key

        private String countryCodeKey; // 国家编码 key

        private String provinceKey; // 省份 key

        private String cityKey;// 市 key

        private String cityCodeKey;// 市编码 key

        private String districtKey; // 区级 key

        private String streetKey;// 街道 key

        private String streetNumKey;// 街道号 key

        private String townKey;// 镇 key

        private String adcodeKey;//行政区域编码

        private boolean lngLat;//顺序是否是 "维度,精度"

        private boolean latLngSplit;//经纬度参数拆分

    }

    @Data
    public static class UserExpireCfg {

        private String username;//用户名

        private int expire;//超时时间 分钟
    }

    public static void main2222(String[] args) {
        UserExpireCfg cfg = new UserExpireCfg();
        cfg.setUsername("admin");
        cfg.setExpire(100);
        UserExpireCfg cfg2 = new UserExpireCfg();
        cfg2.setUsername("admin2");
        cfg2.setExpire(120);
        List<UserExpireCfg> list = Lists.newArrayList();
        list.add(cfg2);
        list.add(cfg);

        String json = JSON.toJSONString(cfg);
        JSONObject obj = JSON.parseObject(json);
        System.out.println(obj.getString("expire"));

//        JSONPath.read(nluRet, "$.code").toString()

        System.out.println(JSON.toJSONString(list));
    }
}
