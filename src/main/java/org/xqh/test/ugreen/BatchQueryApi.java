package org.xqh.test.ugreen;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.xqh.utils.ThreadPoolUtils;
import org.xqh.utils.http.FxHttpClientUtils;

import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @ClassName BatchQueryApi
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/3/12 12:25
 * @Version 1.0
 */
@Slf4j
public class BatchQueryApi {

//    public final static String reqUrl = "https://josen.cn1.ug.link/ugreen/v1/verify/is_login?token=F272CC5564EB4E59B7C8696444F54016";
//    public final static String reqUrl = "https://josen.ug.link/ugreen/v1/verify/is_login?token=F272CC5564EB4E59B7C8696444F54016";
    public final static String reqUrl = "https://john.cn-test.ug.link:8088/ugreen/v1/user/list?token=752B7C69-FABC-46A5-A7F6-922EFE5A2F86";

    public static List<Object> respList = Lists.newArrayList();

    public static void main(String[] args) throws InterruptedException {
        String json = "{\"lanv6\":[\"syn6-wauchpgagbof5hv4osgp2qsmqm72aaaaaaaaaaaaqrgl774zmtya.wanmin.direct.quickconnect.cn\"],\"host\":\"WANMIN.direct.quickconnect.cn\",\"lan\":[\"192-168-0-2.WANMIN.direct.quickconnect.cn\"],\"hole_punch\":\"127-0-0-1.WANMIN.direct.quickconnect.cn\"}";
//        String json = "{\n" +
//                "\"ext\":\"{\\\"netInfo\\\":{\\\"smartdns\\\":{\\\"lanv6\\\":[\\\"syn6-wauchpgagbof5hv4osgp2qsmqm72aaaaaaaaaaaaqrgl774zmtya.wanmin.direct.quickconnect.cn\\\"],\\\"host\\\":\\\"WANMIN.direct.quickconnect.cn\\\",\\\"lan\\\":[\\\"192-168-0-2.WANMIN.direct.quickconnect.cn\\\"],\\\"hole_punch\\\":\\\"127-0-0-1.WANMIN.direct.quickconnect.cn\\\"},\\\"ddns\\\":\\\"www.test.site\\\"}}\"\n" +
//                "}";

        JSONObject obj = JSON.parseObject(json);
        JSONObject test = new JSONObject();
        JSONObject last = new JSONObject();
        test.put("ddns", "www.test.site");
        test.put("smartdns", obj);
        last.put("ext", test);
        String tttt = "{\"ext\":{\"smartdns\":{\"lanv6\":[\"syn6-wauchpgagbof5hv4osgp2qsmqm72aaaaaaaaaaaaqrgl774zmtya.wanmin.direct.quickconnect.cn\"],\"host\":\"WANMIN.direct.quickconnect.cn\",\"lan\":[\"192-168-0-2.WANMIN.direct.quickconnect.cn\"],\"hole_punch\":\"127-0-0-1.WANMIN.direct.quickconnect.cn\"},\"ddns\":\"www.test.site\"}}";
        System.out.println(last.toJSONString());
//        int size = 100;
//        CountDownLatch latch = new CountDownLatch(size);
//        for(int i = 0; i < size; i ++){
//            ExecuteThread t = new ExecuteThread(latch);
//            ThreadPoolUtils.execute(t);
//        }
//
//        latch.await();
//        System.out.println("==================>");
//        System.out.println(JSON.toJSONString(respList));
    }

    public static class ExecuteThread implements Runnable{

        private CountDownLatch latch;

        public ExecuteThread(CountDownLatch latch){
            this.latch = latch;
        }

        @Override
        public void run() {
            reqIsLoginApi();
            latch.countDown();
        }
    }
    public static void reqIsLoginApi(){
        HttpConfig httpConfig = FxHttpClientUtils.getHttpConfig(reqUrl);
        try {
            HttpResult httpRet = HttpClientUtil.sendAndGetResp(httpConfig);
            if(httpRet.getStatusCode() == 200){
                JSONObject json = JSON.parseObject(httpRet.getResult());
                if(json.getInteger("code") != 200){
                    respList.add(httpRet.getResult());
                }
            }else {
                respList.add(String.format("http status: %s, result: %s", httpRet.getStatusCode(), httpRet.getResult()));
            }
        } catch (Exception e) {
           respList.add(e.getMessage());
           log.error("", e);
        }
    }

}
