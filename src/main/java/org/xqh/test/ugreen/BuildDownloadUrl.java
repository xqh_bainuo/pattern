package org.xqh.test.ugreen;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Map.Entry;

/**
 * @ClassName BuildDownloadUrl
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/1/13 18:11
 * @Version 1.0
 */
public class BuildDownloadUrl {

    public final static String bash_url = "curl ";

//    public final static String server_url = "http://127.0.0.1:43254/v1/link/download?key=DH-tjO700002006aPpm-001103&api_token=ZmZmOTIwNWFjODg0MjdkYTc5MTNkNTRiMWNmYThjMGY1NDRhY2IxNg==&file_id=1&path=&uuid=53f03749-a98d-487d-99ad-6d6d18972dc8&play=1";
    public final static String server_url = "http://127.0.0.1:43254/v1/link/download?api_token=ZmZmOTIwNWFjODg0MjdkYTc5MTNkNTRiMWNmYThjMGY1NDRhY2IxNg==&requestUrl=/v1/link/download&key=DH-tjO700002006aPpm-001103&file_id=1&uuid=&path=&token=OTAyNDMyMDE5NDFFQUNDM0Q2RjAzM0E4MTQzNTk0RTI2QzU4RTUyM0M1RTg5MTUwNDYwQjQ4Mzk4QzA1RUQ0OQ%3D%3D&%24time=1641972857263&play=1";

    public static void main(String[] args) {
        String json = "{\n" +
                "    \"sec-fetch-mode\":\"no-cors\",\n" +
                "    \"referer\":\"https://test.ugreen.cloud/v1/opt/3.mp4?requestUrl=/v1/link/download&key=DH-tjO700002006aPpm-001103&file_id=1&uuid=&path=&token=OTAyNDMyMDE5NDFFQUNDM0Q2RjAzM0E4MTQzNTk0RTI2QzU4RTUyM0M1RTg5MTUwNDYwQjQ4Mzk4QzA1RUQ0OQ%3D%3D&%24time=1641972857368&play=1\",\n" +
                "    \"sec-fetch-site\":\"same-origin\",\n" +
                "    \"accept-language\":\"zh-CN,zh;q=0.9\",\n" +
                "    \"range\":\"bytes=0-\",\n" +
                "    \"pragma\":\"no-cache\",\n" +
                "    \"accept\":\"*/*\",\n" +
                "    \"sec-ch-ua\":\"\\\" Not A;Brand\\\";v=\\\"99\\\", \\\"Chromium\\\";v=\\\"96\\\", \\\"Google Chrome\\\";v=\\\"96\\\"\",\n" +
                "    \"sec-ch-ua-mobile\":\"?0\",\n" +
                "    \"sec-ch-ua-platform\":\"\\\"Windows\\\"\",\n" +
                "    \"host\":\"test.ugreen.cloud\",\n" +
                "    \"connection\":\"keep-alive\",\n" +
                "    \"cache-control\":\"no-cache\",\n" +
                "    \"accept-encoding\":\"identity;q=1, *;q=0\",\n" +
                "    \"user-agent\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36\",\n" +
                "    \"sec-fetch-dest\":\"video\"\n" +
                "}";
         String txt = "Accept: */*\n" +
                "Accept-Encoding: identity;q=1, *;q=0\n" +
                "Accept-Language: zh-CN,zh;q=0.9\n" +
                "Cache-Control: no-cache\n" +
                "Connection: keep-alive\n" +
//                "Host: 127.0.0.1:43254\n" +
                "Pragma: no-cache\n" +
                "Range: bytes=0-\n" +
//                "Referer: http://127.0.0.1:43254/v1/link/download?key=DH-tjO700002006aPpm-001103&api_token=ZmZmOTIwNWFjODg0MjdkYTc5MTNkNTRiMWNmYThjMGY1NDRhY2IxNg==&file_id=1&path=&uuid=53f03749-a98d-487d-99ad-6d6d18972dc8&play=1\n" +
                "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36";
        String header = buildReqHeaderFromTxt(txt);
//        String header = buildReqHeaderParams(json);
        System.out.println(parseUrlParams(server_url));
    }

    public static String buildReqHeaderParams(String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        StringBuffer sb = new StringBuffer(bash_url);
        for (Entry<String, Object> entry : jsonObject.entrySet()) {
            sb.append("-H ").append("\"").append(entry.getKey()).append(":").append(entry.getValue()).append("\" ");
        }
        return sb.toString();
    }

    public static String buildReqHeaderFromTxt(String txt) {
        StringBuffer sb = new StringBuffer(bash_url);
        String[] array = txt.split("\n");
        for (String str : array) {
            int idx = str.indexOf(":");
            String key = str.substring(0, idx);
            String value = str.substring(idx + 1);
            sb.append("-H ").append("\"").append(key).append(":").append(value).append("\" ");
        }
        return sb.toString();

    }

    public static Map<String, String> parseUrlParams(String url){
        if(url.contains("?")){
            url = url.substring(url.indexOf("?") + 1);
        }
        String[] array = url.split("&");
        Map<String, String> map = Maps.newHashMap();
        for(String str:array){
            String[] item = str.split("=");
            if(item.length == 1){
                map.put(item[0], "");
            }else {
                map.put(item[0], item[1]);
            }
        }
        return map;
    }
}
