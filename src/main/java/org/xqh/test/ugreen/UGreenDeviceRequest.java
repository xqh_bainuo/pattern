package org.xqh.test.ugreen;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName UGreenDeviceRequest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/11/16 14:50
 * @Version 1.0
 */
public class UGreenDeviceRequest {

    public final static String api = "https://api-us.ugnas.com/api/system/v2/sa/resolve/geo";

    public final static String device_sn = "UG670UU372300039";

    public final static String device_secret = "9ayo0gRap87ZaywRSZiZxd0pcRmGYGXMOTG0roRmVmsU9EYwq0yQzsH3WNPMPhKx";

    public static void main(String[] args) {
        GeoQueryDTO dto = GeoQueryDTO.builder()
                .gpsX("34.0375903447169")
                .gpsY("-117.20744088954731")
                .build();
        UGreenApiCallUtils.deviceCallCloudApi(dto, api, device_secret, device_sn);
    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class GeoQueryDTO {
        private String gpsX;
        private String gpsY;
    }
}
