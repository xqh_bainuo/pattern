package org.xqh.test.ugreen;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.Header;
import org.springframework.util.StringUtils;
import org.xqh.utils.encrypt.EncryptUtils;
import org.xqh.utils.encrypt.RSAUtils;
import org.xqh.utils.file.ReadTxtFileUtils;
import org.xqh.utils.http.FxHttpClientUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @ClassName UGreenInstorageTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/4/18 16:43
 * @Version 1.0
 */
@Slf4j
public class UGreenInStorageTest {

//    public static String api = "https://ugnas.ugreengroup.com/prod-api/device/basic/register";
    public static String api = "https://proadmin.ugnas.com/prod-api/device/basic/register";

    public static String token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjFhYWExYzI2LWYzMzQtNGRmZi04NmM4LTNlYzFhYzE2MTcyMCJ9.8j1DhertInqx2zylMJDM3ta3fGkImI1sUVoG7NxdopcN5NXfQVF2UiHZDOXAjPkUgFm1ELY_sshir1yUdj5i3w";

    public static List<String> failLst = new ArrayList<>();


    // DX4600 Pro  13
    // DX4600+  12

    //661 -DX4600 PRO
    //660 -DX4600+


    public static void main222(String[] args) throws HttpProcessException {
//        batchInsert("D:\\work\\program\\入库设备\\660-DX4600+.txt", 1);
//        batchInsert("D:\\work\\program\\入库设备\\661-DX4600 PRO.txt", 2);
//        batchInsert("D:\\work\\program\\入库设备\\231213\\35259.txt", 2);
//        batchInsert("D:\\work\\program\\入库设备\\231213\\35258美国.txt", 2);
//        batchInsert("D:\\work\\program\\入库设备\\231213\\35258德国.txt", 2);
        batchInsert("D:\\work\\program\\入库设备\\20240125\\入库.txt", 2);

        System.out.println("失败设备号集合");
        System.out.println(JSON.toJSONString(failLst));
    }

    public static void batchInsert(String filename, int model){
        List<String> lst1 = ReadTxtFileUtils.readTxt(new File(filename));
        for(String sn:lst1){
            JSONObject reqJson = reqJson4600Pro(sn);
            if(model == 1){
                reqJson = reqJson4600X(sn);
            }
            try {
                requestSingle(reqJson.toJSONString(), sn);
            } catch (HttpProcessException e) {
                log.error("", e);
            }
        }
    }

    public static void requestSingle(String json, String sn) throws HttpProcessException {
        HttpConfig config = FxHttpClientUtils.getHttpConfig(api);
        config.method(HttpMethods.POST);
        config.json(json);
        config.headers(getReqHeaders());
        HttpResult result = HttpClientUtil.sendAndGetResp(config);
        if(Objects.nonNull(result) && StringUtils.hasText(result.getResult())){
            JSONObject resp = JSON.parseObject(result.getResult());
            if(resp.getInteger("code") != 200){
                failLst.add(sn);
            }
        }
    }

    public static Header[] getReqHeaders(){
        return HttpHeader.custom()
                .other("Authorization", token)
                .build();
    }

    public static JSONObject reqJson4600Pro(String deviceSn) {
        JSONObject json = new JSONObject();
        json.put("model", "16");
        json.put("modelName", "DX4700");
        json.put("modelOption", "16|DX4700");
        json.put("sn", deviceSn);
        return json;
    }

    public static JSONObject reqJson4600X(String deviceSn) {
        JSONObject json = new JSONObject();
        json.put("model", "17");
        json.put("modelName", "DX4700+");
        json.put("modelOption", "17|DX4700+");
        json.put("sn", deviceSn);
        return json;
    }


    public static void main(String[] args) {
        try {
            System.out.println(versionNumber("1.00.0011"));    //your test cases
            System.out.println(versionNumber("1.0.0.0023"));   //your test cases
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Integer versionNumber(String version) throws Exception {

        String[] parts = version.split("\\.");

        if (parts.length == 3) { //Compatible with old version 1.00.0000
            parts[1] = String.format("%02d", Integer.parseInt(parts[1]));
            parts[2] = String.format("%04d", Integer.parseInt(parts[2]));

            if (parts[1].length() > 2 || parts[2].length() > 4) {
            }
            version = String.join("", parts);
            return Integer.parseInt(version);
        }

        if (parts.length == 4) { //New version of the algorithm 1.0.0.0000
            parts[1] = String.format("%02d", Integer.parseInt(parts[1]));
            parts[2] = String.format("%02d", Integer.parseInt(parts[2]));
            parts[3] = String.format("%04d", Integer.parseInt(parts[3]));
            if (parts[1].length() > 2 || parts[2].length() > 2 || parts[3].length() > 4) {
            }
            version = String.join("", parts);
            return Integer.parseInt(version);
        }

        return null;
    }

}
