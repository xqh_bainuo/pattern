package org.xqh.test.ugreen;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName UGreenLogTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/2/26 13:57
 * @Version 1.0r
 */
@Slf4j
public class UGreenLogTest {

    public static void main(String[] args) {
        readFromFileByLine("D:\\work\\secureCRT\\logs\\info.log.2024-02-26.1");
    }

    /**
     * 写入数据 到txt
     *
     * @param list
     * @param txtFile
     */
    public static void readFromFileByLine(String txtFile) {
        String regex = "api: /system/v2/sa/help/check, sn: ([0-9A-Z]{16})";
        Pattern pattern = Pattern.compile(regex);
        Map<String, AtomicInteger> countMap = new HashMap<>();

        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(txtFile));
            String line = reader.readLine();
            while (line != null) {
                // read next line
                String sn = getDeviceSn(pattern, line);
                if (StringUtils.hasText(sn)) {
                    if (countMap.containsKey(sn)) {
                        countMap.get(sn).incrementAndGet();
                    } else {
                        countMap.put(sn, new AtomicInteger(1));
                    }
                }
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(JSON.toJSONString(countMap));
    }

    public static String getDeviceSn(Pattern pattern, String text) {
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
