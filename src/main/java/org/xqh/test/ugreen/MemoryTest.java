package org.xqh.test.ugreen;

import com.alibaba.fastjson.JSON;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Sets;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openjdk.jol.info.ClassLayout;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName MemoryTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/11/1 10:43
 * @Version 1.0
 */
@Slf4j
public class MemoryTest {

    private LoadingCache<AccessSecretReq, AccessSecretRetDTO> secretCache = CacheBuilder.newBuilder().maximumSize(20000)
            .refreshAfterWrite(1, TimeUnit.MINUTES)// 定时刷新数据, 一个线程去获取最新值, 其他可以先获取旧的缓存值 返回.
            .build(
                    new CacheLoader<AccessSecretReq, AccessSecretRetDTO>() {
                        @Override
                        public AccessSecretRetDTO load(AccessSecretReq req) throws Exception {
                            return loadAccessSecret(req);
                        }
                    }
            );

    public static void main(String[] args) {
        MemoryTest test = new MemoryTest();
        AccessSecretReq req = new AccessSecretReq();
        ApiSignCheckDTO checkDTO = new ApiSignCheckDTO();
        AccessSecretRetDTO retDTO = new AccessSecretRetDTO();
        ClassLayout classLayout = ClassLayout.parseInstance(retDTO);
        System.out.println(classLayout.toPrintable());
    }


    public  AccessSecretRetDTO loadAccessSecret(AccessSecretReq req){
        log.info("load-access-secret start. {}", JSON.toJSONString(req));
        AccessSecretRetDTO result = AccessSecretRetDTO.builder()
                .keySet(Sets.newHashSet("I8Na3m57qcHVcIUcFbSW6nyQ0WYpCxyXfigFsGvV7tjuKb5IVOo1gAmPPlRS5OHA"))
                .build();
        return result;
    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    @ApiModel(value = "秘钥响应结果")
    public static class AccessSecretRetDTO {

        @ApiModelProperty(value = "秘钥集合")
        private Set<String> keySet;

        @ApiModelProperty(value = "是否需要更新(true 不走本地缓存)")
        private boolean needUpd;
    }

    @ApiModel(value = "查询秘钥请求参数")
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class AccessSecretReq {

        private ApiSignCheckDTO accessDTO;

        private boolean defSecret;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ApiSignCheckDTO {

        @ApiModelProperty(value = "接入类型 accessType")
        private String at;

        @ApiModelProperty(value = "接入标识 accessKey")
        private String ak;

        @ApiModelProperty(value = "接口签名")
        private String sign;

        @ApiModelProperty(value = "时间戳")
        private String ts;

        @ApiModelProperty(value = "设备mac(仅限设备端请求)")
        private String mac;

        @ApiModelProperty(value = "签名算法(默认MD5)")
        private String alg;

        /**
         * 以下属性 为兼容旧版本签名逻辑
         */
        @ApiModelProperty(value = "时间戳v1版本签名用")
        private String timestamp;

        @ApiModelProperty(value = "设备序列号")
        private String sn;

    }


public static class ObjA {
}
}
