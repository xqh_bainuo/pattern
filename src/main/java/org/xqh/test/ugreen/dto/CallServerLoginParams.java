package org.xqh.test.ugreen.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName CallServerLoginParams
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/2/10 14:24
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "调用server端登录接口参数")
public class CallServerLoginParams {

    private int platform;
    private String sign;
    private CallServerUserDTO user_basic;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CallServerUserDTO{
        private Long ugreen_no;
        private String phone_no;
        private String nic_name;
        private String email;
        private Integer sex;
        private String birthday;
        private Integer version;
    }

}
