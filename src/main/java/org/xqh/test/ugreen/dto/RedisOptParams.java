package org.xqh.test.ugreen.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClassName RedisOptParams
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/11/11 10:48
 * @Version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RedisOptParams {
    private String url;
    private int port;
    private int db;
    private boolean ssl;
    private String optKey;
    private String optType;
    private List<String> delKeys;
    private String optVal;
    private int expire;
    private String password;
}
