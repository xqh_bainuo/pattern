package org.xqh.test.ugreen.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @ClassName UserDTO
 * @Description: User返回对象Vo信息
 * @Author fuqiang
 * @Date 2019/8/14
 * @Version V1.0
 **/
@Data
@ApiModel(value = "User返回对象", description = "User返回对象")
public class UserDTO {

    /**
     * 用户uid
     */
    @ApiModelProperty(value = "用户UID")
    private Long uid;

    /**
     * 不返回字段给前端
     * 用户号码ugreenNo
     */
    @ApiModelProperty(value = "用户号码")
    private Long ugreenNo;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名")
    private String userName;

    /**
     * 密码返回给前端时需置为空
     */
    @JsonIgnore
    @ApiModelProperty(value = "用户密码")
    private String password;

    /**
     * 用户昵称
     */
    @ApiModelProperty(value = "用户昵称")
    private String nicName;

    /**
     * 用户性别
     */
    @ApiModelProperty(value = "用户性别")
    private Integer sex;

    /**
     * 用户生日
     */
    @ApiModelProperty(value = "用户生日")
    private Long birthday;

    /**
     * 区域代码
     */
    @ApiModelProperty(value = "区域代码")
    private String areaNo;

    /**
     * 手机号码
     */
    @ApiModelProperty(value = "手机号码")
    private String phoneNo;

    /**
     * 邮箱
     */
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     * 头像
     */
    @ApiModelProperty(value = "用户头像")
    private String image;

    /**
     * 个性签名
     */
    @ApiModelProperty(value = "个性签名")
    private String userSay;

    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private Integer status;

    @JsonIgnore
    @ApiModelProperty(value = "秘钥")
    private String priPassWd;

    //特殊字段
    @JsonIgnore
    private String generateKey;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    private Long ctime;

    /**
     * 版本号
     */
    @ApiModelProperty(value = "版本号")
    private Integer versionNo;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "更新时间")
    private Long utime;

    /**
     * 是否绑定微信
     */
    @ApiModelProperty(value = "是否绑定微信")
    private boolean isWeixin;

    /**
     * 是否绑定QQ
     */
    @ApiModelProperty(value = "是否绑定QQ")
    private boolean isQQ;

    /**
     * 是否设置密码
     */
    @ApiModelProperty(value = "是否设置密码")
    private boolean isSetPwd;

}
