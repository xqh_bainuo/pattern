package org.xqh.test.ugreen.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @ClassName CallServerLoginResult
 * @Description 调用server端 登录接口 响应对象
 * @Author xuqianghui
 * @Date 2022/2/10 14:47
 * @Version 1.0
 */
@Data
@ApiModel(value = "调用server登录接口响应对象")
public class CallServerLoginResult {

    @ApiModelProperty(value = "角色")
    private int role;

    @ApiModelProperty(value = "状态")
    private int status;

    @ApiModelProperty(value = "响应接口令牌")
    private String api_token;

    @ApiModelProperty(value = "刷新令牌")
    private String refresh_token;

    private int rom_upgrade;

//    "role": 2,
//            "status": 1,
//            "api_token": "MzJhZjU3MWQ1ZGZiMDA5NTQwYWNiZGNmMWViMDI4YTkzM2NmMzdiNg==",
//            "refresh_token": "MzczMmRhNzBkNjEzOTA3Njc3Yjc1Yjg0MjU0OTVjODUyODE1MjUwNw==",
//            "rom_upgrade" : 1
}
