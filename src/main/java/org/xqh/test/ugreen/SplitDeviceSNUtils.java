package org.xqh.test.ugreen;

/**
 * @ClassName SplitDeviceSNUtils
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/6/13 15:04
 * @Version 1.0
 */
public class SplitDeviceSNUtils {

  public static void main(String[] args) {
    String txt = "DH00002006001276\n" +
            "DH00002010004045\n" +
            "DH00002010002139\n" +
            "DH00002010004929\n" +
            "DH00002006001219\n" +
            "DH00002010001338\n" +
            "DH00002010004843\n" +
            "DH00002010002308\n" +
            "HB316F4021001511\n" +
            "HB316F34210008F7\n" +
            "DH00002010004246\n" +
            "DH00002010000929\n" +
            "DH00002010003913\n" +
            "HB316F50210059CE\n" +
            "DH00002010003904\n" +
            "DH00002010002061\n" +
            "DH00002010003883\n" +
            "DH00002010000653\n" +
            "DH00002010004677\n" +
            "DH00002010004340\n" +
            "DH00002010001117\n" +
            "DH00002010002130\n" +
            "DH00002006000002\n" +
            "DH00002010004537\n" +
            "DH00002010003119\n" +
            "DH00002010003136\n" +
            "DH00002010001395\n" +
            "DH00002010004387\n" +
            "DH00002010000493\n";

    String[] array = txt.split("\n");
    StringBuilder result = new StringBuilder();
    result.append("'");
    for(String sn:array){
        result.append(sn).append("','");
    }
    System.out.println(result.toString());
  }
}
