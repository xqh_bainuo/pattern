package org.xqh.test.ugreen.devicethirdinfo;

import com.google.common.collect.Lists;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName DeviceThirdInfoSql
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/10/31 14:09
 * @Version 1.0
 */
public class DeviceThirdInfoSql {

    public static void main(String[] args) {
        List<String> newList = Lists.newArrayList();
//        String workPath = "D:\\work\\program\\项目迭代\\需求\\广联授权码迁移\\1112德国迁国内\\";
        String workPath = "D:\\work\\program\\项目迭代\\需求\\广联授权码迁移\\20250102美国迁国内\\";
//        buildTargetInsertSql(workPath + "sn.sql", workPath + "国内-insert.sql");
        List<String> list = ReadTxtFileUtils.readTxt(new File(workPath+"国内-insert.sql"));
        String regex = "'(GL[a-z0-9]{32})',";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        for(String txt : list){
            Matcher matcher = pattern.matcher(txt);
            while (matcher.find()){
                String secret = matcher.group(1);
                newList.add(secret);
            }
        }
        ReadTxtFileUtils.writeToTxt(newList, workPath + "secret.txt");
//        String fileName1 = workPath + "secret-1.txt";
//        String fileName2 = workPath + "secret-2.txt";
        String fileName3 = "D:\\work\\program\\项目迭代\\需求\\广联授权码迁移\\secret-3.txt";
        String fileName4 = "D:\\work\\program\\项目迭代\\需求\\广联授权码迁移\\secret-4.txt";
        String fileName5 = "D:\\work\\program\\项目迭代\\需求\\广联授权码迁移\\secret-5.txt";
//        List<String> list1 = newList.subList(0, 5000);
//        ReadTxtFileUtils.writeToTxt(list1, fileName1);
//
//        List<String> list2 = newList.subList(5000, 10000);
//        ReadTxtFileUtils.writeToTxt(list2, fileName2);
//
//        List<String> list3 = newList.subList(4000, 6000);
//        ReadTxtFileUtils.writeToTxt(list3, fileName3);

//        List<String> list4 = newList.subList(6000, 8000);
//        ReadTxtFileUtils.writeToTxt(list4, fileName4);
//
//        List<String> list5 = newList.subList(8000, 10000);
//        ReadTxtFileUtils.writeToTxt(list5, fileName5);
    }

    /**
     * 解析广联授权码数据
     * @param resourceSql
     * @param outSecretPath
     */
    private static void parseGuanglianSecret(String resourceSql, String outSecretPath){
        List<String> newList = Lists.newArrayList();
        List<String> list = ReadTxtFileUtils.readTxt(new File(resourceSql));
        String regex = "'(GL[a-z0-9]{32})',";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        for(String txt : list){
            Matcher matcher = pattern.matcher(txt);
            while (matcher.find()){
                String secret = matcher.group(1);
                newList.add(secret);
            }
        }
        ReadTxtFileUtils.writeToTxt(newList, outSecretPath);
    }





    private static void buildTargetInsertSql(String sqlPath, String outPath){
        List<String> newList = Lists.newArrayList();
        List<String> list = ReadTxtFileUtils.readTxt(new File(sqlPath));
        String regex = "VALUES \\(([0-9]+), NULL,";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        for(String txt : list){
            Matcher matcher = pattern.matcher(txt);
            while (matcher.find()){
                String replace = matcher.group(0);
                String id = matcher.group(1);
                String newReplace = replace.replace(id + ",", "");
                String newTxt = txt.replace(replace, newReplace).replace("<table_name>", "device_third_info").replace("`id`, ", "");
                newList.add(newTxt);
            }
        }

        ReadTxtFileUtils.writeToTxt(newList, outPath);
    }
}
