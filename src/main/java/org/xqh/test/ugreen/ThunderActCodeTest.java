package org.xqh.test.ugreen;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.google.common.collect.Lists;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.xqh.utils.CertificateUtils;
import org.xqh.utils.file.ReadTxtFileUtils;
import org.xqh.utils.http.FxHttpClientUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

/**
 * @ClassName ThunderActCodeTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/3/24 19:22
 * @Version 1.0
 */
@Slf4j
public class ThunderActCodeTest {

    public static String cloudDomain = "https://dev.ugreengroup.com/api/device/v2/sa/thunder/activate";


    public static String accessSecret = "UGN123NAS@#$";

    public static String accessType = "nas";

    public static String mac = "98:6E:E8:20:41:30";


    private static List<CallResultModel> retList = Lists.newArrayList();


    public static void main(String[] args) throws InterruptedException {
        List<String> snList = ReadTxtFileUtils.readTxt(new File("D:\\work\\program\\测试\\新建文本文档.txt"));
        CountDownLatch latch = new CountDownLatch(snList.size());
        for(String sn:snList){
            new Thread(new TestThread(sn, latch)).start();
        }
        latch.await();
        log.info("执行结束, 总任务数: {}, 失败任务数: {}, 失败任务: {}", snList.size(), retList.size(), JSON.toJSONString(retList));
    }

    public static class TestThread implements Runnable{

        private String sn;

        private CountDownLatch latch;

        public TestThread(String sn, CountDownLatch latch){
            this.sn = sn;
            this.latch = latch;
        }

        @Override
        public void run() {
            CallResultModel ret = wrapperCallCloudApi(sn);
            if(!ret.isSuccess()){
                ret.setSn(sn);
                retList.add(ret);
            }
            latch.countDown();
        }
    }

    private static Header[] buildReqHeaders(String ts, String sign, String accessKey){
        Header[] result = HttpHeader.custom()
                .other("ak", accessKey)
                .other("at", accessType)
                .other("ts", ts)
                .other("mac", mac)
                .other("sign", sign)
                .build();
        return result;
    }

    private static String getReqTimestamp(){
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    private static  <T> String buildReqSign(T params, String ts, String accessKey){
        Map<String, Object> reqMap = JSONObject.parseObject(JSON.toJSONString(params), Map.class);
        reqMap.put("ak", accessKey);
        reqMap.put("at", accessType);
        reqMap.put("mac", mac);
        reqMap.put("ts", ts);
//        String sign = CertificateUtils.createSignNew(reqMap, accessSecret);
//        return sign;
        return null;
    }

    public static  <T> void handleReqHeader(T params, HttpConfig config, String accessKey){
        String ts = getReqTimestamp();
        String sign = buildReqSign(params, ts, accessKey);
        Header[] headers = buildReqHeaders(ts, sign, accessKey);
        config.headers(headers);//设置请求头
    }

    /**
     * 判断调用结果 是否成功
     * @param httpResult
     * @return
     */
    private static CallResultModel checkCallResult(HttpResult httpResult){
        CallResultModel result = new CallResultModel();
        if(StringUtils.isNotEmpty(httpResult.getResult())){
            JSONObject retJson = JSON.parseObject(httpResult.getResult());
            if(Objects.nonNull(retJson)){
                if(retJson.containsKey("code") && retJson.getInteger("code") == 200){
                    result.setSuccess(true);
                    if(retJson.containsKey("data")){
                        result.setData(retJson.getString("data"));
                    }
                }else {
                    log.error("请求异常==> {}", httpResult.getResult());
                }
                result.setMsg(retJson.getString("msg"));
            }
        }
        return result;
    }

    @Data
    public static class CallResultModel{
        private boolean success;
        private String data;
        private String msg;
        private String sn;
    }

    /**
     * 封装调用 cloud通用逻辑
     * @return
     */
    public static  CallResultModel wrapperCallCloudApi(String accessKey){
        String reqUrl = cloudDomain;//请求路径
        HttpConfig config = FxHttpClientUtils.getHttpConfig(reqUrl);
        config.method(HttpMethods.POST);
        handleReqHeader(new HashMap<>(), config, accessKey);
        try {
            HttpResult httpResult = HttpClientUtil.sendAndGetResp(config);
            CallResultModel checkResult = checkCallResult(httpResult);
            return checkResult;
        } catch (HttpProcessException e) {
            log.error("", e);
        }
        return null;
    }
}
