package org.xqh.test.ugreen.webrelay;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import org.xqh.utils.http.FxHttpClientUtils;

/**
 * @ClassName WebRelayTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/5/2 12:17
 * @Version 1.0
 */
public class WebRelayTest {

    public static void main(String[] args) throws HttpProcessException {
        /**
         * r2, r4, r6, r7, r8 请求超时
         * Connect to r4.ugreen.cloud:443 [r4.ugreen.cloud/120.232.29.231] failed: connect timed out
         */
        String url = "https://r2.ugreen.cloud/UGMXCJZ3L8Q7VR2GWWWFNTBC/v2/storage/disk/list?api_token=Yzk3ODY5YzlmYjEwYWVkZThkNWEyODNkYTRmMDIwZGE1MDE1ZGUzMg%3D%3D";

        HttpConfig config = FxHttpClientUtils.getGetConfig(url, null);

        HttpResult httpRet = HttpClientUtil.sendAndGetResp(config);
        System.out.println(httpRet.getResult());

        /**
         * postman测试 r4, r6, r7 不通
         */

        /**
         *
         *  r1.ugreen.cloud [120.241.29.228]
         *  r2.ugreen.cloud [120.241.29.229]
         *  r3.ugreen.cloud [120.241.29.230]
         *  r4.ugreen.cloud [120.232.29.231]
         *  r5.ugreen.cloud [120.241.29.235]
         *  r6.ugreen.cloud [120.232.29.232]
         *  r7.ugreen.cloud [120.232.29.233]
         *  r8.ugreen.cloud [120.232.29.234]
         */
    }
}
