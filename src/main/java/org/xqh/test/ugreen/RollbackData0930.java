package org.xqh.test.ugreen;

import com.alibaba.fastjson2.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * @ClassName RollbackData0930
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/9/30 0:37
 * @Version 1.0
 */
public class RollbackData0930 {

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TempModel{
        private String deviceSn;
        private Long ugreenNo;
        private Long uid;
    }

    public static final String sql = "INSERT INTO t_user_device(uid, is_admin, device_id, file_sercetKey, private_key, public_key, remark, c_time, u_time, ugreen_no, device_sn, conn_pwd) VALUES(308006, '1', '218', NULL, NULL, NULL, NULL, '2023-12-28 10:33:17', '2023-12-28 10:33:17', 308006, 'HB718HH4623089EB', '7T9gtR4S5cX0PneG');";

    public static void main(String[] args) {
        List<String> list = ReadTxtFileUtils.readTxt(new File("D:\\work\\program\\项目迭代\\需求\\20240930线上问题\\rollback.txt"));
        List<String> ulist = ReadTxtFileUtils.readTxt(new File("D:\\work\\program\\项目迭代\\需求\\20240930线上问题\\user.txt"));
        List<String> sn36 = ReadTxtFileUtils.readTxt(new File("D:\\work\\program\\项目迭代\\需求\\20240930线上问题\\36.txt"));
        List<String> sn18 = ReadTxtFileUtils.readTxt(new File("D:\\work\\program\\项目迭代\\需求\\20240930线上问题\\18.txt"));
        List<TempModel> mList = Lists.newArrayList();
        Set<Long> ugreenNos = Sets.newHashSet();
        List<String> sqlList = Lists.newArrayList();
        List<String> snList = Lists.newArrayList();
        for(String sn:sn36){
            if(!sn18.contains(sn)){
                System.out.println(sn);
            }
        }
//        for(String str:list){
//            TempModel m = new TempModel();
//            String[] arr = str.split(", ");
//            m.setDeviceSn(arr[0]);
//            m.setUgreenNo(Long.valueOf(arr[1]));
//            mList.add(m);
//            ugreenNos.add(m.getUgreenNo());
//            for(String item:ulist){
//                String[] iarr = item.split(",");
//                Long uno = Long.valueOf(iarr[1]);
//                Long uid = Long.valueOf(iarr[0]);
//                if(Objects.equals(uno, m.getUgreenNo())){
//                    m.setUid(uid);
//                }
//            }
//            snList.add(m.getDeviceSn());
////            System.out.println(JSON.toJSONString(m));
//        }
//        System.out.println(JSON.toJSONString(mList));
//        System.out.println(ugreenNos.size());
//        System.out.println(JSON.toJSONString(ugreenNos));
        System.out.println(JSON.toJSONString(snList));
    }
}
