package org.xqh.test.ugreen.nps;

import com.sun.jna.Library;

/**
 * @ClassName HelloLibrary
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/7/30 10:29
 * @Version 1.0
 */
public interface HelloLibrary extends Library {
    void SayHello(String text);
}
