package org.xqh.test.ugreen.nps;

import com.sun.jna.Native;

import java.util.Objects;

/**
 * @ClassName JnaHelloTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/7/30 10:28
 * @Version 1.0
 */
public class JnaHelloTest {

    private static HelloLibrary INSTANCE;

    public static HelloLibrary getNpcInstance() {
        if (Objects.nonNull(INSTANCE)) {
            return INSTANCE;
        }
        synchronized (UGreenNpcManager.class) {
            if (Objects.nonNull(INSTANCE)) {
                return INSTANCE;
            }
            INSTANCE = Native.loadLibrary("D:\\work\\so\\hello.dll", HelloLibrary.class);
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        getNpcInstance().SayHello("hahaa");
    }
}
