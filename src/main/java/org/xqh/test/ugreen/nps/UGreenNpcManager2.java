package org.xqh.test.ugreen.nps;

import com.sun.jna.Native;

import java.util.Objects;

/**
 * @ClassName UGreenNpcManager @Description TODO @Author xuqianghui @Date 2022/7/6 9:15 @Version 1.0
 */
public class UGreenNpcManager2 {

    private static UGreenNpcLibrary INSTANCE;

    public static UGreenNpcLibrary getNpcInstance() {
        if (Objects.nonNull(INSTANCE)) {
            return INSTANCE;
        }
        synchronized (UGreenNpcManager2.class) {
            if (Objects.nonNull(INSTANCE)) {
                return INSTANCE;
            }
            INSTANCE = Native.loadLibrary("p2p_sdk.dll", UGreenNpcLibrary.class);
        }
        return INSTANCE;
    }

    public static void main(String[] args) throws InterruptedException {
        String deviceSn = "UG549HH08229999A";
        // 测试使用, app客户端StartClientForApp()启动方法 非阻塞
        new Thread(
                () -> {
                    // 启动app客户端
                    getNpcInstance()
                            .startClientForApp(
                                    "https://dev.ugreengroup.com",
                                    deviceSn,
                                    "ec468091595709e708efc9797c9d5dc2",
                                    "test:abc",
                                    "192.168.1.8:9999",
//                      "8788",
                                    2000,
                                    2);

                    // 启动nas客户端
                    //              getNpcInstance().StartClientForNas(
                    //                                            "https://dev.ugreengroup.com",
                    //                                            "UG549HH11220F122",
                    //
                    // "W8FiIt1CcJMfVbTt877SYEsulSVZ9aS75dTy5MGmams=",
                    //                                            "test:abc", 5);
                })
                .start();

        // 测试hold住main线程
        int count = 0;
        int status = 0;
        while (true) {
            count++;
            int curStatus = getNpcInstance().getClientStatus(deviceSn);
            if (status != curStatus) {
                status = curStatus;
                System.out.println("current status: " + curStatus + ", count: " + count);
            }

            //      if (count == 30) {
            //        getNpcInstance().RefreshVerifyKey("74cbbb3ddc2705cb37e46c0979bc1eab");
            //      }
            Thread.sleep(1000);
        }

        //      new Thread(
        //            new Runnable() {
        //              @SneakyThrows
        //              @Override
        //              public void run() {
        //                  int count = 0;
        //                while (true) {
        //                    count ++;
        //                    System.out.println("current client status: "+
        // getNpcInstance().GetClientStatus() +" current count: "+count);
        //                    Thread.sleep(1000);
        ////                    if (count >= 15){
        ////                        instance.CloseClient();
        ////                        break;
        ////                    }
        //                }
        //              }
        //            })
        //        .start();
        //      new Thread(new Runnable() {
        //          @Override
        //          public void run() {
        //              getNpcInstance().StartClientForApp("https://dev.ugreengroup.com",
        //                      "UG549HH08229999A",
        //                      "b2788a00f6020ed2966a389cf60e7eb9",
        //                      "test:abc",
        //                      "8788",
        //                      2001,
        //                      2);
        ////              getNpcInstance().StartClientForNas(
        ////                      "https://dev.ugreengroup.com",
        ////                      "UG549HH11220F122",
        ////                      "W8FiIt1CcJMfVbTt877SYOHAJL6osiELnBsdJ1GGwdA=",
        ////                      "test:abc", 5);
        //          }
        //      }).start();
    }
}
