package org.xqh.test.ugreen.nps;

import com.sun.jna.Library;

/**
 * @ClassName UGreenNpcLibrary
 * @Description
 * @Date 2022/7/1 17:43
 * @Version 1.0
 * */
public interface UGreenNpcLibrary extends Library {

  /**
   * app客户端 启动方法
   * @param cloudAddr      对应环境cloud地址
   * @param sn             设备号
   * @param vkey           验证key, md5(access_token), access_token 为 cloud登录接口 下发令牌
   * @param cliDesc        客户端标识
   * @param p2pTarget      对应p2p目标地址, 默认为 127.0.0.1:9999 (对应nas-server web服务)
   * @param port           本地tcp服务端口号 默认 2000
   * @param disconnectTime 请求超时次数配置, 单次对应 5秒, eg: 设置为 5, 超时时间 = (5+1)*5 = 30 秒
   * @return
   */
  int startClientForApp(String cloudAddr, String sn, String vkey, String cliDesc, String p2pTarget, int port, int disconnectTime);

  /**
   * nas客户端 启动方法
   * @param cloudAddr
   * @param sn
   * @param vkey
   * @param cliDesc
   * @param disconnectTime
   * @return
   */
  int startClientForNas(String cloudAddr, String sn, String vkey, String cliDesc, int disconnectTime);

  /**
   * 获取客户端状态
   * @return
   */
  int getClientStatus(String sn);

  /**
   * 关闭当前客户端
   */
  void closeClient(String sn);

  void closeAllClient();

  /**
   * 获取客户端版本
   * @return
   */
  String getVersion();

}
