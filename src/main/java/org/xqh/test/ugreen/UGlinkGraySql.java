package org.xqh.test.ugreen;

import com.google.common.collect.Lists;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @ClassName UGlinkGraySql
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/7/6 18:31
 * @Version 1.0
 */
public class UGlinkGraySql {
//'HB718HH462301E5A','EC660JJ29230669E','UG671UU33230A6F1','EC671JJ452304507','HB670UU45230000B','EC671JJ4523073F2'  EC660JJ292308498
    public static void main(String[] args) {
        String path = "D:\\work\\program\\项目迭代\\发布记录\\pro20240925\\";
        List<String> set = Lists.newArrayList();
        List<String> result = Lists.newArrayList();
//        List<String> list = ReadTxtFileUtils.readTxt(new File("D:\\迅雷授权码3.txt"));
        List<String> exclude = ReadTxtFileUtils.readTxt(new File(path.concat("uglink内测名单.txt")));
        String sql = "INSERT INTO uglink_gray_item(rule_id, gray_type, cfg_value, create_time, create_by) VALUES(2, 'gray_sn', '%s', NULL, NULL);";
//        String sql = "INSERT INTO device_third_info (device_sn, batch_id, batch_no, third_type, third_status, ext_content, third_secret, create_time, create_by, update_time, update_by, activation_time, expire_time, last_upd_id) VALUES(null, 12, '20220330102822', '1', 1, 'pro迁移迅雷授权码', '%s', '2024-08-28 10:28:22', 'liuqinlan', '2024-08-28 14:58:59', 'admin', '2022-04-02 11:07:13', NULL, 100);";
        for(String s:exclude){
            s = s.trim();
//            if(exclude.contains(s)){
//                continue;
//            }
            if(set.contains(s)){
               continue;
            }
            set.add(s);
            String out = String.format(sql, s);
            result.add(out);
        }

        ReadTxtFileUtils.writeToTxt(result, path.concat("sql.sql"));
    }
}
