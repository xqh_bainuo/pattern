package org.xqh.test.ugreen;

import org.apache.commons.codec.binary.Base64;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;
import org.xqh.utils.encrypt.EncryptConstants;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.KeyPair;

/**
 * @ClassName JwtRSAKey
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/9/2 15:06
 * @Version 1.0
 */
public class JwtRSAKey {

    public static void main(String[] args) throws UnsupportedEncodingException {
        String storePass = "ugreen123";
        String keyPass = "ugreen123qwe";
        KeyStoreKeyFactory keyStoreKeyFactory = new KeyStoreKeyFactory(
                //设置加密的加载文件
                new ClassPathResource("classpath:jwt.jks"),
//                new FileSystemResource("D:\\code\\ugreen\\ugreen-service-user\\ugreen-service-user-svc\\src\\main\\resources\\jwt.jks"),
                //设置读取秘钥库文件的密码
                storePass.toCharArray());


        //设置获取秘钥的密码
//        KeyPair keyPair = keyStoreKeyFactory.getKeyPair(jksProperties.getAlias());
        //设置获取秘钥的密码
        KeyPair keyPair = keyStoreKeyFactory.getKeyPair("jwt", keyPass.toCharArray());
        Key privateKey = keyPair.getPrivate();
        byte[] privateKeyBytes = privateKey.getEncoded();
        String pri = new String(Base64.encodeBase64(privateKeyBytes), EncryptConstants.CHAR_ENCODING);

        Key publicKey = keyPair.getPublic();
        byte[] publicKeyBytes = publicKey.getEncoded();
        String pub = new String(Base64.encodeBase64(publicKeyBytes), EncryptConstants.CHAR_ENCODING);
        System.out.println(pri);
        System.out.println(pub);
    }
}
