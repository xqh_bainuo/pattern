package org.xqh.test.ugreen;

import com.alibaba.druid.filter.config.ConfigTools;
import com.beust.jcommander.internal.Sets;
import com.google.common.collect.Lists;
import org.apache.poi.util.StringUtil;
import org.springframework.util.StringUtils;
import org.xqh.utils.encrypt.AESUtils;
import org.xqh.utils.encrypt.EncryptConstants;
import org.xqh.utils.encrypt.EncryptUtils;
import org.xqh.utils.excel.ExcelReader;
import org.xqh.utils.excel.ExcelUtil;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Set;

/**
 * @ClassName UgreenPwdTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/5/18 9:37
 * @Version 1.0
 */
public class UgreenPwdTest {

  public static void main(String[] args) {
      userPwd();
//      buildInsertDeviceThirdSql();
//      String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsidG9ybGVzc2VzZXJ2aWNlIl0sInVpIjoiNTUwMDA6NDcwOTM6MTgwMDAwMDAwMDE6bmFzIiwidXNlcl9uYW1lIjoiMTgwMDAwMDAwMDEiLCJzY29wZSI6WyJURVNUIl0sImlzcyI6IlVHUkVFTiIsImV4cCI6MTY2MTQxNTkwMywianRpIjoiMmY2ZjRmODUtODUzZC00MjZlLWExMmMtMzc5MDkzMmZiMDYzIiwiY2xpZW50X2lkIjoiYzEifQ.etRgi3rIB930tjbav8tWGGVEcWQOOqo9_cjSYqOpzQI";
//
//      System.out.println(token.getBytes().length);
  }

  public static String sql = "INSERT INTO `device_third_info` (`device_sn`, `batch_id`, `batch_no`, `third_type`, `third_status`, `ext_content`, `third_secret`, `create_time`, `create_by`, `update_time`, `update_by`, `activation_time`, `expire_time`) VALUES (NULL, -1, 'thunder_un_in_storage', '1', 1, '迅雷已授权未入库SN', '%s', now(), 'system', now(), 'fuqiang', NULL, NULL);";
  public static void buildInsertDeviceThirdSql(){
      List<String> list = ReadTxtFileUtils.readTxt(new File("D:\\work\\test\\sql\\write_to_device_third.txt"));
      List<String> sqlList = Lists.newArrayList();

      for(String sn : list){
          if(StringUtils.hasText(sn)){
              sn = sn.trim();
              sqlList.add(String.format(sql, sn));
          }
      }
      ReadTxtFileUtils.writeToTxt(sqlList, "D:\\work\\test\\sql\\device_third_out.txt");
  }
    public final static String default_aes_iv = "69dELVM5l78OguUd";//默认偏移量

    private final static String bak_ext_info = "UGenk3PgE6dDTWbo";//备份数据key
  public static void userPwd(){
      String pwd = "Aa123456";
      pwd = EncryptUtils.getMd5(EncryptUtils.getMd5(pwd));
      System.out.println("Aa123456 password ====>" + pwd);
      pwd = EncryptUtils.getMd5( "50000000"+pwd );
      System.out.println("mysql store pwd ====>" + pwd);
      String text = "sDOvQntenjngx3/wygT7aLleWAI5PtHFC+4j3t7nkfBchWNEOtRdN58SL2ij+tHACc9D+o3SOLiBjqv54VRrOA==";
      String msg = AESUtils.decryptFromBase64(text, bak_ext_info , EncryptConstants.ALGORITHM_CBC_PKCS5, default_aes_iv);
      System.out.println(msg);
//      DecimalFormat df = new DecimalFormat("0.000");
//m
//      System.out.println(df.format(0.0005777));
//      System.out.println(df.format(0.0003111));
//      String workPath = "D:\\work\\program\\ugreen-cloud\\迅雷\\迅雷数据\\授权数据\\";
//      removeRepeatSn(workPath+"设备号汇总.xlsx", workPath+"out.txt");
  }

  public static void parseDBPwd(){
      String pubKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAIQwZriNtyyx4eH6EmA3wwk4q/TDlA8TNIH3R3PIxdkkVoRgJS7KnICgy4LzHvqdLdLzxi45pMS9GwsX3MDRtC0CAwEAAQ==";
      String pwd = "NEXu2e/Ze5ROt/l2ENcH9sf47eadzEL/QueelQkAGntG62ugfYCnqpiHZKp90qMrN623Y/3Qw9NRwPIDA/vJVg==";
      try {
          System.out.println(ConfigTools.decrypt(pubKey, pwd));
      } catch (Exception e) {
          e.printStackTrace();
      }
  }

  public static void removeRepeatSn(String filePath, String outPath){
      List<String[]> data = ExcelReader.getExcelData(filePath, 0);
      Set<String> set = Sets.newHashSet();
      List<String> writeList = Lists.newArrayList();
      System.out.println("===================>"+data.size());
      for(String[] arr:data){
          if(arr.length >= 1 && StringUtils.hasText(arr[0])){
              String sn = arr[0].trim();
              if(set.contains(sn)){
                  continue;
              }
              set.add(sn);
              writeList.add(sn);
          }
      }
      ReadTxtFileUtils.writeToTxt(writeList, outPath);
  }
}
