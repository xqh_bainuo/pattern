package org.xqh.test.ugreen.secret;

import com.google.common.collect.Lists;
import org.xqh.utils.encrypt.EncryptUtils;

import java.util.List;
import java.util.Objects;

/**
 * @ClassName MatchSecretTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/10/24 14:45
 * @Version 1.0
 */
public class MatchSecretTest {

    public static void main(String[] args) {
        String md5 = "933d4fcf22a12d18db0fae9c50ed4a21";
        List<String> keyList = Lists.newArrayList(
                "rQIbHxsJWoXQSxdXPsgW8svKvJPC5ARDuAXyRvqxwt6HNcVvR4aMP5GwqKAELPj6",
                        "HJwO4N0kcpdvRwI8fVelxYUbMZblcDM09hoF9zjhJjIaqxDMQhfCWTBsuZtRef1I",
                        "ckVyrS0trObqt98yNUkaONGUlzXjskM12woHO80kClRZ2l8dpCuLDwVx3w8iaEJn",
                "xoLpW8aCmbOkLpjxoL3QUiFLrpJdBUsXkOHHqYuCmmuG4asBOQk93yexJURZp5tc",
                "UGN123NAS@#$"
        );
        String msg = "ak=UG671UU35230B894&ext={\"isInitialized\":1}&ipv4=172.17.20.212&mac=98:6E:E8:23:C6:A1&runTime=0&shutdown=0&sn=UG671UU35230B894&timestamp=1698311684&ts=1698311684&SecretKey=%s";
        for(String key:keyList){
            String md5Ret = EncryptUtils.getMd5(String.format(msg, key));
            if(md5Ret.equalsIgnoreCase(md5)){
                System.out.println(key + "=================>");
                System.out.println();
                System.out.println(md5Ret);
            }
        }
    }
}
