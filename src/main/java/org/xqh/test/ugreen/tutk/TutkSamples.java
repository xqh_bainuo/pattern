package org.xqh.test.ugreen.tutk;

import com.sun.jna.Native;
import org.xqh.study.jna.guanglian.GLLibrary;

/**
 * @ClassName TutkSamples
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/8/24 11:17
 * @Version 1.0
 */
public class TutkSamples {

    public static void main(String[] args) {
        TutkLibrary instance = Native.load("D:\\work\\so\\P2PTunnelAPIs.dll", TutkLibrary.class);
        int ret = instance.TUTK_SDK_Set_License_Key("test license key");
        System.out.println("set license key result: " + ret);
    }
}
