package org.xqh.test.ugreen.tutk;

import com.sun.jna.Library;

/**
 * @ClassName TutkLibrary
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/8/24 11:26
 * @Version 1.0
 */
public interface TutkLibrary  extends Library {

    int TUTK_SDK_Set_License_Key(String license_key);

    int P2PTunnelAgentInitialize(int MAX_SERVER_CONNECT_NUM);


    void P2PTunnelAgent_GetStatus(TutkModel.TutkTunnelStatusCB cb);

    int P2PTunnelAgent_Connect_Ex(String uid, TutkModel.TunnelClientAuthCB cb, TutkModel.TutkTunnelInfo info);
}
