package org.xqh.test.ugreen.tutk;

import com.sun.jna.Callback;
import com.sun.jna.Structure;
import lombok.extern.slf4j.Slf4j;
import org.xqh.study.jna.guanglian.GlModel;

/**
 * @ClassName TutkModel
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/8/24 11:43
 * @Version 1.0
 */
@Slf4j
public class TutkModel {

    @Structure.FieldOrder({"uid", "nWebIndex", "nTelnetIndex", "nSshIndex", "nRetryConnectFailedCnt", "nSID", "bException"})
    public static class TutkTunnelInfo extends Structure {

        public String uid;

        public int nWebIndex;

        public int nTelnetIndex;
        public int nSshIndex;
        public int nRetryConnectFailedCnt;
        public int nSID;
        public boolean bException;

        public static class ByReference extends TutkModel.TutkTunnelInfo implements Structure.ByReference {
        }

        public static class ByValue extends TutkModel.TutkTunnelInfo implements Structure.ByValue {
        }
    }


    /**
     * 通道状态 回调
     */
    public interface TutkTunnelStatusCB extends Callback {
        void TunnelStatusCB(int nErrorCode, int nSID);
    }

    public class TutkTunnelStatusCBImpl implements TutkTunnelStatusCB {
        @Override
        public void TunnelStatusCB(int nErrorCode, int nSID) {
            log.info("receive tunnel status callback, code: {}, sid: {}", nErrorCode, nSID);
        }
    }

    public interface TunnelClientAuthCB extends Callback {
        void TunnelClientAuthentication(String cszAccount, int nAccountMaxLength, String cszPassword, int nPasswordMaxLength);
    }

    public class TunnelClientAuthCBImpl implements TunnelClientAuthCB {
        @Override
        public void TunnelClientAuthentication(String cszAccount, int nAccountMaxLength, String cszPassword, int nPasswordMaxLength) {
            log.info("authentication callback {} {}", cszAccount, cszPassword);
        }
    }
}
