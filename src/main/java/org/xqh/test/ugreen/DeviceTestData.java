package org.xqh.test.ugreen;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.util.List;

/**
 * @ClassName DeviceTestData
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/12/22 10:37
 * @Version 1.0
 */
public class DeviceTestData {

  public static String sql =
      "INSERT INTO `t_device` (`sn`, `name`, `mac`, `model`, `firmwareVer`, `nasVer`, `hardwareVer`, `status`, `image`, `p2pId`, `remark`, `ur_version`, `c_time`, `u_time`, `emmc`) VALUES ('%s', 'p2p-test-%s', '24:97:ED:18:65:E4', '14', 'ugreen.20220516.212822-V2.0.0.111', 'V3.0.0.220516', 'Default string Default string', 1, NULL, NULL, '最近一次心跳同步时间:2022-06-04 14:05:24', 1, '2022-03-23 15:16:06', '2022-06-04 14:05:24', NULL);";

    public static String share_sql = "INSERT INTO `ugreen_nas`.`t_device_share_info` (`sn`, `ugreen_no`, `s_key`, `code`, `s_times`, `on_times`, `exp_time`, `status`, `api_token`, `share_link`, `remark`, `c_time`, `u_time`) VALUES ('%s', '6546', '%s', '3UXX', 10, NULL, '2021-09-17 09:38:47', 0, 'YjE3MjU2NzAwZDM1MzdlMGY0NTJjNTJkYzFkYzkzYWIzZTc0NTRhNg==', NULL, '2021-09-10 09:38:47 用户：6546分享文件，文件分享止期：2021-09-17 09:38:47, 文件分享有效次数：10', '2021-09-10 09:38:47', '2021-09-10 09:38:47');";

    private static String status_sql = "INSERT INTO `device_status_info` (`device_sn`, `device_status`, `in_storage_count`, `repair_status`, `repair_start_time`, `repair_end_time`, `create_time`, `remark`, `create_by`, `update_time`, `update_by`) VALUES ('%s', 'activated', 1, 'under_warranty', '2022-07-11 14:38:21', NULL, '2022-07-11 14:39:54', 'old-device-status-info-init', NULL, '2022-07-11 14:39:54', NULL);";

    public static String sn = "DH00088888";
    public static String share = "00002188";

    public static String work_path = "D:\\work\\test\\";

    public static void main(String[] args) {

        String txt = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYWxsIl0sInVpIjoiNzE2MzoxODgxIiwidXNlcl9uYW1lIjoiNzE2MyIsInNjb3BlIjpbIm5hcyJdLCJleHAiOjE2NjQxOTI2MDQsImp0aSI6ImI4YTU1MTJhLThkNmQtNGUxNC1iMWI2LWM1ZjYxNjFiZTVmZCIsImNsaWVudF9pZCI6InVncmVlbiJ9.ZbBy3IM3MV4nLWoopU9FMTamW_kdxOS_9gaktpmUdKSGRR00AV9jGF6TK_FVo9kgKrTt0dijVyWPK2qXuzES_CWm-61Ny-RKgr03MmlFUnqWb9dRvePk50-nWjcYcI7EHXl2z8tBZPPQEjyK83ucBISL6apQBjJFTkSplsIe62FBN8eOZFDD2ojhRM8s950QkbaYRTUv22vQNbQRsMACeD7iFQMvgA4_6qYhCkLszDQuQzjaGqoClWqEfTqbCHNE9CYV1Ns6ebRFItPdN_";

        System.out.println(txt.getBytes().length);

//        writeDeviceTestData2File();
    }

    public static void writeDeviceTestData2File(){
       List<String> snList = ReadTxtFileUtils.readTxt(new File(work_path + "500sn.txt"));
       List<String> writeList = Lists.newArrayList();

       for(String sn:snList){
           TestData data = TestData.builder()
                   .sn(sn)
                   .mac(randomMac())
                   .shutdown("0")
                   .build();
           writeList.add(JSON.toJSONString(data));
       }

       ReadTxtFileUtils.writeToTxt(writeList, work_path + "output.txt");

    }

    public static String randomMac(){
        String str = RandomStringUtils.randomAlphanumeric(12);
        String[] array = str.split("");
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < array.length; i ++){
            if(i > 0 && i % 2 == 0){
                sb.append(":");
            }
            sb.append(array[i]);
        }
        return sb.toString().toUpperCase();
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TestData {

        private String sn;
        private String mac;
        private String shutdown;
        private String timestamp;
        private String sign;
    }



    //生成设备相关数据 insert 脚本
    public static void generateDeviceSql() {
        List<String> sqlList = Lists.newArrayList();
        List<String> shareSqlList = Lists.newArrayList();
        List<String> deviceSnList = Lists.newArrayList();
        for(int i = 0; i < 20000; i ++){
            String prefix = ""+i;
            if(prefix.length() != 6){
                int count = 6 - prefix.length();
                for(int j = 0; j < count; j++){
                    prefix = "0"+prefix;
                }
            }
            String deviceSn = sn + prefix;
            String qian = RandomStringUtils.randomAlphanumeric(4).toUpperCase();
            String hou = RandomStringUtils.randomAlphanumeric(4).toUpperCase();
            String shareKey = "DH-"+qian +share+hou+"-"+prefix;
            sqlList.add(String.format(sql, deviceSn, i));
            sqlList.add(String.format(status_sql, deviceSn));

            deviceSnList.add(deviceSn);
            shareSqlList.add(String.format(share_sql, deviceSn, shareKey));
        }


        ReadTxtFileUtils.writeToTxt(deviceSnList, "D:\\work\\test\\device-sn2.txt");
        ReadTxtFileUtils.writeToTxt(sqlList, "D:\\work\\test\\device2.sql");
//        ReadTxtFileUtils.writeToTxt(shareSqlList, "D:\\work\\test\\share.sql");

    }
}
