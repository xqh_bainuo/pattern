package org.xqh.test.ugreen.application;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.springframework.util.StringUtils;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.io.ObjectInputStream;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName ApplicationRelDataSql
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/1/17 13:36
 * @Version 1.0
 */
@Slf4j
public class ApplicationRelDataSql {

    private final static AtomicInteger id = new AtomicInteger(200);

    public static void main(String[] args) {

        String name = "f";
        getCacheKeyList();
        try {
            //todo
        }catch (Exception e){
            log.error("test api err. {}", name, e);
        }

    }

    public static List<String> getCacheKeyList(){
        List<String> infoList = ReadTxtFileUtils.readTxt(new File("D:\\test\\application\\应用信息.txt"));
        List<String> existedAppInfo = ReadTxtFileUtils.readTxt(new File("D:\\test\\application\\已存在应用名称信息.txt"));
        List<String> jsonList = ReadTxtFileUtils.readTxt(new File("D:\\test\\application\\application_json.txt"));

//        List<String> infoList = ReadTxtFileUtils.readTxt(new File("D:\\test\\application\\test\\应用信息.txt"));
//        List<String> existedAppInfo = ReadTxtFileUtils.readTxt(new File("D:\\test\\application\\test\\已存在应用名称信息.txt"));
//        List<String> jsonList = ReadTxtFileUtils.readTxt(new File("D:\\test\\application\\test\\application_json.txt"));

        List<ApplicationInfo> existAppList = Lists.newArrayList();
        for(String app:existedAppInfo){
            ApplicationInfo info = new ApplicationInfo();
            String[] arr = app.split(",");
            info.setId(Long.parseLong(arr[0]));
            info.setLanguage(arr[1]);
            String appName = arr[2];
            if(!"null".equals(appName)){
                info.setAppName(appName);
                existAppList.add(info);
            }
        }

        List<JSONObject> jsonObjList = Lists.newArrayList();
        for(String str:jsonList){
            jsonObjList.add(JSON.parseObject(str));
        }
        List<String> sqlLst = Lists.newArrayList();
        List<ApplicationInfo> allApp = Lists.newArrayList();
        for(String txt:infoList){
            if(StringUtils.isEmpty(txt)){
                continue;
            }
            String[] arr = txt.split(",");
            ApplicationInfo obj = new ApplicationInfo();
            obj.setId(Long.parseLong(arr[0].trim()));
            obj.setAppId(arr[1].trim());
            allApp.add(obj);
            for(JSONObject item:jsonObjList){
                String appId = item.getString("appId");
                if(appId.equals(obj.getAppId())){
                    JSONArray i18n = item.getJSONArray("i18n");
                    buildInsertSql(obj, i18n, sqlLst, existAppList);
                }
            }
        }

        for(String sql:sqlLst){
            System.out.println(sql);
        }
        List<String> cacheList = getApplicationCacheKey(allApp);
        System.out.println(JSON.toJSONString(cacheList));
        return cacheList;
    }

    public static List<String> getApplicationCacheKey(List<ApplicationInfo> list){
        List<String> keyList = Lists.newArrayList();
        List<String> languageList = com.google.common.collect.Lists.newArrayList("zh-CN", "en-US", "de-DE");
        for(ApplicationInfo app:list){
            for(String lan:languageList){
                String key = String.format("application:manager:appName:%s:%s", app.getAppId(), lan);
                keyList.add(key);
            }
            String key2 = String.format("application:manager:relation:%s", app.getAppId());
            keyList.add(key2);
        }
        return keyList;
    }

    private static void buildInsertSql(ApplicationInfo app, JSONArray i18n, List<String> sqlLst, List<ApplicationInfo> exists){
        String sql = "INSERT INTO common_relation_data VALUES(%s, 'application_mgr', '%s', 'name', 0, '%s', NULL, '%s', 'jianjierong', '2023-12-11 18:19:54');";
        for(Object item:i18n){
            JSONObject obj = (JSONObject) item;
            String name = obj.getString("name");
            String langName = obj.getString("langName");
            int curId = id.incrementAndGet();
            Long appMgrId = app.getId();
            Optional<ApplicationInfo> opt = exists.stream().filter(e-> e.getId().equals(appMgrId) && e.getLanguage().equals(langName)).findAny();
            if(opt.isPresent()){
                System.out.println("当前应用名称已存在: " + JSON.toJSONString(opt.get()));
                continue;
            }
            String result = String.format(sql, curId, appMgrId, langName, name);
            sqlLst.add(result);
        }
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ApplicationInfo {
        private String appId;
        private Long id;
        private String language;
        private String appName;
    }
}
