package org.xqh.test.ugreen.tmdb;

/**
 * @ClassName TMDBApiTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2024/9/6 13:54
 * @Version 1.0
 */
public class TMDBApiTest {

    private final static String api_key = "07a84f556b333b97c6aadfa827ebf1d1";

    private final static String language_cn = "zh-CN";

    private final static String language_us = "en-US";

    private final static String tmdb_domain = "https://api.themoviedb.org";



    //查询热门电影
    /**
     * https://api.themoviedb.org/3/discover/movie?include_adult=true&include_video=true&language=en-US&page=2&api_key={{tmdb-key}}&primary_release_date.gte=2020-03-12&sort_by=primary_release_date.asc
     */
    private final static String discover_movie_api = "/3/discover/movie?include_adult=false&include_video=false&sort_by=primary_release_date.asc&language=%s&page=2&primary_release_date.gte=%s&api_key=%s";

    ///3/movie/519182?api_key={{tmdb-key}}&language=zh-CN
    private final static String movie_detail_api = "/3/movie/%s?append_to_response=credits&language=%s&api_key=%s";

    //查询热门剧集
    private final static String discover_tv_api = "/3/discover/tv?include_adult=false&include_video=false&language=%s&page=%s&sort_by=popularity.desc&api_key=%s";

    //剧集详情即可欧 /3/tv/206559?api_key={{tmdb-key}}&language=zh-CN
    private final static String tv_detail_api = "/3/tv/206559?api_key={{tmdb-key}}&language=zh-CN";


}
