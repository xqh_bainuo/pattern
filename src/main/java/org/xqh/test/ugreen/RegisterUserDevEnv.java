package org.xqh.test.ugreen;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import org.xqh.utils.http.FxHttpClientUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName RegisteryUserDevEnv
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/12/19 13:48
 * @Version 1.0
 */
public class RegisterUserDevEnv {

    public final static String url = "http://dev.ugreengroup.com/api/oauth/token";

    public static void main(String[] args) throws HttpProcessException {
        HttpConfig config = FxHttpClientUtils.getHttpConfig(url);
        config.method(HttpMethods.POST);
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("grant_type", "sms");
        reqMap.put("phoneNo", "18000000016");
        reqMap.put("vcode", "666666");
        reqMap.put("platform", "PC");
        reqMap.put("bid", "12233444555asdfgg");
        reqMap.put("otherType", "1");
        config.map(reqMap);
        HttpResult result = HttpClientUtil.sendAndGetResp(config);
        System.out.println( result.getResult());
    }
}
