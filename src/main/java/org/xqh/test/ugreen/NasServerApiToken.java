package org.xqh.test.ugreen;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.xqh.test.ugreen.dto.CallServerLoginParams;
import org.xqh.test.ugreen.dto.CallServerLoginParams.CallServerUserDTO;
import org.xqh.test.ugreen.dto.UserDTO;
import org.xqh.utils.encrypt.RSAUtils;

/**
 * @ClassName NasServerApiToken @Description TODO @Author xuqianghui @Date 2022/7/20 17:58 @Version
 * 1.0
 */
@Slf4j
public class NasServerApiToken {

    private static final String nas_server_sign_str = "ugreen_no=%s&phone_no=%s&nic_name=%s&version=%s";

    public static void main(String[] args) {
        String pubKey =
                "-----BEGIN PUBLIC KEY-----\n"
                        + "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2PJI8HrIV8vHpKx8ghjl\n"
                        + "tui+R6E9C+5nT3vyS/qMd30yDDD9Dwg/XIizA/pJZdgp0BpFHuoKjrB7BstMpshY\n"
                        + "4RT+yqAWSXPVy8xn7J1ZkeJ/l3mf504294WgB6mNU8/8/4pzQIm9XwiosJ3ywReY\n"
                        + "TlZPgRSH4rIXGoCq4/trofUH6pfLCOqho06Fb21uxEodkSF1PI9O7WohCCI2qGan\n"
                        + "IjCtH/A+HbRY7xYHZF4UIS+bQJWX0iFUbAxu2CpiM0HfaMvvERJU1EXPKqFREx8Z\n"
                        + "oKQpxxSD+SHP7nYXIPMFWgzOmi5zx+ofcP+t2e2G7DGBIodwwusxqExhTQzB5RQa\n"
                        + "+wIDAQAB\n"
                        + "-----END PUBLIC KEY-----\n";
        UserDTO user = new UserDTO();
        user.setUgreenNo(55000L);
        user.setPhoneNo("18000000001");
        user.setVersionNo(4);
        String sign =
                buildServerSign(
                        user.getUgreenNo().toString(),
                        user.getPhoneNo(),
                        "",
                        user.getVersionNo().toString(),
                        pubKey);
        CallServerLoginParams loginParams = buildCallServerLoginParams(user, sign);
        System.out.println(JSON.toJSONString(loginParams));
    }

    /**
     * 构建调用 server端 登录接口 参数
     *
     * @param userDTO
     * @param sign
     * @return
     */
    public static CallServerLoginParams buildCallServerLoginParams(UserDTO userDTO, String sign) {
        CallServerUserDTO user =
                CallServerUserDTO.builder()
                        .ugreen_no(userDTO.getUgreenNo())
                        .birthday(String.valueOf(userDTO.getBirthday()))
                        .email(userDTO.getEmail())
                        .nic_name(userDTO.getNicName())
                        .phone_no(userDTO.getPhoneNo())
                        .sex(userDTO.getSex())
                        .version(userDTO.getVersionNo())
                        .build();

        CallServerLoginParams loginParams =
                CallServerLoginParams.builder().platform(0).sign(sign).user_basic(user).build();
        return loginParams;
    }

    public static String buildServerSign(
            String ugreenNo, String phone, String nickName, String versionNo, String pubKey) {
        String signStr =
                String.format(
                        nas_server_sign_str,
                        ugreenNo,
                        phone,
                        StringUtils.isNotEmpty(nickName) ? nickName : "",
                        versionNo);
        String sign = null;
        try {
            sign = RSAUtils.encrypt(signStr, getPublicKey(pubKey));
            System.out.println(sign);
        } catch (Exception e) {
            log.error("", e);
        }

        return sign;
    }

    public static String getPublicKey(String publicKey) {
        return publicKey
                .replaceAll("-----END PUBLIC KEY-----", "")
                .replaceAll("-----BEGIN PUBLIC KEY-----\n", "");
    }
}
