package org.xqh.test.regex;

import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName RegexTest2
 * @Description TODO
 * @Author xuqianghui
 * @Date 2023/3/30 7:41
 * @Version 1.0
 */
public class RegexTest2 {


    public static void main(String[] args) {
        String text = "2024-02-26 14:00:44.175 [http-nio-8030-exec-14] INFO  c.u.c.f.c.i.UGreenCommonInterceptor [UGreenCommonInterceptor.java : 237] api: /system/v2/sa/help/check, sn: UG670UU372300049, ugreenNo:  clientIp: 113.118.134.75, params: {\"at\":\"nas\",\"sign\":\"22fd7328a55b915e9e74a2fe52ede3f1\",\"checkList\":[{\"code\":\"q2lfw1\",\"clientType\":\"PC\",\"md5sum\":\"\",\"appId\":\"com.ugreen.taskmgr\",\"versionNo\":\"1.00.0383\",\"language\":\"zh-CN\",\"model\":\"DXP2800\"}],\"ak\":\"UG670UU372300049\",\"mac\":\"50:A1:32:3B:C4:F6\",\"ts\":\"1708927244\"}, start: 1708927244170, end: 1708927244174, cost-time: 4, http-nio-8030-exec-14\n" +
                "2024-02-26 14:00:44.202 [http-nio-8030-exec-12] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] ==>  Preparing: SELECT id,article_relation_id,app_id,article_version,code,path_code,relative_path,file_url,md5sum,client_type,model,language,status,create_time,create_by,update_time,update_by FROM article_file_record WHERE (app_id IN (?) AND language IN (?) AND model LIKE ? AND client_type IN (?) AND status = ?) ORDER BY app_id DESC,article_version DESC \n" +
                "2024-02-26 14:00:44.202 [http-nio-8030-exec-12] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] ==> Parameters: com.ugreen.storagemgr(String), zh-CN(String), %DXP2800%(String), PC(String), 1(Integer)\n" +
                "2024-02-26 14:00:44.204 [http-nio-8030-exec-12] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] <==      Total: 5\n" +
                "2024-02-26 14:00:44.204 [http-nio-8030-exec-12] INFO  c.u.c.f.c.i.UGreenCommonInterceptor [UGreenCommonInterceptor.java : 237] api: /system/v2/sa/help/check, sn: UG670UU37230003C, ugreenNo:  clientIp: 113.118.135.72, params: {\"at\":\"nas\",\"sign\":\"c177461ef9e242e7eb62857285a4f957\",\"checkList\":[{\"code\":\"rstol3\",\"clientType\":\"PC\",\"md5sum\":\"8703c35995c17b7780dd50f0e6e4061c\",\"appId\":\"com.ugreen.storagemgr\",\"versionNo\":\"1.00.0346\",\"language\":\"zh-CN\",\"model\":\"DXP2800\"}],\"ak\":\"UG670UU37230003C\",\"mac\":\"50:A1:32:3B:C5:24\",\"ts\":\"1708927244\"}, start: 1708927244200, end: 1708927244204, cost-time: 4, http-nio-8030-exec-12\n" +
                "2024-02-26 14:00:44.320 [http-nio-8030-exec-24] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] ==>  Preparing: SELECT id,article_relation_id,app_id,article_version,code,path_code,relative_path,file_url,md5sum,client_type,model,language,status,create_time,create_by,update_time,update_by FROM article_file_record WHERE (app_id IN (?) AND language IN (?) AND model LIKE ? AND client_type IN (?) AND status = ?) ORDER BY app_id DESC,article_version DESC \n" +
                "2024-02-26 14:00:44.320 [http-nio-8030-exec-24] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] ==> Parameters: com.ugreen.ctlmgr(String), en-US(String), %DX4700%(String), PC(String), 1(Integer)\n" +
                "2024-02-26 14:00:44.323 [http-nio-8030-exec-24] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] <==      Total: 40\n" +
                "2024-02-26 14:00:44.323 [http-nio-8030-exec-24] INFO  c.u.c.f.c.i.UGreenCommonInterceptor [UGreenCommonInterceptor.java : 237] api: /system/v2/sa/help/check, sn: EC554JJ472304555, ugreenNo:  clientIp: 114.222.74.120, params: {\"at\":\"nas\",\"sign\":\"96f522987148e86346eff6c9b949729c\",\"checkList\":[{\"code\":\"3nxqq9\",\"clientType\":\"PC\",\"md5sum\":\"35271dba36bb27ba4e2911f1289b2f54\",\"appId\":\"com.ugreen.ctlmgr\",\"versionNo\":\"1.00.0346\",\"language\":\"en-US\",\"model\":\"DX4700\"}],\"ak\":\"EC554JJ472304555\",\"mac\":\"98:6E:E8:23:C1:7D\",\"ts\":\"1708927243\"}, start: 1708927244318, end: 1708927244323, cost-time: 5, http-nio-8030-exec-24\n" +
                "2024-02-26 14:00:44.324 [http-nio-8030-exec-9] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] ==>  Preparing: SELECT id,article_relation_id,app_id,article_version,code,path_code,relative_path,file_url,md5sum,client_type,model,language,status,create_time,create_by,update_time,update_by FROM article_file_record WHERE (app_id IN (?) AND language IN (?) AND model LIKE ? AND client_type IN (?) AND status = ?) ORDER BY app_id DESC,article_version DESC \n" +
                "2024-02-26 14:00:44.324 [http-nio-8030-exec-9] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] ==> Parameters: com.ugreen.ctlmgr(String), en-US(String), %DXP480T Plus%(String), PC(String), 1(Integer)\n" +
                "2024-02-26 14:00:44.327 [http-nio-8030-exec-9] DEBUG c.u.c.s.d.m.u.A.selectList [BaseJdbcLogger.java : 143] <==      Total: 39\n" +
                "2024-02-26 14:00:44.327 [http-nio-8030-exec-9] INFO  c.u.c.f.c.i.UGreenCommonInterceptor [UGreenCommonInterceptor.java : 237] api: /system/v2/sa/help/check, sn: HB718HH462302FB2, ugreenNo:  clientIp: 113.118.134.75, params: {\"at\":\"nas\",\"sign\":\"9174bd13e0fb494e94e6bfd13e6676dd\",\"checkList\":[{\"code\":\"wzdutk\",\"clientType\":\"PC\",\"md5sum\":\"514eaca498fed4ae1d0c6d6138e23aa1\",\"appId\":\"com.ugreen.ctlmgr\",\"versionNo\":\"1.00.0346\",\"language\":\"en-US\",\"model\":\"DXP480T Plus\"}],\"ak\":\"HB718HH462302FB2\",\"mac\":\"24:97:ED:27:7F:AB\",\"ts\":\"1708927244\"}, start: 1708927244322, end: 1708927244327, cost-time: 5, http-nio-8030-exec-9";
//        String replace = "\\[xxsub]";
//        String replace2 = "\\[x月新番]";
//        if(replace.startsWith("[")){
//            replace = "\\".concat(replace);
//        }

//        text = text.replaceAll(replace, "");
//        text = text.replaceAll(replace2, "");
        String regex = "api: /system/v2/sa/help/check, sn: ([0-9A-Z]{16})";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()){
            System.out.println(matcher.group(1));
        }

    }


    public static void main111(String[] args) {
        String file = "D:\\download\\QQ\\out2.txt";
        List<String> snlist = ReadTxtFileUtils.readTxt(new File(file));
        Set<String> set = new HashSet<>();
        List<String> newlist = new ArrayList<>();
        for(String sn:snlist){
            if(!set.contains(sn)){
                set.add(sn);
                newlist.add(sn);
            }
        }
        newlist.sort((a,b) -> a.compareTo(b));
        ReadTxtFileUtils.writeToTxt(newlist, "D:\\download\\QQ\\new-out.txt");
    }
}
