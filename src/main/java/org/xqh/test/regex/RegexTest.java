package org.xqh.test.regex;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPath;
import com.aliyun.oss.ServiceException;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;
import org.xqh.test.ugreen.scrape.ScrapeTest;
import org.xqh.utils.DateUtil;
import org.xqh.utils.encrypt.EncryptUtils;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @ClassName RegexTest
 * @Description 正则测试
 * @Author xuqianghui
 * @Date 2019/5/15 18:11
 * @Version 1.0
 */
public class RegexTest {

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TestModel {
        private String name;
        private int age;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class UGLinkIDFormatCfg {

        private Set<String> exact;

        private Set<String> regex;

        public static void main(String[] args) {
            UGLinkIDFormatCfg cfg = UGLinkIDFormatCfg.builder()
                    .exact(Sets.newHashSet("www", "xijinping", "test", "gongchandang", "ugreen", "user",
                            "admin", "exp", "nas", "uglink",
                            "ugreenlink", "ugreennas", "errpage", "666", "888"))
                    .regex(Sets.newHashSet("^demo[0-9]+$"))
                    .build();

            String test = JSON.toJSONString(cfg);
            UGLinkIDFormatCfg cfg2 = JSON.parseObject(test, UGLinkIDFormatCfg.class);
            System.out.println(JSON.toJSONString(cfg2));
        }
    }

    public static void main(String[] args) {
        String regex = "VALUES \\(([0-9]{5}), NULL,";
//        List<String> list = ReadTxtFileUtils.readTxt(new File(""));
        String txt = "INSERT INTO `<table_name>`(`id`, `device_sn`, `batch_id`, `batch_no`, `third_type`, `third_status`, `ext_content`, `third_secret`, `create_time`, `create_by`, `update_time`, `update_by`, `activation_time`, `expire_time`, `last_upd_id`, `platform`) VALUES (20040, NULL, 28, '20230710151033', 'guanglian', 1, '广联授权码03', 'GL3d2fa74f28f8469eb9b5280e21b02a76', '2023-07-10 15:10:33', 'admin', '2023-08-18 19:04:08', 'admin', NULL, NULL, 1692492568643895296, 'ugos');";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(txt);
        while (matcher.find()) {
            for (int i = 0; i <= matcher.groupCount(); i++) {
                System.out.println(i + " ==> " + matcher.group(i));
            }
        }
    }

    public static void main1122211(String[] args) {
//        String text = "[电影之家www.dyzj18.com]富江2.冤有头.Tomie.Replay.2000.HD720P.日语中字.mkv";
//        String text = "富江2.冤有头.Tomie.Replay";
//        String text = "<a class=\"item-title str-truncated-100 ref-name prepend-left-8 qa-branch-name\" href=\"/cloud/ugreen-service-user/tree/feature/pro2311/userPrivacy\">feature/pro2311/userPrivacy\n</a>";
        String cfg = "{\"exact\":[\"nas\",\"xijinping\",\"test\",\"ugreenlink\",\"ugreen\",\"admin\",\"uglink\",\"errpage\",\"gongchandang\",\"ugreennas\",\"www\",\"exp\",\"user\"],\"regex\":[\"^demo[0-9]+$\",\"^file-([a-z0-9\\\\-]+)$\"]}";
        JSONObject cfgJson = JSON.parseObject(cfg);
        JSONArray array = cfgJson.getJSONArray("regex");
        System.out.println(array.get(1));
        String text = "file-abcdeeee";
        String regex = "^demo[0-9]+$";
        String regex2 = "^file-([a-z0-9\\-]+)$";
//        String regex = "([a-z]*[\\u4e00-\\u9fa5,0-9,.,：]*)[.]([a-z,.,-]+)";//[12 END]
        Pattern pattern = Pattern.compile(array.get(1).toString(), Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(text);
//        System.out.println(matcher.matches());
        while (matcher.find()) {
            for (int i = 0; i <= matcher.groupCount(); i++) {
                System.out.println(i + " ==> " + matcher.group(i));
            }
        }

//        按指定模式在字符串查找
//        String line = "{\"a\":\"${b}+${c}\"}";
//        String pattern = "\\$\\{([^}]*)}";
//
//        // 创建 Pattern 对象
//        Pattern r = Pattern.compile(pattern);
//
//        // 现在创建 matcher 对象
//        Matcher m = r.matcher(line);
//        while (m.find()) {
//            //System.out.println(m.group());
//            String variableStr = m.group().substring(2).substring(0, m.group().length() - 3);//去掉  ${}
//            System.out.println(variableStr);
//        }
    }

    public static void main1111(String[] args) {

        JSONObject json = new JSONObject();
        json.put("email", "noreply@ugreen.com");
        json.put("domain", "*.wxf.direct.ug.link");
        json.put("secretID", "ffff");
        json.put("secretKey", "seeekey");
        System.out.println(json.toJSONString());

        String ipv6 = "fe80::2697:edff:fe18:6609";
        System.out.println(ipv6.replaceAll("::", "-").replaceAll(":", "-"));

        Date date = new Date(1711881781000L);
        System.out.println(DateUtil.formatDate(date, DateUtil.DATE_PATTERN_COMPLEX));

        String text = "{\n" +
                "    \"ext\": {\n" +
                "        \"netInfo\": {\n" +
                "            \"smartdns\": {\n" +
                "                \"lanv6\": [\n" +
                "                    \"syn6-wauchpgagbof5hv4osgp2qsmqm72aaaaaaaaaaaaqrgl774zmtya.wanmin.direct.quickconnect.cn\"\n" +
                "                ],\n" +
                "                \"host\": \"WANMIN.direct.quickconnect.cn\",\n" +
                "                \"lan\": [\n" +
                "                    \"192-168-0-2.WANMIN.direct.quickconnect.cn\"\n" +
                "                ],\n" +
                "                \"hole_punch\": \"127-0-0-1.WANMIN.direct.quickconnect.cn\"\n" +
                "            },\n" +
                "            \"ddns\": \"www.test.site\"\n" +
                "        }\n" +
                "    }\n" +
                "}";
        Object obj = JSONPath.read(text, "$.ext.netInfo.smartdns");
        Object obj2 = JSONPath.read(text, "$.ext.netInfo.ddns");
        System.out.println(obj);
        System.out.println(obj2);

        String abc = "$.netInfo.ddns";
        System.out.println(abc.substring(abc.lastIndexOf(".") + 1));
    }

    public static void main122333(String[] args) throws UnsupportedEncodingException {

        // regExp (?=.*我是谁)(?=.*C)^.*$
// java code
        List<String> list = Arrays.asList(new String[]{

                "PorosSecRoleController.updateUidAndOrg 请求参数：{\\\"admOrgList\\\":[],\\\"admPosCodes\\\":[],\\\"id\\\":\\\"3c613bb462fb4f25882fbaf1b96de043\\\",\\\"tenantId\\\":\\\"geek\\\",\\\"uids\\\":[\\\"13177778888\\\",\\\"15000004005\\\",\\\"13500000000\\\"],\\\"workOrgList\\\":[]}\\n\",\"stream\":\"stdout\",\"time\":\"2021-08-24T08:57:35.612869895Z\"}",
                "我是谁我是B",
                "我是谁我是C"
        });

        List<String> matches = new ArrayList<String>();
        String pattern = "(?=.*updateUidAndOrg)(?=.*3c613bb462fb4f25882fbaf1b96de043)^.*$";
        for (String word : list) {

            //包含我是谁且包含C
            if (word.matches(pattern))
                matches.add(word);
        }

        System.out.println(Arrays.toString(matches.toArray()));
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(pattern);
        System.out.println(builder.build().encode().toString());

        String regex = "[0-9a-z]";

//        Pattern p = Pattern.compile("-?[1-9]\\d*");
//        String text = "度ddd22222";
//        System.out.println(text.substring(0,8));
//        System.out.println(URLDecoder.decode("%E5%B0%8F%E8%8C%82%E5%B0%8F%E8%8C%82%E7%AC%AC%E5%8D%81+%E4%B8%80%E4%B8%AA%EF%BC%8C","UTF-8"));
//        String text = "19521086666";
//        String text2 = "as#$d.ggg@gmail.com";
//        Pattern pattern = Pattern.compile(mobile_regex);
//        Pattern email_pattern = Pattern.compile(email_regex);
//        Matcher m = pattern.matcher(text);
//        Matcher m2 = email_pattern.matcher(text2);
//        System.out.println(m.matches());
//        System.out.println(m2.matches());
//        String text = "<s><_107_>小茂小茂 </_107_> 打开所有灯光 </s>";
//        getReplaceOneshotText("", text, "小茂小茂");

//        try {
//            System.out.println("start....");
//            if(1 == 1){
//                throw new ServiceException("fffffffffffffffffffffffffffffffffffffff");
//            }
//        }catch (RuntimeException e){
//            System.out.println(e.getMessage());
//        }finally {
//            System.out.println("finally execute...");
//        }
    }

    //    public final static String email_regex = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
    public final static String email_regex = "(?:[0-9a-zA-Z_]+.)+@[0-9a-zA-Z]{1,13}\\.[com,cn,net]{1,3}";

    public final static String mobile_regex = "^1[3|4|5|7|8|9][0-9]{9}$";

    public final static String ONESHOT_WORD_START = "<_383_>";


    public final static String ONESHOT_WORD_END = "</_383_>";

    /**
     * 匹配数字
     */
    private static Pattern p = Pattern.compile("-?[1-9]\\d*");

    private static Pattern FIRST_PATTERN = Pattern.compile("^<s>\\s{0,10}(<_\\d{3,5}_>)([A-Za-z0-9_\\s_\\u4e00-\\u9fa5]+)(</_\\d{3,5}_>)");

    public static String getReplaceOneshotText(String sessionId, String text, String oneshotKey) {
        Matcher m = FIRST_PATTERN.matcher(text);
        String replaceText = null;//需要替换的字符
        String matchText = null;//匹配到的字符
        String domainStart = null;//
        String domainEnd = null;
        while (m.find()) {
            replaceText = m.group(0);
            domainStart = m.group(1);
            matchText = m.group(2);
            domainEnd = m.group(3);
            System.out.println(replaceText);
            System.out.println(domainStart);
            System.out.println(matchText);
            System.out.println(domainEnd);
        }
        if (!StringUtils.isEmpty(matchText)
                && !StringUtils.isEmpty(replaceText)
                && !domainStart.contains(ONESHOT_WORD_START)
                && matchText.contains(oneshotKey)) {
            // 如果正则匹配出的 字符串 和 唤醒词一致
            String replaceToText = replaceText.replaceFirst(domainStart, ONESHOT_WORD_START).replaceFirst(domainEnd, ONESHOT_WORD_END);
            text = text.replaceFirst(replaceText, replaceToText);
            System.out.println(replaceText + " change to " + replaceToText);
            System.out.println(text);
            return text;
        }
        return null;
    }

    /**
     * 处理包含数字的文本
     *
     * @param str
     * @return
     */
    public static String handleNumberStr(String str) {
        Matcher m = p.matcher(str);
        while (m.find()) {
            String num = m.group(); // 只替换一个 数字
            return str.replace(num, "%s");
        }
        return str;
    }

    /**
     * 过滤 换行符 \n 正则匹配
     */
    public static void filterLnMatch() {
        String line_2 = "<asr_contact>\n张三\n李四\n网络名称7\n</asr_contact>\n<channel>\n复仇者联盟\n战狼\n流浪地球\n</channel>\n";
        Pattern p = Pattern.compile("<(.*?)>(.*?)</(.*?)>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
        Matcher m = p.matcher(line_2);
        List<HashMap<String, String>> mList = new ArrayList<>();
        while (m.find()) {
            for (int i = 0; i <= m.groupCount(); i++) {
                if (i > 0) {

                    System.out.println(m.group(i));
                }
            }
        }
    }
}
