package org.xqh.test.k8sapi;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.Configuration;
import io.kubernetes.client.util.ClientBuilder;
import io.kubernetes.client.util.credentials.AccessTokenAuthentication;

/**
 * @ClassName K8sApiClient
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/5/18 10:56
 * @Version 1.0
 */
public class K8sApiClient {

    public final static String httpApi = "https://10.74.20.18/k8s/clusters/c-9468j";
    public final static String token = "kubeconfig-user-kmtkm.c-9468j:wzdgms9pv92gvqzrrlhn4vnh2nqd472g5ccfzhzqcn2cbr9cddj8z7";

    /**
     * k8s的初始化
     *
     * @return
     */
    public static ApiClient getConnection() {
        ApiClient client = new ClientBuilder().
                setBasePath(httpApi).setVerifyingSsl(false).
                setAuthentication(new AccessTokenAuthentication(token)).build();
        Configuration.setDefaultApiClient(client);
        return client;
    }
}
