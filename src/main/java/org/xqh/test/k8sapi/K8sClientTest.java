package org.xqh.test.k8sapi;

import io.kubernetes.client.openapi.ApiClient;
import io.kubernetes.client.openapi.ApiException;
import io.kubernetes.client.openapi.apis.*;
import io.kubernetes.client.openapi.models.V1APIGroup;
import io.kubernetes.client.openapi.models.V1APIGroupList;
import io.kubernetes.client.openapi.models.V1APIVersions;
import io.kubernetes.client.openapi.models.V1TokenReview;
import org.junit.Test;

/**
 * @ClassName K8sClientTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/5/18 10:58
 * @Version 1.0
 */
public class K8sClientTest {

    public static ApiClient defaultClient = K8sApiClient.getConnection();

    @Test
    public void test1(){
        OpenidApi apiInstance = new OpenidApi(defaultClient);
        try {
            String result = apiInstance.getServiceAccountIssuerOpenIDKeyset();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling OpenidApi#getServiceAccountIssuerOpenIDKeyset");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }


    }

    @Test
    public void test2(){
        CoreApi apiInstance = new CoreApi(defaultClient);
        try {
            V1APIVersions result = apiInstance.getAPIVersions();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling CoreApi#getAPIVersions");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }

    }
}
