package org.xqh.test;

import java.util.concurrent.CountDownLatch;

/**
 * @ClassName RunTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/4/3 12:35
 * @Version 1.0
 */
public class RunTest {

    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(3);
        System.out.println("比赛开始......");
        new Thread(new RunThread(3000, latch, "赛跑队员1")).start();
        new Thread(new RunThread(5000, latch, "赛跑队员2")).start();
        new Thread(new RunThread(8000, latch, "赛跑队员3")).start();
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("比赛结束......");
    }

    public static class RunThread implements Runnable{
        private long runTime;

        private CountDownLatch latch;

        private String runner;

        public RunThread(long runTime, CountDownLatch latch, String runner){
            this.runTime = runTime;
            this.latch = latch;
            this.runner = runner;
        }

        @Override
        public void run() {
            System.out.println(runner + "开始起跑....");
            try {
                Thread.sleep(runTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(runner + "跑到终点.... 耗时:"+ runTime/1000 + "秒");
            latch.countDown();
        }
    }
}
