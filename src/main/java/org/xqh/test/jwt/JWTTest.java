package org.xqh.test.jwt;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.collect.Maps;
import org.xqh.utils.DateUtil;
import lombok.extern.slf4j.Slf4j;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @ClassName JWTTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2019/6/26 14:16
 * @Version 1.0
 */
@Slf4j
public class JWTTest {

    public static void main(String[] args) {
        String secret = "dddsecret";
        JSONObject json = new JSONObject();
        json.put("id", 1L);
        json.put("ugreenNo", "6690");
        json.put("devType", "nas");
        Long start = System.currentTimeMillis();
        String token = buildJWT("abddffg", secret);
        Long end = System.currentTimeMillis();
        System.out.println("build jwt cost: "+(end - start));
        wrapperVerifyJWT(token, secret);
        System.out.println("parse jwt cost: "+(System.currentTimeMillis() - end));

        Long a = 35184372088832L;

    System.out.println(System.currentTimeMillis());
//        System.out.println(EncryptUtils.base64Decode(token));
//        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));
    }

    /**
     * JWT验证
     *
     * @param token
     * @return userName
     */
    public static String wrapperVerifyJWT(String token, String secretKey) {
        log.info("[verifyJWT] 解析token开始...token={}", token);
        String userName = "";
        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("UGREEN")
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            userName = jwt.getClaim("userName").asString();
        } catch (JWTVerificationException e) {
            log.error(e.getMessage(), e);
            return "";
        }
        return userName;
    }

    public static String buildJWT(String userName, String secretKey) {
        //生成jwt
        Date now = new Date();
        Algorithm algo = Algorithm.HMAC256(secretKey);
        String token = JWT.create()
                .withNotBefore(DateUtil.plusDate(new Date(), -15, ChronoUnit.MINUTES))
                .withIssuer("UGREEN")
                .withIssuedAt(now)
                .withExpiresAt(new Date(now.getTime() + 7200 * 1000))
                .withClaim("userName", userName)//保存身份标识
                .sign(algo);
        return token;
    }

    /**
     * 获取 Jwt请求头
     * @return
     */
    public static Map<String, Object> getJwtHead(){
        Map<String, Object> map = Maps.newHashMap();
        map.put("alg", "HS256");
        map.put("typ", "JWT");
        return map;
    }

    public static String getJwtToken(String appKey, String secret){
        Date now = new Date();
        Algorithm algorithm = Algorithm.HMAC256(secret);
        String token = JWT.create().withHeader(getJwtHead())
                .withClaim("appKey", appKey)
                .withIssuer("hotel-service")
                .withSubject("auth token")
                .withAudience("app") // 签名的观众 可以理解为 谁接收签名
                .withIssuedAt(now) // 生成签名时间
                .withExpiresAt(DateUtil.plusDate(now, 1, ChronoUnit.MINUTES)) // 有效期30分钟
                .sign(algorithm);
        return token;
    }

    public static boolean verfierJwt(String token, String secret){
        Algorithm algorithm = Algorithm.HMAC256(secret);
        JWTVerifier verifier = JWT.require(algorithm)
//                .withIssuer("hotel-service")
                .build();
        DecodedJWT jwt = verifier.verify(token);


        // 可以从 token中获取 签名的主题, 观众 和自主定义的声明信息.
        String subjet = jwt.getSubject();
        List<String> audience = jwt.getAudience();
        Map<String, Claim> claims = jwt.getClaims();
        System.out.println(claims.get("appKey").asString());
        log.info("subject ==>{}, audience ==>{}, claims ==>{}", subjet, JSON.toJSONString(audience), JSON.toJSONString(claims));

        return true;
    }
}
