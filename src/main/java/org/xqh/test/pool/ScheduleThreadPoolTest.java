package org.xqh.test.pool;

import com.google.common.collect.Sets;

import java.util.Date;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * @ClassName ScheduleThreadPoolTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/12/2 11:25
 * @Version 1.0
 */
public class ScheduleThreadPoolTest {

    public static void main(String[] args) {
        Date start = new Date();

        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();

        }
        Date end = new Date();

        System.out.println(end.getTime() - start.getTime());
    }

    public static AtomicLong siz = new AtomicLong(0);
    public static Long siz2 = new Long(0);
    public static void main123(String[] args) {
//        String code = "0,-11,-16,-18,13000";
//        Set<Integer> codeSet = Sets.newHashSet(code.split(",")).stream().map(s-> Integer.parseInt(s)).collect(Collectors.toSet());
//        System.out.println(codeSet.contains(-16));
//        System.out.println(codeSet.contains(13000));
          for(int i = 0; i < 100000; i++){
              new Thread(new Runnable() {
                  @Override
                  public void run() {
                      siz.addAndGet(10L);
                      siz2 = siz2 + 10L;
                  }
              }).start();
          }

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(siz.intValue());
        System.out.println(siz2.intValue());

    }

    public static void main211222(String[] args) {

        BlockingQueue<Integer> removeQueue = new LinkedBlockingQueue<>(100);

        for(int i = 0; i < 200; i ++){
            removeQueue.add(i);
        }

        while (!removeQueue.isEmpty()){
            System.out.println(removeQueue.poll());
        }
    }

    public static void main2222(String[] args) {
        //异步执行线程池
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(5);

//        pool.schedule(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("延迟执行");
//            }
//        },2, TimeUnit.SECONDS);

//        /**
//         * 这个执行周期是固定，不管任务执行多长时间，还是每过3秒中就会产生一个新的任务
//         */
//        pool.scheduleAtFixedRate(new Runnable() {
//            @Override
//            public void run() {
//                //这个业务逻辑需要很长的时间，定时任务去统计一张数据上亿的表，财务财务信息，需要30min
//                System.out.println("重复执行1");
//            }
//        },1,3, TimeUnit.SECONDS);
//
        /**
         * 假设12点整执行第一次任务12:00，执行一次任务需要30min，下一次任务 12:30 + 3s 开始执行
         */
        pool.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                //30min
//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                System.out.println("" + new Date() +"重复执行2");
            }
        },1, 3, TimeUnit.SECONDS);
    }
}
