package org.xqh.test.jni.callback;

/**
 * @ClassName MyJavaClass
 * @Description TODO
 * @Author xuqianghui
 * @Date 2025/1/8 13:48
 * @Version 1.0
 */
// CallbackExample.java
public class CallbackExample {
    // Declare the native method
    public native void nativeMethod();

    static {
        // Load the shared library
        System.loadLibrary("callback");
    }

    // Method to be called from Golang
    public void callback() {
        System.out.println("Callback method called from Golang!");
    }

    public static void main(String[] args) {
        new CallbackExample().nativeMethod();
    }
}
