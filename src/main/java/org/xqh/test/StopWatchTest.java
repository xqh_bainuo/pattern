package org.xqh.test;

import org.springframework.util.StopWatch;

/**
 * @ClassName StopWatchTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/8/3 11:22
 * @Version 1.0
 */
public class StopWatchTest {

    public static void main(String[] args) throws InterruptedException {
        StopWatch stopWatch = new StopWatch("test");
        stopWatch.start("dbbb");
        Thread.sleep(1000);
        stopWatch.stop();
        stopWatch.start("eeeee");
        Thread.sleep(1200);
        stopWatch.stop();
        System.out.println(stopWatch.prettyPrint());
        System.out.println(stopWatch.getTaskCount());
    }
}
