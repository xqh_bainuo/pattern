package org.xqh.test;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @ClassName Test3
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/3/16 18:06
 * @Version 1.0
 */
public class Test3 {

    public static void main(String[] args) {
//        HandleUserDeptPositionModel mode = HandleUserDeptPositionModel.builder().build();
//        System.out.println(JSON.toJSONString(mode));
        System.out.println("包含自定义信息的rsa jwt:");
        printStrLen("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYWxsIl0sInVpIjoiNTUwMDA6NDcwOTM6MTgwMDAwMDAwMDE6bmFzIiwidXNlcl9uYW1lIjoiMTgwMDAwMDAwMDEiLCJzY29wZSI6WyJuYXMiXSwiaXNzIjoiVUdSRUVOIiwiZXhwIjoxNzMzOTM4MTY1LCJqdGkiOiIxODA1MTk0OC0yYmQyLTRjODEtYjc4OC0wODFjMmQ4YTQ3M2YiLCJjbGllbnRfaWQiOiJjMSJ9.g268McgS7CIUMv15lchoHEPc_72cDcp6VELp_V90MMOTodMA-NSrfQXltPdtWetUKtcGU8i1EWASdTbf83vIFpANr5JklKBGGRPW2LPE4WdVsxY7psl7xaFJp9DIKayUKT34uGByXTyF_QUJCAeo5V248GcvAaKAdNpihQJQSNUgXl3DKaUTxjWJJDiIUXgu8TIESuM53VHazzibc9QUaL6UusvWfV24yBa8FY_kgW4mcFLkd2gRIPjCMqdXPnz_OP61vFeaRecTEuGVquxtH9tDs50QuPJb2jBwI4kOvksWZwBIHyB22q26kUgrqp2BOPYt1LggbbVq-Gg2q_1gUw");
        System.out.println("包含自定义信息的签名 jwt:");
        printStrLen("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYWxsIl0sInVpIjoiNTUwMDA6NDcwOTM6MTgwMDAwMDAwMDE6bmFzIiwidXNlcl9uYW1lIjoiMTgwMDAwMDAwMDEiLCJzY29wZSI6WyJURVNUMiJdLCJpc3MiOiJVR1JFRU4iLCJleHAiOjE3MzM5MjkwMjQsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXSwianRpIjoiMzU5NjkwNTktYWM4Yi00OGE4LWIyYzYtMWZkMDk4ZjI3NGY3IiwiY2xpZW50X2lkIjoiYzEifQ.xlLY77pf0PWCCar1vssqpvNTH-dTBb-7o7wn5oo1wmE");
        System.out.println("旧版本 jwt:");
        printStrLen("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYmYiOjE2NjE5Mzc1MjYsInVpIjoiNTUwMDA6NDcwOTM6MTgwMDAwMDAwMDE6bmFzIiwiaXNzIjoiVUdSRUVOIiwiZXhwIjoxNzMzOTM4NDI2LCJpYXQiOjE2NjE5Mzg0MjZ9.uT6uEaiRZAOssYjLE9w67S2F3e5rHGGWqeVo80h8JxA");

        System.out.println("不包含自定义信息的rsa jwt: ");
        printStrLen("eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYWxsIl0sInVpIjoiNTUwMDA6NDcwOTMiLCJ1c2VyX25hbWUiOiI1NTAwMCIsInNjb3BlIjpbIm5hcyJdLCJleHAiOjE3MzM5OTkxNDUsImp0aSI6Ijg2ZWRlYzBjLWZmOTQtNDgwMy1iZDZhLTExNTE0YTI3YTcxYyIsImNsaWVudF9pZCI6ImMxIn0.hnOV-BfJikQDA55VFO6C8hiun56cVPasg6DuZAYWLYoSQnbDYPvenTHSD5l8b46ErQdtKoJmoajLY3ujhZcyVaeaT_27IQ7LBK1f9-Hj6RYgMdKvMxcMSzpeiQwBX1dWNXv02JfcADFeX49Sabr9TB1RIy8-a6UNpGBmRxraKokGWuTD-ouDl7Drn94d-HkdGRRxdd4X407L6NtxFQPeqDQF8g3aybuH3h54DDrlsXxfbbWkxNo7caeQFEhse7fO7TdKF5vM2OeFtppBcaIvulZsyI8eaABqlz7Ith36bSEzoDmAhPZUDlLjm5YvFGK73m-82stHThfKyz29yP-MhQ");

    }

    public static void printStrLen(String token){
        System.out.println(token.getBytes().length);
    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class HandleUserDeptPositionModel{
        private String deptName;//部门名称
        private String position;//职位名称
        private String tenantId;//租户标识
        private String positionId;//职位ID
        private List<String> positionIds;//职位ID集合
        private List<String> orgIds;//部门ID集合
        private boolean createNoExistsData = true;//不存在的 部门 职位 是否创建 (默认创建)
    }
}
