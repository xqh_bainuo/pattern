package org.xqh.test;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Sets;
import org.xqh.utils.excel.ExcelReader;
import org.xqh.utils.encrypt.EncryptUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Auther: leo
 * @Date: 2018/7/16 14:29
 * @Description: 计算工具类
 */
public class NumberUtils {

    /**
     * 减法 默认两位小数点
     * @param a
     * @param b
     * @return
     */
    public static BigDecimal subtract(BigDecimal a, BigDecimal... b){
        BigDecimal res=a;
        for(BigDecimal t:b){
            if(null != res){
                res=subtract(res,t,2);
            }
        }
        return res;
    }

    public static BigDecimal subtract(BigDecimal a,BigDecimal b,int scale){
        if(null != a && null != b){
            return a.subtract(b).setScale(scale,BigDecimal.ROUND_HALF_UP);
        }
        return null;
    }

    /**
     * 加法默认两位小数
     * @param a
     * @param b
     * @return
     */
    public static BigDecimal add(BigDecimal a,BigDecimal... b){
        BigDecimal res=a;
        for(BigDecimal t:b){
            if(null != res){
                res=add(res,t,2);
            }
        }
        return res;
    }

    public static BigDecimal add(BigDecimal a,BigDecimal b, int scale){
        if(null != a && null != b){
            return a.add(b).setScale(scale,BigDecimal.ROUND_HALF_UP);
        }
        return null;
    }

    /**
     * 乘法 默认保留两位
     * @param a
     * @param b
     * @return
     */
    public static BigDecimal multiply(BigDecimal a, BigDecimal b){
        return multiply(a,b,2);
    }

    /**
     * 乘法
     * @param a
     * @param b
     * @param scale 小数点位数
     * @return
     */
    public static BigDecimal multiply(BigDecimal a, BigDecimal b, int scale){
        if(null != a && null != b){
            return a.multiply(b).setScale(scale,BigDecimal.ROUND_HALF_UP);
        }
        return null;
    }

    /**
     * 除法 默认保留两位小数点
     * @param a
     * @param b
     * @return
     */
    public static BigDecimal divide(BigDecimal a, BigDecimal b){
        return divide(a,b,2);
    }


    /**
     * 除法
     * @param a
     * @param b
     * @param scale 小数点位数
     * @return
     */
    public static BigDecimal divide(BigDecimal a, BigDecimal b, int scale){
        if(null != a && null != b){
            return a.divide(b,scale, BigDecimal.ROUND_HALF_UP);
        }
        return null;
    }

    /**
     * 设置小数点位数
     * @param a
     * @return
     */
    public static BigDecimal setScale(BigDecimal a){
        return setScale(a,2);
    }

    /**
     * 设置小数点位数
     * @param a
     * @param scale
     * @return
     */
    public static BigDecimal setScale(BigDecimal a, int scale){
        if(null != a){
            return a.setScale(scale,BigDecimal.ROUND_HALF_UP);
        }
        return null;
    }

    private static boolean isMatch(String regex, String orginal){
        if (orginal == null || orginal.trim().equals("")) {
            return false;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher isNum = pattern.matcher(orginal);
        return isNum.matches();
    }

    public static boolean isPositiveInteger(String orginal) {
        return isMatch("^\\+{0,1}[1-9]\\d*", orginal);
    }

    public static boolean isNegativeInteger(String orginal) {
        return isMatch("^-[1-9]\\d*", orginal);
    }

    public static boolean isWholeNumber(String orginal) {
        return isMatch("[+-]{0,1}0", orginal) || isPositiveInteger(orginal) || isNegativeInteger(orginal);
    }

    public static boolean isPositiveDecimal(String orginal){
        return isMatch("\\+{0,1}[0]\\.[1-9]*|\\+{0,1}[1-9]\\d*\\.\\d*", orginal);
    }

    public static boolean isNegativeDecimal(String orginal){
        return isMatch("^-[0]\\.[1-9]*|^-[1-9]\\d*\\.\\d*", orginal);
    }

    /**
     * 判断是否是 小数
     * @param orginal
     * @return
     */
    public static boolean isDecimal(String orginal){
        return isMatch("[-+]{0,1}\\d+\\.\\d*|[-+]{0,1}\\d*\\.\\d+", orginal);
    }

    public static boolean isRealNumber(String orginal){
        return isWholeNumber(orginal) || isDecimal(orginal);
    }

    public static String[] numbers = {"零", "一", "二", "三", "四", "五", "六", "七", "八", "九"};
    public static String[] units = {"", "十", "百", "千", "万", "十", "百", "千", "亿", "十"};
    public final static String point = ".";//小数点
    public final static String point_chinese = "点";

    public final static String number_regex = "([1-9]\\d*\\.?\\d*)|(0\\.\\d*[1-9])";

    private static String numberToChinese(int number, boolean hasPoint) {
        String sign = number < 0 ? "负" : "";
        if (number < 0) {
            number = -number;
        }
        StringBuilder result = new StringBuilder(sign);
        String string = String.valueOf(number);
        int n = string.length();
        char[] numberCharArray = string.toCharArray();
        for (int i = 0; i < n; i++) {
            int digNum = n - i; // 位数
            int num = numberCharArray[i] - '0';
            if (num != 0) {
                result.append(numbers[num]).append(units[digNum - 1]);
                continue;
            }

            if (result.toString().endsWith(numbers[0])) {
                // 如果是单位所在的位数，则去除上一个0，加上单位
                if (digNum % 4 == 1) {
                    result.deleteCharAt(result.length() - 1);
                    result.append(units[digNum - 1]);
                }
            }else{
                result.append(numbers[0]);
            }
        }
        String retStr = result.toString();
        // 处理两位数的问题
        if(string.length() == 2){
            if(number%10 == 0){// 整数 去除 后面的 "零"
                retStr = retStr.substring(0, retStr.length()-1);
            }

            if(number < 20){ //如果 是 10 - 20 之间的数 去除 第一个 "一"
                retStr = retStr.substring(1);
            }

        }

        if(number == 2 && !hasPoint){
            retStr = "两";// 处理 2...
        }

        return retStr;
    }

    /**
     * 数字 转中文
     * @param number
     * @return
     */
    public static String changeNumToChinese(String number){
        StringBuffer retStr = new StringBuffer();
        if(!number.contains(point)){
            return numberToChinese(Integer.parseInt(number), false);
        }
        // 处理包含小数点情况

        //整数部分
        String intNum = number.substring(0, number.indexOf(point));
        String smallNum = number.substring(number.indexOf(point)+1);//小数部分
        retStr.append(numberToChinese(Integer.parseInt(intNum), true))
                .append(point_chinese)
                .append(decimalPoint(smallNum));
        return retStr.toString();
    }

    /**
     * 处理小数点
     * @param num
     * @return
     */
    public static String decimalPoint(String num){
        if(!isPositiveInteger(num)){
            // 如果不是数字 直接返回
            return "";
        }
        String[] list = num.split("");
        StringBuffer retStr = new StringBuffer();
        for(String n:list){
            int idx = Integer.parseInt(n);
            retStr.append(numbers[idx]);
        }
        return retStr.toString();
    }

    public static String removeSymbol(String text){
        text = text.replaceAll("。", "")
                .replaceAll(" ", "")
                .trim();
        return text;
    }

    /**
     * 比对 tts文本 和 asr识别结果
     * @param tts
     * @param asrRet
     * @return
     */
    public static boolean compareTtsAsrRet(String tts, String asrRet){
        if(StringUtils.isEmpty(asrRet)){
            return false;
        }
        tts = removeSymbol(tts);
        asrRet = removeSymbol(asrRet);
        String ttsChange = changeTextReplaceNum(tts);
        String asrRetChange = changeTextReplaceNum(asrRet);
//        System.out.println("tts ==> "+tts+", asr==> "+asrRet+"");
//        System.out.println("ttsChange ==> "+ttsChange+", asrChange==> "+asrRetChange+"");
        return asrRetChange.toUpperCase().contains(ttsChange.toUpperCase());
    }

    /**
     *
     * @param text
     * @return
     */
    public static String changeTextReplaceNum(String text){
        Pattern p = Pattern.compile(number_regex);
        Matcher m = p.matcher(text);
        while (m.find()){
            String t = m.group();
            text = text.replaceFirst(t, changeNumToChinese(t));
        }
        return text;
    }

    public static void main(String[] args) {
        Set<String> prefixSet = Sets.newHashSet("".split(","));
        System.out.println(prefixSet);
        System.out.println(EncryptUtils.getMd5(EncryptUtils.getMd5("admin")));
//        System.out.println(isRealNumber("12.1s1"));
//        System.out.println(Double.valueOf("12"));
//        System.out.println(EncryptUtils.getMd5("unisound@123!@#"));
//        compareTtsAsrRet("你好平安那就借2万吧", "你好平安那就借两万吧。");
        List<String[]> array = ExcelReader.getExcelData(new File("E:\\document\\yzs\\program\\shimao-iot\\shimao-pms\\房间导入test - 副本.xlsx"), 1);
        System.out.println(JSON.toJSONString(array));
    }
}
