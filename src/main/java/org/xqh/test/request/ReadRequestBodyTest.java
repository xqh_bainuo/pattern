package org.xqh.test.request;

import cn.hutool.core.io.IoUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @ClassName ReadRequestBodyTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/8/24 14:41
 * @Version 1.0
 */
public class ReadRequestBodyTest {

    /**
     *
     * @param request
     * @throws IOException
     */
    public static void readRequestBody(HttpServletRequest request) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String body = IoUtil.read(reader);
        System.out.println("request body ===>"+body);
    }
}
