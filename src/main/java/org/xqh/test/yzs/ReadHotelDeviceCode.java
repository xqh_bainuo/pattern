package org.xqh.test.yzs;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.xqh.utils.file.ReadTxtFileUtils;
import lombok.Data;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName ReadHotelDeviceCode
 * @Description 读取 酒店设备编号
 * @Author xuqianghui
 * @Date 2020/10/26 9:39
 * @Version 1.0
 */
public class ReadHotelDeviceCode {

    public static final String work_path = "E:\\document\\yzs\\program\\shimao-iot\\";
    public static void main(String[] args) {
//        String text = "{\"武夷山世茂御榕庄酒店\":\"\",\"测试dd酒店\":\"D:/TestSpace/device-warn/test.txt\"}";
//        JSONObject json = JSON.parseObject(text);

        File file = new File(work_path+"武汉凡象.txt");
        String json = ReadTxtFileUtils.readJson(file);
        JSONObject obj = JSON.parseObject(json);
        List<DeviceModel> list = JSONArray.parseArray(obj.getString("data"), DeviceModel.class);
        List<String> strList = list.stream().map(DeviceModel::getDeviceCode).collect(Collectors.toList());
        ReadTxtFileUtils.writeToTxt(strList, work_path+"fanxiang.txt");
    }

    @Data
    public static class DeviceModel{

        private String deviceName;
        private String deviceCode;
    }
}
