package org.xqh.test.yzs;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName ThreadTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2019/8/31 12:16
 * @Version 1.0
 */
@Slf4j
public class ThreadTest {

    private static boolean is_success = false;

    private static AtomicInteger atomic = new AtomicInteger(0);

    public static void main(String[] args) throws InterruptedException, ExecutionException {
//        new ThreadTest().testExecute();

        CountDownLatch latch = new CountDownLatch(1);
        InterruptThreadTest test = new InterruptThreadTest(latch);
        FutureTask<String> future = new FutureTask<>(test);
        Thread t1 = new Thread(future);
        t1.start();
        latch.await(5, TimeUnit.SECONDS);
        t1.interrupt();

        System.out.println("closed "+future.get());

        Thread.sleep(50000);
    }

    public void testExecute() {
        log.info("{} is executing..", Thread.currentThread().getName());
        new Thread(() -> {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            testExecute();
        }, "test_thread_" + atomic.incrementAndGet()).start();
    }


    public static class InterruptThreadTest implements Callable<String> {

        private CountDownLatch latch;

        public InterruptThreadTest(CountDownLatch latch){
            this.latch = latch;
        }

        @Override
        public String call() throws Exception {
            int i = 0;
            while (true){
                i ++;
                System.out.println("=======>"+System.currentTimeMillis());
                if (i >= 6){
                    break;
                }
                Thread.sleep(1000);
            }
            latch.countDown();
            return "OK";
        }
    }
}
