package org.xqh.test.yzs.wakeupaudiocollector;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName WakeAudioCollectorTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/2/17 10:51
 * @Version 1.0
 */
public class WakeAudioCollectorTest {
    public static void main(String[] args) throws Exception {
        String text =
//                "你好魔方-3.2_123123";
                "你好魔方测试";
        String encodeTxt = URLEncoder.encode(text, "UTF-8");

        String file = "E:\\0\\";
        byte[] sdata = Files.readAllBytes(new File("D:\\TestSpace\\forTest\\你好小美.pcm").toPath());
        String url ="http://192.168.3.240:8281/uc/m/v2/upload";
        //"http://10.20.222.128:8777/uc/m/v2/upload";
        // url = "http://127.0.0.1:8080/uc/m/v2/upload";
        //url = "http://127.0.0.1:8080/uc/m/upload";
        // url = "http://10.20.222.128:8777/uc/m/upload";

        // 公有云的appkey和secret rxme5kxzyaaujkrdskrufkmcswodxoa4k2ondgqj：b82a378808c253bdc58ab86413d21068
        Map<String, String> header = new HashMap<>();
        header.put("K", "cyrfjmalvdvtiwpibypvx26pkizyyacyj6rvifiy");
        header.put("UI", "test002");
        header.put("U", "");
        header.put("WakeupWord", encodeTxt);
        header.put("Text", encodeTxt);
        header.put("Timestamp", System.currentTimeMillis() + "");
        header.put("AC", "pcm");
        header.put("Score", "88.8");

        String sign = getSign(header.get("K") + "" +
                "" + header.get("UI") + "" +
                "" + header.get("U") + "" +
                "" + text + "" +
                "" + header.get("Timestamp") + "" +
                "" + header.get("AC") + "" +
                "" + "ed7123f98ee6ffdc70d0d1c018742008" + "" +
                "" + header.get("Score"));

        header.put("Signature", sign);

        byte[] result = httpPost(url, sdata, header, -1 , -1);
        System.out.println(new String(result));
    }

    public static String getSign(String data) {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            byte[] bytes = md.digest(data.getBytes("UTF-8"));
            digest = byte2hex(bytes);
        } catch (Exception e) {

        }
        return digest;
    }

    private static String byte2hex(byte[] bytes) {
        StringBuilder sign = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(bytes[i] & 0xFF);
            if (hex.length() == 1) {
                sign.append("0");
            }
            sign.append(hex.toUpperCase());
        }
        return sign.toString();
    }

    public static String getSign(List<String> params) {
        String paramStr = "";

        for (String tmp : params) {
            if (tmp != null) {
                paramStr += tmp;
            }
        }

        return getSign(paramStr);
    }

    public static byte[] stream2Bytes(InputStream in) {
        byte[] bytes = null;

        try {
            ByteArrayOutputStream bufferStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[2048];
            boolean var4 = false;

            int len;
            while((len = in.read(buffer)) != -1) {
                bufferStream.write(buffer, 0, len);
            }

            if (bufferStream.toByteArray().length > 0) {
                bytes = bufferStream.toByteArray();
            }

            bufferStream.close();
            bufferStream = null;
            in.close();
            return bytes;
        } catch (IOException var5) {
            var5.printStackTrace();
            return null;
        }
    }

    public static byte[] httpPost(String url, byte[] sdata, Map<String, String> requestParam, int connectTimeout, int readTimeout) throws Exception{
        OutputStream out = null;

        URL uRL = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) uRL.openConnection();

        conn.setRequestMethod("POST");

        if(connectTimeout > 0){
            conn.setConnectTimeout(connectTimeout);
        }

        if(readTimeout > 0){
            conn.setReadTimeout(readTimeout);
        }

        if(null != requestParam && requestParam.size() > 0){
            for(String key : requestParam.keySet()){
                conn.addRequestProperty(key, requestParam.get(key) + "");
            }
        }

        if(null != sdata){

            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);

            out = conn.getOutputStream();
            out.write(sdata);
            out.flush();
        }

        conn.connect();

        byte[] data = stream2Bytes(conn.getInputStream());

        if(out != null){
            out.close();
        }

        if(null != data){
            return data;
        }
        return null;
    }
}
