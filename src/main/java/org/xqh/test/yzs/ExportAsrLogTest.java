package org.xqh.test.yzs;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.xqh.test.Test;
import org.xqh.test.yzs.nlu.NLURequestUtils;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.file.ReadTxtFileUtils;

import java.io.File;
import java.util.List;

/**
 * @ClassName ExportAsrLogTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/4/20 14:17
 * @Version 1.0
 */
public class ExportAsrLogTest {

    public static String path = "E:\\document\\yzs\\program\\工作交接\\file\\";

    public static void main(String[] args) {
        String json = ReadTxtFileUtils.readJson(new File(path + "new 3.txt"));
        JSONObject obj = JSONObject.parseObject(json);
        List<TestM> list = JSONArray.parseArray(obj.getString("data"), TestM.class);
        for(TestM t:list){
            t.setNluLog(NLURequestUtils.reqNluLog(t.getRequest_id()));
        }
        String[] headers = new String[]{"文本", "nlu日志", "udid", "requestId", "时间"};
        String[] properties = new String[]{"core_server_log", "nluLog", "userId", "request_id", "start_time"};
        String newFile = path.concat("导出结果");
        ExcelExportUtil.export(headers, properties, list, false, newFile);
    }

    @Data
    public static class TestM{
        private String core_server_log;
        private String userId;
        private String request_id;
        private String nluLog;
        private String start_time;
    }
}
