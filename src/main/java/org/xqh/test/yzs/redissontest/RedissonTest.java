package org.xqh.test.yzs.redissontest;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;

import java.util.*;

/**
 * @ClassName RedissonTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/4/8 18:02
 * @Version 1.0
 */
public class RedissonTest {

    private static List<String> randomList = Arrays.asList("LxmGFcEtB4","Yq18SPF4Jj","U7BjDjOW13","PLP5kdvQsL","D1kHGqF8Vc","OqfxXFnG9t","wcDPfoDvEj","jkIcDbDTZ5");

    public static void main(String[] args) {
        String requestUrl = "http://localhost:8822/lock/t2";
        Random random = new Random();
        for(int i = 0; i< 5; i++){
            new Thread(()-> {
                int idx = random.nextInt(randomList.size());
                String name = randomList.get(1);
                Map<String, Object> map = new HashMap<String, Object>(){{
                    put("name", name);
                }};
                HttpConfig config = HttpConfig.custom()
                        .url(requestUrl)
                        .method(HttpMethods.POST)
                        .map(map);
                try {
                    HttpResult httpRet = HttpClientUtil.sendAndGetResp(config);
                    System.out.println("请求参数name= "+name+", 请求结果==========>"+httpRet.getResult());
                } catch (HttpProcessException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
