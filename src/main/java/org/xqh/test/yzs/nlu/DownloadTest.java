package org.xqh.test.yzs.nlu;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import org.apache.http.Header;
import org.xqh.utils.http.FxHttpClientUtils;

/**
 * @ClassName DownloadTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/12/14 13:55
 * @Version 1.0
 */
public class DownloadTest {

    public static void main(String[] args) throws HttpProcessException {

        String url = "https://ugreentest.oss-cn-shenzhen.aliyuncs.com/UGREEN_Nas_v3.11.0.6255_release.exe";
        Header[] headers = HttpHeader.custom()
                .other("Referer", "https://cloud2.ugreengroup.com/")
                .build();
        HttpConfig config = FxHttpClientUtils.getGetConfig(url, headers);
        HttpClientUtil.sendAndGetResp(config);
    }
}
