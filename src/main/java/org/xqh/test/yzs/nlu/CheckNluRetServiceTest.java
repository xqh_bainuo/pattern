package org.xqh.test.yzs.nlu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONPath;
import com.google.common.collect.Lists;
import org.xqh.test.yzs.nlu.NLURequestUtils.NluReqResult;
import org.xqh.test.yzs.nlu.NLURequestUtils.ReqNluThread;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.excel.ExcelReader;
import org.xqh.utils.ThreadPoolUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.stream.Collectors;

/**
 * @ClassName CheckNluRetServiceTest
 * @Description 校验 nlu service 结果
 * @Author xuqianghui
 * @Date 2019/12/24 14:10
 * @Version 1.0
 */
@Slf4j
public class CheckNluRetServiceTest {


    private final static String work_path = "E:\\document\\yzs\\program\\世茂\\电视项目\\";

    private final static  String tv_service = "cn.yunzhisheng.setting.tv";

    public static void main(String[] args) {
        List<AsrResult> asrRetList = parseAsrRetList();
        for(AsrResult r:asrRetList){
            FutureTask<NluReqResult> future = new FutureTask<>(new ReqNluThread(r.getTts()));
            ThreadPoolUtils.submit(future);
            r.setFuture(future);
        }
        for(AsrResult m:asrRetList){
            try {
                NluReqResult nluRet = m.getFuture().get();
                m.setNluRet(nluRet.getReqRet());
                m.setNluReqUrl(nluRet.getReqUrl());
            } catch (InterruptedException e) {
                log.error("", e);
            } catch (ExecutionException e) {
                log.error("", e);
            }
            String service = JSONPath.read(m.getNluRet(),"$.service").toString();
            m.setService(service);
            m.setTv(service.equals(tv_service));
        }
        String[] headers = {"tts文本", "asr识别结果", "识别是否正确", "sessionId", "nlu结果", "service", "nlu请求链接"};
        String[] properties = {"tts", "asrRet", "isCorrect", "sessionId", "nluRet", "service", "nluReqUrl"};
        String filePath = work_path+ "nluResult5.xlsx";
        System.out.println("===========>"+JSON.toJSONString(
                asrRetList.stream().filter(a-> !a.getService().equals(tv_service)).map(AsrResult::getTts).collect(Collectors.toList())
        ));
        ExcelExportUtil.export(headers, properties, asrRetList, false, filePath);
    }

    public static List<AsrResult> parseAsrRetList(){
        List<AsrResult> resList = Lists.newArrayList();
        List<String[]> list = ExcelReader.getExcelData(new File(work_path+"电视结果333-all-result.xlsx"), 1);
        for(String[] array:list){
            if(Objects.nonNull(array) && array.length >= 4 && StringUtils.hasText(array[0])){
                resList.add(AsrResult.builder()
                        .tts(array[0])
                        .asrRet(array[1])
                        .isCorrect(array[2])
                        .sessionId(array[3])
                        .build());
            }
        }
        return resList;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class AsrResult{
        private String tts;

        private String asrRet;

        private String isCorrect;

        private String sessionId;

        private String nluRet;

        private String nluReqUrl;

        private String service;

        private FutureTask<NluReqResult> future;

        private boolean isTv;

    }
}
