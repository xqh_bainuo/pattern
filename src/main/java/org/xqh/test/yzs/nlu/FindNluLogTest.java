package org.xqh.test.yzs.nlu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.xqh.utils.DateUtil;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.excel.ExcelReader;
import org.xqh.utils.file.ReadTxtFileUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @ClassName FindNluLogTest
 * @Description 提取nlu日志信息
 * @Author xuqianghui
 * @Date 2019/8/22 11:13
 * @Version 1.0
 */
@Slf4j
public class FindNluLogTest {

    private static List<String> matchList = Arrays.asList("NLU url:", "nlu result:");

    private static String pattern = "yyyy-MM-dd HH:mm:ss";

    private static Date start = DateUtil.parseDate("2019-08-23 00:00:00", pattern);

    private static Date end = DateUtil.parseDate("2019-08-23 23:59:59", pattern);

    private static Map<String, MatchResult> logMap = Maps.newConcurrentMap();

    private static List<String> udidList = Arrays.asList(
            "LTY2OTg5Mjk4NTAwRUNFMTU0RjAyNkMz",
            "LTY2OTg5Mjk4NTAwRUNFMTU0RjAyNkM4",
            "LTY2OTg5Mjk4NTAwRUNFMTU0RjAwMzQw",
            "LTY2OTg5Mjk4NTAwRUNFMTU0RjAyNjkx",
            "LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMjY5MQ",
            "LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMjZDMw",
            "LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMjIxRA");
//    private static List<String> udidList = Arrays.asList(
//            "LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMjZCRA",
//            "LTY2OTg5Mjk4NTAwRUNFMTU0RjAyNkM1",
//            "LTY2OTg5Mjk4NTAwRUNFMTU0RjAyNkJE",
//            "LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMjZDNQ");

    private final static String regex = "(\\[).*?(\\])";

    private final static String udid_regex = "([&|;]udid=)+([A-Za-z0-9])+[&|;]";

    private final static String appKey_regex = "(appKey=)+([A-Za-z0-9])+[,]";//解析appKey

    private final static String work_path = "E:\\secureCRT_file\\数据标注\\";

    private final static String out_path = "E:\\document\\secureCRT\\4222-23-24.asrcheck";

    private final static String audio_path = "E:\\document\\secureCRT\\audio";

    private static Map<String, File> mp3Map = Maps.newHashMap();

    private static List<String> nluContentFilterList = Arrays.asList("]NLU url:http://", "nlu result:{", "]nlu error:");

    private static List<String> udidFilter = Arrays.asList("udid=LTQ4NTIzODEzOTAwOENGNzEwODQ0MTVF");

    public static void main333(String[] args) {
        String date = "2019-11-01 23:47:18,734";

        System.out.println(DateUtil.parseDate(date, DateUtil.DATE_PATTERN_COMPLEX_MILLI).getTime());
    }

    public static void main(String[] args) {
//        List<String> udidList = getHotelUdid();
////        filterOneDayData("2019-10-14", udidList);
//        filterOneDayData("2019-10-22", udidList);
//        String path = "E:\\secureCRT_file\\download\\";
//        analysisNluReqCostTime(path.concat("webapi_debug.log"),
//                path.concat("output11"));
        String appLog = "2020-04-28 16:21:53,071:DEBUG http-nio-8080-exec-14 (CommonFilter.java243) - [bfd40968bb68e14f2afd1cc9d1a7b123]requestParam: {serviceType=asr, remoteIP=114.236.137.226, trafficParameter=filterName=nlu3;returnType=json;city=ËÕÖÝÊÐ;gps=31.666952,120.773764;time=2020-04-28 16:21:48;scenario=hotelDefault;screen=;dpi=;history=;udid=LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMUVCOQ;ver=3.2;appver=;oneshotKeyProperty=wakeup;additionalService=athenaAppService;req_nlu_length=1;appendLength=1;fullDuplex=false;audioUrl=false;filterUrl=http://route.igaicloud.cn:8088/hotel-service/process//tr/dataProcess;, requestID=bfd40968bb68e14f2afd1cc9d1a7b123, imei=c5f40b460ea48e1f, appKey=nmugoqugf3ikbhkhbaixhefxdinqcmgyhobsvjiv, ClientInfo=PN=com.unisound.aios.pandora.clock:OS=0:CR=0:NT=1:MD=A1:SV=v4.3.0_for_yunmao,1645022b:RPT=0:SID=ce15cee2092b21f04e28e873ac094448:NPT=164:IP=192.168.8.101:EC=0     0:1:0, udid=LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMUVCOQ, serviceParameter=oneshot_key=Ð¡Ã¯Ð¡Ã¯;productLine=normal;voiceField=far;best_result_return=true;variable=true;audioFormat=audio/x-wav;modelType=home,song,hotel;audioCodec=opus;oneshot=true;sampleRate=16k;;textFormat=json, userID=LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMUVCOQ}\n" +
                "2020-04-28 16:21:53,086:DEBUG http-nio-8080-exec-14 (ContextBuilder.java65) - [bfd40968bb68e14f2afd1cc9d1a7b123]attribute:{serviceType=asr, requestUrl__=http://route.yunmaolink.com:8080/trafficRouter/cs, serviceReqCmd=probeService, remoteIP=114.236.137.226, audioVersion=2.1.36, isOldSdk=false, sessionID=null, serviceParameter=null, sessionRequestID=bfd40968bb68e14f2afd1cc9d1a7b123, userID=null, DEVICE_TOKEN=null, httpID=null, trafficParameter=null, requestID=bfd40968bb68e14f2afd1cc9d1a7b123, imei=null, appKey=null, ClientInfo=null, udid=null, probeNum=15}\n";
        String log = "2020-04-28 14:04:25,066:DEBUG http-nio-8080-exec-9 (ContextBuilder.java94) - [e6e2d1bfbc1e497aa1a78e34fd0a15d4]after user info pick, context:CommonContext [attributes={serviceType=asr, requestUrl__=http://route.yunmaolink.com:8080/trafficRouter/cs, serviceReqCmd=requestService, remoteIP=119.145.9.203, audioVersion=2.1.36, isOldSdk=false, serviceParameter=audioCodec=opus;audioFormat=audio/x-wav;best_result_return=true;modelType=home,song,hotel,movietv,ymhl;oneshot=false;productLine=normal;sampleRate=16k;textFormat=json;variable=true;voiceField=far;, sessionRequestID=e6e2d1bfbc1e497aa1a78e34fd0a15d4, userID=MjMwMTE4MjE0MDBBMzQxREI0Nzg4QzI, DEVICE_TOKEN=%7B%22deviceToken%22%3A%22%22%2C%22activateVersion%22%3A%22v3.1%22%7D, trafficParameter=filterName=nlu3;returnType=json;city=深圳市;gps=22.554746,113.948733;time=2020-04-28 14:04:24;scenario=hotelDefault;screen=;dpi=;history=;udid=MjMwMTE4MjE0MDBBMzQxREI0Nzg4QzI;ver=3.2;appver=;oneshotKeyProperty=no;additionalService=homeService;req_nlu_length=1;appendLength=1;fullDuplex=false;audioUrl=false;filterUrl=http://route.igaicloud.cn:8088/ym-home/process//tr/dataProcess;, requestID=e6e2d1bfbc1e497aa1a78e34fd0a15d4, imei=5200fa3dfbe08d38, serviceTypes=[Ljava.lang.String;@5d30abe0, appKey=umttxtibfsv2uv3jld7hfkix6eugpsugfxk7u2ak, ClientInfo=PN=com.unisound.aios.pandora.clock:OS=0:CR=0:NT=1:MD=A3:SV=v4.3.0_for_yunmao_xiaomao_v1.0.4,84722f3c:RPT=0:SID=12c3b94c75492a39a335cf604176d949:NPT=:IP=192.168.21.101:EC=0\t0:0:0, udid=MjMwMTE4MjE0MDBBMzQxREI0Nzg4QzI, probeNum=0}, id=e6e2d1bfbc1e497aa1a78e34fd0a15d4, input=null, output=null]";

        String nluStr = "2020-0|4-28 07:52:54,225: INFO http-nio-8080-exec-1 (NLUFilter.java184) - [ed74f90756177a50bd2872e53ac970a5]nlu result:{\\\"rc\\\":0,\\\"text\\\":\\\"小茂小茂暂停播放\\\",\\\"service\\\":\\\"cn.yunzhisheng.setting.mp\\\",\\\"code\\\":\\\"SETTING_EXEC\\\",\\\"semantic\\\":{\\\"intent\\\":{\\\"operations\\\":[{\\\"operator\\\":\\\"ACT_PAUSE\\\"}]}},\\\"general\\\":{\\\"type\\\":\\\"T\\\",\\\"text\\\":\\\"已暂停\\\"},\\\"history\\\":\\\"cn.yunzhisheng.setting.mp\\\",\\\"responseId\\\":\\\"1a1fae3c90c34f5e887baba1ea3b5925\\\"}";
        System.out.println(nluStr.substring(nluStr.indexOf("]nlu result:")+12));
        System.out.println(getAppKey(appLog));
    }

    /**
     * TR 日志 请求nlu 耗时 分析
     * @param nluLogFile tr日志
     */
    public static void analysisNluReqCostTime(String nluLogFile, String outputFile){
        List<String> logList = getFileList(nluLogFile);
        logList.stream().forEach(l -> {
            judgeContainNluMsg(l);
        });
        for(Entry<String, TextParseRet> entry:uniqueMap.entrySet()){
            TextParseRet t = entry.getValue();
            boolean add = false;
            for(String udid:udidFilter){
                Optional<String> optional = t.getContent().stream().filter(c-> c.contains(udid)).findAny();
                if (optional.isPresent()){
                    add = true;
                    break;
                }
            }
            if(!add){
                continue;
            }
            nluLogRetList.add(t);
        }
        nluLogRetList.stream().filter(n-> Objects.nonNull(n.getRetDate())).forEach(n-> {
            n.setDistance(n.getRetDate().getTime() - n.getReqDate().getTime());
            n.setReqDateStr(DateUtil.formatDate(n.getReqDate(), DateUtil.DATE_PATTERN_COMPLEX_MILLI));
            n.setRetDateStr(DateUtil.formatDate(n.getRetDate(), DateUtil.DATE_PATTERN_COMPLEX_MILLI));
            n.setLogContent(JSON.toJSONString(n.getContent()));
        });

        nluLogRetList.sort((a, b)-> b.getDistance().compareTo(a.getDistance()));
        String[] headers = {"请求ID", "udid","请求时间", "响应时间", "耗时", "请求nlu是否成功", "日志"};
        String[] properties = {"requestId", "udid", "reqDateStr", "retDateStr", "distance", "reqSuccess", "logContent"};
        ExcelExportUtil.export(headers, properties, nluLogRetList, false, outputFile);
    }

    public static void filterOneDayData(String day, List<String> udidList) {
        String curDayPath = work_path.concat(day).concat(File.separator).concat(day);
        String excelPath = curDayPath.concat(".xlsx");
        List<String[]> excelData = ExcelReader.getExcelData(new File(excelPath), 1);
        List<MatchResult> resultList = Lists.newArrayList();
        for (String[] array : excelData) {
            if (null != array && array.length >= 5 && StringUtils.hasText(array[1])) {
                if (udidList.contains(array[2].trim())) {
                    resultList.add(MatchResult.builder()
                            .reqTxt(array[0])
                            .nluRet(array[1])
                            .udid(array[2])
                            .mp3(array[3])
                            .timeStr(array[4])
                            .build());
                }
            }
        }

        String[] headers = {"请求文本", "nlu结果", "udid", "mp3名称", "时间"};
        String[] properties = {"reqTxt", "nluRet", "udid", "mp3", "timeStr"};
        ExcelExportUtil.export(headers, properties, resultList, false, curDayPath.concat("-new"));
    }

    public static List<String> getHotelUdid() {
        List<String> udidList = ReadTxtFileUtils.readTxt(new File(work_path.concat("udid.asrcheck")));
        return udidList;
    }

    public static void initMp3Map() {
        File audioDir = new File(audio_path);
        for (File date : audioDir.listFiles()) {
            for (File audio : date.listFiles()) {
                if (audio.getName().equals("mp3")) {
                    for (File mp3 : audio.listFiles()) {
                        String name = mp3.getName().replaceAll(".mp3", "");
                        System.out.println("====>" + name);
                        mp3Map.put(name, mp3);
                    }
                }
            }
        }

    }

    public static String regexMatchStr(String log, String regex, List<String> replaceList){
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(log);
        String ret = null;
        while (m.find()) {
            String matchStr = m.group();
            if(replaceList.size()>0){
                for(String r:replaceList){
                    ret = matchStr.replaceFirst(r, "");
                }
            }
            ret = ret.substring(0, ret.length() - 1);
            break;
        }
        return ret;
    }

    public static String getUdid(String log){
        return regexMatchStr(log, udid_regex, Arrays.asList("&udid=", ";udid="));
    }

    public static String getAppKey(String log){
        return regexMatchStr(log, appKey_regex, Arrays.asList("appKey="));
    }

    public static String getRequestId(String log) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(log);
        String ret = null;
        while (m.find()) {
            if(m.group().length() == 34){
                ret = m.group().substring(1, m.group().length() - 1);
                break;
            }
        }
        return ret;
    }


    public static void filterLogByTime() {
        List<String> list = getFileList("E:\\document\\secureCRT\\webapi_debug.log.1", "E:\\document\\secureCRT\\webapi_debug.log");
        List<String> writeList = Lists.newArrayList();
        for (String str : list) {
            Date d = parseDateFromLog(str);
            if (Objects.nonNull(d) && d.compareTo(start) >= 0 && d.compareTo(end) <= 0) {
                writeList.add(str);
            }
        }
        ReadTxtFileUtils.writeToTxt(writeList, out_path);
    }

    /**
     * 解析 日志时间
     *
     * @param log
     * @return
     */
    public static Date parseDateFromLog(String log) {
        if (StringUtils.hasText(log) && log.length() > 23) {
            String time = log.substring(0, 23);
            Date d = null;
            try {
                d = DateUtil.parseDate(time, DateUtil.DATE_PATTERN_COMPLEX_MILLI);
            } catch (Exception e) {

            }
            return d;
        }
        return null;
    }

    private static List<TextParseRet> nluLogRetList = Lists.newArrayList(); // 输出结果集合

    private static Map<String, TextParseRet> uniqueMap = Maps.newHashMap();// 用于 设置 一个请求nlu 以及 响应nlu 的map


    /**
     * 过滤 nlu 请求 & nlu 结果日志
     * @param content
     */
    public static void judgeContainNluMsg(String content) {

        if (StringUtils.isEmpty(content)) {
            return;
        }
        TextParseRet parseRet = null;
        String requestId = getRequestId(content);
        if (StringUtils.hasText(requestId)) {
            if (content.contains(nluContentFilterList.get(0))) {// nlu 请求日志
                parseRet = wrapperJudgeCode(requestId);
                parseRet.setNluReqUrl(content);
                parseRet.getContent().add(content);
                parseRet.setUdid(getUdid(content));
                parseRet.setReqDate(parseDateFromLog(content));
            }

            if (content.contains(nluContentFilterList.get(1))) {// nlu请求结果日志
                parseRet = wrapperJudgeCode(requestId);
                parseRet.setNluRet(content);
                parseRet.getContent().add(content);
                parseRet.setRetDate(parseDateFromLog(content));
            }

            if (content.contains(nluContentFilterList.get(2))) {// nlu请求结果日志
                parseRet = wrapperJudgeCode(requestId);
                parseRet.setNluRet(content);
                parseRet.getContent().add(content);
                parseRet.setReqSuccess("否");
                parseRet.setRetDate(parseDateFromLog(content));
            }
        }
//        if (Objects.nonNull(parseRet) && StringUtils.hasText(parseRet.getRequestId())
//                && Objects.nonNull(parseRet.getReqDate())
//                && Objects.nonNull(parseRet.getRetDate())) {
//            nluLogRetList.add(parseRet);
//            uniqueMap.remove(parseRet.getRequestId());
//        }
    }

    public static TextParseRet wrapperJudgeCode(String requestId){
        TextParseRet parseRet = null;
        if (uniqueMap.containsKey(requestId)) {
            parseRet = uniqueMap.get(requestId);
        } else {
            parseRet = new TextParseRet();
            parseRet.setRequestId(requestId);
            parseRet.setContent(new ArrayList<>());
            uniqueMap.put(requestId, parseRet);
        }
        return parseRet;
    }

    @Data
    public static class TextParseRet {

        private String requestId;

        private String udid;

        private Date reqDate; // nlu 请求时间

        private String reqDateStr;

        private String retDateStr;

        private String logContent;

        private Date retDate; // nlu 响应时间

        private List<String> content;

        private Long distance;// 时间差

        private String nluReqUrl;

        private String nluRet;

        private String reqSuccess="是";//是否 nlu请求出错.
    }

    /**
     * 匹配mp3
     *
     * @param filePath
     */
    public static void matchMp3(String filePath) {
        File dir = new File(filePath);
        File excel = null;// 找到excel文件
        for (File file : dir.listFiles()) {
            if (file.getName().endsWith(".xlsx")) {
                excel = file;
            }
        }

        //创建音频目录
        String audioDir = filePath.concat(File.separator).concat("audio");
        File dirFile = new File(audioDir);
        if (!dirFile.exists()) {
            dirFile.mkdir();
        }
        List<String[]> excelData = ExcelReader.getExcelData(excel, 1);
        for (String[] array : excelData) {
            if (Objects.nonNull(array) && array.length >= 5 && StringUtils.hasText(array[2])) {
                String requestId = array[2];
                if (mp3Map.containsKey(requestId)) {
                    File file = mp3Map.get(requestId);
                    try {
                        FileCopyUtils.copy(file, new File(audioDir.concat(File.separator).concat(requestId + ".mp3")));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 根据 文件名 获取 文本数据
     *
     * @param fileNames
     * @return
     */
    public static List<String> getFileList(String... fileNames) {
        List<String> retList = Lists.newArrayList();
        for (String file : fileNames) {
            retList.addAll(ReadTxtFileUtils.readTxt(new File(file)));
        }
        return retList;
    }


    /**
     * 导出日志分析结果
     */
    public static void exportExcel(String... files) {
        List<String> list = getFileList(files);
        List<MatchResult> retList = Lists.newArrayList();
        for (String str : list) {
            if (str.indexOf(matchList.get(0)) != -1) {
                String time = str.substring(0, 19);
                String nluUrl = str.substring(str.indexOf(matchList.get(0)) + matchList.get(0).length());
                String reqId = getRequestId(str);
                logMap.put(reqId, MatchResult.builder()
                        .time(DateUtil.parseDate(time, pattern))
                        .requestId(reqId)
                        .nluUrl(nluUrl).build());
            }
            if (str.indexOf(matchList.get(1)) != -1) {
                String nluRet = str.substring(str.indexOf(matchList.get(1)) + matchList.get(1).length());
                String reqId = getRequestId(str);
                if (logMap.containsKey(reqId)) {
                    MatchResult last = logMap.get(reqId);
                    JSONObject nluJson = JSON.parseObject(nluRet);
                    last.setNluRet(nluRet);
                    if (Objects.nonNull(nluJson) && nluJson.containsKey("rc")) {
                        last.setSuccess("成功");
                        if (nluJson.containsKey("text")) {
                            last.setReqTxt(nluJson.getString("text"));
                        }
                    }
                }
            }
        }
        //遍历 map
        for (Entry<String, MatchResult> entry : logMap.entrySet()) {
            retList.add(entry.getValue());
        }

        retList.sort((a, b) -> a.getTime().compareTo(b.getTime()));// 时间倒序

        // 根据udid过滤数据

        retList = retList.stream().filter(r -> {
            boolean flag = false;
            for (String udid : udidList) {
                if (r.getNluUrl().contains(udid)) {
                    flag = true;
                    break;
                }
            }
            return flag;
        }).collect(Collectors.toList());

        String[] headers = {"时间", "请求文本", "requestId", "是否成功", "nlu请求", "nlu结果"};
        String[] properties = {"timeStr", "reqTxt", "requestId", "success", "nluUrl", "nluRet"};

//        List<MatchResult> filterList = retList.stream().filter(r-> {
//            Date d = DateUtil.parseDate(r.getTime(), pattern);
//            return d.compareTo(start) >=0 && d.compareTo(end) <=0;
//        }).collect(Collectors.toList());
        ExcelExportUtil.export(headers, properties, retList, false, out_path);
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class MatchResult {

        private String requestId;

        private Date time;

        private String udid;

        private String timeStr;

        private String mp3;

        private String success;

        private String reqTxt;

        private String nluUrl;

        private String nluRet;
    }

}
