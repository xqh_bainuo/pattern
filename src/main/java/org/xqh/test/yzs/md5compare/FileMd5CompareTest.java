package org.xqh.test.yzs.md5compare;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import org.xqh.utils.file.ReadTxtFileUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @ClassName FileMd5CompareTest
 * @Description 多个文件md5 值 对比
 * @Author xuqianghui
 * @Date 2020/4/10 18:10
 * @Version 1.0
 */
public class FileMd5CompareTest {

    public final static String split_char = "  ";

    public final static String end_with_char = ".dat";

    public final static String WORK_PATH = "E:\\secureCRT_file\\upload\\";

    public static void main(String[] args) {
        List<Md5ParseRet> ym_prod = parseTxt(WORK_PATH + "ym-prod-md5.txt");
        List<Md5ParseRet> ig_uat = parseTxt(WORK_PATH + "ig-uat-md5.txt");
        for(Md5ParseRet m:ym_prod){
            Optional<Md5ParseRet> optional = ig_uat.stream().filter(i-> i.getModel().equals(m.getModel())).findAny();
            if(optional.isPresent()){
                m.setSame(m.getMd5().equals(optional.get().getMd5()));
            }
        }
        List<Md5ParseRet> diffList = ym_prod.stream().filter(m-> !m.isSame()).collect(Collectors.toList());
        System.out.println(JSON.toJSONString(diffList));
    }

    public static List<Md5ParseRet> parseTxt(String filePath){
        List<Md5ParseRet> retList = Lists.newArrayList();
        List<String> txtList = ReadTxtFileUtils.readTxt(new File(filePath));
        for(String t:txtList){
            if(StringUtils.isEmpty(t) || StringUtils.isEmpty(t.trim())){
                break;
            }
            String[] array = t.split(split_char);
                if(Objects.nonNull(array) && array.length == 2 && array[1].endsWith(end_with_char)){
                    retList.add(Md5ParseRet.builder()
                            .md5(array[0])
                            .model(array[1])
                            .build());
            }
        }
        return retList;
    }


    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Md5ParseRet{

        private String md5;

        private String model;

        private boolean same;

        private String different;//差异内容
    }
}
