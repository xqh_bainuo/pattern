package org.xqh.test.yzs.asr;

import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;
import org.apache.http.Header;
import org.apache.tools.ant.taskdefs.condition.Http;
import org.xqh.utils.file.ReadTxtFileUtils;
import org.xqh.utils.http.FxHttpClientUtils;
import org.xqh.utils.http.HttpRetModel;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName PublishAsrOpt
 * @Description 实施平台 asr发布
 * @Author xuqianghui
 * @Date 2021/1/22 14:09
 * @Version 1.0
 */
public class PublishAsrOpt {

    /**
     * 发布url
     */
    public static String pub_url = "http://192.168.3.240:9081/app/publish/asr";

    public static String cookie = "JSESSIONID=1A60E01F648A5E26390518716651612B";

    public static Header[] headers = HttpHeader.custom()
            .cookie(cookie)
            .build();

    public static void main(String[] args) throws InterruptedException {

        List<String> appKeys = ReadTxtFileUtils.readTxt(new File("E:\\document\\yzs\\program\\ig-cloud\\appKey-test.txt"));
        for(String appKey:appKeys){
            executePub(appKey);
            Thread.sleep(1000);
        }
    }

    public static HttpRetModel getHttpModel(){
        return HttpRetModel.builder()
                .statusKey("code")
                .successVal("200")
                .msgKey("message")
                .dataKey("obj")
                .build();
    }

    public static void executePub(String appKey){
        HttpConfig config = FxHttpClientUtils.getHttpConfig(pub_url);
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("scopeValue", appKey);
        reqMap.put("scopeType", "appKey");
        config.map(reqMap);
        config.method(HttpMethods.POST);
        config.headers(headers);
        FxHttpClientUtils.requestReturnString(config, getHttpModel());
    }
}
