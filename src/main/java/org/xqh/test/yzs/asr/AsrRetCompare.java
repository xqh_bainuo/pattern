package org.xqh.test.yzs.asr;

import com.google.common.collect.Lists;
import org.xqh.test.NumberUtils;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.excel.ExcelReader;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Objects;

/**
 * @ClassName AsrRetCompare
 * @Description asr结果识别对比
 * @Author xuqianghui
 * @Date 2019/11/29 11:19
 * @Version 1.0
 */
public class AsrRetCompare {

    public final static String tmp_path = "D:\\TestSpace\\outExcel\\";

    public static void main(String[] args) {
        List<String[]> list = ExcelReader.getExcelData(new File(tmp_path.concat("平安语料.xlsx")), 1);
        List<AsrResult> asrList = Lists.newArrayList();
        for(String[] array:list){
           if(Objects.nonNull(array) && array.length >= 3 && StringUtils.hasText(array[2])){
               AsrResult ret = AsrResult.builder()
                       .tts(array[0])
                       .asrRet(array[1])
                       .requestId(array[2])
                       .build();
               if(!NumberUtils.compareTtsAsrRet(ret.getTts(), ret.getAsrRet())){
                   asrList.add(ret);
               }
           }
        }
        String outFile = tmp_path.concat("平安语料-不一致结果2");
        //导出
        String[] headers = {"tts文本", "识别结果", "请求ID"};
        String[] properties = {"tts", "asrRet", "requestId"};
        ExcelExportUtil.export(headers, properties, asrList, false, outFile);

    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class AsrResult{

        private String tts;

        private String asrRet;

        private String requestId;
    }
}
