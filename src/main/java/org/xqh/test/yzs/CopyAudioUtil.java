package org.xqh.test.yzs;

import com.alibaba.fastjson.JSONArray;
import org.xqh.utils.file.ReadTxtFileUtils;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * @ClassName CopyAudioUtil
 * @Description 拷贝音频
 * @Author xuqianghui
 * @Date 2020/3/4 10:58
 * @Version 1.0
 */
public class CopyAudioUtil {

    private final static String CUR_PATH = "E:\\secureCRT_file\\download\\";

    public static void main(String[] args) {
//        copyAudio(CUR_PATH + "1000未识别sessionId-55.txt", CUR_PATH + "ym-hotel\\mp3\\", CUR_PATH + "1000验证不识别音频\\", "mp3");
        checkTwoList();
    }

    /**
     * 拷贝音频
     *
     * @param txt          解析sessionId
     * @param resourcePath 音频原目录
     * @param targetPath   音频目标目录
     * @param suffix       文件后缀
     */
    public static void copyAudio(String txt, String resourcePath, String targetPath, String suffix) {
        List<String> list = ReadTxtFileUtils.readTxt(new File(txt));
        try {
            for (String sessionId : list) {
                String fileName = sessionId.concat(".").concat(suffix);
                File c = new File(resourcePath.concat(fileName));
                File t = new File(targetPath.concat(fileName));
                if (c.exists() && !t.exists()) {
                    //执行拷贝
                    FileCopyUtils.copy(c, t);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void checkTwoList(){
        String str55 = "[\"fe4aedd03962bdf203e2afbb242bcd41\",\"602aad2cdb5f89e1b0364efb9de280a7\",\"3a184aea2a588bd2b022375cdb891d68\",\"f2c5a46228e5a55101477c079d96ea03\",\"57c3fee1af772ef30020073d5fe91a9a\",\"654b8e691f49e6b1ceb7035b7ec503d5\",\"edd0fc73c7f246afc34798017e82f753\",\"15a3388f8c6f4ea8983d0ab2e2a4649e\",\"055983cb0c23408b96e906de542595c2\",\"163d3041f9c3fc31a7109fe98c77e38c\",\"1b361194fb22526497de94aca0b83e27\",\"0504053ebd311f860e87fd17e9a40f6d\",\"4be87ef170f743b228d17b3aaf3ccfe3\",\"6766a73b00825f169d4ea3ca26d3441b\",\"6b4373de067f063ab156c264437e0bf2\",\"fd15f8182507acbae2acea31e38aed11\",\"de140a0f23567143bad4c81ded7b3040\",\"be94d94a5a616dcb2097555acfe45b00\",\"3be7012036441d06cdfa6c12e074e256\",\"fd65ed6b50b6c70d98698a3fba04b139\",\"7a82849d0d8cfa99066bba9e56aaa632\",\"1aaa4f2f16d1cd6efcfe72b4938ea607\",\"a6c7238541d6cc444b19c1c1cc4c8c5e\",\"9f439d5a232d03516b26a327f501ad79\",\"fa634139785a394b5c362fb17fb41bd7\",\"d1636a852a30b6c9977a43c48553b9c7\",\"27c54231d9760aff7447e2e4f184318b\",\"d4e73e5f0b615b7cfa161ea3f460692c\",\"1b9ad939a1d38df22fc61acef8a43235\",\"ce1e4ac0eabf5c35c51c39dcf9d89e26\",\"51719be8616d923fc641c0b5c70beb4e\",\"605a170b7d9a4bf9dfc7dd75db9ddae7\",\"9d4f1e2f28147edd70017624ea9bb371\",\"27c5b7af3f4050f364125df76f04690a\",\"c4d82a0bab251e81921f28711268db09\",\"16aa70d7256b57a42d23e392d57a8ae5\",\"7477b7419ac3b3298e8026fda309da98\",\"4e1fac2950b8f9084ef8501af4ccc90c\",\"7bad3578ec1e02cda09cf336701abdc3\",\"fd0ea0fb5f5e691996e0539622d28a91\",\"c011560bf380eb2487d7d0cf4766a73e\",\"fff2cf2f54bc20b9854229c1bdfb7469\",\"61505128e3a87fff91d4674fdd1e9769\",\"1135a09919db3b1cd1bb7fa719eab6d5\",\"3de3bc6d777c4f18917f6aaa22189216\",\"78a7173acddc47417b794b00139d4bd6\",\"cc7c84a4d89b0f886cfd094b885b23bd\",\"d9bd9329c420777fd0f2616f23593a06\",\"21611dad9242cd492e0b681b7544a6c4\",\"c8ca7f21385e2e154a88a2390ad7c5b6\",\"37683e43b122c0c0e333b7b720083680\",\"589cf15e93c62fa0674d875ff9bd028d\",\"7d813a15202500b3fd7cd234ead4e15b\",\"ccd29a65486c93d0871d3af59e139d59\",\"3924b9b9f0c3252bcc6c69bb6a7c65ab\"]";
        String str42 = "[\"fe4aedd03962bdf203e2afbb242bcd41\",\"602aad2cdb5f89e1b0364efb9de280a7\",\"f2c5a46228e5a55101477c079d96ea03\",\"654b8e691f49e6b1ceb7035b7ec503d5\",\"15a3388f8c6f4ea8983d0ab2e2a4649e\",\"055983cb0c23408b96e906de542595c2\",\"163d3041f9c3fc31a7109fe98c77e38c\",\"1b361194fb22526497de94aca0b83e27\",\"6766a73b00825f169d4ea3ca26d3441b\",\"6b4373de067f063ab156c264437e0bf2\",\"fd15f8182507acbae2acea31e38aed11\",\"be94d94a5a616dcb2097555acfe45b00\",\"3be7012036441d06cdfa6c12e074e256\",\"fd65ed6b50b6c70d98698a3fba04b139\",\"a6c7238541d6cc444b19c1c1cc4c8c5e\",\"9f439d5a232d03516b26a327f501ad79\",\"fa634139785a394b5c362fb17fb41bd7\",\"27c54231d9760aff7447e2e4f184318b\",\"d4e73e5f0b615b7cfa161ea3f460692c\",\"1b9ad939a1d38df22fc61acef8a43235\",\"ce1e4ac0eabf5c35c51c39dcf9d89e26\",\"51719be8616d923fc641c0b5c70beb4e\",\"605a170b7d9a4bf9dfc7dd75db9ddae7\",\"9d4f1e2f28147edd70017624ea9bb371\",\"c4d82a0bab251e81921f28711268db09\",\"7477b7419ac3b3298e8026fda309da98\",\"4e1fac2950b8f9084ef8501af4ccc90c\",\"7bad3578ec1e02cda09cf336701abdc3\",\"c011560bf380eb2487d7d0cf4766a73e\",\"fff2cf2f54bc20b9854229c1bdfb7469\",\"61505128e3a87fff91d4674fdd1e9769\",\"1135a09919db3b1cd1bb7fa719eab6d5\",\"3de3bc6d777c4f18917f6aaa22189216\",\"cc7c84a4d89b0f886cfd094b885b23bd\",\"d9bd9329c420777fd0f2616f23593a06\",\"21611dad9242cd492e0b681b7544a6c4\",\"c8ca7f21385e2e154a88a2390ad7c5b6\",\"37683e43b122c0c0e333b7b720083680\",\"589cf15e93c62fa0674d875ff9bd028d\",\"7d813a15202500b3fd7cd234ead4e15b\",\"ccd29a65486c93d0871d3af59e139d59\",\"3924b9b9f0c3252bcc6c69bb6a7c65ab\"]";
        List<String> l55 = JSONArray.parseArray(str55, String.class);
        List<String> l42 = JSONArray.parseArray(str42, String.class);
        for(String s:l55){
            if(!l42.contains(s)){
                System.out.println(s);
            }
        }

        System.out.println("===================");

        for(String s:l42){
            if(!l55.contains(s)){
                System.out.println(s);
            }
        }
    }
}
