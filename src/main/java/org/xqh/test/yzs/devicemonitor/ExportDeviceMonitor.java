package org.xqh.test.yzs.devicemonitor;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.xqh.utils.DateUtil;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.excel.ExcelReader;
import org.xqh.utils.file.ReadTxtFileUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

/**
 * @ClassName ExportDeviceMonitor
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/8/14 17:36
 * @Version 1.0
 */
public class ExportDeviceMonitor {

    public static String work_path = "E:\\document\\yzs\\program\\ig-cloud\\device-monitor-service\\";

    public static Map<String, String> deviceMap = Maps.newHashMap();

    public static void main(String[] args) {
        parseRoomInfo();
        ReadTxtFileUtils.readDaoxu(work_path + "es/6.txt", "UTF-8");
        ReadTxtFileUtils.readDaoxu(work_path + "es/5.txt", "UTF-8");
        ReadTxtFileUtils.readDaoxu(work_path + "es/4.txt", "UTF-8");
        ReadTxtFileUtils.readDaoxu(work_path + "es/3.txt", "UTF-8");
        ReadTxtFileUtils.readDaoxu(work_path + "es/2.txt", "UTF-8");
        ReadTxtFileUtils.readDaoxu(work_path + "es/1.txt", "UTF-8");
//        ReadTxtFileUtils.readDaoxu(work_path + "es/net1.txt", "UTF-8");
        List<WarnLog> wlist = Lists.newArrayList();
        for(Entry<String, List<WarnLog>> entry:ReadTxtFileUtils.dataMap.entrySet()){

            wlist.addAll(entry.getValue());
        }
        wlist.sort((a,b)-> a.getCreateTime().compareTo(b.getCreateTime()));

        String[] headers = {"设备空间", "设备ID", "设备名称", "告警时间", "最近心跳时间", "恢复时间"};
        String[] properties = {"spaceName", "deviceSn", "deviceName", "warnTimeStr", "bhTimeStr", "backTimeStr"};
        ExcelExportUtil.export(headers, properties, wlist, false, work_path + "out.xlsx");

//        String text = "[{\"createTime\":1597315198331,\"deviceName\":\"智能音箱\",\"deviceSn\":\"LTE3Nzg0MjcyMTUwMEVDRTE1NEYwNTI2Rg\",\"finish\":true,\"lastHbTime\":1597315075000,\"recoverTime\":1597336683498,\"spaceName\":\"世茂皇家艾美酒店 房型2 2806\",\"up_time\":1597339500,\"warn\":true,\"warnTime\":1597315195000},{\"createTime\":1597335243136,\"deviceName\":\"智能音箱\",\"deviceSn\":\"LTE3Nzg0MjcyMTUwMEVDRTE1NEYwNTI2Rg\",\"finish\":true,\"lastHbTime\":1597314535000,\"recoverTime\":1597339143860,\"spaceName\":\"世茂皇家艾美酒店 房型2 2806\",\"up_time\":1597340700,\"warn\":true,\"warnTime\":1597314655000},{\"createTime\":1597338843822,\"deviceName\":\"智能音箱\",\"deviceSn\":\"LTE3Nzg0MjcyMTUwMEVDRTE1NEYwNTI2Rg\",\"finish\":true,\"lastHbTime\":1597318856000,\"recoverTime\":1597341424419,\"spaceName\":\"世茂皇家艾美酒店 房型2 2806\",\"up_time\":1597342081,\"warn\":true,\"warnTime\":1597318976000},{\"createTime\":1597340404091,\"deviceName\":\"智能音箱\",\"deviceSn\":\"LTE3Nzg0MjcyMTUwMEVDRTE1NEYwNTI2Rg\",\"finish\":true,\"lastHbTime\":1597316276000,\"recoverTime\":1597339683921,\"spaceName\":\"世茂皇家艾美酒店 房型2 2806\",\"up_time\":1597341241,\"warn\":true,\"warnTime\":1597316396000},{\"createTime\":1597337883669,\"deviceName\":\"智能音箱\",\"deviceSn\":\"LTE3Nzg0MjcyMTUwMEVDRTE1NEYwNTI2Rg\",\"finish\":false,\"lastHbTime\":1597337760000,\"spaceName\":\"世茂皇家艾美酒店 房型2 2806\",\"up_time\":1597337760,\"warn\":false}]";
//        List<WarnLog> list = JSONArray.parseArray(text, WarnLog.class);
//        list.forEach(l-> {
//            System.out.println(DateUtil.formatDate(l.getLastHbTime(), "yyyy-MM-dd HH:mm:ss"));
//            System.out.println(DateUtil.formatDate(l.getWarnTime(), "yyyy-MM-dd HH:mm:ss"));
//            System.out.println(DateUtil.formatDate(l.getRecoverTime(), "yyyy-MM-dd HH:mm:ss"));
//        });

    }


    public static List<WarnLog> getWarnLog(){
        File path = new File(work_path + "es");

        return null;
    }






    public static void parseRoomInfo(){
        List<String[]> list = ExcelReader.getExcelData(new File(work_path+"世茂皇家艾美酒店.xlsx"), 1);
        for(String[] array:list){
            String udid = array[1];
            String room = array[0];
            if(StringUtils.hasText(udid) && StringUtils.hasText(room)){
                if(!deviceMap.containsKey(udid)){
                    deviceMap.put(udid, room);
                }
            }
        }
    }

    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class WarnLog{

        private String deviceSn;

        private Long up_time;

        private Long createTime;

        private String spaceName;

        private String deviceName;

        private boolean finish;//记录是否结束

        private boolean warn;//是否异常记录

        private Date warnTime;

        private Date lastHbTime;

        private Date recoverTime;//恢复时间

        private String warnTimeStr;

        private String bhTimeStr;

        private String backTimeStr;

        public String getDeviceSn() {
            return deviceSn;
        }

        public void setDeviceSn(String deviceSn) {
            this.deviceSn = deviceSn;
        }

        public Long getUp_time() {
            return up_time;
        }

        public void setUp_time(Long up_time) {
            this.up_time = up_time;
        }

        public Long getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Long createTime) {
            this.createTime = createTime;
        }

        public String getSpaceName() {
            return spaceName;
        }

        public void setSpaceName(String spaceName) {
            this.spaceName = spaceName;
        }

        public String getDeviceName() {
            return deviceName;
        }

        public void setDeviceName(String deviceName) {
            this.deviceName = deviceName;
        }

        public boolean isFinish() {
            return finish;
        }

        public void setFinish(boolean finish) {
            this.finish = finish;
        }

        public boolean isWarn() {
            return warn;
        }

        public void setWarn(boolean warn) {
            this.warn = warn;
        }

        public Date getWarnTime() {
            return warnTime;
        }

        public void setWarnTime(Date warnTime) {
            this.warnTime = warnTime;
        }

        public Date getLastHbTime() {
            return lastHbTime;
        }

        public void setLastHbTime(Date lastHbTime) {
            this.lastHbTime = lastHbTime;
        }

        public Date getRecoverTime() {
            return recoverTime;
        }

        public void setRecoverTime(Date recoverTime) {
            this.recoverTime = recoverTime;
        }

        public String getWarnTimeStr() {
            if(Objects.nonNull(warnTime)){
                return DateUtil.formatDate(warnTime, "yyyy-MM-dd HH:mm:ss");
            }
            return warnTimeStr;
        }

        public void setWarnTimeStr(String warnTimeStr) {
            this.warnTimeStr = warnTimeStr;
        }

        public String getBhTimeStr() {
            if(Objects.nonNull(lastHbTime)){
                return DateUtil.formatDate(lastHbTime, "yyyy-MM-dd HH:mm:ss");
            }
            return bhTimeStr;
        }

        public void setBhTimeStr(String bhTimeStr) {
            this.bhTimeStr = bhTimeStr;
        }

        public String getBackTimeStr() {
            if(Objects.nonNull(recoverTime)){
                return DateUtil.formatDate(recoverTime, "yyyy-MM-dd HH:mm:ss");
            }
            return backTimeStr;
        }

        public void setBackTimeStr(String backTimeStr) {
            this.backTimeStr = backTimeStr;
        }
    }
}
