package org.xqh.test.yzs.shimao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.file.ReadTxtFileUtils;
import org.xqh.utils.oss.OssUtils;
import lombok.Data;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Objects;

/**
 * @ClassName DownloadFaceImg
 * @Description 下载人脸图片
 * @Author xuqianghui
 * @Date 2020/9/10 14:55
 * @Version 1.0
 */
public class DownloadFaceImg {

    public final static String work_path = "E:\\document\\yzs\\program\\shimao-iot\\";

    public static void main(String[] args) {
        String jsonStr = ReadTxtFileUtils.readJson(new File(work_path + "人脸记录.txt"));
        JSONObject json = JSON.parseObject(jsonStr);
        List<FaceImg> imgList = JSON.parseArray(json.getString("data"), FaceImg.class);
        imgList.stream().filter(i -> Objects.nonNull(i) && StringUtils.hasText(i.getFace_url())
                && i.getFace_url().contains("shimao-iot.oss-cn-shanghai.aliyuncs.com/png")).forEach(img -> {
            String fileName = IdWorker.get32UUID() + ".png";
            img.setFilename(fileName);
            String filePath = work_path + "download\\" + fileName;
            String objName = getOssObjectName(img.getFace_url());
            OssUtils.downFile(filePath, objName);
        });
        String[] h = {"idCard","对应图片"};
        String[] p = {"idcard","filename"};
        ExcelExportUtil.export(h, p, imgList, false, work_path.concat("face-export.xlsx"));
//        getOssObjectName(null);
    }

    public static String getOssObjectName(String url) {
//        url = "https://shimao-iot.oss-cn-shanghai.aliyuncs.com/png/624c50418f7f4f4b9365c756b43942e4?Expires=1914334783\\u0026OSSAccessKeyId=LTAI4FdYb6pAU2syWxWtz3o6\\u0026Signature=F%2FaA%2BVUJQvF5mi3BG9WnNtnLa3g%3D";
        url = url.substring(url.indexOf(".com/") + 5, url.indexOf("?Expires"));
        System.out.println(url);
        return url;
    }

    @Data
    public static class FaceImg {
        private String id;
        private String face_url;
        private String idcard;
        private String filename;
    }
}
