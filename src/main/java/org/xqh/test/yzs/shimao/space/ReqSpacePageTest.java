package org.xqh.test.yzs.shimao.space;

import com.alibaba.fastjson.JSON;
import com.arronlong.httpclientutil.common.HttpConfig;
import org.xqh.utils.CertificateUtils;
import org.xqh.utils.http.FxHttpClientUtils;
import org.xqh.utils.http.HttpRetModel;

import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName ReqSpacePageTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/4/9 10:42
 * @Version 1.0
 */
public class ReqSpacePageTest {

    public static String url = "https://dev-api.shimaoiot.com/space-server/signature/query-child-space-page";

    public static String appKey = "shimao62ae1634e0b32d0";

    public static String appSecret = "f6339f4974cac548080ecdeca4037489";

    public static String brand = "yzs";

    public static void main(String[] args) {
        reqSpacePage(2106064L);
    }

    public static void reqSpacePage(Long spaceId){
        Map<String, Object> reqMap = new HashMap<>();
        reqMap.put("id", spaceId);
        reqMap.put("pageNum", 1);
        reqMap.put("pageSize", 5);
        String reqJson = JSON.toJSONString(reqMap);
        HttpConfig config = FxHttpClientUtils.getPostJsonConfig(url, CertificateUtils.getReqHeaders(reqJson, appKey, appSecret, brand), reqJson);
        String result = FxHttpClientUtils.requestReturnString(config, HttpRetModel.builder()
                .statusKey("retcode")
                .successVal("0")
                .dataKey("data")
                .msgKey("msg")
                .build());
        System.out.println(result);
    }

}
