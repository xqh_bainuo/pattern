package org.xqh.test.yzs.shimao.hongxu;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class SpaceTreeDto implements Serializable {
    private static final long serialVersionUID = 2L;

    private Long id;

    private String spaceName;

    private Double measure;

    private Integer spaceType;

    private String typeCode;

    private String typeName;

    private String typeRemark;

    private String remarks;

    private Long rootId;

    private Long pid;

    private Integer orderShow;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private String createName;

    private String updateName;

    private Boolean isSuite;

    private Boolean isSpecialMode;

    private Integer status;

    private Boolean isProject;

    private String spacePath;

    private Long provinceCode;

    private Long cityCode;

    private String icon;

    private Boolean isExistSubspace;

    private List<Long> spaceTreeInfo;

    private List<SpaceExtensionEntity> spaceExtensionEntities;

    private List<SpaceTreeDto> spaceEntityList;

}