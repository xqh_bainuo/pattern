package org.xqh.test.yzs.shimao.hongxu;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author bka
 * @date 2020/6/8 2:56 下午
 */
@Data
public class SpaceExtensionEntity implements Serializable {
    private static final long serialVersionUID = 2L;

    private Long id;
    private Long spaceId;
    private Long settingId;
    private String settingKey;
    private String settingValue;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
