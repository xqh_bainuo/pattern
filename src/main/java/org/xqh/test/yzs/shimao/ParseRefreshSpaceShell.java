package org.xqh.test.yzs.shimao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.xqh.utils.file.ReadTxtFileUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @ClassName ParseRefreshSpaceShell
 * @Description 解析刷新空间数据脚本
 * @Author xuqianghui
 * @Date 2020/8/15 15:24
 * @Version 1.0
 */
public class ParseRefreshSpaceShell {


    public static void main111(String[] args) {

        String json = "{\n" +
                "    \"data\":[\n" +
                "        {\n" +
                "            \"hotel_id\":\"0081aa97963341bca54571d923d7cbca\",\n" +
                "            \"hotel_name\":\"世茂酒店测试4\",\n" +
                "            \"room\":\"10_1001,1_101\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"0477ec0a3c844e1aa6202d9ae9806127\",\n" +
                "            \"hotel_name\":\"世茂武汉凡象酒店\",\n" +
                "            \"room\":\"10_1001,10_1002,10_1003,10_1005,10_1006,10_1007,10_1008,10_1009,10_1010,10_1011,10_1012,10_1015,10_1016,10_1017,10_1018,10_1020,10_1066,10_1088,10_1099,11_1101,1_1102,11_1103,11_1105,11_1106,11_1107,11_1108,11_1109,11_1110,11_1111,11_1115,11_1117,11_1118,11_1120,11_1166,11_1188,11_1199,12_1201,12_1202,12_1203,12_1205,12_1206,12_1207,12_1208,12_1209,12_1210,12_1211,12_1212,12_1215,12_1216,12_1217,12_1218,12_1220,12_1266,12_1288,12_1299,15_1501,15_1502,15_1503,15_1505,15_1506,15_1507,15_1508,15_1509,15_1510,15_1511,15_1512,15_1515,15_1516,15_1517,15_1518,15_1520,15_1566,15_1588,15_1599,16_1601,16_1602,16_1603,16_1605,16_1606,16_1607,16_1608,16_1609,16_1610,16_1611,16_1612,16_1615,16_1616,16_1617,16_1618,16_1620,16_1666,16_1688,16_1699,17_1701,17_1702,17_1703,17_1706,17_1707,17_1708,17_1709,17_1710,17_1712,17_1766,17_1788,18_1801,18_1802,18_1803,18_1805,18_1806,18_1807,18_1808,18_1809,18_1810,18_1812,18_1866,18_1888,19_1901,19_1902,19_1903,19_1905,19_1906,19_1907,19_1908,19_1909,19_1910,19_1912,19_1966,5_8501,5_\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"0ac8568eb51741408a9b5c7481ab7cc0\",\n" +
                "            \"hotel_name\":\"世茂皇家艾美酒店\",\n" +
                "            \"room\":\"20_2013,36_3606,23_2322,37_3718,22_2203,24_2417,27_2721,36_3613,20_2015,21_2116,32_3214,20_2011,30_3017,30_3011,29_2914,26_2615,37_3702,24_2411,39_3915,24_2418,22_2207,32_3210,20_2021,31_3120,32_3209,22_2202,32_3224,26_2626,30_3004,28_2813,27_2703,28_2825,24_2426,28_2804,32_3213,30_3009,22_2204,38_3819,39_3901,30_3013,20_2017,20_2008,39_3913,25_2510,38_3811,37_3705,29_2907,23_2326,25_2507,22_2223,25_2505,39_3912,24_2410,36_3611,21_2109,22_2208,31_3103,32_3221,32_3225,29_2921,24_2407,38_3801,22_2211,29_2908,39_3919,30_3012,31_3115,37_3710,28_2824,23_2306,21_2111,38_3803,38_3806,21_2114,24_2424,27_2718,88_8888,27_2708,39_3916,20_2010,28_2810,25_2511,20_2006,22_2206,27_2717,25_2521,24_2409,38_3820,30_3019,39_3917,32_3219,24_2408,22_2214,31_3114,29_2922,27_2722,36_3617,23_2316,29_2904,31_3102,38_3805,22_2209,31_3125,39_3902,29_2912,29_2913,25_2503,29_2918,28_2808,21_2126,36_3618,25_2518,30_3023,22_2218,28_2823,30_3001,32_3220,38_3815,24_2402,36_3609,31_3108,26_2612,31_3107,20_2009,22_2213,36_3608,20_2023,36_3616,\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"47415f360f9e4c42bb1d7b95fe15864b\",\n" +
                "            \"hotel_name\":\"香港喜来登酒店\",\n" +
                "            \"room\":\"1_101\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"6496918c9f7642fe80ca1e336bfc1cc0\",\n" +
                "            \"hotel_name\":\" \",\n" +
                "            \"room\":\"6_6602,5_2512,2_6208,1_2107,3_2303,5_6511,5_6507,5_1505,6_6601,2_6203,5_3506,2_3201,1_3101,5_3501,3_1302,6_6606,1_6103,1_8899,1_3112,3_1305,2_2206,2_3206,5_6503,2_2212,1_6201,1_1110,5_3510,1_8866,3_2305,3_3309,3_3301,1_1111,1_3115,3_3307,3_2315,5_1502,5_1511,3_1301,6_6608,5_3502,1_1109,2_1212,3_6305,5_6508,5_6509,2_6210,5_6502,5_2505,3_1315,5_3503,1_2101,3_1309,1_3108,5_3508,5_3505,5_3512,5_3509,3_3303,1_6105,1_2103,2_2205,7_6707,1_6107,1_1106,2_6306,2_1210,2_3208,1_2105,3_1303,5_3511,1_2115,3_2311,1_3102,1_2111,2_3203,6_6611,2_2208,2_1202,5_2511,1_3106,2_3205,2_6202,2_6207,5_1509,1_2110,3_1310,5_1503,3_3308,3_2302,6_6605,3_6307,1_6108,1_3107,3_3311,3_2301,2_1201,1_8888,2_1209,2_1205,5_6510,2_6205,3_3305,7_6705,2_2209,1_3105,2_2210,3_6309,1_1101,3_2306,2_2215,1_3110,2_3212,3_1307,3_6302,3_1312,5_6501,1_2106,5_1512,1_3109,1_6102,3_1311,2_3211,3_3312,1_2109,2_3209,5_6506,5_1507,2_2201,5_2503,5_3515,1_3111,1_1108,3_2308,1_1103,5_2509,3_6303,2_1206,2_1203,3_3306,5_1501,1_1107,7_6706,1_1102,5_2510,5_2501,2_3215,2_\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"7c56ac156f52433dbad22008e1590add\",\n" +
                "            \"hotel_name\":\"炯为智慧酒店\",\n" +
                "            \"room\":\"8_8888,8_6666,9_9999,11_222,5_101\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"82a5dc1578924099855dbd3a8f9b7e0e\",\n" +
                "            \"hotel_name\":\"上海佘山茂御臻品之选酒店\",\n" +
                "            \"room\":\"4_4004,2_2032,3_3020,4_4008,3_3006,4_4007,4_4026,2_2001,3_3022,3_3030,2_2010,3_3025,4_4013,4_4024,3_3007,2_2003,2_2028,4_4012,2_2012,4_4023,4_4010,4_4022,3_3026,2_2019,4_4002,4_4025,3_3032,3_3009,4_4005,4_4019,2_2022,99_9999,2_2030,2_2029,3_3017,4_4029,3_3028,2_2020,2_2018,2_2033,2_2009,3_3013,4_4020,3_3005,3_3019,2_2016,2_2027,4_4030,3_3001,3_3027,2_2025,2_2002,4_4017,3_3004,3_3023,4_4016,3_3029,3_3018,3_3021,3_3008,2_2015,2_2011,2_2017,3_3024,3_3010,3_3033,2_2005,4_4031,4_4018,3_3012,2_2031,2_2013,88_8888,4_4006,4_4001,3_3031,3_3011,3_3016,3_3015,2_2024,4_4027,4_4028,2_2021,4_4032,4_4003,2_2008,4_4014,4_4033,2_2026,4_4015,3_3014,4_4021,3_3003,2_2023,2_2007,2_2014,3_3002,2_2006,4_4009,4_4011,2_2004\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"9488535f11084859a0941685ffdf0f64\",\n" +
                "            \"hotel_name\":\"世茂测试酒店\",\n" +
                "            \"room\":\"1_102,1_108,2_202,1_112,10_1001,2_203,1_110,1_1809,1_101,2_201,1_103,3_303,1_1866,5_0507,1_120,1_107,8_8620,5_0505,1_109,1_106,1_105,8_8617,1_104,1_111\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"96fe74b0093e490084b40c588e8dc3a7\",\n" +
                "            \"hotel_name\":\"上海皇家艾美酒店test\",\n" +
                "            \"room\":\"1_102,1_101\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"ab9d59251b1c435cb473742f4193ae8b\",\n" +
                "            \"hotel_name\":\"长沙世茂希尔顿酒店\",\n" +
                "            \"room\":\"41_4108,41_4107\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"b08143ca9fa4470a942f2cd277ed6465\",\n" +
                "            \"hotel_name\":\"高碑店世茂世御酒店\",\n" +
                "            \"room\":\"7_7011,7_7017,18_1806,7_7009,7_7007,7_7016,7_7012,18_1805,7_7010,7_7005,7_7006,7_7013,7_7008,7_7015\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"c5933aff6765484bbf91e1f6ba7f3e84\",\n" +
                "            \"hotel_name\":\"世茂酒店测试\",\n" +
                "            \"room\":\"1_103,1_101,1_102,88_8808,8_8088\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"e96398b6fcd447489f035348ae8ee838\",\n" +
                "            \"hotel_name\":\"世茂天鹅湾\",\n" +
                "            \"room\":\"42_4206,24_2406,24_2408,42_4208\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"hotel_id\":\"e9dcf00028b94b539b6a0a960523e49b\",\n" +
                "            \"hotel_name\":\"长春世茂莲花山小镇酒店\",\n" +
                "            \"room\":\"80_8088\"\n" +
                "        }\n" +
                "    ],\n" +
                "    \"status\":false,\n" +
                "    \"time\":15,\n" +
                "    \"title\":[\n" +
                "        {\n" +
                "            \"fixed\":\"left\",\n" +
                "            \"key\":\"room\",\n" +
                "            \"title\":\"room\",\n" +
                "            \"width\":\"200\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\":\"hotel_id\",\n" +
                "            \"title\":\"hotel_id\",\n" +
                "            \"width\":\"200\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"key\":\"hotel_name\",\n" +
                "            \"title\":\"hotel_name\",\n" +
                "            \"width\":\"200\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        JSONObject obj = JSON.parseObject(json);
        JSONArray list = obj.getJSONArray("data");
        for(Object j:list){
            JSONObject cur = (JSONObject) j;
            String roomList = cur.getString("room");
            String[] arr = roomList.split(",");
            Map<String, List<String>> dataMap = Maps.newConcurrentMap();
            for(String a:arr){
                String[] items = a.split("_");
                if(Objects.nonNull(items) && items.length == 2){
                   if(dataMap.containsKey(items[0])){
                       dataMap.get(items[0]).add(items[1]);
                   }else {
                       List<String> nlist = Lists.newArrayList();
                       nlist.add(items[1]);
                       dataMap.put(items[0], nlist);
                   }
                }
            }
            List<TestM> dlist = Lists.newArrayList();
            for(Entry<String, List<String>> entry:dataMap.entrySet()){
                dlist.add(TestM.builder()
                        .floor(entry.getKey())
                        .rooms(entry.getValue())
                        .build());
            }
            dlist.sort((a,b)-> a.getFloor().compareTo(b.getFloor()));
            System.out.println("==============="+cur.getString("hotel_name")+" "+cur.getString("hotel_id")+"=================");
            System.out.println(JSON.toJSONString(dlist));
        }
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TestM{
        private String floor;

        private List<String> rooms;
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class TestRoom{
        private String hotel_id;

        private String hotel_name;

        private String floor;

        private List<String> rooms;

        private String room_num;
    }

    public static void main(String[] args) {
        parseAllRoom();
    }

    public static List<TestRoom> parseTxt(){
        List<TestRoom> retList = Lists.newArrayList();
        List<String> txtList = ReadTxtFileUtils.readTxt(new File("E:\\document\\yzs\\program\\shimao-iot\\shimao-pms\\刷数据脚本.txt"));
        StringBuilder sb = new StringBuilder();
        for(String str:txtList){
            str = str.trim();
            if(StringUtils.isEmpty(str)){
                continue;
            }
            if("},".equals(str)){
                sb.append("}");
                retList.add(JSON.parseObject(sb.toString(), TestRoom.class));
                sb = new StringBuilder();
            }else {
                sb.append(str);
            }
        }
        return retList;
    }


    public static void parseAllRoom(){
        List<TestRoom> list = parseTxt();
        Map<String, List<TestRoom>> map = list.stream().collect(Collectors.groupingBy(TestRoom::getHotel_id));
        for(Entry<String, List<TestRoom>> entry:map.entrySet()){
            System.out.println(" ================ "+entry.getKey() + entry.getValue().get(0).getHotel_name()+" ================ ");
            entry.getValue().stream().forEach(r-> {
                if(Objects.nonNull(r.getFloor()) && r.getFloor().contains("层")){
                    r.setFloor(r.getFloor().replaceAll("层", ""));
                }
            });
            //按楼层分组
            Map<String, List<TestRoom>> rMap = entry.getValue().stream().collect(Collectors.groupingBy(TestRoom::getFloor));
            List<TestM> printList = Lists.newArrayList();
            for(Entry<String, List<TestRoom>> e:rMap.entrySet()){
                TestM tm = TestM.builder()
                        .floor(e.getKey())
                        .rooms(Lists.newArrayList())
                        .build();
                e.getValue().stream().forEach(em-> {
                    tm.getRooms().add(em.getRoom_num());
                });
                printList.add(tm);
            }
            printList.sort((a,b)-> a.getFloor().compareTo(b.getFloor()));
            printList.stream().forEach(p-> {
                System.out.println(JSON.toJSONString(p));
                System.out.println("---------------");
            });
        }
    }
}
