package org.xqh.test.yzs.shimao;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Builder;
import lombok.Data;
import org.apache.http.Header;
import org.springframework.util.CollectionUtils;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.excel.ExcelReader;
import org.xqh.utils.file.ReadTxtFileUtils;
import org.xqh.utils.http.FxHttpClientUtils;
import org.xqh.utils.http.HttpRetModel;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ExportDeviceStrategyTest
 * @Description 导出 设备 & 策略
 * @Author xuqianghui
 * @Date 2021/2/1 17:31
 * @Version 1.0
 */
public class ExportDeviceStrategyTest {

    public static String device_page_url = "https://open.yunmaolink.com/API/tech-mgmt/v2/business-device-shadow/device/query-page";

    /**
     * 设备编组
     */
    public static String device_group_url = "https://open.yunmaolink.com/API/tech-mgmt/v2/business-device-shadow/device-group/query-device-group-page-list";

    public static String strategy_page_url = "https://open.yunmaolink.com/API/tech-mgmt/v2/business-strategy-server/strategy/query-page-by-space-id-and-name";

    //编组策略
    public static String strategy_group_url = "https://open.yunmaolink.com/API/tech-mgmt/v2/business-strategy-server/strategy-group/query-page-group-strategy";

    public static String req_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjp7InRlcnJhY2UiOiJDT05ORUNUIiwidXNlcklkIjoiTWpVNCIsImlwIjoiTVRFNUxqRTBOUzQ1TGpJd013PT0iLCJ1c2VybmFtZSI6IuaWsOeUqOaItzE1OTMifSwic3ViIjoi5paw55So5oi3MTU5MyIsImlzcyI6IlNTTyIsImlhdCI6MTYxMjE2OTc1OCwiYXVkIjoiQ09OTkVDVCIsImV4cCI6MjYxMDE2OTY5OCwibmJmIjoxNjEyMTY5Njk4fQ.qr_NDdlirD3KgAYBA65QVk_fIqmtym2Vkbrfh6XvM_k";

    public static String device_export_url = "https://open.yunmaolink.com/API/tech-mgmt/v2/business-device-shadow/export-device";

    public static String work_path = "E:\\document\\yzs\\program\\shimao-iot\\";

    public static void main(String[] args) {
        Map<String, DeviceSpaceModel> spaceMap = parseDeviceSpace();
//        downloadDevice(2650L, work_path+"武夷山御榕new-output.xlsx", spaceMap);
//        downloadDevice(1392L, work_path+"武汉凡象new-output.xlsx", spaceMap);

        downloadStrategy(work_path+"武汉凡象空间ID.txt", work_path+"武汉凡象策略-output.xlsx", spaceMap);
        downloadStrategy(work_path+"武夷山御榕空间ID.txt", work_path+"武夷山御榕策略-output.xlsx", spaceMap);
    }

    public static Map<String, DeviceSpaceModel> parseDeviceSpace(){
         Map<String, DeviceSpaceModel> map = Maps.newHashMap();
         List<String[]> excelList = ExcelReader.getExcelData(work_path + "空间树导出.xlsx", 2);
         for(String[] array:excelList){
             map.put(array[0], DeviceSpaceModel.builder()
                     .spaceId(Long.valueOf(array[0]))
                     .spaceName(array[1])
                     .spacePath(array[2])
                     .build());
         }
         return map;
    }

    @Data
    @Builder
    public static class DeviceSpaceModel{
        private String spaceName;
        private Long spaceId;
        private String spacePath;
    }

    /**
     * 技术运营平台 设备列表导出
     * @param entityIds
     */
    public static void exportDevice(List<Integer> entityIds){
        Map<String, Object> req = Maps.newHashMap();
        req.put("entityIds", entityIds);
        try {
            FxHttpClientUtils.downloadFile(device_export_url, new File("E:\\document\\yzs\\program\\shimao-iot\\device-export.xlsx"), getReqHeader(), JSON.toJSONString(req));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Header[] getReqHeader(){
        return HttpHeader.custom()
                .other("token", req_token)
                .build();
    }

    public static HttpRetModel getRetModel(){
        return HttpRetModel.builder()
                .dataKey("data")
                .statusKey("retcode")
                .successVal("0")
                .msgKey("msg")
                .build();
    }

    public static void downloadStrategy(String spaceIdFile,String outFile, Map<String, DeviceSpaceModel> spaceMap){
        List<String> spaceIds = ReadTxtFileUtils.readTxt(new File(spaceIdFile));
        JSONArray array = new JSONArray();
        for(String spaceId:spaceIds){
            queryStrategy(spaceId, "entity", array);
            queryStrategy(spaceId, "group", array);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        String[] headers = {"策略ID", "策略名称", "标签", "策略类型", "status", "空间ID", "空间名称", "空间路径", "是否隐藏"};
        String[] properties = {"strategyId", "name", "tagName", "strategyType", "status", "spaceId", "spaceName", "spacePath", "hiddernInTerminal"};
        List<Map<String,Object>> dataMap = Lists.newArrayList();
        for(Object obj:array){
            Map<String, Object> map = JSON.parseObject(JSON.toJSONString(obj), Map.class);
            dataMap.add(map);
            if(spaceMap.containsKey(map.get("spaceId").toString())){
                map.put("spacePath", spaceMap.get(map.get("spaceId").toString()).getSpacePath());
                map.put("spaceName", spaceMap.get(map.get("spaceId").toString()).getSpaceName());
            }
        }
        ExcelExportUtil.export(headers, properties, dataMap, true, outFile);
    }

    public static void downloadDevice(Long spaceId, String outFile, Map<String, DeviceSpaceModel> spaceMap){
        JSONArray array = new JSONArray();
        try {
            queryDevice(spaceId, 1, 200, array, true);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String[] headers = {"设备名称", "设备ID", "设备品牌", "设备模型", "设备编码", "空间ID", "空间名称", "空间路径", "是否隐藏", "status"};
        String[] properties = {"entityName", "entityId", "brand.brandName", "panType.panTypeName", "entityCode", "spaceId", "spaceName", "spacePath", "isHidden", "status"};
        List<Map<String,Object>> dataMap = Lists.newArrayList();
        for(Object obj:array){
            Map<String, Object> map = JSON.parseObject(JSON.toJSONString(obj), Map.class);
            dataMap.add(map);
            if(spaceMap.containsKey(map.get("spaceId").toString())){
                map.put("spacePath", spaceMap.get(map.get("spaceId").toString()).getSpacePath());
            }
        }
        ExcelExportUtil.export(headers, properties, dataMap, true, outFile);
    }

    public static void queryDevice(Long spaceId, int page, int pageSize, JSONArray array, boolean findAll) throws InterruptedException {
        Map<String, Object> reqMap = Maps.newHashMap();
        reqMap.put("spaceId", spaceId);
        reqMap.put("projectSpaceId", spaceId);
        reqMap.put("isIncludeChildSpace", 1);
        reqMap.put("pageNum", page);
        reqMap.put("pageSize", pageSize);
        reqMap.put("rootSpaceId", 504);

        /**
         * {"spaceId":504,"brandCode":null,"rootSpaceId":504,"projectSpaceId":null,"pageNum":1,"pageSize":10,"isIncludeChildSpace":0}
         */
        HttpConfig config = FxHttpClientUtils.getPostJsonConfig(device_page_url, getReqHeader(), JSON.toJSONString(reqMap));
        JSONObject json = FxHttpClientUtils.requestReturnObj(config, JSONObject.class, getRetModel());
        array.addAll(json.getJSONArray("list"));
        if(findAll && !json.getBoolean("isLastPage")){
            //循环调用
            Thread.sleep(200);
            page ++;
            queryDevice(spaceId, page, pageSize, array, findAll);
        }
    }

    public static List<Object> queryDeviceGroup(){
        /**
         * {"spaceId":2650,"rootSpaceId":504,"projectSpaceId":2650,"pageNum":1,"pageSize":10,"typeId":"","groupName":"","isQuerySubSpace":"Y","brandCode":null}
         */
        return null;
    }

    public static void queryStrategy(String spaceId, String type, JSONArray array){

        /**
         * {"name":"","spaceId":504,"pageNum":1,"pageSize":10,"strategyType":"entity"}
         */
        Map<String, Object> reqMap = Maps.newHashMap();
        reqMap.put("spaceId", spaceId);
        reqMap.put("strategyType", type);
        reqMap.put("pageNum", 200);
        reqMap.put("pageSize", 1);

        HttpConfig config = FxHttpClientUtils.getPostJsonConfig(strategy_page_url, getReqHeader(), JSON.toJSONString(reqMap));
        JSONObject json = FxHttpClientUtils.requestReturnObj(config, JSONObject.class, getRetModel());
        JSONArray items = json.getJSONArray("list");
        if(!CollectionUtils.isEmpty(items)){
            array.addAll(items);
        }
    }

    public static List<Object> queryStrategyGroup(){

        /**
         * {"name":"","pageNum":1,"pageSize":20,"spaceId":504,"strategyType":"group"}
         */
        String reqStr = "";
        return null;

    }

}
