package org.xqh.test.yzs.mqtt;

/**
 * @ClassName SubClientTest
 * @Description 订阅test
 * @Author xuqianghui
 * @Date 2020/6/24 14:35
 * @Version 1.0
 */
public class SubClientTest3 {

    private static final String sub_client_id = "sub_client_id3";//默认clientId


    public static void main(String[] args) {
        EmqxClient client = new EmqxClient(sub_client_id);
        client.setSub(true);//订阅端
        client.start();
        client.defaultSub("$share/g2/test/abc/1");
        client.subscribeTopic();
    }
}
