package org.xqh.test.yzs.mqtt;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.xqh.utils.DateUtil;

import java.util.Date;
import java.util.Properties;
import java.util.UUID;

/**
 * @ClassName PubClientTest
 * @Description 发布client
 * @Author xuqianghui
 * @Date 2020/6/17 13:28
 * @Version 1.0
 */
public class PubClientTest {

    /**
     * 默认发布端 clientId
     */
    private final static String pub_client_id = "publish_client_id2";
//    private final static String pub_client_id = "1ed3475435b440d2ad32e0ae0a31c96e";

    public static void main(String[] args) throws InterruptedException {

//        obj.put("msg", "normal-1.jpg");
//        obj.put("msg", "retain-3.jpg");
//        String msg = "delay msg "+ DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");

        // 发布消息
//        pubMsg(msg);

        String str = "{\n" +
                "    \"msgType\":\"dc_001\",\n" +
                "    \"msgId\":\"120062113322\",\n" +
                "    \"ts\":1669425652063,\n" +
                "    \"data\": \"online\" \n" +
                "}";
        JSONObject obj = JSON.parseObject(str);

        //指定队列发送消息

        String topic = "nas/device/pub/UG549HH11220F123";
//        for(int i = 0 ; i < 10 ; i ++){
//            obj.put("idx", i);
//            String msg = obj.toJSONString();
//            pubMsg(msg, topic);
//        }

        obj.put("ts", System.currentTimeMillis());
        obj.put("msgId", UUID.randomUUID().toString().replaceAll("-", ""));
        String msg = obj.toJSONString();
        pubMsg(msg, topic);

        //发送保留消息
//        pubMsg(msg, topic, true);

        //发布延迟消息 10秒
//        pubMsg(msg, "$delayed/10/delay/topic");

//        System.out.println(UUID.randomUUID().toString().replaceAll("-", ""));


    }

    /**
     * 普通发布
     * @param msg
     */
    public static void pubMsg(String msg){
        pubMsg(msg, EmqxClient.TOPIC);
    }

    public static void pubMsg(String msg, String topic){
        pubMsg(msg, topic, false);
    }

    public static void pubMsg(String msg, boolean retain){
        pubMsg(msg, EmqxClient.TOPIC, retain);
    }

    public static void pubMsg(String msg, String topic, boolean retain){
        EmqxClient client = new EmqxClient(pub_client_id, topic, retain);
        client.start();
        client.publishMsg(msg);
    }

}
