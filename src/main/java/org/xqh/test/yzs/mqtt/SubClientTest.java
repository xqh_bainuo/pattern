package org.xqh.test.yzs.mqtt;

import java.text.MessageFormat;

/**
 * @ClassName SubClientTest
 * @Description 订阅test
 * @Author xuqianghui
 * @Date 2020/6/24 14:35
 * @Version 1.0
 */
public class SubClientTest {

//    private static final String sub_client_id = "sub_client_id";//默认clientId
    private static final String sub_client_id = "DH00002006000270";//默认clientId


    public static void main(String[] args) {
        EmqxClient client = new EmqxClient(sub_client_id);
        client.setSub(true);//订阅端
        client.start();
        client.setCleanSession(true);
//        client.defaultSub("/*/topic/sub/space");
        client.defaultSub("nas/device/pub/DH00002006000270");
//        client.defaultSub("/nas/+/pub/#");
        client.subscribeTopic();

    }
}
