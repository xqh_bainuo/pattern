package org.xqh.test.yzs.mqtt;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import org.apache.http.Header;
import org.xqh.utils.http.FxHttpClientUtils;

/**
 * @ClassName MqttRestApiTest
 * @Description 请求emqx http接口
 * @Author xuqianghui
 * @Date 2022/12/12 15:28
 * @Version 1.0
 */
public class MqttRestApiTest {

    private final static String domain = "120.232.63.149:18083";

    private final static String client_url = "http://%s:%s@%s/api/v5/clients/%s";

    private final static String apiKey = "a4b69656842c7623";

    private final static String apiSecret = "gOKDyTHI7rNfp1rJgEmYr9CDSpHDo32kpm7dcvN6tnkF";


    public static void main(String[] args) throws HttpProcessException {

        requestClientStatus("cloud_mqtt_client_6762_subscriber");
    }

    public static void requestClientStatus(String clientId) throws HttpProcessException {
        String reqUrl = String.format(client_url, apiKey, apiSecret, domain, clientId);
//        Header[] headers = HttpHeader.custom()
//                .other("Authorization", String.format("%s:%s", apiKey, apiSecret))
//                .build();
        HttpConfig config = FxHttpClientUtils.getGetConfig(reqUrl, null);
        HttpResult httpRet = HttpClientUtil.sendAndGetResp(config);
        System.out.println(httpRet.getResult());
    }
}
