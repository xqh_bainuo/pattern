package org.xqh.test.yzs.trlog;

import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpMethods;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * @ClassName DownloadTRLogAudio
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/3/20 11:03
 * @Version 1.0
 */
public class DownloadTRLogAudio {

    //http://route.igaicloud.cn:8088/trafficRouter/m/audioDownload?requestId=db1fbb32cdc6303582164a9209102a32&audioFormat=audio/x-wav&audioCodec=opus&decode=true&wav=true

    /**
     * 下载TR 音频
     * @param host
     * @param requestId
     * @param session
     * @param outPath
     * @param suffix
     */
    public static void downloadAudio(String host, String requestId, String session, String outPath, String suffix){
        String url = host+"trafficRouter/m/audioDownload?requestId="+requestId+"&audioFormat=audio/x-wav&audioCodec=opus&decode=true&wav=true";

        FileOutputStream outputStream = null;
        try {
            //指定 接收流对象
            outputStream = new FileOutputStream(new File(outPath.concat(requestId).concat(suffix)));
            HttpConfig config = HttpConfig.custom()
                    .url(url)
                    .headers(HttpHeader.custom().cookie(session).build())
                    .method(HttpMethods.GET) // 指定请求方式
                    .out(outputStream);
            HttpClientUtil.down(config);//下载文件
        }catch (Exception e){

        }finally {
            if(Objects.nonNull(outputStream)){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
