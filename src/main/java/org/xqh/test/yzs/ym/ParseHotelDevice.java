package org.xqh.test.yzs.ym;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.xqh.utils.file.ReadTxtFileUtils;
import lombok.Data;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @ClassName ParseHotelDevice
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/5/18 15:20
 * @Version 1.0
 */
public class ParseHotelDevice {

    public final static String PATH = "E:\\document\\yzs\\program\\世茂\\";

    public static void main22(String[] args) {

        List<String> list = ReadTxtFileUtils.readTxt(new File(PATH+ "皇家艾美设备.txt"));
        for(String s:list){
            JSONObject json = JSON.parseObject(s);

            List<DeviceModel> dlist = JSONArray.parseArray(json.getString("data"), DeviceModel.class);
            List<String> slist = dlist.stream().map(DeviceModel::getDeviceCode).collect(Collectors.toList());

            ReadTxtFileUtils.writeToTxt(slist, PATH+"皇家艾美设备ID.txt");
        }

    }

    private final static String udid_regex = "(%s)+(.*?)+(>)";//解析udid正则

    @Data
    public static class DeviceModel{
        private String deviceCode;
    }

    public static void main(String[] args) {
         String text = ">设备编号: LTE3Nzg0MjcyMTUwMEVDRTE1NEYwMjhGNA\n" +
                 "\n" +
                 " >最近心跳: 2019-09-10 13:39:00\n" +
                 "\n" +
                 " >设备空间: 上海佘山茂御臻品之选酒店 豪华双床房 3031\n" +
                 "\n" +
                 " >设备名称: 智能音箱\n" +
                 "\n" +
                 " >设备类型: 智能音箱\n" +
                 "\n" +
                 " >告警时间: 2019-09-10 13:41:00\n" +
                 "\n";

         text = text.replaceAll("\\n", "");
         text = text + ">";
        System.out.println(text);
        System.out.println(regexMatchStr(text, String.format(udid_regex, ">设备编号: ")));
        System.out.println(regexMatchStr(text, String.format(udid_regex, "告警时间: ")));
        System.out.println(regexMatchStr(text, String.format(udid_regex, ">设备空间: ")));
    }

    public static String regexMatchStr(String log, String regex){
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(log);
        String ret = null;
        while (m.find()) {
            String matchStr = m.group();
            ret = matchStr;
            ret = ret.substring(0, ret.length() -1);
            ret = ret.trim();
        }
        return ret;
    }
}
