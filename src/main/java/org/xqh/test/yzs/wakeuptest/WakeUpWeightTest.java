package org.xqh.test.yzs.wakeuptest;

import com.google.common.collect.Lists;
import org.xqh.test.NumberUtils;
import org.xqh.utils.excel.ExcelExportUtil;
import org.xqh.utils.excel.ExcelReader;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @ClassName WakeUpWeightTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/2/21 10:09
 * @Version 1.0
 */
public class WakeUpWeightTest {


    private final static String FILE_PATH = "E:\\document\\yzs\\program\\运营数据分析\\1.20-1.26\\小茂音箱1.20-1.26标注\\";

    private static List<String> nameList = Arrays.asList("上海佘山茂御臻品之选酒店", "皇家艾美酒店");

    private final static int limit_size = 100000;

    private final static String oneshot_key = "小茂小茂";

    public static void main(String[] args) {
//        parseAndExport("newfile.xlsx", "testpcm", "pcm", true);
        outputCompareResult("newfile.xlsx", "dev-oneshot1-2.0.xlsx", "dev-oneshot1-2.0-output.xlsx");
//        String a = "我是一个兵来自老百姓";
//        String b = "来自老百姓qqqq";
////        String b = "来自老百姓的人是我的";
//        CombineModel match = new CombineModel();
//        matchTwoProbeRet(a, b, b.length(),match);
//        System.out.println(JSON.toJSONString(match));
    }

    public static void matchTwoProbeRet(String a, String b, int len, CombineModel match){
        if(a.equals(b)){
            match.setType(1);//两次结果一致的情况 不处理
            return ;
        }
        if(a.startsWith(b)){
            match.setType(2);//后续结果 变短 并且是 前面结果的前面内容
            return ;
        }

        if(len == 0){
            match.setType(4);//两次结果 没有 匹配到结果
            return;
        }

        String mStr = b.substring(0, len);
        if(a.endsWith(mStr)){
            match.setType(3);//
            match.setCombineStr(a.concat(b.substring(len)));
            return;
        }else {
            matchTwoProbeRet(a, b, len - 1, match);
        }
    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CombineModel {

        private int type;

        private String combineStr;

    }

    /**
     * 输出对比结果
     */
    public static void outputCompareResult(String parseFile, String asrRetFile, String outputFile) {
        File f1 = new File(FILE_PATH.concat(parseFile));
        List<WakeUpModel> resource = Lists.newArrayList();
        List<String[]> rList = ExcelReader.getExcelData(f1, 1);
        for (String[] r : rList) {
            if (Objects.nonNull(r) && r.length >= 2 && StringUtils.hasText(r[1])) {
                WakeUpModel w = new WakeUpModel();
                w.setText(r[0]);
                w.setSessionId(r[1]);
                resource.add(w);
            }
        }

        File f2 = new File(FILE_PATH.concat(asrRetFile));
        List<String[]> oList = ExcelReader.getExcelData(f2, 1);
        List<WakeUpModel> outList = Lists.newArrayList();
        for (String[] o : oList) {
            if (Objects.nonNull(o) && o.length >= 6 && StringUtils.hasText(o[0])) {
                WakeUpModel m = new WakeUpModel();
                m.setSessionId(o[0]);
                m.setUatAsrRet(o[1]);
                m.setUatSessionId(o[3]);
                m.setProbeRet(o[5]);
                outList.add(m);
            }
        }

        for (WakeUpModel r : resource) {
            Optional<WakeUpModel> optional = outList.stream().filter(o -> o.getSessionId().equals(r.getSessionId())).findAny();
            if (optional.isPresent()) {
                WakeUpModel cur = optional.get();
                r.setUatSessionId(cur.getUatSessionId());
                r.setUatAsrRet(cur.getUatAsrRet());
                r.setProbeRet(cur.getProbeRet());
                r.setMatch(true);
            }

            // 线上 环境 识别出 文本,  测试未识别出文本
            if(StringUtils.hasText(r.getText()) && r.getText().startsWith(oneshot_key) && StringUtils.isEmpty(r.getUatAsrRet())){
                r.setOneshotError("是");
            }else {
                r.setOneshotError("否");
            }

            if(NumberUtils.compareTtsAsrRet(r.getText(), r.getUatAsrRet())){
                r.setSame("是");
            }else{
                r.setSame("否");
            }
        }

        resource = resource.stream().filter(r-> r.isMatch()).collect(Collectors.toList());
        String[] h = {"生产识别结果","生产requestId","oneshot:1结果", "dev requestId", "是否误唤醒", "是否一致", "中间结果"};
        String[] p = {"text","sessionId","uatAsrRet", "uatSessionId", "oneshotError", "same", "probeRet"};
        ExcelExportUtil.export(h, p, resource, false, FILE_PATH.concat(outputFile));

    }


    /**
     *
     * @param parseFile 解析导出excel 文件名
     * @param targetPath 移动音频 目标目录
     * @param suffix 音频后缀  .pcm  .mp3
     * @param moveAudio 是否 移动 音频文件
     */
    public static void parseAndExport(String parseFile, String targetPath, String suffix, boolean moveAudio) {
        List<WakeUpModel> data = parseData();
        String[] head = {"文本", "sessionId"};
        String[] p = {"text", "sessionId"};
        ExcelExportUtil.export(head, p, data, false, FILE_PATH.concat(parseFile));
        if(!moveAudio){
            return ;
        }
        // copy pcm到对应目录
        for (WakeUpModel w : data) {
            String fileName = w.getSessionId().concat("."+suffix);
            File f = new File(FILE_PATH.concat("ym-hotel\\"+suffix+"\\").concat(fileName));
            File tf = new File(FILE_PATH.concat(targetPath+"\\").concat(fileName));
            if(!tf.exists()){
                try {
                    FileUtils.copyFile(f, tf);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static List<WakeUpModel> parseDataByFile(String file) {
        List<WakeUpModel> retList = Lists.newArrayList();
        List<String[]> fileData = ExcelReader.getExcelData(new File(FILE_PATH+file), 1);
        for (String[] a : fileData) {
            if (Objects.nonNull(a) && a.length > 2 && StringUtils.hasText(a[1])) {
                WakeUpModel m = new WakeUpModel();
                m.setText(a[0]);
                m.setSessionId(a[1]);
                retList.add(m);
            }
        }
        return retList;
    }



    public static List<WakeUpModel> parseData() {
        List<WakeUpModel> retList = Lists.newArrayList();
        File pf = new File(FILE_PATH.concat("ym-hotel"));
        b1:
        for (File f : pf.listFiles()) {
            if (f.isFile() && checkIsNeedFile(f.getName())) {
                List<String[]> fileData = ExcelReader.getExcelData(f, 1);
                b2:
                for (String[] a : fileData) {
                    if (Objects.nonNull(a) && a.length > 15 && StringUtils.hasText(a[14])) {
                        WakeUpModel m = new WakeUpModel();
                        m.setText(a[0]);
                        String mp3 = a[14];
                        m.setSessionId(mp3.substring(0, mp3.length() - 4));
                        retList.add(m);
                        if (retList.size() >= limit_size) {
                            break b1;
                        }
                    }
                }
            }
        }
        return retList;
    }

    public static boolean checkIsNeedFile(String fName) {
        for (String n : nameList) {
            if (fName.contains(n)) {
                return true;
            }
        }
        return false;
    }

    @Data
    public static class WakeUpModel {

        private String text;

        private String sessionId;

        private String uatSessionId;

        private String uatAsrRet;//预发布环境 识别结果

        private String same;

        private String probeRet;

        private boolean match;//是否匹配到

        /**
         * 线上 识别出 唤醒词, 测试未识别出 唤醒词
         */
        private String oneshotError;
    }

}
