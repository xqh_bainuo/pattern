package org.xqh.test.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import org.xqh.utils.excel.EasyPoiUtils;
import org.xqh.utils.excel.EasyPoiUtils.ExportSheetModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.compress.utils.Lists;

import java.util.Date;
import java.util.List;

/**
 * @ClassName ExcelTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2020/12/15 9:54
 * @Version 1.0
 */
public class ExcelTest {

    public static final String work_path = "D:\\TestSpace\\";

    public static void main(String[] args) {

    }

    public static void exportTest(){
        ExcelDemo demo = ExcelDemo.builder().name("mingcheng").date(new Date()).build();
        List<ExcelDemo> list = Lists.newArrayList();
        list.add(demo);
        ExportSheetModel<ExcelDemo> model = new ExportSheetModel<>();
        model.setTitle("test title");
        model.setSheetName("test sheet");
        model.setData(list);
        model.setEntity(ExcelDemo.class);
        EasyPoiUtils.exportToFile(model, work_path + "abc11.xls");
    }

    public static void importTest(){

    }

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class ExcelDemo{

        @Excel(name = "名称")
        private String name;

        @Excel(name = "操作时间", exportFormat = "yyyy-MM-dd HH:mm:ss", width = 30)
        private Date date;
    }
}
