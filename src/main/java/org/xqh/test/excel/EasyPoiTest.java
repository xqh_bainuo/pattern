//package org.xqh.test.excel;
//
//import cn.afterturn.easypoi.excel.ExcelExportUtil;
//import cn.afterturn.easypoi.excel.entity.ExportParams;
//import cn.afterturn.easypoi.excel.entity.TemplateExportParams;
//import org.apache.commons.compress.utils.Lists;
//import org.apache.poi.ss.usermodel.Workbook;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.util.*;
//
///**
// * @ClassName EasyPoiTest
// * @Description TODO
// * @Author xuqianghui
// * @Date 2021/8/25 14:08
// * @Version 1.0
// */
//public class EasyPoiTest {
//
//    public static String work_path = "E:\\test\\";
//
//    public static void main(String[] args) throws Exception {
////        fe_map();
//        exportStudentDemo();
//    }
//
//
//    /**
//     * 注解导出示例
//     * @throws Exception
//     */
//    public static void exportStudentDemo() throws Exception {
//        List<StudentEntity> list = Lists.newArrayList();
//        list.add(
//                StudentEntity.builder()
//                        .id("addfid")
//                        .name("张三")
//                        .birthday(new Date())
//                        .registrationDate(new Date())
//                        .sex(1)
//                        .build()
//        );
//        list.add(
//                StudentEntity.builder()
//                        .id("addfid2")
//                        .name("张四")
//                        .birthday(new Date())
//                        .registrationDate(new Date())
//                        .sex(1)
//                        .build()
//        );
//        list.add(
//                StudentEntity.builder()
//                        .id("addfid3")
//                        .name("张五")
//                        .birthday(new Date())
//                        .registrationDate(new Date())
//                        .sex(1)
//                        .build()
//        );
//        TeacherEntity teacher = TeacherEntity.builder().name("王老师").id("11").build();
//        List<CourseEntity> courseList = Lists.newArrayList();
//        courseList.add(CourseEntity.builder().name("海贼王必修1").students(list).mathTeacher(teacher).build());
//        courseList.add(CourseEntity.builder().name("海贼王必修2").students(list).mathTeacher(teacher).build());
//        courseList.add(CourseEntity.builder().name("海贼王必修3").students(list).mathTeacher(teacher).build());
//        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams("2412312", "测试", "测试"),
//                CourseEntity.class, courseList);
//        File savefile = new File("E:/test/excel");
//        if (!savefile.exists()) {
//            savefile.mkdirs();
//        }
//        FileOutputStream fos = new FileOutputStream(work_path + "annotation_export.xlsx");
//        workbook.write(fos);
//        fos.close();
//    }
//
//    public static void test(int index, String str, int idx2){
//        System.out.println(index + ", " + idx2);
//    }
//
//    /**
//     * 模板导出 示例
//     * @throws Exception
//     */
//    public static void fe_map() throws Exception {
//        TemplateExportParams params = new TemplateExportParams(
//                work_path+"专项支出用款申请书_template.xlsx");
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("date", "2014-12-25");
//        map.put("money", 2000000.00);
//        map.put("upperMoney", "贰佰万");
//        map.put("company", "执笔潜行科技有限公司");
//        map.put("bureau", "财政局");
//        map.put("person", "JueYue");
//        map.put("phone", "1879740****");
//        params.setStyle(ExcelExportStatisticStyler.class);
//        List<Map<String, String>> listMap = new ArrayList<Map<String, String>>();
//        for (int i = 0; i < 20; i++) {
//            Map<String, String> lm = new HashMap<String, String>();
//            lm.put("id", i + "");
//            lm.put("zijin", i * 10000 + "");
//            lm.put("bianma", "A00"+i);
//            lm.put("mingcheng", "设计");
//            lm.put("xiangmumingcheng", "EasyPoi " + i + "期");
//            lm.put("quancheng", "开源项目");
//            lm.put("sqje", i * 10000 + "");
//            lm.put("hdje", i * 10000 + "");
//
//            listMap.add(lm);
//        }
//        map.put("maplist", listMap);
//
//        Workbook workbook = ExcelExportUtil.exportExcel(params, map);
//        File savefile = new File("E:/test/excel");
//        if (!savefile.exists()) {
//            savefile.mkdirs();
//        }
//        FileOutputStream fos = new FileOutputStream(work_path + "专项支出用款申请书_result.xlsx");
//        workbook.write(fos);
//        fos.close();
//    }
//}
