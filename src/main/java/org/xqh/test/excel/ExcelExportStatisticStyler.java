//package org.xqh.test.excel;
//
//import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
//import cn.afterturn.easypoi.excel.export.styler.ExcelExportStylerDefaultImpl;
//import com.alibaba.fastjson.JSON;
//import org.apache.poi.ss.usermodel.*;
//
//import java.util.Map;
//import java.util.Objects;
//
///**
// * Excel 自定义styler 的例子
// * @author JueYue
// *   2015年3月29日 下午9:04:41
// */
//public class ExcelExportStatisticStyler extends ExcelExportStylerDefaultImpl {
//
//    private CellStyle numberCellStyle;
//    private CellStyle stringCellStyle;
//
//    public ExcelExportStatisticStyler(Workbook workbook) {
//        super(workbook);
//        createNumberCellStyler();
//        createStringCellStyler();
//    }
//
//    public CellStyle getStyleByColorIndex(int idx){
//        CellStyle result = workbook.createCellStyle();
//        result.setAlignment(HorizontalAlignment.CENTER);
//        result.setVerticalAlignment(VerticalAlignment.CENTER);
//        result.setWrapText(true);
//        result.setFillForegroundColor((short) idx);
//        result.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//        return result;
//    }
//
//    private void createStringCellStyler() {
//        stringCellStyle = workbook.createCellStyle();
//        stringCellStyle.setAlignment(HorizontalAlignment.CENTER);
//        stringCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
//        stringCellStyle.setWrapText(true);
//        stringCellStyle.setFillForegroundColor((short) 56);
//        stringCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//    }
//
//    private void createNumberCellStyler() {
//        numberCellStyle = workbook.createCellStyle();
//        numberCellStyle.setAlignment(HorizontalAlignment.CENTER);
//        numberCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
//        numberCellStyle.setDataFormat((short) BuiltinFormats.getBuiltinFormat("0.00"));
//        numberCellStyle.setWrapText(true);
//        numberCellStyle.setFillForegroundColor((short) 33);
//        numberCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
//    }
//
//    @Override
//    public CellStyle getStyles(boolean noneStyler, ExcelExportEntity entity) {
//        System.out.println(JSON.toJSONString(entity));
//        return super.getStyles(noneStyler, entity);
//    }
//
//    @Override
//    public CellStyle getStyles(int dataRow, int dataCol, ExcelExportEntity entity, Object obj, Object data) {
//        System.out.println("row ==> "+dataRow +", col ==>" + dataCol +", colName ==>"+entity.getName() +", colValue ==>"+data +", rowObj ==>"+ JSON.toJSONString(obj));
//        if(dataCol == 2 && Objects.equals("15", String.valueOf(data))){
//            return stringCellStyle;
//        }
//
//        if(dataCol == 4 && Objects.equals("1212", String.valueOf(data))){
//            return numberCellStyle;
//        }
//        return super.getStyles(dataRow, dataCol, entity, obj, data);
//    }
//
//    @Override
//    public CellStyle getTemplateMapStyles(int dataRow, int dataCol, Map<String, Object> map, Object data, String templateString) {
////        System.out.println("map-template row ==> "+dataRow +", col ==>" + dataCol +", colValue ==>"+data +", rowObj ==>"+ templateString);
////        if(1 == 1){
////            return stringCellStyle;
////        }
//        return super.getTemplateMapStyles(dataRow, dataCol, map, data, templateString);
//    }
//
//    @Override
//    public CellStyle getTemplateMapForeachStyles(Object data, String listKey, String mapCellKey, String cellVal, int dataCol, Integer rowIndex, Integer listIndex) {
////        System.out.println("foreach-map-style: "+JSON.toJSONString(data)+", "+listKey+", "+mapCellKey+", "+cellVal+", "+dataCol+", "+rowIndex+", "+listIndex);
//        System.out.println("foreach-map-style: "+listKey+", "+mapCellKey +", "+cellVal +", "+dataCol+", "+rowIndex+", "+listIndex);
//        if(Objects.equals("t.id", mapCellKey)){
//            return getStyleByColorIndex(Integer.parseInt(cellVal));
//        }
//        return super.getTemplateMapForeachStyles(data, listKey, mapCellKey, cellVal, dataCol, rowIndex, listIndex);
//    }
//}
