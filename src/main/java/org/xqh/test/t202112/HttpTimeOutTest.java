package org.xqh.test.t202112;

import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpMethods;
import org.xqh.utils.http.FxHttpClientUtils;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName HttpTimeOutTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/12/11 11:04
 * @Version 1.0
 */
public class HttpTimeOutTest {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        CountDownLatch latch = new CountDownLatch(1);
        Thread t = new Thread("test-thread");
        executor.execute(new TestThread(latch));
        latch.await(5, TimeUnit.SECONDS);
        executor.shutdownNow();
        System.out.println("线程池关闭");
        System.out.println(executor.isShutdown());

    }

    public static class TestThread implements Runnable{
        private CountDownLatch latch;

        public TestThread(CountDownLatch latch){
            this.latch = latch;
        }

        @Override
        public void run() {
            try {
                HttpConfig config = HttpConfig.custom()
                        .url("http://localhost:8081/download")
                        .method(HttpMethods.GET)
                        .timeout(1000);
                FxHttpClientUtils.requestReturnString(config);
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
            latch.countDown();
        }
    }
}
