package org.xqh.test.t202112;

import com.alibaba.fastjson.JSON;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import lombok.SneakyThrows;
import org.xqh.utils.http.FxHttpClientUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * @ClassName TomcatConnectionsTest
 * @Description TODO
 * @Author xuqianghui
 * @Date 2021/12/15 11:05
 * @Version 1.0
 */
public class TomcatConnectionsTest {

    private static volatile BlockingQueue<Integer> idlePortQueue = new LinkedBlockingQueue<>(2000);

    private static Set<Integer> set = new HashSet<>();

    public static void main(String[] args) {
        idlePortQueue.add(123);
        idlePortQueue.add(456);
        idlePortQueue.add(123344);
        idlePortQueue.add(4555);
        System.out.println(JSON.toJSONString(idlePortQueue));

        set.add(112334);
        set.add(1123334);
        set.add(1126334);
        set.add(13123334);
        System.out.println(set.contains(13123334));
    }

    public static void mainasdg(String[] args) throws IOException {
        File file = new File("C:\\Users\\UGREEN\\Desktop\\aqf.docx.docx");
        FileInputStream inputStream = new FileInputStream(file);
        System.out.println(file.length());byte[] buf = new byte[1024 * 100];
        int readLength;
        int total = 0;
        while (((readLength = inputStream.read(buf)) != -1)) {
            total += readLength;
        }
        System.out.println(total);
        System.out.println((int)file.length());

    }

    public static void main11(String[] args) {
        for(int i = 0; i < 1000; i ++){
            new Thread(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    HttpConfig config = FxHttpClientUtils.getHttpConfig("https://test.ugreen.cloud/test/test");
                    config.method(HttpMethods.GET);
                    HttpResult httpRet = HttpClientUtil.sendAndGetResp(config);
                    System.out.println("============================="+httpRet.getResult());
                }
            }).start();
        }
    }
}
