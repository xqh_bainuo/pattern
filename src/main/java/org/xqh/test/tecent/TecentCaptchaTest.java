package org.xqh.test.tecent;

import com.alibaba.fastjson.JSONObject;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpHeader;
import com.arronlong.httpclientutil.common.HttpResult;
import org.apache.http.Header;
import org.xqh.utils.http.FxHttpClientUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @ClassName TecentCaptchaTest
 * @Description 腾讯验证码
 * @Author xuqianghui
 * @Date 2023/9/6 10:33
 * @Version 1.0
 */
public class TecentCaptchaTest {

    private final static String domain = "https://captcha.tencentcloudapi.com/";

    private final static Charset UTF8 = StandardCharsets.UTF_8;
    private final static String CT_JSON = "application/json; charset=utf-8";

    private final static Long captchaAppId = 190932221L;

    private final static String captchaAppSecret = "I1EqQjuhXWjFSscnJrCA3HA8D";

    private final static String apiSecretId = "AKIDGQPCrKdpuqYHO332tY6XwDNVdgbNzYw7";
    private final static String apiSecretKey = "62XyJXt7mIebR6iCXipyIWfhaaS8eO9r";

    private final static String action = "DescribeCaptchaResult";//核查验证码票据结果(Web及APP)

    private final static String version = "2019-07-22";

    private final static String service = "captcha";
    private final static String host = "captcha.tencentcloudapi.com";
    private final static String region = "ap-guangzhou";
    private final static String algorithm = "TC3-HMAC-SHA256";

    private final static String ticket = "tr03hEF28oXWs2ovNN1y5-7hP__UJyurYTxUPw9Wq_5gcvK6CkvP7EMpsyDqoJyg8D6SyDmy_jHW1y5w49Hjhwj7B1hOy8Clxg9AJFEy1PuY-v5C5Y1APZKHLo0XlcmxeuIrUmdCqFIUTqU*";
    private final static String randstr = "@2BG";

    public static String buildReqJson(){
        JSONObject json = new JSONObject();
        json.put("CaptchaAppId", captchaAppId);
        json.put("CaptchaType", 9L);
        json.put("UserIp", "113.90.12.40");
        json.put("Randstr", randstr);
        json.put("Ticket", ticket);
        json.put("AppSecretKey", captchaAppSecret);
        return json.toJSONString();
    }
    public static byte[] hmac256(byte[] key, String msg) throws Exception {
        Mac mac = Mac.getInstance("HmacSHA256");
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, mac.getAlgorithm());
        mac.init(secretKeySpec);
        return mac.doFinal(msg.getBytes(UTF8));
    }

    public static String sha256Hex(String s) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] d = md.digest(s.getBytes(UTF8));
        return DatatypeConverter.printHexBinary(d).toLowerCase();
    }

    public static void main(String[] args) throws Exception {
        String timestamp = String.valueOf(System.currentTimeMillis()/ 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        // 注意时区，否则容易出错
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = sdf.format(new Date(Long.valueOf(timestamp + "000")));

        // ************* 步骤 1：拼接规范请求串 *************
        String httpRequestMethod = "POST";
        String canonicalUri = "/";
        String canonicalQueryString = "";
        String canonicalHeaders = "content-type:application/json; charset=utf-8\n"
                + "host:" + host + "\n" + "x-tc-action:" + action.toLowerCase() + "\n";
        String signedHeaders = "content-type;host;x-tc-action";

        String payload = buildReqJson();
        String hashedRequestPayload = sha256Hex(payload);
        String canonicalRequest = httpRequestMethod + "\n" + canonicalUri + "\n" + canonicalQueryString + "\n"
                + canonicalHeaders + "\n" + signedHeaders + "\n" + hashedRequestPayload;
        System.out.println(canonicalRequest);

        // ************* 步骤 2：拼接待签名字符串 *************
        String credentialScope = date + "/" + service + "/" + "tc3_request";
        String hashedCanonicalRequest = sha256Hex(canonicalRequest);
        String stringToSign = algorithm + "\n" + timestamp + "\n" + credentialScope + "\n" + hashedCanonicalRequest;
        System.out.println(stringToSign);

        // ************* 步骤 3：计算签名 *************
        byte[] secretDate = hmac256(("TC3" + apiSecretKey).getBytes(UTF8), date);
        byte[] secretService = hmac256(secretDate, service);
        byte[] secretSigning = hmac256(secretService, "tc3_request");
        String signature = DatatypeConverter.printHexBinary(hmac256(secretSigning, stringToSign)).toLowerCase();
        System.out.println(signature);

        // ************* 步骤 4：拼接 Authorization *************
        String authorization = algorithm + " " + "Credential=" + apiSecretId + "/" + credentialScope + ", "
                + "SignedHeaders=" + signedHeaders + ", " + "Signature=" + signature;
        System.out.println("authorization=====>" + authorization);

        Header[] headers = HttpHeader.custom()
                .other("X-TC-Action", action)
                .other("X-TC-Region", region)
                .other("Content-Type", CT_JSON)
                .other("X-TC-Timestamp", timestamp)
                .other("X-TC-Version", "2019-07-22")
                .other("Authorization", authorization)
                .other("Host", host)
                .build();
        HttpConfig config = FxHttpClientUtils.getPostJsonConfig(domain, headers, payload);
        HttpResult result= HttpClientUtil.sendAndGetResp(config);
        System.out.println(result.getResult());
    }



    /**
     * 获取请求头
     * @return
     */
    public static Header[] getReqHeaders(String timestamp, String authorization){
        return HttpHeader.custom()
                .other("X-TC-Action", action)
                .other("X-TC-Region", "ap-shanghai")
                .other("X-TC-Timestamp", timestamp)
                .other("X-TC-Version", "2019-07-22")
                .other("Authorization", authorization)
                .other("Host", "captcha.tencentcloudapi.com")
                .other("X-TC-RequestClient", "SDK_JAVA_3.1.829")
                .build();

//        String secretKey = this.credential.getSecretKey();
//        byte[] secretDate = Sign.hmac256(("TC3" + secretKey).getBytes(StandardCharsets.UTF_8), date);
//        byte[] secretService = Sign.hmac256(secretDate, service);
//        byte[] secretSigning = Sign.hmac256(secretService, "tc3_request");
//        String signature = DatatypeConverter.printHexBinary(Sign.hmac256(secretSigning, stringToSign)).toLowerCase();
//        authorization = "TC3-HMAC-SHA256 Credential=" + url + "/" + credentialScope + ", SignedHeaders=" + signedHeaders + ", Signature=" + signature;

    }
}
