package org.xqh.test.alibaba;

import com.alibaba.fastjson.JSON;
import com.aliyun.auth.credentials.Credential;
import com.aliyun.auth.credentials.provider.StaticCredentialProvider;
import com.aliyun.sdk.service.alidns20150109.AsyncClient;
import com.aliyun.sdk.service.alidns20150109.models.*;
import darabonba.core.client.ClientOverrideConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * @ClassName DNSParseTest
 * @Description DNS动态解析测试
 * // Endpoint 请参考 https://api.aliyun.com/product/Alidns
 * @Author xuqianghui
 * @Date 2024/3/18 16:17
 * @Version 1.0
 */
@Slf4j
public class DNSParseTest {

    /**
     * access-key: LTAI5tPu5a3f2QNKoSonnNhA
     * access-secret: xNpe2dichBYf1TO2ErWY6quGtA1ZTP
     */
//    public final static String access_key = "LTAI5tM7Bm6j2DppEHXSViDU";
//    public final static String access_secret = "A5QpVzc9zJyDjOG78M8oZquzL4Jpl3";


    public final static String access_key = "LTAI5tPu5a3f2QNKoSonnNhA";
    public final static String access_secret = "xNpe2dichBYf1TO2ErWY6quGtA1ZTP";
    public final static String region_id = "cn-hangzhou";
    public final static String end_point = "alidns.cn-hangzhou.aliyuncs.com";
//    public final static String region_id = "us-east-1";
//    public final static String end_point = "alidns.us-east-1.aliyuncs.com";

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //查询dns记录参数
        DescribeDomainRecordsRequest describeDomainRecordsRequest = DescribeDomainRecordsRequest.builder()
                .domainName("ug.link")
//                .valueKeyWord("192.168.78.36")
                .RRKeyWord("direct-test")
//                .type("A")
                .pageSize(300L)
                .pageNumber(1L)
                .build();


        //删除dns解析参数
        DeleteDomainRecordRequest deleteDomainRecordRequest = DeleteDomainRecordRequest.builder()
                .recordId("916726419593751552")
                .build();

        //更新dns解析参数
        UpdateDomainRecordRequest updateDomainRecordRequest = UpdateDomainRecordRequest.builder()
                .recordId("916726419593751552")
                .rr("192-168-0-3.johnson.direct-test")
                .type("A")
                .value("192.168.78.148")
                .build();

        AddDomainRequest addDomainRequest = AddDomainRequest.builder()
                // Request-level configuration rewrite, can set Http request parameters, etc.
                // .requestConfiguration(RequestConfiguration.create().setHttpHeaders(new HttpHeaders()))
                .domainName("amy.direct-test.ug.link")
                .lang("zh")
                .build();

        //添加dns解析参数
        AddDomainRecordRequest addDomainRecordRequest = AddDomainRecordRequest.builder()
                .domainName("ug.link")
                .lang("zh")
                .rr("192-168-0-2.johnson.direct-test")  //wxf.direct-test.ugnas.com.cn
                .type("A")
                .value("192.168.78.147")// 192-168-78-36.xqh-19.direct-test.ug.link
                .build();
//            String recordId = asyncAddDomainRecord(addDomainRecordRequest);
//
//        System.out.println("add dns record result: " + recordId);
        asyncDelDomainRecord(deleteDomainRecordRequest);
//        addDomain(addDomainRequest);
        //批量删除 dns解析
//        DescribeDomainRecordsResponse dnsList = queryDnsRecordList(describeDomainRecordsRequest);
//        dnsList.getBody().getDomainRecords().getRecord().stream().forEach(re-> {
//            DeleteDomainRecordRequest del = DeleteDomainRecordRequest.builder()
//                    .recordId(re.getRecordId())
//                    .build();
//            asyncDelDomainRecord(del);
//        });
//        asyncUpdDomainRecord(updateDomainRecordRequest);

    }


    private static StaticCredentialProvider credentialProvider() {
        return StaticCredentialProvider.create(Credential.builder()
                .accessKeyId(access_key)
                .accessKeySecret(access_secret)
                .build());
    }

    private static AsyncClient asyncClient() {
        //默认杭州站点
        return AsyncClient.builder().region(region_id) // Region ID
                .credentialsProvider(credentialProvider())
                .overrideConfiguration(
                        ClientOverrideConfiguration.create()
                                // Endpoint 请参考 https://api.aliyun.com/product/Alidns
                                .setEndpointOverride(end_point)
                                .setConnectTimeout(Duration.ofSeconds(30))
                )
                .build();
    }

    public static void addDomain(AddDomainRequest req) {
        AsyncClient client = asyncClient();
        // Parameter settings for API request


        // Asynchronously get the return value of the API request
        CompletableFuture<AddDomainResponse> response = client.addDomain(req);
        // Synchronously get the return value of the API request
        AddDomainResponse resp = null;
        try {
            resp = response.get();
            System.out.println(JSON.toJSONString(resp));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
        // Asynchronous processing of return values
        /*response.thenAccept(resp -> {
            System.out.println(new Gson().toJson(resp));
        }).exceptionally(throwable -> { // Handling exceptions
            System.out.println(throwable.getMessage());
            return null;
        });*/

        // Finally, close the client
        client.close();
    }

    /**
     * 异步添加域名解析记录
     */
    public static String asyncAddDomainRecord(AddDomainRecordRequest request) {
        AsyncClient client = asyncClient();
        try {
            CompletableFuture<AddDomainRecordResponse> response = client.addDomainRecord(request);
            // Synchronously get the return value of the API request
            AddDomainRecordResponse resp = response.get();
            System.out.println(JSON.toJSONString(resp));
            return resp.getBody().getRecordId();
        } catch (Exception e) {
            log.error("add domain record err.", e);
            //解析记录已存在 需要查询到对应的recordId更新到DB
            if (StringUtils.hasText(e.getMessage()) && e.getMessage().contains("DomainRecordDuplicate")) {
                return handleAlreadyExistDnsRecord(request);
            }
        } finally {
            client.close();
        }
        return null;
    }

    private static String handleAlreadyExistDnsRecord(AddDomainRecordRequest addReq) {
        DescribeDomainRecordsRequest queryReq = DescribeDomainRecordsRequest.builder()
                .domainName(addReq.getDomainName())
                .valueKeyWord(addReq.getValue())
                .RRKeyWord(addReq.getRr())
                .type(addReq.getType())
                .pageNumber(1L)
                .pageSize(10L)
                .build();
        DescribeDomainRecordsResponse queryResult = queryDnsRecordList(queryReq);
        if (Objects.nonNull(queryResult) && !CollectionUtils.isEmpty(queryResult.getBody().getDomainRecords().getRecord())) {
            return queryResult.getBody().getDomainRecords().getRecord().get(0).getRecordId();
        }
        return null;
    }

    /**
     * 更新域名解析
     *
     * @param request
     * @return
     */
    public static UpdateDomainRecordResponse asyncUpdDomainRecord(UpdateDomainRecordRequest request) {
        AsyncClient client = asyncClient();
        try {
            CompletableFuture<UpdateDomainRecordResponse> response = client.updateDomainRecord(request);
            // Synchronously get the return value of the API request
            UpdateDomainRecordResponse resp = response.get();
            System.out.println(JSON.toJSONString(resp));
            return resp;
        } catch (Exception e) {
            log.error("update domain record err.", e);
        } finally {
            client.close();
        }
        return null;
    }

    /**
     * 删除域名解析
     *
     * @param request
     * @return
     */
    public static DeleteDomainRecordResponse asyncDelDomainRecord(DeleteDomainRecordRequest request) {
        AsyncClient client = asyncClient();
        try {
            CompletableFuture<DeleteDomainRecordResponse> response = client.deleteDomainRecord(request);
            // Synchronously get the return value of the API request
            DeleteDomainRecordResponse resp = response.get();
            System.out.println("删除解析记录结果: " + request.getRecordId() + " ===>" + JSON.toJSONString(resp));
            return resp;
        } catch (Exception e) {
            log.error("del domain record err.", e);
        } finally {
            client.close();
        }
        return null;
    }

    public static DescribeDomainRecordsResponse queryDnsRecordList(DescribeDomainRecordsRequest request) {
        AsyncClient client = asyncClient();
        try {
            CompletableFuture<DescribeDomainRecordsResponse> response = client.describeDomainRecords(request);
            // Synchronously get the return value of the API request
            DescribeDomainRecordsResponse resp = response.get();
            System.out.println(JSON.toJSONString(resp));
            return resp;
        } catch (Exception e) {
            log.error("query domain record err.", e);
        } finally {
            client.close();
        }
        return null;
    }
}
