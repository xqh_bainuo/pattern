package org.xqh.test.alibaba;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.aliyun.dingtalkrobot_1_0.models.BatchSendOTOHeaders;
import com.aliyun.dingtalkrobot_1_0.models.BatchSendOTORequest;
import com.aliyun.dingtalkrobot_1_0.models.BatchSendOTOResponse;
import com.aliyun.tea.TeaException;
import com.aliyun.teaopenapi.models.Config;
import com.aliyun.teautil.models.RuntimeOptions;
import com.arronlong.httpclientutil.HttpClientUtil;
import com.arronlong.httpclientutil.common.HttpConfig;
import com.arronlong.httpclientutil.common.HttpMethods;
import com.arronlong.httpclientutil.common.HttpResult;
import com.arronlong.httpclientutil.exception.HttpProcessException;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;
import org.xqh.common.constants.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @ClassName DingDingPushTest @Description TODO @Author xuqianghui @Date 2020/6/30 17:57 @Version
 * 1.0
 */
@Slf4j
public class DingDingPushTest {

  public static final String URL =
      "https://oapi.dingtalk.com/robot/send?access_token=899c7308fc7fb4d37e54b038390337d23e5ac68e524454c32aaaf5f22757d9be";

  public static final String SECRET =
      "SEC240561267866f4ff0aaaeea0531b7846beeb4915cdf5c78c65753298f562e39b";

  public static void main(String[] args) throws HttpProcessException {
    String json = buildPushMsg2();
    System.out.println(json);
    Long timestamp = System.currentTimeMillis();
    String sign = getDingDingPushSign(SECRET, timestamp);
    String url = URL.concat("&timestamp=" + timestamp).concat("&sign=").concat(sign);
    HttpConfig config = HttpConfig.custom().url(url).method(HttpMethods.POST).json(json);
    HttpResult httpResult = HttpClientUtil.sendAndGetResp(config);
    System.out.println(httpResult.getResult());
  }

  public static String getMarkDownMsg() {
    String msg =
        "<font color=#FF0000 size=6 face='黑体'>事故等级:  </font>        \n"
            + "    ##### **发生时间**:       \n"
            + "    ##### **机器**:       \n"
            + "    **问题**:      \n"
            + "    **报警值**: ";
    return msg;
  }

  public static String buildTestMsg() {
    return "{\n"
        + "     \"msgtype\": \"markdown\",\n"
        + "     \"markdown\": {\n"
        + "         \"title\":\"杭州天气\",\n"
        + "         \"text\": \"#### 杭州天气 @181XXXXXXXX \\n> 9度，西北风1级，空气良89，相对温度73%\\n> ![screenshot](https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png)\\n> ###### 10点20分发布 [天气](https://www.dingalk.com) \\n\"\n"
        + "     },\n"
        + "      \"at\": {\n"
        + "          \"atMobiles\": [\n"
        + "              \"18123801593\"\n"
        + "          ],\n"
        +
        //              "          \"atUserIds\": [\n" +
        //              "              \"user123\"\n" +
        //              "          ],\n" +
        "          \"isAtAll\": true\n"
        + "      }\n"
        + " }";
  }

  public static String buildPushMsg2() {
    //    String txt =
    //        "{\n"
    //            + "    \"msgtype\":\"markdown\",\n"
    //            + "    \"markdown\":{\n"
    //            + "        \"title\":\"杭州天气\",\n"
    //            + "        \"text\":\"#### 杭州天气 @181XXXXXXXX  \\n> 9度，西北风1级，空气良89，相对温度73%\\n>
    // ![screenshot](https://img.alicdn.com/tfs/TB1NwmBEL9TBuNjy1zbXXXpepXa-2400-1218.png)\\n>
    // ###### 10点20分发布 [天气](https://www.dingalk.com) \\n\"\n"
    //            + "    },\n"
    //            + "    \"at\":{\n"
    //            + "        \"atMobiles\":[\n"
    //           + "            \"18123801593\"\n"
    //            + "        ],\n"
    //            + "        \"atUserIds\":[\n"
    //            + "        ],\n"
    //            + "        \"isAtAll\":true\n"
    //            + "    }\n"
    //            + "}";
    //        return txt;

    ErrMsgModel err =
        ErrMsgModel.builder()
            .serverName("<font color=#FF0000 size=4 face='黑体'>设备异常报警</font>")
            .ip("192.168.3.125")
            .title("服务告警")
            .errMsgItems(
                Lists.newArrayList(
                    ErrMsgItem.builder()
                        .errTitle("rabbit")
                        .errMsg(
                            "org.springframework.amqp.AmqpConnectException: java.net.ConnectException: Connection timed out: connect")
                        .build(),
                    ErrMsgItem.builder()
                        .errTitle("redis")
                        .errMsg(
                            "org.springframework.data.redis.RedisConnectionFailureException: Unable to connect to Redis; nested exception is org.springframework.data.redis.connection.PoolException: Could not get a resource from the pool; nested exception is io.lettuce.core.RedisConnectionException: Unable to connect to 172.18.78.233:6379")
                        .build()))
            .build();
    List<String> atMobiles = Lists.newArrayList("18123801593", "13129589351");
    return buildMarkDownMsg(err, atMobiles, false);
  }

  public static String buildMarkDownMsg(ErrMsgModel errMsg, List<String> atMobiles, boolean atAll) {
    DingDingMsg msg = new DingDingMsg();
    DingDingMsgMD md = DingDingMsgMD.builder().title(errMsg.getTitle()).build();
    msg.setMarkdown(md);
    DingDingMsgAt at =
        DingDingMsgAt.builder()
            .atMobiles(atMobiles)
            .atUserIds(new ArrayList<>())
            .isAtAll(atAll)
            .build();
    StringBuilder textSb = new StringBuilder();
    textSb.append("## ").append(errMsg.getServerName()).append("|").append(errMsg.getIp());

    if (!CollectionUtils.isEmpty(atMobiles) && !atAll) {
      for (String m : atMobiles) {
        textSb.append("@").append(m);
      }

      textSb.append("\n>");
    }
    for (ErrMsgItem err : errMsg.getErrMsgItems()) {
      textSb.append("### ").append(err.getErrTitle()).append("\n>"); // 设置标题
      textSb.append(err.getErrMsg()).append("\n>");
    }
    md.setText(textSb.toString());
    msg.setAt(at);
    return JSON.toJSONString(msg);
  }

  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor
  public static class ErrMsgModel {
    private String title; // 钉钉消息列表显示内容
    private String serverName;
    private String ip;

    @JSONField(serialize = false)
    private List<ErrMsgItem> errMsgItems;
  }

  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor
  public static class ErrMsgItem {
    private String errTitle;
    private String errMsg;
  }

  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor
  public static class DingDingMsg {
    private String msgtype = "markdown";
    private DingDingMsgAt at;
    private DingDingMsgMD markdown;
  }

  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor
  public static class DingDingMsgMD {
    private String title;
    private String text;
  }

  @Data
  @Builder
  @AllArgsConstructor
  @NoArgsConstructor
  public static class DingDingMsgAt {

    private List<String> atMobiles = new ArrayList<>();

    private List<String> atUserIds = new ArrayList<>();

    @JSONField(name = "isAtAll")
    private boolean isAtAll;
  }

  public static String buildPushMsg() {
    StringBuffer stringBuffer = new StringBuffer();
    stringBuffer
        .append(">### ")
        .append("<font color=#FF0000 size=4 face='黑体'>设备异常报警</font>")
        .append("\n\n >设备编号: #deviceNo#")
        .append("\n\n >最近心跳: #beatDate#")
        .append("\n\n >设备空间: #spaceInfo#")
        .append("\n\n >设备名称: #deviceName#")
        .append("\n\n >设备类型: #deviceType#");
    StringBuffer actionCardMessageSb = new StringBuffer();
    actionCardMessageSb.append("{\"actionCard\":{\"title\":\"#msgTitle#\",\"text\":\"");
    actionCardMessageSb.append(stringBuffer.toString());
    actionCardMessageSb
        .append("\",\"hideAvatar\":\"0\",\"btnOrientation\":\"0\",\"btns\":[{\"title\":\"")
        .append("warn!!");
    actionCardMessageSb.append("\",\"actionURL\":\"\"}]},\"msgtype\":\"actionCard\"}");
    return putVal(actionCardMessageSb.toString());
  }

  public static String putVal(String msg) {

    Map<String, String> map =
        new HashMap<String, String>() {
          {
            put("deviceNo", "dddgggg");
            put("deviceType", "吉祥");
          }
        };
    for (Entry<String, String> entry : map.entrySet()) {
      String replaceKey = "#" + entry.getKey() + "#";
      if (msg.contains(replaceKey)) {
        msg = msg.replaceFirst(replaceKey, entry.getValue());
      }
    }

    return msg;
  }

  public static String getDingDingPushSign(String secret, long timestamp) {
    String stringToSign = timestamp + "\n" + secret;
    try {
      Mac mac = Mac.getInstance(Constants.SIGN_HMAC_SHA256);
      mac.init(
          new SecretKeySpec(secret.getBytes(Constants.CHARSET_UTF8), Constants.SIGN_HMAC_SHA256));
      byte[] signData = mac.doFinal(stringToSign.getBytes(Constants.CHARSET_UTF8));
      return URLEncoder.encode(new String(Base64.encodeBase64(signData)), Constants.CHARSET_UTF8);
    } catch (Exception e) {
      log.error("", e);
    }

    return null;
  }

  /**
   * 使用 Token 初始化账号Client
   *
   * @return Client
   * @throws Exception
   */
  public static com.aliyun.dingtalkrobot_1_0.Client createClient() throws Exception {
    Config config = new Config();
    config.protocol = "https";
    config.regionId = "central";
    return new com.aliyun.dingtalkrobot_1_0.Client(config);
  }

  public static void main123(String[] args_) throws Exception {
    java.util.List<String> args = java.util.Arrays.asList(args_);
    com.aliyun.dingtalkrobot_1_0.Client client = DingDingPushTest.createClient();
    BatchSendOTOHeaders batchSendOTOHeaders = new BatchSendOTOHeaders();
    batchSendOTOHeaders.xAcsDingtalkAccessToken = "<your access token>";
    // 发送文本
    Map<String, String> param = new HashMap<>();
    param.put(
        "content",
        "对于开发者而言，钉钉机器人是全局唯一的应用，即无论是用在单聊还是群聊，无论是用来推送微应用的通知还是用来对用户进行对话式服务，其对应的机器人ID都可以是唯一的，这意味开发者既可以选择仅创建一个机器人，而后将其放在各个场景下进行使用，也可以创建多个机器人，并分别部署在不同场景下。");
    BatchSendOTORequest batchSendOTORequest =
        new BatchSendOTORequest()
            .setRobotCode("dingxxxxxx")
            .setUserIds(java.util.Arrays.asList("manager1234"))
            .setMsgKey("sampleText")
            .setMsgParam(JSON.toJSONString(param));
    try {
      BatchSendOTOResponse response =
          client.batchSendOTOWithOptions(
              batchSendOTORequest, batchSendOTOHeaders, new RuntimeOptions());
      System.out.println(JSON.toJSONString(response));
    } catch (TeaException err) {
      if (!com.aliyun.teautil.Common.empty(err.code)
          && !com.aliyun.teautil.Common.empty(err.message)) {
        // err 中含有 code 和 message 属性，可帮助开发定位问题
      }

    } catch (Exception _err) {
      TeaException err = new TeaException(_err.getMessage(), _err);
      if (!com.aliyun.teautil.Common.empty(err.code)
          && !com.aliyun.teautil.Common.empty(err.message)) {
        // err 中含有 code 和 message 属性，可帮助开发定位问题
      }
    }
  }
}
