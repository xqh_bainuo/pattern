package com.guanglian.sdk;

public class GlpInterop
{
    public native long init(String peerid, String sip, String stun, String license, String report);
    public native int connect(long obj, String remotePeerId, String accessToken,int timeout_seconds);
    
//    private native long init();
    public native void uninit(long obj);
    public native void test(long obj, String str);
    static {
//        java.io.File lib = new java.io.File("D:\\code\\gitee\\pattern\\src\\main\\resources\\libglpinterop.so");
//        System.load(lib.getAbsolutePath());
        System.loadLibrary("guanglian-sdk-jni");
    }
    private native void sayHello();

    public void OnConnected(String remotePeerId, int type)
    {
        System.out.println("Connected:" + remotePeerId);
    }
    public void OnDisconnected(String remotePeerId, int type)
    {
        System.out.println("Disconnected:" + remotePeerId);
    }
    public void OnRecv(String remotePeerId, int type, byte[] bytes)
    {
        System.out.println("got data from :" + remotePeerId);
    }
    public void OnTest(String str)
    {
        if (str == null)
            System.out.println("OnTest NULL");
        else
            System.out.println("OnTest:" + str);
    }
    public void OnError(String desc)
    {
        System.out.println(desc);
    }

    public static void main(String[] args) throws InterruptedException {
        GlpInterop g = new GlpInterop();
        g.sayHello();
        long obj = g.init("GL5df4d2d030a04b6d9e501e2b035ebc61", "glmgr-gl.ugreen.cloud", "stun-gl.ugreen.cloud", "license-gl.ugreen.cloud", "glrpt-gl.ugreen.cloud");
//        long obj = g.init();
        if (0 == obj)
        {
            System.out.println("init failed");
            return;
        }
//        g.connect()
        System.out.println(obj);
        g.uninit(obj);
        Thread.sleep(100000);

    }
}