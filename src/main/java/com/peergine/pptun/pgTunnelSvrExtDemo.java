/**********************************************************
  copyright   : Copyright (C) 2013-2014, chenbichao,
                All rights reserved.
  filename    : pgTunnelSvrExtDemo.java
  discription : 
  modify      : create, chenbichao, 2014/4/22
**********************************************************/

package com.peergine.pptun;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.peergine.applib.pgErrCode;


public class pgTunnelSvrExtDemo extends pgTunnelSvrExt2 {
    
	public static void OutString(String sOut) {
		System.out.println(sOut);
	}

	private pgTunnelSvrProc m_Proc = null;
	private String m_sCfg = "";

	public boolean Initialize(pgTunnelSvrProc Proc) {
		
		OutString("pgTunnelSvrExtDemo.Initialize");

		// Store the 'pgTunnelSvrProc' class' instance.
		m_Proc = Proc;
		m_sCfg = m_Proc.ConfigGet();
	
		return true;
	}

	public void Clean() {
		OutString("pgTunnelSvrExtDemo.Clean");
	}

	public int UserAddAuto(String sUser, String sPass, String sParam) {
		OutString("pgTunnelSvrExtDemo.UserAddAuto");
		return pgErrCode.PG_ERR_Normal;
	}

	public int UserAdd(String sUser, String sPass, String sParam) {
		OutString("pgTunnelSvrExtDemo.UserAdd");
		return pgErrCode.PG_ERR_Normal;
	}

	public int UserDelete(String sUser) {
		OutString("pgTunnelSvrExtDemo.UserDelete");
		return pgErrCode.PG_ERR_Normal;
	}

	public int PeerLoginFilter(String sPeer, String sPassDigest, String sRawPeer, String sClient, String sAddr, String sParam) {
		//OutString("pgTunnelSvrExtDemo.PeerLoginFilter");

		/* Sample of checking password.
		String sPassReg = GetRegisterPasswordFromDatabase(sPeer);
		int iErr = m_Proc.PeerCheckPassword(sRawPeer, sPassReg, sPassDigest);
		if (iErr != pgErrCode.PG_ERR_Normal) {
			return iErr;
		}
		*/

		return pgErrCode.PG_ERR_Normal;
	}

	public int PeerLogin(String sPeer, String sClient, String sAddr, String sParam) {
		//OutString("pgTunnelSvrExtDemo.PeerLogin");
        return pgErrCode.PG_ERR_Normal;
	}

	public int PeerLoginFinish(String sPeer) {
		//OutString("pgTunnelSvrExtDemo.PeerLoginFinish");
		return pgErrCode.PG_ERR_Normal;
	}

	public int PeerLogout(String sPeer, String sClient) {
		//OutString("pgTunnelSvrExtDemo.PeerLogout");
		return pgErrCode.PG_ERR_Normal;
	}

	public int PeerExtend(String sPeer, String sData, int iHandle) {
		OutString("pgTunnelSvrExtDemo.PeerExtend");

		m_Proc.PeerNotify(sPeer, "UserExtend", ("Push: " + sData));
		m_Proc.SetReplyData("Reply: " + sData);

		return pgErrCode.PG_ERR_Normal;
	}

	public int PeerStatusAccess(String sPeer, String sPeerQuery, int iHandle) {
		OutString("pgTunnelSvrExtDemo.PeerStatusAccess: sPeer=" + sPeer + ", sPeerQuery=" + sPeerQuery);
		return pgErrCode.PG_ERR_Normal;
	}

	public int RelayReport(String sListen, String sAddrTCP, int iType, int iLoad, int iOption) {
		return pgErrCode.PG_ERR_Normal;
	}

	public int RelayVerify(String sPeer, String sAddrClient, String sAddrUDP) {
		// Not implement, return PG_ERR_Unknown.
		return pgErrCode.PG_ERR_Unknown;
	}

	public int ClusterPeerCallRequest(String sPeer, String sData) {
		OutString("pgTunnelSvrExtDemo.ClusterPeerCallRequest");
		return pgErrCode.PG_ERR_Normal;
	}

	public void ClusterPeerCallReply(String sPeer, int iErr, String sData, String sParam) {
		OutString("pgTunnelSvrExtDemo.ClusterPeerCallReply");
	}

	public void ClusterPeerMessageReceive(String sPeer, String sData) {
		OutString("pgTunnelSvrExtDemo.ClusterPeerMessageReceive");
	}

	public void ClusterBroadcastReceive(String sData) {
		OutString("pgTunnelSvrExtDemo.ClusterBroadcastReceive");
	}

	public void ClusterSlotMessageReceive(int iSlot, String sData) {
	}

	public void QueueProc(String sData) {
	}

	public void TimerProc(int iSecCount) {
	}

	public int RelayFlowStat(String sPeerSrc, String sPeerDst, int iBytes) {
		return pgErrCode.PG_ERR_Normal;
	}
}
