package com.ugreen.p2p;

import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;

/**
 * @ClassName npc
 * @Description TODO
 * @Author xuqianghui
 * @Date 2022/7/30 13:50
 * @Version 1.0
 */
@Slf4j
public class sdk {

    static {
        System.loadLibrary("libp2p_sdk");
    }

    public static void main(String[] args) throws InterruptedException, UnsupportedEncodingException {
//        testString("what do you want!");
//        testString("123344");
//        System.out.println(resolveMessyCode("dddddd顶顶顶顶"));
//        System.out.println(testReturnStr("呵呵呵额addddd"));
        System.out.println(testAdd(11, 992));
        setLogPath("D:\\work\\test\\p2p\\log\\p2p.log");
        new Thread(() -> {
            startClientForApp(
                    "https://dev.ugreengroup.com",
                    "UG549HH08229999A",
                    "c2358be21a818c1d80a27e448a4c9075",
                    "app:test:ding", "192.168.44.173:9999", 2000, 2);
            log.info("start app client end...");
        }).start();
//
//        new Thread(() -> {
//            setLogPath("D:\\work\\test\\p2p\\log2");
//            startClientForNas(
//                    "https://dev.ugreengroup.com",
//                    "UG549HH11220F120",
//                    "B8bkv0dFvEoCtO1sBzlns2SU0NbNOQO1kd1ovykQRg0=",
//                    "nas:test:ding", 3);
//            log.info("start nas client end...");
//        }).start();
//
////        new Thread(() -> {
////            StartClientForNas(
////                    "https://dev.ugreengroup.com",
////                    "UG549HH08229999A",
////                    "b/+wBcnrqrwhlZhALFmqkR0+tu+Y+3ukTaiVTAlfvbw=",
////                    "nas:test:ding", "5");
////            log.info("start nas client end...");
////        }).start();
        int count = 0;
        while (true) {
            count++;
            Thread.sleep(1000);
            log.info("task is running {}, client-status: {}", count, getClientStatus("UG549HH08229999A"));
            if (count == 10) {
                log.info("start to close client {}", "UG549HH08229999A");
//                closeClient("UG549HH08229999A");
            }

            if (count == 100) {
                log.info("start to close all client.");
                closeAllClient();
                break;
            }
        }
    }

    public static native int startClientForApp(String server, String deviceSn, String verifyKey, String clientDesc, String p2pTarget, int port, int disconnectTime);

    public static native int startClientForNas(String server, String deviceSn, String verifyKey, String clientDesc, int disconnectTime);

    public static native int getClientStatus(String deviceSn);

    public static native void closeClient(String deviceSn);

    public static native void setLogPath(String logPath);

    public static native void closeAllClient();

    public static native String getVersion();
//
    public static native int testAdd(int a, int b);
//    public static native void testString(String str);
////
//    public static native String resolveMessyCode(String a);
////
//    public static native String testReturnStr(String a);


}
