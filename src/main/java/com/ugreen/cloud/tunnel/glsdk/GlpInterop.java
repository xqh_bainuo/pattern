package com.ugreen.cloud.tunnel.glsdk;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GlpInterop
{
    public native long init(String peerid, String sip, String stun, String logPath, int logDev);
    public native int connect(long obj, String remotePeerId, String accessToken,int timeout_seconds);
    public native int addMapping(long obj, String remotePeerId, int listenPort, int destinationPort, String destinationHost, int rule);
    public native int delMapping(long obj, String remotePeerId, int listenPort, int destinationPort, String destinationHost, int rule);

//    private native long init();
    public native void uninit(long obj);
    public native void test(long obj, String str);
    static {
//        java.io.File lib = new java.io.File("D:\\code\\gitee\\pattern\\src\\main\\resources\\libglpinterop.so");
//        System.load(lib.getAbsolutePath());
        System.loadLibrary("guanglian-sdk-jni");
    }
    private native void sayHello();

    public void OnConnected(String remotePeerId, int type)
    {
        System.out.println("Connected:" + remotePeerId);
    }
    public void OnDisconnected(String remotePeerId, int type)
    {
        System.out.println("Disconnected:" + remotePeerId);
    }
    public void OnRecv(String remotePeerId, int type, byte[] bytes)
    {
        System.out.println("got data from :" + remotePeerId);
    }
    public void OnTest(String str)
    {
        if (str == null)
            System.out.println("OnTest NULL");
        else
            System.out.println("OnTest:" + str);
    }
    public void OnError(String desc)
    {
        System.out.println(desc);
    }

    public static void main(String[] args) throws InterruptedException {
        GlpInterop g = new GlpInterop();
        String remotePeerId = "GL98860e2e071044a09350865c38b62811";
        long obj = g.init("local_peer_id", "glmgr-gl.ugreen.cloud", "stun-gl.ugreen.cloud", "D:\\work\\so\\guanglian\\log\\gl-jni.log", 1);
        if (0 == obj){
            System.out.println("init failed");
            return;
        }
        new Thread(()-> {
            //连接设备
            int connect = g.connect(obj, remotePeerId, "", 10);

            //添加本地映射
            int addMapping = g.addMapping(obj, remotePeerId, 2000, 9999, "127.0.0.1", 1);
            log.info("gl skd: {}, connect result: {}, add mapping result: {}", obj, connect, addMapping);
        }).start();

        Thread.sleep(1000000);
        g.uninit(obj);

    }
}